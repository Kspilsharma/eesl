﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeBehind="Default.aspx.cs" Inherits="CRM._Default" %>

<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <link href="Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="Styles/morris.css" />
    <script src="Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="Scripts/jquery-ui.min.js" type="text/javascript"></script>
    <script src="Scripts/raphael-min.js" type="text/javascript"></script>
    <script src="Scripts/morris.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            var d = new Date();
            var priorDate = new Date().setDate(d.getDate() - 10)
            var fromDate = new Date(priorDate);
            $("#<%= txtFrom.ClientID  %>").datepicker(
            {

                dateFormat: 'dd-mm-yy',
                //minDate: new Date(),
                changeMonth: true,
                numberOfMonths: 1,
                onSelect: function (selected) {
                    var arr = selected.split("-");
                    var dmy = arr[1] + "-" + arr[0] + "-" + arr[2];
                    var dt = new Date(dmy);
                    dt.setDate(dt.getDate());
                    $("#<%= txtTo.ClientID  %>").datepicker("option", "minDate", dt);
                }

            }).datepicker("setDate", fromDate);


            $("#<%= txtTo.ClientID  %>").datepicker(
            {
                dateFormat: 'dd-mm-yy',
                //minDate: new Date(),
                changeMonth: true,
                numberOfMonths: 1,
                onSelect: function (selected) {
                    var arr = selected.split("-");
                    var dmy = arr[1] + "-" + arr[0] + "-" + arr[2];
                    var dt = new Date(dmy);
                    dt.setDate(dt.getDate());
                    $("#<%= txtFrom.ClientID  %>").datepicker("option", "maxDate", dt);
                }

            }).datepicker("setDate", new Date());

            $.ajax({
                type: "POST",
                url: "Default.aspx/GetChartData",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: "true",
                cache: "false",
                success: function (result) {
                    OnSuccess(result.d);
                },
                error: function (xhr, status, error) {
                    alert(error);
                }
            });

            function OnSuccess(response) {
                var shcemewiseLine = new Morris.Line({
                    element: 'shcemewise-chart',
                    resize: true,
                    data: response.SchemeWiseData,
                    xkey: 'Y',
                    ykeys: ['TOTALCALLS', 'SL', 'UJALA', 'AGDSM', 'BUILDING', 'OTHERS'],
                    labels: ['Total Calls', 'Street Light', 'Ujala', 'AGDSM', 'Building', 'Others'],
                    hideHover: false,
                    xLabels: "day",
                    xLabelFormat: function (d) {
                        return ("0" + (d.getDate())).slice(-2) + '-' +
                                ("0" + (d.getMonth() + 1)).slice(-2) + '-' +
                                ("0" + (d.getYear())).slice(-2);
                    }
                });

                var callcategorywiseLine = new Morris.Line({
                    element: 'callcategorywise-chart',
                    resize: true,
                    data: response.CallCategoryWiseData,
                    xkey: 'Y',
                    ykeys: ['TOTALCALLS', 'QUERY', 'REGISTRATION', 'COMPLAINT', 'OTHERS'],
                    labels: ['Total Calls', 'Query', 'Registration', 'Complaint', 'Others'],
                    hideHover: false,
                    xLabels: "day",
                    xLabelFormat: function (d) {
                        return ("0" + (d.getDate())).slice(-2) + '-' +
                                ("0" + (d.getMonth() + 1)).slice(-2) + '-' +
                                ("0" + (d.getYear())).slice(-2);
                    }
                });

                var callstatuswiseLine = new Morris.Line({
                    element: 'callstatuswise-chart',
                    resize: true,
                    data: response.CallStatusWiseData,
                    xkey: 'Y',
                    ykeys: ['TOTALCALLS', 'OPEN', 'ESCALATED', 'VERIFIED', 'NOTVERIFIED', 'CLOSED', 'RECTIFIED'],
                    labels: ['Total Calls', 'Open', 'Escalated', 'Verified', 'Not Verified', 'Closed', 'Rectified'],
                    hideHover: false,
                    xLabels: "day",
                    xLabelFormat: function (d) {
                        return ("0" + (d.getDate())).slice(-2) + '-' +
                                ("0" + (d.getMonth() + 1)).slice(-2) + '-' +
                                ("0" + (d.getYear())).slice(-2);
                    }
                });
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table width="99%" cellpadding="0" cellspacing="0">
        <tr>
            <td width="60%">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td width="60%" colspan="2">
                <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
                    <tr>
                        <td>
                            <fieldset class="fieldset">
                                <legend class="Legendheading">Call Search</legend>
                                <table id="tblUIDFilter" runat="server" cellpadding="0" cellspacing="3" style="width: 100%; margin: 0px; padding: 0px;">
                                    <tr>
                                        <td style="width: 10%" class="tdlabel">&nbsp;Language/Date </td>
                                        <td class="tdlabel">
                                            <asp:DropDownList ID="ddlLanguage" runat="server" Width="150px" />

                                            &nbsp;<asp:TextBox type="text" ID="txtFrom" runat="server" Width="30%" />&nbsp;To
                                            <asp:TextBox type="text" ID="txtTo" runat="server" Width="30%" />
                                        </td>
                                        <td style="width: 5%" class="tdlabel">State</td>
                                        <td style="width: 10%" class="tdlabel">
                                            <asp:DropDownList ID="ddlState" runat="server" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddlState_SelectedIndexChanged" />
                                        </td>
                                        <td style="width: 5%" class="tdlabel">District</td>
                                        <td style="width: 15%" class="tdlabel">
                                            <asp:DropDownList ID="ddlDistrict" runat="server" Width="150px" OnSelectedIndexChanged="ddlDistrict_SelectedIndexChanged" AutoPostBack="true" />

                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%" class="tdlabel"></td>
                                        <td class="tdlabel">
                                        </td>
                                        <td style="width: 5%" class="tdlabel">ULB</td>
                                        <td style="width: 10%" class="tdlabel">
                                            <asp:DropDownList ID="ddlULB" runat="server" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddlULB_SelectedIndexChanged" />
                                        </td>
                                        <td style="width: 5%" class="tdlabel">Zone</td>
                                        <td style="width: 10%" class="tdlabel">
                                            <asp:DropDownList ID="ddlZone" runat="server" Width="150px" />
                                        </td>                                  
                                    </tr>
                                    <tr>
                                        <td colspan="6" style="width: 100%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" style="width: 100%; text-align: right" class="tddata">
                                            <asp:Button ID="btnSearch" runat="server" Text="Search" ValidationGroup="Search" OnClick="btnSearch_Click" />
                                            <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>

            </td>
        </tr>
        <tr>
            <td width="60%">
                <h2>In Bound</h2>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gvInBoundCallScheme" TabIndex="106" PageSize="50" Width="95%" runat="server"
                    AllowPaging="True" AutoGenerateColumns="False" EmptyDataText="No Record Found" CellPadding="4"
                    ForeColor="#333333" class="tabStyle" GridLines="None" HeaderStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center" ShowFooter="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Sr." ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink
                                    runat="server"
                                    ID="dlDate"
                                    NavigateUrl='<%# String.Format("~/View/DashboardDetail.aspx?Date={0}&LanguageId={1}&StateId={2}&DistrictId={3}&SchemeId={4}", Eval("CallDate"), Eval("LanguageId"), Eval("StateId"),Eval("DistrictId"),0) %>' Text='<%# Eval("CallDate", "{0:dd-MM-yyyy}") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total Calls" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink
                                    runat="server"
                                    ID="dlTotalCall"
                                    NavigateUrl='<%# String.Format("~/View/DashboardDetail.aspx?Date={0}&LanguageId={1}&StateId={2}&DistrictId={3}&SchemeId={4}", Eval("CallDate"), Eval("LanguageId"), Eval("StateId"),Eval("DistrictId"),0) %>' Text='<%# Eval("TotalCalls") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Street Light" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink
                                    runat="server"
                                    ID="hlStreetLight"
                                    NavigateUrl='<%# String.Format("~/View/DashboardDetail.aspx?Date={0}&LanguageId={1}&StateId={2}&DistrictId={3}&SchemeId={4}", Eval("CallDate"), Eval("LanguageId"), Eval("StateId"),Eval("DistrictId"),1) %>' Text='<%# Eval("Street Light") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ujala" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink
                                    runat="server"
                                    ID="hlUjala"
                                    NavigateUrl='<%# String.Format("~/View/DashboardDetail.aspx?Date={0}&LanguageId={1}&StateId={2}&DistrictId={3}&SchemeId={4}", Eval("CallDate"), Eval("LanguageId"), Eval("StateId"),Eval("DistrictId"),2) %>' Text='<%# Eval("Ujala") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="AgDSM" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink
                                    runat="server"
                                    ID="hlAGDSM"
                                    NavigateUrl='<%# String.Format("~/View/DashboardDetail.aspx?Date={0}&LanguageId={1}&StateId={2}&DistrictId={3}&SchemeId={4}", Eval("CallDate"), Eval("LanguageId"), Eval("StateId"),Eval("DistrictId"),3) %>' Text='<%# Eval("AGDSM") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Building" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink
                                    runat="server"
                                    ID="hlBuilding"
                                    NavigateUrl='<%# String.Format("~/View/DashboardDetail.aspx?Date={0}&LanguageId={1}&StateId={2}&DistrictId={3}&SchemeId={4}", Eval("CallDate"), Eval("LanguageId"), Eval("StateId"),Eval("DistrictId"),4) %>' Text='<%# Eval("Building") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Other" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink
                                    runat="server"
                                    ID="hlOther"
                                    NavigateUrl='<%# String.Format("~/View/DashboardDetail.aspx?Date={0}&LanguageId={1}&StateId={2}&DistrictId={3}&SchemeId={4}", Eval("CallDate"), Eval("LanguageId"), Eval("StateId"),Eval("DistrictId"),5) %>' Text='<%# Eval("Other") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle BackColor="" />
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#6c95d9" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#6c95d9" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" />
                    <RowStyle BackColor="" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </td>
            <td align="center">
                <label class="label label-success" style="font-weight: bold">Scheme wise call details</label>
                <div id="shcemewise-chart" style="height: 250px;"></div>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gvInBoundCallType" TabIndex="106" PageSize="50" Width="95%" runat="server"
                    AllowPaging="True" AutoGenerateColumns="False" EmptyDataText="No Record Found" CellPadding="4"
                    ForeColor="#333333" class="tabStyle" GridLines="None" HeaderStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center" ShowFooter="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Sr." ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink
                                    runat="server"
                                    ID="dlDate"
                                    NavigateUrl='<%# String.Format("~/View/DashboardDetail.aspx?Date={0}&LanguageId={1}&StateId={2}&DistrictId={3}&CallCategoryId={4}", Eval("CallDate"), Eval("LanguageId"), Eval("StateId"),Eval("DistrictId"),0) %>' Text='<%# Eval("CallDate", "{0:dd-MM-yyyy}") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total Calls" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink
                                    runat="server"
                                    ID="dlTotalCall"
                                    NavigateUrl='<%# String.Format("~/View/DashboardDetail.aspx?Date={0}&LanguageId={1}&StateId={2}&DistrictId={3}&CallCategoryId={4}", Eval("CallDate"), Eval("LanguageId"), Eval("StateId"),Eval("DistrictId"),0) %>' Text='<%# Eval("TotalCalls") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Query" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink
                                    runat="server"
                                    ID="hlQuery"
                                    NavigateUrl='<%# String.Format("~/View/DashboardDetail.aspx?Date={0}&LanguageId={1}&StateId={2}&DistrictId={3}&CallCategoryId={4}", Eval("CallDate"), Eval("LanguageId"), Eval("StateId"),Eval("DistrictId"),1) %>' Text='<%# Eval("Query") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Registration" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink
                                    runat="server"
                                    ID="hlRegistration"
                                    NavigateUrl='<%# String.Format("~/View/DashboardDetail.aspx?Date={0}&LanguageId={1}&StateId={2}&DistrictId={3}&CallCategoryId={4}", Eval("CallDate"), Eval("LanguageId"), Eval("StateId"),Eval("DistrictId"),2) %>' Text='<%# Eval("Registration") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Complaint" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink
                                    runat="server"
                                    ID="hlComplaint"
                                    NavigateUrl='<%# String.Format("~/View/DashboardDetail.aspx?Date={0}&LanguageId={1}&StateId={2}&DistrictId={3}&CallCategoryId={4}", Eval("CallDate"), Eval("LanguageId"), Eval("StateId"),Eval("DistrictId"),3) %>' Text='<%# Eval("Complaint") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Other" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink
                                    runat="server"
                                    ID="hlComplaint"
                                    NavigateUrl='<%# String.Format("~/View/DashboardDetail.aspx?Date={0}&LanguageId={1}&StateId={2}&DistrictId={3}&CallCategoryId={4}", Eval("CallDate"), Eval("LanguageId"), Eval("StateId"),Eval("DistrictId"),4) %>' Text='<%# Eval("Other") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle BackColor="" />
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#6c95d9" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#6c95d9" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" />
                    <RowStyle BackColor="" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </td>
            <td align="center">
                <label class="label label-success" style="font-weight: bold">Category wise call details</label>
                <div id="callcategorywise-chart" style="height: 250px;"></div>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gvInBoundCallStatus" TabIndex="106" PageSize="50" Width="95%" runat="server"
                    AllowPaging="True" AutoGenerateColumns="False" EmptyDataText="No Record Found" CellPadding="4"
                    ForeColor="#333333" class="tabStyle" GridLines="None" HeaderStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center" ShowFooter="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Sr." ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink
                                    runat="server"
                                    ID="dlDate"
                                    NavigateUrl='<%# String.Format("~/View/DashboardDetail.aspx?Date={0}&LanguageId={1}&StateId={2}&DistrictId={3}&StatusId={4}", Eval("CallDate"), Eval("LanguageId"), Eval("StateId"),Eval("DistrictId"),0) %>' Text='<%# Eval("CallDate", "{0:dd-MM-yyyy}") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total Calls" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink
                                    runat="server"
                                    ID="dlTotalCall"
                                    NavigateUrl='<%# String.Format("~/View/DashboardDetail.aspx?Date={0}&LanguageId={1}&StateId={2}&DistrictId={3}&StatusId={4}", Eval("CallDate"), Eval("LanguageId"), Eval("StateId"),Eval("DistrictId"),0) %>' Text='<%# Eval("TotalCalls") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Open" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink
                                    runat="server"
                                    ID="hlOpen"
                                    NavigateUrl='<%# String.Format("~/View/DashboardDetail.aspx?Date={0}&LanguageId={1}&StateId={2}&DistrictId={3}&StatusId={4}", Eval("CallDate"), Eval("LanguageId"), Eval("StateId"),Eval("DistrictId"),1) %>' Text='<%# Eval("Open") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Escalated" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink
                                    runat="server"
                                    ID="hlEscalated"
                                    NavigateUrl='<%# String.Format("~/View/DashboardDetail.aspx?Date={0}&LanguageId={1}&StateId={2}&DistrictId={3}&StatusId={4}", Eval("CallDate"), Eval("LanguageId"), Eval("StateId"),Eval("DistrictId"),2) %>' Text='<%# Eval("Escalated") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Verified" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink
                                    runat="server"
                                    ID="hlVerified"
                                    NavigateUrl='<%# String.Format("~/View/DashboardDetail.aspx?Date={0}&LanguageId={1}&StateId={2}&DistrictId={3}&StatusId={4}", Eval("CallDate"), Eval("LanguageId"), Eval("StateId"),Eval("DistrictId"),3) %>' Text='<%# Eval("Verified") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Non Verified" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink runat="server"
                                    ID="hlNonVerified"
                                    NavigateUrl='<%# String.Format("~/View/DashboardDetail.aspx?Date={0}&LanguageId={1}&StateId={2}&DistrictId={3}&StatusId={4}", Eval("CallDate"), Eval("LanguageId"), Eval("StateId"),Eval("DistrictId"),4) %>' Text='<%# Eval("Non Verified") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Closed" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink
                                    runat="server"
                                    ID="hlClosed"
                                    NavigateUrl='<%# String.Format("~/View/DashboardDetail.aspx?Date={0}&LanguageId={1}&StateId={2}&DistrictId={3}&StatusId={4}", Eval("CallDate"), Eval("LanguageId"), Eval("StateId"),Eval("DistrictId"),5) %>' Text='<%# Eval("Closed") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Rectified" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:HyperLink
                                    runat="server"
                                    ID="hlRectified"
                                    NavigateUrl='<%# String.Format("~/View/DashboardDetail.aspx?Date={0}&LanguageId={1}&StateId={2}&DistrictId={3}&StatusId={4}", Eval("CallDate"), Eval("LanguageId"), Eval("StateId"),Eval("DistrictId"),6) %>' Text='<%# Eval("Rectified") %>'>
                                </asp:HyperLink>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <AlternatingRowStyle BackColor="" />
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#6c95d9" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#6c95d9" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" />
                    <RowStyle BackColor="" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#f8f8f8" />
                    <SortedAscendingHeaderStyle BackColor="#f8f8f8" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </td>
             <td align="center">
                <label class="label label-success" style="font-weight: bold">Call Status wise call details</label>
                <div id="callstatuswise-chart" style="height: 250px;"></div>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="display: none">&nbsp;
            </td>
            <td style="display: none">&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblMessage" runat="server" CssClass="failureNotification"></asp:Label>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="left"></td>
            <td align="left"></td>
        </tr>
    </table>
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
</asp:Content>



