﻿using System;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;
using System.Xml.Serialization;

namespace CRM.Models
{
    public static class CommonUtils
    {
        public const string CONSTACTIVITYID = "ActivityId";
        public const string CONSTACTIVITYNAME = "ActivityName";

        /// <summary>
        /// Fill the respective treeview
        /// </summary>
        /// <param name="pRootNodeName"></param>
        /// <param name="pTreeNode"></param>
        /// <param name="pDataTable"></param>
        public static void FillTreeView(this TreeView pTreeView, String pRootNodeName, DataTable pDataTable)
        {
            try
            {
                pTreeView.Nodes.Clear();
                TreeNode tNode = new TreeNode();
                tNode.Text = pRootNodeName;
                tNode.Value = "0";
                AddTreeNode(0, ref tNode, ref pDataTable);
                pTreeView.Nodes.Add(tNode);
                pTreeView.DataBind();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Recursive function to fill tree view
        /// </summary>
        /// <param name="pParentId">ParentId</param>
        /// <param name="pNode">Node</param>
        /// <param name="pDataTable">DataTable</param>
        private static void AddTreeNode(int pParentId, ref TreeNode pNode, ref DataTable pDataTable)
        {
            try
            {
                DataRow[] tResultSet = pDataTable.Select(pDataTable.Columns["ParentMenuId"].ColumnName + " =" + pParentId);

                foreach (DataRow iDataRow in tResultSet)
                {
                    TreeNode tNode = new TreeNode();
                    tNode.Text = iDataRow["MenuDesc"].ToString();
                    tNode.Value = iDataRow["MenuId"].ToString();
                    for (int i = 0; i <= pDataTable.Columns.Count - 1; i++)
                    {
                        if ((pDataTable.Columns[i].ColumnName) == "UrlDesc")
                        {
                            if (iDataRow["UrlDesc"].ToString() == "")
                            {
                                tNode.NavigateUrl = "~/PageUnderConstruction.aspx";
                            }
                            else
                            {
                                string tNavigateURL = iDataRow["UrlDesc"] + "?" + CONSTACTIVITYID + "=" + iDataRow["MenuId"] + "&" + CONSTACTIVITYNAME + "=" + iDataRow["MenuDesc"];
                                tNode.NavigateUrl = tNavigateURL;
                            }
                            tNode.Target = "RightBInnerContent";
                        }
                    }
                    pNode.ChildNodes.Add(tNode);
                    AddTreeNode(Convert.ToInt32(iDataRow["MenuId"].ToString()), ref tNode, ref pDataTable);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Populate DropDown With Select & All
        /// </summary>
        /// <param name="pddlName">ddlName</param>
        /// <param name="pObject">object</param>
        /// <param name="pIsValue">IsValue</param>
        /// <param name="IsAllOpt">IsAllOpt</param>
        public static void PopulateDropDownList(this DropDownList pddlName, DataTable pObject, Boolean pIsValue, Boolean IsAllOpt)
        {

            ListItem mListItem;
            mListItem = new ListItem("--Select--", "0");

            pddlName.DataSource = pObject;
            pddlName.DataBind();
            pddlName.Items.Insert(0, mListItem);

            try
            {
                if (IsAllOpt == true)
                {
                    mListItem.Text = "--All--";
                    mListItem.Value = "-1";
                    pddlName.Items.Insert(1, mListItem);
                }
            }
            catch (Exception)
            {
                //ErrorLogger.NexErrorLogger.Write(SessionManager.SessionManager.DiscomCode, ErrorLogger.NexErrorLogger.Mode.Database, ex);
            }
        }

        /// <summary>
        /// Populate DropDown Without Select & All
        /// </summary>
        /// <param name="pddlName"></param>
        /// <param name="tobject"></param>
        /// <param name="pIsValue"></param>
        /// <param name="IsAllOpt"></param>
        public static void PopulateDropDownList(this DropDownList pddlName, object tobject)
        {
            pddlName.DataSource = tobject;
            pddlName.DataBind();
        }

        /// <summary>
        /// Encrypt String
        /// </summary>
        /// <param name="str"></param>
        /// <returns>Encrypted string</returns>
        public static string EncryptString(string str)
        {
            byte[] b = null;
            b = System.Text.ASCIIEncoding.ASCII.GetBytes(str);
            return Convert.ToBase64String(b);

        }

        /// <summary>
        /// Decrypt String
        /// </summary>
        /// <param name="str"></param>
        /// <returns>Decrypted string</returns>
        public static string DecryptString(string str)
        {
            byte[] b = null;
            b = System.Convert.FromBase64String(str);
            return System.Text.ASCIIEncoding.ASCII.GetString(b);
        }

        /// <summary>
        /// Serialize object
        /// </summary>
        /// <param name="pObject">Object</param>
        /// <returns>string</returns>
        public static string SerializeObject(object pObject)
        {
            XmlSerializer tXmlSerializer = new XmlSerializer(pObject.GetType());
            System.IO.MemoryStream tStream = new System.IO.MemoryStream();

            tXmlSerializer.Serialize(tStream, pObject);
            String tSerilaizedObject = System.Text.Encoding.UTF8.GetString(tStream.ToArray());
            tStream.Close();
            return tSerilaizedObject;
        }

      
    }
}