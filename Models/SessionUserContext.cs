using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM.Models
{
    /// <summary>
    /// Session User Context
    /// </summary>
    public class SessionUserContext
    {
        public int UserId { get; set; }

        public string UserName { get; set; }

        public string Name { get; set; }

        public long BatchId { get; set; }

        public long EntryId { get; set; }

        public string JulianDate { get; set; }

        public string BatchNo { get; set; }

        public int RecordNo { get; set; }

        public int RoleId { get; set; }

        public string RoleName { get; set; }

        public string ContactNo { get; set; }

        public string UserProfile { get; set; }

        public DateTime LastLogin { get; set; }

        public byte? Status { get; set; }

        public int AdminPageIndex { get; set; }

        public int BatchPageIndex { get; set; }

        public int EntryPageIndex { get; set; }

        public string VendorSchemeName { get; set; }

        public HttpContext CurrentContext
        {
            get { return HttpContext.Current; }
        }

        public Boolean StartSession()
        {
            if (UserId == 0)
            {
                return false;
            }
            HttpContext.Current.Session.Add("UserContext", this);
            return true;
        }

        public Boolean EndSession()
        {
            HttpContext.Current.Session.Remove("UserContext");
            HttpContext.Current.Session.RemoveAll();
            HttpContext.Current.Session.Abandon();
            return true;
        }

    }

    /// <summary>
    /// Summary description for UIManager
    /// </summary>
    public class UIManager
    {
        public static SessionUserContext CurrentUserSession()
        {
            return (SessionUserContext)HttpContext.Current.Session["UserContext"];
        }
    }
}