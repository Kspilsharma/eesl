using System;
using System.Configuration;

namespace CRM.Models
{
    public class UIBase : System.Web.UI.Page
    {
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();

        protected override void OnPreInit(EventArgs e)
        {
            try
            {
                if (UIManager.CurrentUserSession() == null)
                {
                    Response.Redirect(AppBaseURL + "frmSessionExpired.aspx", true);
                }
                else
                {
                    base.OnPreInit(e);
                }
            }
            catch (Exception)
            {
                Context.ApplicationInstance.CompleteRequest();
            }
        }
    }
}