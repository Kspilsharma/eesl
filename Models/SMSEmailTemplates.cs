﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using CRM.Models;

namespace CRM.Models
{
    public class SMSEmailTemplates
    {
        #region Properties

        public int Id { get; set; }
        public int SettingTypeId { get; set; }
        public int StatusOfCallId { get; set; }
        public string SMSTemplateBody { get; set; }
        public string EmailTemplateSubject { get; set; }
        public string EmailTemplateBody { get; set; }
        public bool IsActive { get; set; }
        public int LoginId { get; set; }
        public int UpdateId { get; set; }

        #endregion


        #region Pulic Methods

        public bool UpdateSMSEmailTemplate(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_SMSEmailTemplates_InsertUpdate", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@SettingTypeId", SettingTypeId);
                    command.Parameters.AddWithValue("@StatusOfCallId", StatusOfCallId);
                    command.Parameters.AddWithValue("@SMSTemplateBody", SMSTemplateBody);
                    command.Parameters.AddWithValue("@EmailTemplateSubject", EmailTemplateSubject);
                    command.Parameters.AddWithValue("@EmailTemplateBody", EmailTemplateBody);
                    command.Parameters.AddWithValue("@LoginId", LoginId);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }

        public DataSet Select()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_SMSEmailTemplates_Select", sqlConn);
                    command.Parameters.AddWithValue("@SettingTypeId", SettingTypeId);
                    command.Parameters.AddWithValue("@StatusOfCallId", StatusOfCallId);
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public DataSet SelectAll()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_SMSEmailTemplates_SelectAll", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        #endregion
    }
}