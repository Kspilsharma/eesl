﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;

namespace CRM.Models
{
    public class Mapping
    {
        # region Properties

        public int SchemeId { get; set; }
        public int StateId { get; set; }
        public int DistrictId { get; set; }
        public int NOMappingId { get; set; }
        public string NOName { get; set; }
        public string NOEmail { get; set; }
        public string NOMobileNo { get; set; }
        public int VendorMappingId { get; set; }
        public string VendorName { get; set; }
        public string VendorEmail { get; set; }
        public string VendorMobileNo { get; set; }
        public string VendorType { get; set; }
        public int ULBMappingId { get; set; }
        public string ULB { get; set; }
        public int DiscomMappingId { get; set; }
        public string Discom { get; set; }
        public int BuildingMappingId { get; set; }
        public string Building { get; set; }
        public int ZoneMappingId { get; set; }
        public string Zone { get; set; }
        public bool IsActive { get; set; }
        public int LoginId { get; set; }
        public int UpdateId { get; set; }
        public string Address { get; set; }
        public string Landmark { get; set; }
        #endregion

        #region Methods

        public bool NOMappingInsert(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_NOMapping_Insert", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@SchemeId", SchemeId);
                    command.Parameters.AddWithValue("@StateId", StateId);
                    command.Parameters.AddWithValue("@DistrictId", DistrictId);
                    command.Parameters.AddWithValue("@NOName", NOName);
                    command.Parameters.AddWithValue("@NOEmail", NOEmail);
                    command.Parameters.AddWithValue("@NOMobileNo", NOMobileNo);
                    command.Parameters.AddWithValue("@LoginId", LoginId);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }
        public bool NOMappingUpdate(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_NOMapping_Update", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@NOMappingId", NOMappingId);
                    command.Parameters.AddWithValue("@SchemeId", SchemeId);
                    command.Parameters.AddWithValue("@StateId", StateId);
                    command.Parameters.AddWithValue("@DistrictId", DistrictId);
                    command.Parameters.AddWithValue("@NOName", NOName);
                    command.Parameters.AddWithValue("@NOEmail", NOEmail);
                    command.Parameters.AddWithValue("@NOMobileNo", NOMobileNo);
                    command.Parameters.AddWithValue("@LoginId", LoginId);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }
        public DataSet NOMappingSelect()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_NOMapping_Select", sqlConn);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public DataSet NOMappingEdit()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_NOMapping_Command", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Command", "Edit");
                    command.Parameters.AddWithValue("@NOMappingId", NOMappingId);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public bool NOMappingStatusUpdate(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_NOMapping_Command", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Command", "StatusUpdate");
                    command.Parameters.AddWithValue("@NOMappingId", NOMappingId);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }
        public bool NOMappingDelete(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_NOMapping_Command", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Command", "Delete");
                    command.Parameters.AddWithValue("@NOMappingId", NOMappingId);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }        



        public bool ULBMappingInsert(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_ULBMapping_Insert", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@StateId", StateId);
                    command.Parameters.AddWithValue("@DistrictId", DistrictId);
                    command.Parameters.AddWithValue("@ULB", ULB);
                    command.Parameters.AddWithValue("@LoginId", LoginId);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }
        public bool ULBMappingUpdate(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_ULBMapping_Update", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ULBMappingId", ULBMappingId);
                    command.Parameters.AddWithValue("@StateId", StateId);
                    command.Parameters.AddWithValue("@DistrictId", DistrictId);
                    command.Parameters.AddWithValue("@ULB", ULB);

                    command.Parameters.AddWithValue("@LoginId", LoginId);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }
        public DataSet ULBMappingSelect()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_ULBMapping_Select", sqlConn);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public DataSet ULBMappingEdit()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_ULBMapping_Command", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Command", "Edit");
                    command.Parameters.AddWithValue("@ULBMappingId", ULBMappingId);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public bool ULBMappingStatusUpdate(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_ULBMapping_Command", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Command", "StatusUpdate");
                    command.Parameters.AddWithValue("@ULBMappingId", ULBMappingId);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }
        public bool ULBMappingDelete(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_ULBMapping_Command", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Command", "Delete");
                    command.Parameters.AddWithValue("@ULBMappingId", ULBMappingId);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }


        public bool DiscomMappingInsert(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_DiscomMapping_Insert", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@StateId", StateId);
                    command.Parameters.AddWithValue("@DistrictId", DistrictId);
                    command.Parameters.AddWithValue("@Discom", Discom);

                    command.Parameters.AddWithValue("@LoginId", LoginId);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }
        public bool DiscomMappingUpdate(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_DiscomMapping_Update", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@DiscomMappingId", DiscomMappingId);
                    command.Parameters.AddWithValue("@StateId", StateId);
                    command.Parameters.AddWithValue("@DistrictId", DistrictId);
                    command.Parameters.AddWithValue("@Discom", Discom);

                    command.Parameters.AddWithValue("@LoginId", LoginId);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }
        public DataSet DiscomMappingSelect()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_DiscomMapping_Select", sqlConn);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public DataSet DiscomMappingEdit()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_DiscomMapping_Command", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Command", "Edit");
                    command.Parameters.AddWithValue("@DiscomMappingId", DiscomMappingId);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public bool DiscomMappingStatusUpdate(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_DiscomMapping_Command", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Command", "StatusUpdate");
                    command.Parameters.AddWithValue("@DiscomMappingId", DiscomMappingId);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }
        public bool DiscomMappingDelete(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_DiscomMapping_Command", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Command", "Delete");
                    command.Parameters.AddWithValue("@DiscomMappingId", DiscomMappingId);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }


        public bool BuildingMappingInsert(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_BuildingMapping_Insert", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@StateId", StateId);
                    command.Parameters.AddWithValue("@DistrictId", DistrictId);
                    command.Parameters.AddWithValue("@Building", Building);
                    command.Parameters.AddWithValue("@Address", Address);
                    command.Parameters.AddWithValue("@Landmark", Landmark);

                    command.Parameters.AddWithValue("@LoginId", LoginId);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }
        public bool BuildingMappingUpdate(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_BuildingMapping_Update", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@BuildingMappingId", BuildingMappingId);
                    command.Parameters.AddWithValue("@StateId", StateId);
                    command.Parameters.AddWithValue("@DistrictId", DistrictId);
                    command.Parameters.AddWithValue("@Building", Building);
                    command.Parameters.AddWithValue("@Address", Address);
                    command.Parameters.AddWithValue("@Landmark", Landmark);

                    command.Parameters.AddWithValue("@LoginId", LoginId);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }
        public DataSet BuildingMappingSelect()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_BuildingMapping_Select", sqlConn);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public DataSet BuildingMappingEdit()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_BuildingMapping_Command", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Command", "Edit");
                    command.Parameters.AddWithValue("@BuildingMappingId", BuildingMappingId);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public bool BuildingMappingStatusUpdate(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_BuildingMapping_Command", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Command", "StatusUpdate");
                    command.Parameters.AddWithValue("@BuildingMappingId", BuildingMappingId);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }
        public bool BuildingMappingDelete(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_BuildingMapping_Command", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Command", "Delete");
                    command.Parameters.AddWithValue("@BuildingMappingId", BuildingMappingId);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }


        public bool ZoneMappingInsert(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_ZoneMapping_Insert", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@StateId", StateId);
                    command.Parameters.AddWithValue("@DistrictId", DistrictId);
                    command.Parameters.AddWithValue("@ULBMappingId", ULBMappingId);
                    command.Parameters.AddWithValue("@Zone", Zone);

                    command.Parameters.AddWithValue("@LoginId", LoginId);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }
        public bool ZoneMappingUpdate(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_ZoneMapping_Update", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ZoneMappingId", ZoneMappingId);
                    command.Parameters.AddWithValue("@StateId", StateId);
                    command.Parameters.AddWithValue("@DistrictId", DistrictId);
                    command.Parameters.AddWithValue("@ULBMappingId", ULBMappingId);
                    command.Parameters.AddWithValue("@Zone", Zone);

                    command.Parameters.AddWithValue("@LoginId", LoginId);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }
        public DataSet ZoneMappingSelect()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_ZoneMapping_Select", sqlConn);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public DataSet ZoneMappingEdit()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_ZoneMapping_Command", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Command", "Edit");
                    command.Parameters.AddWithValue("@ZoneMappingId", ZoneMappingId);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public bool ZoneMappingStatusUpdate(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_ZoneMapping_Command", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Command", "StatusUpdate");
                    command.Parameters.AddWithValue("@ZoneMappingId", ZoneMappingId);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }
        public bool ZoneMappingDelete(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_ZoneMapping_Command", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Command", "Delete");
                    command.Parameters.AddWithValue("@ZoneMappingId", ZoneMappingId);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }


        public bool VendorMappingInsert(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_VendorMapping_Insert", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@StateId", StateId);
                    command.Parameters.AddWithValue("@DistrictId", DistrictId);
                    if (SchemeId == 1)
                    {
                        command.Parameters.AddWithValue("@ULBMappingId", ULBMappingId);
                        command.Parameters.AddWithValue("@ZoneMappingId", ZoneMappingId);
                    }
                    command.Parameters.AddWithValue("@VendorName", VendorName);
                    command.Parameters.AddWithValue("@VendorEmail", VendorEmail);
                    command.Parameters.AddWithValue("@VendorMobileNo", VendorMobileNo);
                    command.Parameters.AddWithValue("@VendorType", VendorType);
                    command.Parameters.AddWithValue("@LoginId", LoginId);
                    command.Parameters.AddWithValue("@SchemeId", SchemeId);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }
        public bool VendorMappingUpdate(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_VendorMapping_Update", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@VendorMappingId", VendorMappingId);
                    command.Parameters.AddWithValue("@StateId", StateId);
                    command.Parameters.AddWithValue("@DistrictId", DistrictId);
                    if (SchemeId == 1)
                    {
                        command.Parameters.AddWithValue("@ULBMappingId", ULBMappingId);
                        command.Parameters.AddWithValue("@ZoneMappingId", ZoneMappingId);
                    }
                    command.Parameters.AddWithValue("@VendorName", VendorName);
                    command.Parameters.AddWithValue("@VendorEmail", VendorEmail);
                    command.Parameters.AddWithValue("@VendorMobileNo", VendorMobileNo);
                    command.Parameters.AddWithValue("@VendorType", VendorType);
                    command.Parameters.AddWithValue("@LoginId", LoginId);
                    command.Parameters.AddWithValue("@SchemeId", SchemeId);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }
        public DataSet VendorMappingSelect()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_VendorMapping_Select", sqlConn);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public DataSet VendorMappingEdit()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_VendorMapping_Command", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Command", "Edit");
                    command.Parameters.AddWithValue("@VendorMappingId", VendorMappingId);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public bool VendorMappingStatusUpdate(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_VendorMapping_Command", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Command", "StatusUpdate");
                    command.Parameters.AddWithValue("@VendorMappingId", VendorMappingId);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }
        public bool VendorMappingDelete(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_VendorMapping_Command", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Command", "Delete");
                    command.Parameters.AddWithValue("@VendorMappingId", VendorMappingId);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }
        public DataSet VendorSelectEmailSMS()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_VendorEmailSMS_Select", sqlConn);
                    command.Parameters.AddWithValue("@SchemeId", SchemeId);
                    command.Parameters.AddWithValue("@StateId", StateId);
                    command.Parameters.AddWithValue("@DistrictId", DistrictId);
                    command.Parameters.AddWithValue("@ULBId", ULBMappingId);
                    command.Parameters.AddWithValue("@ZoneId", ZoneMappingId);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        #endregion
    }
}

