﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace CRM.Models
{
    public class EmailUtils
    {
        public static bool SendEmail(string uid, int callStatus, int settingTypeId, int schemeId, string vendorType, string subject, string body, string toAddress, string ccAddress = "", string bccAddress = "")
        {
            bool result = true;
            try
            {
                string insertNewEmailSQL = "INSERT INTO [dbo].[EmailNotifications]([UID],[STATUS],[MAILTO],[MAILCC],[MAILBCC],[SUBJECT],[BODY],[ATTACHMENT],[TIMEIN],[SENDATTEMPTS],[SENT],[SettingTypeId], [SchemeId], [VendorType]) VALUES(@UID, @STATUS, @MAILTO, @MAILCC, @MAILBCC, @SUBJECT, @BODY, @ATTACHMENT, @TIMEIN, @SENDATTEMPTS, @SENT, @SettingTypeId, @SchemeId, @VendorType)";
                string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection con = new SqlConnection(constr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(insertNewEmailSQL))
                    {
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@UID", uid);
                        cmd.Parameters.AddWithValue("@STATUS", callStatus);
                        cmd.Parameters.AddWithValue("@MAILTO", toAddress);
                        cmd.Parameters.AddWithValue("@MAILCC", string.IsNullOrEmpty(ccAddress) ? string.Empty : ccAddress);
                        cmd.Parameters.AddWithValue("@MAILBCC", string.IsNullOrEmpty(bccAddress) ? string.Empty : bccAddress);
                        cmd.Parameters.AddWithValue("@SUBJECT", subject);
                        cmd.Parameters.AddWithValue("@BODY", body);
                        cmd.Parameters.AddWithValue("@ATTACHMENT", string.Empty);
                        cmd.Parameters.AddWithValue("@TIMEIN", DateTime.Now);
                        cmd.Parameters.AddWithValue("@SENDATTEMPTS", 0);
                        cmd.Parameters.AddWithValue("@SENT", false);
                        cmd.Parameters.AddWithValue("@SettingTypeId", settingTypeId);
                        cmd.Parameters.AddWithValue("@SchemeId", schemeId);
                        cmd.Parameters.AddWithValue("@VendorType", vendorType);
                        cmd.ExecuteNonQuery();
                    }

                    con.Close();
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                result = false;
            }

            return result;
        }

        public static bool SendSMS(string uid, int callStatus, int settingTypeId, int schemeId, string vendorType, string body, string toMobileNo)
        {
            bool result = true;
            try
            {
                string insertNewSMSSQL = "INSERT INTO [dbo].[SMSNotifications] ([UID], [STATUS], [SMSTO], [BODY], [TIMEIN], [SENDATTEMPTS], [SENT], [SettingTypeId], [SchemeId], [VendorType]) VALUES (@UID, @STATUS, @SMSTO, @BODY, @TIMEIN, @SENDATTEMPTS, @SENT, @SettingTypeId, @SchemeId, @VendorType)";
                string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                using (SqlConnection con = new SqlConnection(constr))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(insertNewSMSSQL))
                    {
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@UID", uid);
                        cmd.Parameters.AddWithValue("@STATUS", callStatus);
                        cmd.Parameters.AddWithValue("@SMSTO", toMobileNo.Length <= 10 ? "91" + toMobileNo : toMobileNo);
                        cmd.Parameters.AddWithValue("@BODY", body);
                        cmd.Parameters.AddWithValue("@TIMEIN", DateTime.Now);
                        cmd.Parameters.AddWithValue("@SENDATTEMPTS", 0);
                        cmd.Parameters.AddWithValue("@SENT", false);
                        cmd.Parameters.AddWithValue("@SettingTypeId", settingTypeId);
                        cmd.Parameters.AddWithValue("@SchemeId", schemeId);
                        cmd.Parameters.AddWithValue("@VendorType", vendorType);
                        cmd.ExecuteNonQuery();
                    }

                    con.Close();
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                result = false;
            }

            return result;
        }

        /// <summary>
        /// Determines whether an email address is valid.
        /// </summary>
        /// <param name="emailAddress">The email address to validate.</param>
        /// <returns>
        /// 	<c>true</c> if the email address is valid; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsValidEmailAddress(string emailAddress)
        {
            // An empty or null string is not valid
            if (String.IsNullOrEmpty(emailAddress))
            {
                return (false);
            }

            // Regular expression to match valid email address
            string emailRegex = @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}" +
                                @"\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\" +
                                @".)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";

            // Match the email address using a regular expression
            Regex re = new Regex(emailRegex);
            if (re.IsMatch(emailAddress))
                return (true);
            else
                return (false);
        }

    }

    enum SettingType
    {
        NodalOfficer = 1,
        Vendor = 2,
        Customer = 3
    }

    enum CallStatus
    {
        Open = 0,
        Escalated = 1,
        Verified = 2,
        NotVerified = 3,
        Closed = 4
    }
}