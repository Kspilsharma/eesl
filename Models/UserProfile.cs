﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;

namespace CRM.Models
{
    public class UserProfile
    {
        #region Properties

        public long RoleId { get; set; }

        public string UserProfiles { get; set; }

        public int LoginId { get; set; }

        public int UpdateId { get; set; }

        public bool IsActive { get; set; }

        #endregion

        #region Pulic Methods

        public bool InsertUpdate(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_UserProfile_InsertUpdate", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@RoleId", RoleId);
                    command.Parameters.AddWithValue("@UserProfile", UserProfiles);
                    command.Parameters.AddWithValue("@LoginId", LoginId);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }

        public DataSet Edit()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_UserProfile_Select", sqlConn);
                    command.Parameters.AddWithValue("@RoleId", RoleId);
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public DataSet HierarchySelect()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_MenuHierarchy_Select", sqlConn);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        
        #endregion

    }
}