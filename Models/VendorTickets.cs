﻿using CRM.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace EESL.Models
{
    public class VendorTickets
    {
        public string CallId { get; set; }
        public string AssignedToHistoryId { get; set; }
        public string CallerNumber { get; set; }
        public string CallerName { get; set; }
        public string Address { get; set; }
        public string Landmark { get; set; }
        public string ComplaintFrom { get; set; }
        public string UID { get; set; }
        public string StatusOfCall { get; set; }
        public string SL_WardNo { get; set; }
        public string SL_PoleNo { get; set; }
        public string DateOfCall { get; set; }
        public string TicketType { get; set; }
        public string Zone { get; set; }
        public string SL_AssignedToId { get; set; }
        public string VendorName { get; set; }
        public string DateInsert { get; set; }
        public string DateUpdate { get; set; }
        public string ComplaintHoursDiff { get; set; }
        public string StateId { get; set; }
        public string DistrictId { get; set; }
        public string SL_ULBId { get; set; }
        public string SL_LampTypeId { get; set; }
        public string Remark { get; set; }
        public string History { get; set; }
        public string ReAssign { get; set; }
        public string Rectify { get; set; }

        public DataSet GetReAssignedTo(string stateId, string districtId, string ulbId)
        {
            string VendorType = "CCMS";
            DataSet ds = new DataSet();
            using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
            {
                sqlConn.Open();
                SqlCommand command = new SqlCommand("USP_ReAssignedTo_Select", sqlConn);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@StateId", stateId);
                command.Parameters.AddWithValue("@DistrictId", districtId);
                command.Parameters.AddWithValue("@VendorType", VendorType);
                command.Parameters.AddWithValue("@ULBMappingId", ulbId);

                SqlDataAdapter sda = new SqlDataAdapter(command);
                command.CommandTimeout = 500;
                sda.Fill(ds);
            }
            return ds;
        }

        public DataSet GetLampType()
        {
            DataSet ds = new DataSet();
            using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
            {
                sqlConn.Open();
                SqlCommand command = new SqlCommand("USP_LampType_Select", sqlConn);
                command.CommandType = CommandType.StoredProcedure;

                SqlDataAdapter sda = new SqlDataAdapter(command);
                command.CommandTimeout = 500;
                sda.Fill(ds);
            }
            return ds;
        }

    }

    public class HistoryDetail
    {
        public string HistoryId { get; set; }
        public string Date { get; set; }
        public string Status { get; set; }
        public string Response { get; set; }
        public string EscalatedTo { get; set; }

    }

    public class VendorTicketsBL
    {
        public string CallId { get; set; }
        public string AssignedToHistoryId { get; set; }
        public string CallerNumber { get; set; }
        public string CallerName { get; set; }
        public string Address { get; set; }
        public string Landmark { get; set; }
        public string UID { get; set; }
        public string StatusOfCall { get; set; }
        public string DateOfCall { get; set; }
        public string VendorName { get; set; }
        public string DateInsert { get; set; }
        public string DateUpdate { get; set; }
        public string ComplaintHoursDiff { get; set; }
        public string StateId { get; set; }
        public string DistrictId { get; set; }
        public string Remark { get; set; }
        public string History { get; set; }
        public string Rectify { get; set; }
        public string ServiceReportUrl { get; set; }
        public string RectifiedCheck { get; set; }
        public string Building { get; set; }
    }

    public class AgDSMVendorTickets
    {
        public string CallId { get; set; }
        public string CallerNumber { get; set; }
        public string UID { get; set; }
        public string CallerName { get; set; }
        public string Address { get; set; }
        public string Landmark { get; set; }
        public string ComplaintFrom { get; set; }
        public string LastStatus { get; set; }
        public string District { get; set; }
        public string USCNo { get; set; }
        public string AgDSM_AssignedToId { get; set; }
        public string PumpsetType { get; set; }
        public string CallRemark { get; set; }
        public string VendorComment { get; set; }
        public string DateOfCall { get; set; }
        public string VendorName { get; set; }
        public string DateInsert { get; set; }
        public string DateUpdate { get; set; }
        public string ComplaintHoursDiff { get; set; }        
        public string History { get; set; }
        public string ReAssign { get; set; }
        public string Rectify { get; set; }
        public string StateId { get; set; }
        public string DistrictId { get; set; }
    }
    }