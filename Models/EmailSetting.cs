﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using CRM.Models;
using System.Reflection;

namespace CRM.Models
{
    public class EmailSetting
    {
        #region Properties

        public int ID { get; set; }

        public string SMTP_Host { get; set; }

        public int? SMTP_Port { get; set; }

        public bool? SSL_Enable { get; set; }

        public string UserName { get; set; }

        public string Password { get; set; }

        public string From_Email { get; set; }

        public string From_Name { get; set; }

        public string To_Email { get; set; }

        public string Cc_Email { get; set; }

        public string Bcc_Email { get; set; }

        #endregion

        #region Functions

        public DataSet Select()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_EmailSetting_Select", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        #endregion
    }
}