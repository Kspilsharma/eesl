﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Web.UI.WebControls;

namespace CRM.Models
{
    public class SQLDBHelper
    {
        public static string GetConnectionString()
        {
            return ConfigurationManager.ConnectionStrings["DefaultConnection"].ToString();
        }

        public static bool IsFieldValueExist(string tableName, string columnName, string fieldValue)
        {
            bool retVal = false;
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_TableFieldValue_Select", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@tableName", tableName);
                    command.Parameters.AddWithValue("@columnName", columnName);
                    command.Parameters.AddWithValue("@fieldValue", fieldValue);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            retVal = true;
                        }
                        else
                        {
                            retVal = false;
                        }

                    }
                }
                return retVal;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return retVal;
            }
        }

        public static bool IsFieldValueExist(string tableName, string selColumnName, string conColumnName, string conColumnvalue)
        {
            bool retVal = false;
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_FieldValueByCondition_Select", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@tableName", tableName);
                    command.Parameters.AddWithValue("@selColumnName", selColumnName);
                    command.Parameters.AddWithValue("@conColumnName", conColumnName);
                    command.Parameters.AddWithValue("@conColumnvalue", conColumnvalue);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            retVal = true;
                        }
                        else
                        {
                            retVal = false;
                        }

                    }
                }
                return retVal;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return retVal;
            }
        }

        public static bool IsFieldValueExist(string tableName, string selColumnName, string conColumnName1, string conColumnvalue1, string conColumnName2, string conColumnvalue2)
        {
            bool retVal = false;
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_FieldValueByMultipleCondition_Select", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@tableName", tableName);
                    command.Parameters.AddWithValue("@selColumnName", selColumnName);
                    command.Parameters.AddWithValue("@conColumnName1", conColumnName1);
                    command.Parameters.AddWithValue("@conColumnvalue1", conColumnvalue1);
                    command.Parameters.AddWithValue("@conColumnName2", conColumnName2);
                    command.Parameters.AddWithValue("@conColumnvalue2", conColumnvalue2);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            retVal = true;
                        }
                        else
                        {
                            retVal = false;
                        }

                    }
                }
                return retVal;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return retVal;
            }
        }

        public static string getFieldValueByCondition(string tableName, string selColumnName, string conColumnName, string conColumnvalue)
        {
            string retVal = string.Empty;
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_FieldValueByCondition_Select", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@tableName", tableName);
                    command.Parameters.AddWithValue("@selColumnName", selColumnName);
                    command.Parameters.AddWithValue("@conColumnName", conColumnName);
                    command.Parameters.AddWithValue("@conColumnvalue", conColumnvalue);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            retVal = Convert.ToString(ds.Tables[0].Rows[0][selColumnName]);
                        }
                        else
                        {
                            retVal = "";
                        }

                    }
                }
                return retVal;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return retVal;
            }
        }

        public static void PopulateDropDownList(DropDownList ddl, string tableName, string textField, string valueField, bool IsCondition, string condition)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_Dropdown_Select", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@tableName", tableName);
                    command.Parameters.AddWithValue("@textField", textField);
                    command.Parameters.AddWithValue("@valueField", valueField);
                    command.Parameters.AddWithValue("@IsCondition", IsCondition);
                    command.Parameters.AddWithValue("@condition", condition);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddl.Items.Clear();
                    DataView dv = ds.Tables[0].DefaultView;

                    dv.Sort = textField + " asc";
                                       
                    ddl.DataTextField = textField;
                    ddl.DataValueField = valueField;
                    ddl.DataSource = dv.ToTable();
                    ddl.DataBind();
                    ddl.Items.Insert(0, "--Select--");
                }
                else
                {
                    ddl.Items.Clear();
                    ddl.Items.Insert(0, "--Select--");
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void PopulateCheckboxList(CheckBoxList chk, string tableName, string textField, string valueField, bool IsCondition, string condition)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_Checkbox_Select", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@tableName", tableName);
                    command.Parameters.AddWithValue("@textField", textField);
                    command.Parameters.AddWithValue("@valueField", valueField);
                    command.Parameters.AddWithValue("@IsCondition", IsCondition);
                    command.Parameters.AddWithValue("@condition", condition);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                if (ds.Tables[0].Rows.Count > 0)
                {
                    chk.Items.Clear();
                    DataTable tDatatable = ds.Tables[0];
                    chk.DataTextField = textField;
                    chk.DataValueField = valueField;
                    chk.DataSource = tDatatable;
                    chk.DataBind();
                }
                else
                {
                    chk.Items.Clear();
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
            }
        }

        public static void PopulateDataList(DataList dlst, string tableName, string textField, string valueField, bool IsCondition, string condition)
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_Checkbox_Select", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@tableName", tableName);
                    command.Parameters.AddWithValue("@textField", textField);
                    command.Parameters.AddWithValue("@valueField", valueField);
                    command.Parameters.AddWithValue("@IsCondition", IsCondition);
                    command.Parameters.AddWithValue("@condition", condition);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataTable tDatatable = ds.Tables[0];
                    dlst.DataSource = tDatatable;
                    dlst.DataBind();
                }                
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
            }
        }

        public static DataTable GetDataInTable(string Sql)
        {
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand(Sql, sqlConn);
                    command.CommandType = CommandType.Text;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(dt);
                }
            }

            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
            }
            return dt;
        }

        public static void UpdateTableData(string Sql)
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand(Sql, sqlConn);
                    command.CommandType = CommandType.Text;
                    command.ExecuteScalar();
                }
            }

            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
            }
        }

        public static int GetTableCount(string Sql)
        {
            int count = 0;

            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand(Sql, sqlConn);
                    command.CommandType = CommandType.Text;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(dt);
                }
                if (dt.Rows.Count > 0)
                {
                    count = dt.Rows.Count;
                }
            }

            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
            }
            return count;
        }

    }
}
