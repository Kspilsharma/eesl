﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;

namespace CRM.Models
{
    public class Dashboard
    {
        # region Properties

        public DateTime? Date { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public int LanguageId { get; set; }
        public int SchemeId { get; set; }
        public int? CategoryOfCallId { get; set; }
        public int StatusOfCallId { get; set; }
        public int SourceId { get; set; }
        public int StateId { get; set; }
        public int DistrictId { get; set; }
        public int VendorId { get; set; }
        public int ULBId { get; set; }
        public int ZoneId { get; set; }

        public int UserId { get; set; }
        # endregion

        # region Function & Methods

        public DataSet Dashboard_Select()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_Dashboard_Select", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@DateFrom", DateFrom);
                    command.Parameters.AddWithValue("@DateTo", DateTo);
                    command.Parameters.AddWithValue("@LanguageId", LanguageId);
                    command.Parameters.AddWithValue("@StateId", StateId);
                    command.Parameters.AddWithValue("@DistrictId", DistrictId);
                    command.Parameters.AddWithValue("@UserId", UserId);
                    command.Parameters.AddWithValue("@ULBId", ULBId);
                    command.Parameters.AddWithValue("@ZoneId", ZoneId);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public DataSet DashboardDetail_Select()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_DashboardDetail_Select", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Date", Date);
                    command.Parameters.AddWithValue("@LanguageId", LanguageId);
                    command.Parameters.AddWithValue("@StateId", StateId);
                    command.Parameters.AddWithValue("@DistrictId", DistrictId);
                    command.Parameters.AddWithValue("@SchemeId", SchemeId);
                    command.Parameters.AddWithValue("@CallCategoryId", CategoryOfCallId);
                    command.Parameters.AddWithValue("@StatusId", StatusOfCallId);
                    command.Parameters.AddWithValue("@UserId", UserId);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public DataSet DashboardVendor_Select()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_DashboardVendor_Select", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@DateFrom", DateFrom);
                    command.Parameters.AddWithValue("@DateTo", DateTo);
                    command.Parameters.AddWithValue("@VendorId", VendorId);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public DataSet DashboardDetailVendor_Select()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_DashboardDetailVendor_Select", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Date", Date);
                    command.Parameters.AddWithValue("@SchemeId", SchemeId);
                    command.Parameters.AddWithValue("@CallCategoryId", CategoryOfCallId);
                    command.Parameters.AddWithValue("@StatusId", StatusOfCallId);
                    command.Parameters.AddWithValue("@VendorId", VendorId);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        } 

        # endregion

    }
}

