﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;

namespace CRM.Models
{
    public class UserThresold
    {

        #region Properties

        public int ID { get; set; }

        public int UserId { get; set; }

        public int RoleId { get; set; }

        public int QAThresold { get; set; }

        public int QA2Thresold { get; set; }

        public int LoginId { get; set; }

        public int UpdateId { get; set; }

        public bool IsActive { get; set; }

        #endregion

        #region Pulic Methods

        public bool Insert(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_UserThresold_InsertUpdate", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@RoleId", RoleId);
                    command.Parameters.AddWithValue("@UserId", UserId);
                    command.Parameters.AddWithValue("@QAThresold", QAThresold);
                    command.Parameters.AddWithValue("@QA2Thresold", QA2Thresold);
                    command.Parameters.AddWithValue("@LoginId", LoginId);
                    SqlParameter spOutParam = new SqlParameter("@SPOut", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }

        public DataSet Select()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_UserThresold_Select", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@RoleId", RoleId);
                    command.Parameters.AddWithValue("@UserId", UserId);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public DataSet Edit()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_UserThresold_Command", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Command", "Edit");
                    command.Parameters.AddWithValue("@ID", ID);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public bool StatusUpdate(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_UserThresold_Command", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Command", "StatusUpdate");
                    command.Parameters.AddWithValue("@ID", ID);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }

        public bool Delete(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_UserThresold_Command", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@Command", "Delete");
                    command.Parameters.AddWithValue("@ID", ID);
                    command.Parameters.AddWithValue("@IsActive", IsActive);
                    SqlParameter spOutParam = new SqlParameter("@Output", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                pErrorMsg = Ex.ToString();
                return false;
            }
        }

        #endregion
    }
}