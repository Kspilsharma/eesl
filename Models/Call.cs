﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Drawing;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace CRM.Models
{
    public class Call
    {
        # region Properties
        public int LanguageId { get; set; }
        public string OtherLanguage { get; set; }
        public string CallerNumber { get; set; }
        public string CallerName { get; set; }
        public int CallCategoryId { get; set; }
        public string Address { get; set; }
        public string Landmark { get; set; }
        public int StateId { get; set; }
        public int DistrictId { get; set; }
        public string OtherDistrict { get; set; }
        public int SchemeId { get; set; }

        public int CallId { get; set; }
        public string UID { get; set; }
        public DateTime? DateOfCall { get; set; }
        public int SourceId { get; set; }
        public int SL_ULBId { get; set; }
        public int SL_ZoneId { get; set; }
        public string SL_OtherZone { get; set; }
        public string SL_WardNo { get; set; }
        public string SL_PoleNo { get; set; }
        public int SL_TicketTypeId { get; set; }
        public string SL_OtherTicketType { get; set; }
        public int SL_AssignedToId { get; set; }
        public string SL_AssignedToRemark { get; set; }
        public string SL_OtherAssignedTo { get; set; }
        public int SL_ComplaintFromId { get; set; }
        public string SL_OtherComplaintFrom { get; set; }
        public int Sl_LampTypeId { get; set; }
        public int UJ_DiscomId { get; set; }
        public string UJ_Email { get; set; }
        public string UJ_Town { get; set; }
        public string UJ_ConsumerNo { get; set; }
        public string UJ_Products { get; set; }
        public string AGDSM_Town { get; set; }
        public string AGDSM_DistributionCenter { get; set; }
        public string AGDSM_ConsumerNo { get; set; }
        public int BL_BuildingId { get; set; }
        public string BL_FlatNo { get; set; }
        public string BL_ProductTypes { get; set; }
        public string BL_ProductTypesQty { get; set; }
        public string BL_LEDProductTypes { get; set; }
        public string BL_ACProductTypes { get; set; }
        public string BL_ReferenceNo { get; set; }
        public string BL_AssignedToIds { get; set; }
        public int CurrentStatusId { get; set; }


        public string Remark { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string ImageBase64 { get; set; }
        public bool IsMobile { get; set; }

        public int StatusOfCallId { get; set; }
        public int EscalatedToId { get; set; }
        public int UserId { get; set; }

        //Search Filters Variables
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public int? CategoryOfCallId { get; set; }
        public string Mobile { get; set; }
        public int? State { get; set; }
        public int? District { get; set; }
        public int? EscalatedTo { get; set; }
        public string Agent { get; set; }
        public int ComplaintHours { get; set; }
        public string Keyword { get; set; }
        public int TicketTypeId { get; set; }
        public int VendorId { get; set; }
        public string TicketStatus { get; set; }

        public string SL_AreaType { get; set; }

        public int SL_LightTypeId { get; set; }

        public string DateSearchBy { get; set; }

        public int AssignedToHistoryId { get; set; }

        public string AGDSM_USCORServiceConnNo { get; set; }
        public string AGDSM_PanelBoardMobNo { get; set; }
        public string AGDSM_HPkWRating { get; set; }
        public string AGDSM_ComplaintReason { get; set; }
        public int AGDSM_AssignedToId { get; set; }

        #endregion

        #region Methods

        public bool Call_Insert(out string pErrorMsg, out string pDMLType)
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_Call_Insert", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@LanguageId", LanguageId);
                    command.Parameters.AddWithValue("@OtherLanguage", OtherLanguage);
                    command.Parameters.AddWithValue("@CallerNumber", CallerNumber);
                    command.Parameters.AddWithValue("@CallerName", CallerName);
                    command.Parameters.AddWithValue("@Address", Address);
                    command.Parameters.AddWithValue("@Landmark", Landmark);
                    command.Parameters.AddWithValue("@StateId", StateId);
                    command.Parameters.AddWithValue("@DistrictId", DistrictId);
                    command.Parameters.AddWithValue("@OtherDistrict", OtherDistrict);

                    command.Parameters.AddWithValue("@UID", UID);
                    command.Parameters.AddWithValue("@CurrentStatusId", StatusOfCallId);
                    command.Parameters.AddWithValue("@SourceId", SourceId);
                    command.Parameters.AddWithValue("@SchemeId", SchemeId);
                    command.Parameters.AddWithValue("@CallCategoryId", CallCategoryId);

                    command.Parameters.AddWithValue("@SL_ULBId", SL_ULBId);
                    command.Parameters.AddWithValue("@SL_ZoneId", SL_ZoneId);
                    command.Parameters.AddWithValue("@SL_OtherZone", SL_OtherZone);
                    command.Parameters.AddWithValue("@SL_WardNo", SL_WardNo);
                    command.Parameters.AddWithValue("@SL_PoleNo", SL_PoleNo);
                    command.Parameters.AddWithValue("@SL_TicketTypeId", SL_TicketTypeId);
                    command.Parameters.AddWithValue("@SL_OtherTicketType", SL_OtherTicketType);
                    command.Parameters.AddWithValue("@SL_AssignedToId", SL_AssignedToId);
                    command.Parameters.AddWithValue("@SL_OtherAssignedTo", SL_OtherAssignedTo);
                    command.Parameters.AddWithValue("@SL_ComplaintFromId", SL_ComplaintFromId);
                    command.Parameters.AddWithValue("@SL_OtherComplaintFrom", SL_OtherComplaintFrom);
                    command.Parameters.AddWithValue("@SL_AreaType", SL_AreaType);
                    command.Parameters.AddWithValue("@SL_LightTypeId", SL_LightTypeId);

                    command.Parameters.AddWithValue("@UJ_DiscomId", UJ_DiscomId);
                    command.Parameters.AddWithValue("@UJ_Email", UJ_Email);
                    command.Parameters.AddWithValue("@UJ_Town", UJ_Town);
                    command.Parameters.AddWithValue("@UJ_ConsumerNo", UJ_ConsumerNo);
                    command.Parameters.AddWithValue("@UJ_Products", UJ_Products);

                    command.Parameters.AddWithValue("@AGDSM_Town", AGDSM_Town);
                    command.Parameters.AddWithValue("@AGDSM_DistributionCenter", AGDSM_DistributionCenter);
                    command.Parameters.AddWithValue("@AGDSM_ConsumerNo", AGDSM_ConsumerNo);
                    command.Parameters.AddWithValue("@AGDSM_USCORServiceConnNo", AGDSM_USCORServiceConnNo);
                    command.Parameters.AddWithValue("@AGDSM_PanelBoardMobNo", AGDSM_PanelBoardMobNo);
                    command.Parameters.AddWithValue("@AGDSM_HPkWRating", AGDSM_HPkWRating);
                    command.Parameters.AddWithValue("@AGDSM_ComplaintReason", AGDSM_ComplaintReason);
                    command.Parameters.AddWithValue("@AGDSM_AssignedToId", AGDSM_AssignedToId);

                    command.Parameters.AddWithValue("@BL_BuildingId", BL_BuildingId);
                    command.Parameters.AddWithValue("@BL_FlatNo", BL_FlatNo);
                    command.Parameters.AddWithValue("@BL_ProductTypes", BL_ProductTypes);
                    command.Parameters.AddWithValue("@BL_ProductTypesQty", BL_ProductTypesQty);
                    command.Parameters.AddWithValue("@BL_LEDProductTypes", BL_LEDProductTypes);
                    command.Parameters.AddWithValue("@BL_ACProductTypes", BL_ACProductTypes);
                    command.Parameters.AddWithValue("@BL_ReferenceNo", BL_ReferenceNo);
                    command.Parameters.AddWithValue("@BL_AssignedToIds", BL_AssignedToIds);


                    command.Parameters.AddWithValue("@EscalatedToId", EscalatedToId);
                    command.Parameters.AddWithValue("@Remark", Remark);

                    command.Parameters.AddWithValue("@Agent", Agent);
                    SqlParameter spOutParam = new SqlParameter("@SPOut", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    SqlParameter spOutParam1 = new SqlParameter("@DMLType", SqlDbType.NVarChar, 256);
                    spOutParam1.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.Parameters.Add(spOutParam1);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        pDMLType = spOutParam1.Value.ToString();                        
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        pDMLType = spOutParam1.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                //pErrorMsg = Ex.ToString();
                //pErrorMsg = "Application Not Responding : Try Again later";
                pErrorMsg = "Name of Caller and Address must be unique.";
                pDMLType = "Application Not Responding : Try Again later";
                return false;
            }
        }
        public bool InBoundCall_Insert(out string pErrorMsg, out string pDMLType)
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_InBoundCall_Insert", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@LanguageId", LanguageId);
                    command.Parameters.AddWithValue("@OtherLanguage", OtherLanguage);
                    command.Parameters.AddWithValue("@CallerNumber", CallerNumber);
                    command.Parameters.AddWithValue("@CallerName", CallerName);
                    command.Parameters.AddWithValue("@Address", Address);
                    command.Parameters.AddWithValue("@Landmark", Landmark);
                    command.Parameters.AddWithValue("@StateId", StateId);
                    command.Parameters.AddWithValue("@DistrictId", DistrictId);
                    command.Parameters.AddWithValue("@OtherDistrict", OtherDistrict);

                    command.Parameters.AddWithValue("@UID", UID);
                    command.Parameters.AddWithValue("@CurrentStatusId", StatusOfCallId);
                    command.Parameters.AddWithValue("@SourceId", SourceId);
                    command.Parameters.AddWithValue("@SchemeId", SchemeId);
                    command.Parameters.AddWithValue("@CallCategoryId", CallCategoryId);

                    command.Parameters.AddWithValue("@SL_ULBId", SL_ULBId);
                    command.Parameters.AddWithValue("@SL_ZoneId", SL_ZoneId);
                    command.Parameters.AddWithValue("@SL_OtherZone", SL_OtherZone);
                    command.Parameters.AddWithValue("@SL_WardNo", SL_WardNo);
                    command.Parameters.AddWithValue("@SL_PoleNo", SL_PoleNo);
                    command.Parameters.AddWithValue("@SL_TicketTypeId", SL_TicketTypeId);
                    command.Parameters.AddWithValue("@SL_OtherTicketType", SL_OtherTicketType);
                    command.Parameters.AddWithValue("@SL_AssignedToId", SL_AssignedToId);
                    command.Parameters.AddWithValue("@SL_OtherAssignedTo", SL_OtherAssignedTo);
                    command.Parameters.AddWithValue("@SL_ComplaintFromId", SL_ComplaintFromId);
                    command.Parameters.AddWithValue("@SL_OtherComplaintFrom", SL_OtherComplaintFrom);
                    command.Parameters.AddWithValue("@SL_AreaType", SL_AreaType);
                    command.Parameters.AddWithValue("@SL_LightTypeId", SL_LightTypeId);

                    command.Parameters.AddWithValue("@UJ_DiscomId", UJ_DiscomId);
                    command.Parameters.AddWithValue("@UJ_Email", UJ_Email);
                    command.Parameters.AddWithValue("@UJ_Town", UJ_Town);
                    command.Parameters.AddWithValue("@UJ_ConsumerNo", UJ_ConsumerNo);
                    command.Parameters.AddWithValue("@UJ_Products", UJ_Products);

                    command.Parameters.AddWithValue("@AGDSM_Town", AGDSM_Town);
                    command.Parameters.AddWithValue("@AGDSM_DistributionCenter", AGDSM_DistributionCenter);
                    command.Parameters.AddWithValue("@AGDSM_ConsumerNo", AGDSM_ConsumerNo);
                    command.Parameters.AddWithValue("@AGDSM_USCORServiceConnNo", AGDSM_USCORServiceConnNo);
                    command.Parameters.AddWithValue("@AGDSM_PanelBoardMobNo", AGDSM_PanelBoardMobNo);
                    command.Parameters.AddWithValue("@AGDSM_HPkWRating", AGDSM_HPkWRating);
                    command.Parameters.AddWithValue("@AGDSM_ComplaintReason", AGDSM_ComplaintReason);
                    command.Parameters.AddWithValue("@AGDSM_AssignedToId", AGDSM_AssignedToId);

                    command.Parameters.AddWithValue("@BL_BuildingId", BL_BuildingId);
                    command.Parameters.AddWithValue("@BL_FlatNo", BL_FlatNo);
                    command.Parameters.AddWithValue("@BL_ProductTypes", BL_ProductTypes);
                    command.Parameters.AddWithValue("@BL_ProductTypesQty", BL_ProductTypesQty);
                    command.Parameters.AddWithValue("@BL_LEDProductTypes", BL_LEDProductTypes);
                    command.Parameters.AddWithValue("@BL_ACProductTypes", BL_ACProductTypes);
                    command.Parameters.AddWithValue("@BL_ReferenceNo", BL_ReferenceNo);
                    command.Parameters.AddWithValue("@BL_AssignedToIds", BL_AssignedToIds);

                    command.Parameters.AddWithValue("@EscalatedToId", EscalatedToId);
                    command.Parameters.AddWithValue("@Remark", Remark);

                    command.Parameters.AddWithValue("@UserId", UserId);
                    SqlParameter spOutParam = new SqlParameter("@SPOut", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    SqlParameter spOutParam1 = new SqlParameter("@DMLType", SqlDbType.NVarChar, 256);
                    spOutParam1.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.Parameters.Add(spOutParam1);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        pDMLType = spOutParam1.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        pDMLType = spOutParam1.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                //pErrorMsg = Ex.ToString();
                //pErrorMsg = Ex.Message;
                pErrorMsg = "Name of Caller and Address must be unique.";

                //pErrorMsg = "Application Not Responding : Try Again later";
                pDMLType = "Application Not Responding : Try Again later";
                //pDMLType = Ex.Message;
                return false;
            }
        }
        public bool ConsumerEntry_Insert(out string pErrorMsg, out string pDMLType)
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_ConsumerEntry_Insert", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@LanguageId", LanguageId);
                    command.Parameters.AddWithValue("@OtherLanguage", OtherLanguage);
                    command.Parameters.AddWithValue("@CallerNumber", CallerNumber);
                    command.Parameters.AddWithValue("@CallerName", CallerName);
                    command.Parameters.AddWithValue("@Address", Address);
                    command.Parameters.AddWithValue("@Landmark", Landmark);
                    command.Parameters.AddWithValue("@StateId", StateId);
                    command.Parameters.AddWithValue("@DistrictId", DistrictId);
                    command.Parameters.AddWithValue("@OtherDistrict", OtherDistrict);

                    command.Parameters.AddWithValue("@UID", UID);
                    command.Parameters.AddWithValue("@CurrentStatusId", StatusOfCallId);
                    command.Parameters.AddWithValue("@SourceId", SourceId);
                    command.Parameters.AddWithValue("@SchemeId", SchemeId);
                    command.Parameters.AddWithValue("@CallCategoryId", CallCategoryId);

                    command.Parameters.AddWithValue("@SL_ULBId", SL_ULBId);
                    command.Parameters.AddWithValue("@SL_ZoneId", SL_ZoneId);
                    command.Parameters.AddWithValue("@SL_OtherZone", SL_OtherZone);
                    command.Parameters.AddWithValue("@SL_WardNo", SL_WardNo);
                    command.Parameters.AddWithValue("@SL_PoleNo", SL_PoleNo);
                    command.Parameters.AddWithValue("@SL_TicketTypeId", SL_TicketTypeId);
                    command.Parameters.AddWithValue("@SL_OtherTicketType", SL_OtherTicketType);
                    command.Parameters.AddWithValue("@SL_AssignedToId", SL_AssignedToId);
                    command.Parameters.AddWithValue("@SL_OtherAssignedTo", SL_OtherAssignedTo);
                    command.Parameters.AddWithValue("@SL_ComplaintFromId", SL_ComplaintFromId);
                    command.Parameters.AddWithValue("@SL_OtherComplaintFrom", SL_OtherComplaintFrom);
                    command.Parameters.AddWithValue("@SL_AreaType", SL_AreaType);
                    command.Parameters.AddWithValue("@SL_LightTypeId", SL_LightTypeId);

                    command.Parameters.AddWithValue("@UJ_DiscomId", UJ_DiscomId);
                    command.Parameters.AddWithValue("@UJ_Email", UJ_Email);
                    command.Parameters.AddWithValue("@UJ_Town", UJ_Town);
                    command.Parameters.AddWithValue("@UJ_ConsumerNo", UJ_ConsumerNo);
                    command.Parameters.AddWithValue("@UJ_Products", UJ_Products);

                    command.Parameters.AddWithValue("@AGDSM_Town", AGDSM_Town);
                    command.Parameters.AddWithValue("@AGDSM_DistributionCenter", AGDSM_DistributionCenter);
                    command.Parameters.AddWithValue("@AGDSM_ConsumerNo", AGDSM_ConsumerNo);


                    command.Parameters.AddWithValue("@BL_BuildingId", BL_BuildingId);
                    command.Parameters.AddWithValue("@BL_FlatNo", BL_FlatNo);
                    command.Parameters.AddWithValue("@BL_ProductTypes", BL_ProductTypes);
                    command.Parameters.AddWithValue("@BL_ProductTypesQty", BL_ProductTypesQty);
                    command.Parameters.AddWithValue("@BL_LEDProductTypes", BL_LEDProductTypes);
                    command.Parameters.AddWithValue("@BL_ACProductTypes", BL_ACProductTypes);
                    command.Parameters.AddWithValue("@BL_ReferenceNo", BL_ReferenceNo);
                    command.Parameters.AddWithValue("@BL_AssignedToIds", BL_AssignedToIds);

                    command.Parameters.AddWithValue("@EscalatedToId", EscalatedToId);
                    command.Parameters.AddWithValue("@Remark", Remark);

                    command.Parameters.AddWithValue("@UserId", UserId);
                    SqlParameter spOutParam = new SqlParameter("@SPOut", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    SqlParameter spOutParam1 = new SqlParameter("@DMLType", SqlDbType.NVarChar, 256);
                    spOutParam1.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.Parameters.Add(spOutParam1);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        pDMLType = spOutParam1.Value.ToString();
                        // Save Image and Update into CallDetailMaster Table ** Start
                        if (!string.IsNullOrEmpty(ImageBase64))
                        {
                            Image img = GetImage(ImageBase64);
                            string ImageName = pErrorMsg + ".jpeg";
                            string FileSavePath = HttpContext.Current.Server.MapPath("~/CompliantImage");
                            FileSavePath = FileSavePath + "\\" + ImageName;
                            if (!File.Exists(FileSavePath))
                            {                                
                                Image NewImg =  ResizeImageFixedWidth(img, 1920);
                                NewImg.Save(FileSavePath, System.Drawing.Imaging.ImageFormat.Jpeg);
                                string updatesql = "Update CallDetailMaster Set Longitude = '" + Longitude + "', latitude = '" + Latitude + "', ImagePath = '" + FileSavePath + "' where UID = '" + pErrorMsg + "' ";
                                SQLDBHelper.UpdateTableData(updatesql);
                            }
                        }
                        // ** End
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        pDMLType = spOutParam1.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                //pErrorMsg = Ex.ToString();
                if (Ex.Message.Contains("Not a unique customer with address INSERT Statment from dialer."))
                {
                    pErrorMsg = "Name of Caller and Address must be unique.";
                    pDMLType = "Application Not Responding : Try Again later";
                }
                else
                {
                    pErrorMsg = "Invalid Entry";
                    pDMLType = "Application Not Responding : Try Again later";
                }
                return false;
            }
        }
        public bool CallReAssigned_Update(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_CallReAssign_Update", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@CallId", CallId);
                    command.Parameters.AddWithValue("@AssignedToId", SL_AssignedToId);
                    command.Parameters.AddWithValue("@Remark", Remark);
                    command.Parameters.AddWithValue("@UserId", UserId);
                    SqlParameter spOutParam = new SqlParameter("@SPOut", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                //pErrorMsg = Ex.ToString();
                pErrorMsg = "Application Not Responding : Try Again later";
                return false;
            }
        }
        public bool CallRectified_Update(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_CallRectified_Update", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@CallId", CallId);
                    command.Parameters.AddWithValue("@LampTypeId", Sl_LampTypeId);
                    command.Parameters.AddWithValue("@Remark", Remark);
                    command.Parameters.AddWithValue("@UserId", UserId);
                    SqlParameter spOutParam = new SqlParameter("@SPOut", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                //pErrorMsg = Ex.ToString();
                pErrorMsg = "Application Not Responding : Try Again later";
                return false;
            }
        }
        public bool CallRectifiedBuilding_Update(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_CallRectifiedBuilding_Update", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@AssignedToHistoryId", AssignedToHistoryId);
                    command.Parameters.AddWithValue("@CallId", CallId);
                    command.Parameters.AddWithValue("@Remark", Remark);
                    command.Parameters.AddWithValue("@UserId", UserId);
                    SqlParameter spOutParam = new SqlParameter("@SPOut", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                //pErrorMsg = Ex.ToString();
                pErrorMsg = "Application Not Responding : Try Again later";
                return false;
            }
        }
        public bool CallRectifiedAgDSM_Update(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_CallRectifiedAgDSM_Update", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@CallId", CallId);
                    command.Parameters.AddWithValue("@Remark", Remark);
                    command.Parameters.AddWithValue("@UserId", UserId);
                    SqlParameter spOutParam = new SqlParameter("@SPOut", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return false;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                //pErrorMsg = Ex.ToString();
                pErrorMsg = "Application Not Responding : Try Again later";
                return false;
            }
        }
        public DataSet Call_Search()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_CallSearch_Select", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UID", UID);
                    command.Parameters.AddWithValue("@DateFrom", DateFrom);
                    command.Parameters.AddWithValue("@DateTo", DateTo);
                    command.Parameters.AddWithValue("@StatusOfCallId", StatusOfCallId);
                    command.Parameters.AddWithValue("@SourceId", SourceId);
                    command.Parameters.AddWithValue("@SchemeId", SchemeId);
                    command.Parameters.AddWithValue("@CategoryOfCall", CategoryOfCallId);
                    command.Parameters.AddWithValue("@LanguageId", LanguageId);
                    command.Parameters.AddWithValue("@CallerName", CallerName);
                    command.Parameters.AddWithValue("@CallerNumber", CallerNumber);

                    command.Parameters.AddWithValue("@State", StateId);
                    command.Parameters.AddWithValue("@District", DistrictId);
                    command.Parameters.AddWithValue("@EscalatedTo", EscalatedToId);
                    command.Parameters.AddWithValue("@ComplaintHours", ComplaintHours);
                    command.Parameters.AddWithValue("@Keyword", Keyword);

                    command.Parameters.AddWithValue("@DateSearchBy", DateSearchBy);
                    command.Parameters.AddWithValue("@UserId", UserId);

                    command.Parameters.AddWithValue("@SL_ULBId", SL_ULBId);
                    command.Parameters.AddWithValue("@SL_ZoneId", SL_ZoneId);
                    command.Parameters.AddWithValue("@SL_WardNo", SL_WardNo);
                    command.Parameters.AddWithValue("@SL_ComplaintFromId", SL_ComplaintFromId);

                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public DataSet CallVendor_Search()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_CallSearchVendor_Select", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UID", UID);
                    command.Parameters.AddWithValue("@DateFrom", DateFrom);
                    command.Parameters.AddWithValue("@DateTo", DateTo);
                    command.Parameters.AddWithValue("@SourceId", SourceId);
                    command.Parameters.AddWithValue("@LanguageId", LanguageId);
                    command.Parameters.AddWithValue("@CallerName", CallerName);
                    command.Parameters.AddWithValue("@CallerNumber", CallerNumber);
                    command.Parameters.AddWithValue("@TicketTypeId", TicketTypeId);
                    command.Parameters.AddWithValue("@VendorId", VendorId);
                    command.Parameters.AddWithValue("@Keyword", Keyword);
                    command.Parameters.AddWithValue("@TicketStatus", TicketStatus);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public DataSet CallVendorBuilding_Search()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_CallSearchVendorBuilding_Select", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UID", UID);
                    command.Parameters.AddWithValue("@DateFrom", DateFrom);
                    command.Parameters.AddWithValue("@DateTo", DateTo);
                    command.Parameters.AddWithValue("@SourceId", SourceId);
                    command.Parameters.AddWithValue("@LanguageId", LanguageId);
                    command.Parameters.AddWithValue("@CallerName", CallerName);
                    command.Parameters.AddWithValue("@CallerNumber", CallerNumber);
                    command.Parameters.AddWithValue("@VendorId", VendorId);
                    command.Parameters.AddWithValue("@Keyword", Keyword);
                    command.Parameters.AddWithValue("@TicketStatus", TicketStatus);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public DataSet Call_Edit()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_CallSearch_Edit", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@CallId", CallId);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public DataSet GetCallDetailsByMobileNo()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_CallDetails_MobileNo", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@CallerNumber", CallerNumber);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public DataSet GetCallDetailsByUID()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_CallDetails_UID", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UID", UID);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public DataSet GetCallHistory()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_CallHistory", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@CallId", CallId);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public string GetNodalOfficerEmail()
        {
            string sEmail = string.Empty;
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_NodalOfficers_Select", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@EscalatedToId", EscalatedToId);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        sEmail = Convert.ToString(ds.Tables[0].Rows[0]["Email"]);
                    }
                }
                return sEmail;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public DataSet getNodalOfficerEmailMobileSelect()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_NOEmailSMS_Select", sqlConn);
                    command.Parameters.AddWithValue("@SchemeId", SchemeId);
                    command.Parameters.AddWithValue("@StateId", StateId);
                    command.Parameters.AddWithValue("@DistrictId", DistrictId);
                    command.Parameters.AddWithValue("@IsActive", 1);
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public DataSet getVendorEmailMobileSelect()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_VendorEmailSMS_Select", sqlConn);
                    command.Parameters.AddWithValue("@SchemeId", SchemeId);
                    command.Parameters.AddWithValue("@StateId", StateId);
                    command.Parameters.AddWithValue("@DistrictId", DistrictId);
                    command.Parameters.AddWithValue("@ULBId", SL_ULBId);
                    command.Parameters.AddWithValue("@ZoneId", SL_ZoneId);
                    command.Parameters.AddWithValue("@IsActive", 1);
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        public DataSet getParamMapping()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_ParamMapping_Select", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public DataSet NodalOfficerEmailMobileSelectAll()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_NOEmailSMS_SelectAll", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public DataSet VendorEmailMobileSelectAll()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_VendorEmailSMS_SelectAll", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public DataSet ProductTypesSelectAll()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_ProductTypes_SelectAll", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public Image GetImage(string ImgStr)
        {
            //data:image/gif;base64,
            //this image is a single pixel (black)
            byte[] bytes = Convert.FromBase64String(ImgStr);

            Image image;
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                image = Image.FromStream(ms);
            }
            return image;
        }

        public static Image ResizeImageFixedWidth(Image imgToResize, int width)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = ((float)width / (float)sourceWidth);

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap b = new Bitmap(destWidth, destHeight);
            Graphics g = Graphics.FromImage((Image)b);
            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            g.Dispose();

            return (Image)b;
        }

        public DataSet CallVendorAgDSM_Search()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_CallSearchVendorAgDSM_Select", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@UID", UID);
                    command.Parameters.AddWithValue("@DateFrom", DateFrom);
                    command.Parameters.AddWithValue("@DateTo", DateTo);
                    command.Parameters.AddWithValue("@SourceId", SourceId);
                    command.Parameters.AddWithValue("@LanguageId", LanguageId);
                    command.Parameters.AddWithValue("@CallerName", CallerName);
                    command.Parameters.AddWithValue("@CallerNumber", CallerNumber);
                    command.Parameters.AddWithValue("@TicketTypeId", TicketTypeId);
                    command.Parameters.AddWithValue("@VendorId", VendorId);
                    command.Parameters.AddWithValue("@Keyword", Keyword);
                    command.Parameters.AddWithValue("@TicketStatus", TicketStatus);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }
        #endregion
    }
}