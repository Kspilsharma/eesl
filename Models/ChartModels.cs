﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EESL.Models
{
    public class SchemeWiseGraphData
    {
        public string Y { get; set; }
        public int TOTALCALLS { get; set; }
        public int SL { get; set; }
        public int UJALA { get; set; }
        public int AGDSM { get; set; }
        public int BUILDING { get; set; }
        public int OTHERS { get; set; }
    }

    public class CallStatusWiseGraphData
    {
        public string Y { get; set; }
        public int TOTALCALLS { get; set; }
        public int OPEN { get; set; }
        public int ESCALATED { get; set; }
        public int VERIFIED { get; set; }
        public int NOTVERIFIED { get; set; }
        public int CLOSED { get; set; }
        public int RECTIFIED { get; set; }
    }

    public class CallCategoryWiseGraphData
    {
        public string Y { get; set; }
        public int TOTALCALLS { get; set; }
        public int QUERY { get; set; }
        public int REGISTRATION { get; set; }
        public int COMPLAINT { get; set; }
        public int OTHERS { get; set; }
    }

    public class GraphData
    {
        public List<SchemeWiseGraphData> SchemeWiseData { get; set; }
        public List<CallStatusWiseGraphData> CallStatusWiseData { get; set; }
        public List<CallCategoryWiseGraphData> CallCategoryWiseData { get; set; }
    }
}