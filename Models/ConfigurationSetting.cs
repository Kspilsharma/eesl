﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;


namespace CRM.Models
{
    public class ConfigurationSetting
    {
        #region Properties

        //public string BatchDate { get; set; }

        public string ImageFolderPath { get; set; }

        #endregion

        #region Pulic Methods

        public bool Update(out string pErrorMsg)
        {
            try
            {
                using (SqlConnection sqlConn = new
                        SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_Configuration_Update", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@ImageFolderPath", ImageFolderPath);
                    SqlParameter spOutParam = new SqlParameter("@SPOut", SqlDbType.NVarChar, 256);
                    spOutParam.Direction = ParameterDirection.Output;
                    command.Parameters.Add(spOutParam);
                    command.CommandTimeout = 500;
                    if (command.ExecuteNonQuery() > 0)
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                    else
                    {
                        pErrorMsg = spOutParam.Value.ToString();
                        return true;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                //pErrorMsg = Ex.ToString();
                pErrorMsg = "Application Not Responding : Try Again later";
                return false;
            }
        }

        public DataSet Select()
        {
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_Configuration_Select", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                }
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }


        public string getConfigurationSettingFieldValue(string columnName)
        {
            string retVal = string.Empty;
            DataSet ds = new DataSet();
            try
            {
                using (SqlConnection sqlConn = new SqlConnection(SQLDBHelper.GetConnectionString()))
                {
                    sqlConn.Open();
                    SqlCommand command = new SqlCommand("USP_ConfigurationSettingFieldValue_Select", sqlConn);
                    command.CommandType = CommandType.StoredProcedure;
                    command.Parameters.AddWithValue("@columnName", columnName);
                    SqlDataAdapter sda = new SqlDataAdapter(command);
                    command.CommandTimeout = 500;
                    sda.Fill(ds);
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            retVal = Convert.ToString(ds.Tables[0].Rows[0][columnName]);
                        }
                        else
                        {
                            retVal = "";
                        }

                    }
                }
                return retVal;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return retVal;
            }
        }
       

        #endregion
    }
}