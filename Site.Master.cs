﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRM.Models;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace CRM
{
    public partial class SiteMaster : System.Web.UI.MasterPage
    {
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();

        /// <summary>
        /// Set Page Header 
        /// </summary>
        //public string PageHeader
        //{
        //    set { lblPageHeader.Text = value; }
        //}

        public int SessionLengthMinutes
        {
            get { return Session.Timeout; }
        }
        public string SessionExpireDestinationUrl
        {
            get { return AppBaseURL + "frmSessionExpired.aspx"; }
        }
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            this.Page.Header.Controls.Add(new LiteralControl(
                String.Format("<meta http-equiv='refresh' content='{0};url={1}'>",
                SessionLengthMinutes * 60, SessionExpireDestinationUrl)));
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //if (!IsPostBack)
            //{
            if (UIManager.CurrentUserSession() != null)
            {
                //FavIconLink.Text = "<link rel=\"shortcut icon\" href=\"../App_Themes/" + Page.Theme + "/Images/favicon.ico\" type=\"image/vnd.microsoft.icon\" /> ";
                lblUserName.Text = UIManager.CurrentUserSession().UserName;
                string CurrentDate = UIManager.CurrentUserSession().LastLogin.ToString("dd-MM-yyyy HH:mm:ss");
                string[] datevalue = CurrentDate.Split('-');
                string[] monthname = new string[] { "Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
                int month = Convert.ToInt32(datevalue[1]);
                string newdate = datevalue[0] + "-" + monthname[month - 1] + "-" + datevalue[2];
                hlLastLogin.Text = "Last Login : " + newdate;
                lblCopyRight.Text = "&copy; " + DateTime.Now.Year + "- CRM";
                Page.Title = "Call Record Management";
                PopulateMenu();
            }
            else
            { Response.Redirect(AppBaseURL + "frmSessionExpired.aspx", false); }

            //}
        }

        private void PopulateMenu()
        {
            UserMaster tUserMaster = new UserMaster();
            tUserMaster.RoleId = UIManager.CurrentUserSession().RoleId;
            DataSet tDataSet = tUserMaster.MenuBindingSelect();
            DataView view = new DataView(tDataSet.Tables[0]);
            view.RowFilter = "ParentId IS NULL";
            foreach (DataRowView row in view)
            {
                MenuItem menuItem = new MenuItem(row["MenuTitle"].ToString(), row["MenuId"].ToString());
                if (!string.IsNullOrEmpty(row["MenuUrl"].ToString()))
                {
                    menuItem.NavigateUrl = row["MenuUrl"].ToString();
                }
                else
                {
                    menuItem.NavigateUrl = "";
                }
                menuItem.ToolTip = row["Description"].ToString();
                NavigationMenu.Items.Add(menuItem);
                AddChildMenuItems(tDataSet.Tables[0], menuItem);
            }
        }

        private void AddChildMenuItems(DataTable table, MenuItem menuItem)
        {
            DataView viewItem = new DataView(table);
            viewItem.RowFilter = "ParentId = " + menuItem.Value;
            foreach (DataRowView childView in viewItem)
            {
                MenuItem childItem = new MenuItem(childView["MenuTitle"].ToString(), childView["MenuId"].ToString());
                if (!string.IsNullOrEmpty(childView["MenuUrl"].ToString()))
                {
                    menuItem.NavigateUrl = childView["MenuUrl"].ToString();
                }
                childItem.NavigateUrl = childView["MenuUrl"].ToString();
                childItem.ToolTip = childView["Description"].ToString();
                menuItem.ChildItems.Add(childItem);
                AddChildMenuItems(table, childItem);
            }
        }

        protected void lnkLogOut_Click(object sender, EventArgs e)
        {
            try
            {
                UserMaster tUserMaster = new UserMaster();
                tUserMaster.UserId = UIManager.CurrentUserSession().UserId;
                tUserMaster.Logout();
                Session["UserName"] = null;
                if (UIManager.CurrentUserSession() != null)
                {
                    Response.Redirect("~/Login.aspx", false);
                    UIManager.CurrentUserSession().EndSession();
                }
                else
                { Response.Redirect(AppBaseURL + "frmSessionExpired.aspx", false); }
            }
            catch (Exception)
            { }
        }
    }
}
