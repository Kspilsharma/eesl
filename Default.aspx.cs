﻿using CRM.Models;
using EESL.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI.WebControls;

namespace CRM
{
    public partial class _Default : UIBase
    {
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateUI();
                if ((UIManager.CurrentUserSession().RoleName.ToUpper() == "ADMIN") || (UIManager.CurrentUserSession().RoleName.ToUpper() == "SUPERVISOR") || (UIManager.CurrentUserSession().RoleName.ToUpper() == "CUSTOMER"))
                {
                    Dashboard();
                }
                else if ((UIManager.CurrentUserSession().RoleName.ToUpper() == "VENDOR"))
                {
                    Response.Redirect("~/View/frmVendorDashboard.aspx", false);
                }
            }
        }

        private void PopulateUI()
        {
            txtFrom.Text = "";
            txtTo.Text = "";
            SQLDBHelper.PopulateDropDownList(ddlState, "State", "State", "StateId", false, "");
            SQLDBHelper.PopulateDropDownList(ddlLanguage, "Languages", "Language", "LanguageId", false, "");
            ddlDistrict.Items.Clear();
            ddlULB.Items.Clear();
            ddlZone.Items.Clear();
            ddlDistrict.Items.Insert(0, "--Select--");
            ddlULB.Items.Insert(0, "--Select--");
            ddlZone.Items.Insert(0, "--Select--");
        }

        protected void Dashboard()
        {
            try
            {
                Session["dashboard"] = null;
                Dashboard tDashboard = new Dashboard();
                if (ddlLanguage.SelectedIndex > 0)
                {
                    tDashboard.LanguageId = Convert.ToInt32(ddlLanguage.SelectedItem.Value);
                }
                if (ddlState.SelectedIndex > 0)
                {
                    tDashboard.StateId = Convert.ToInt32(ddlState.SelectedItem.Value);
                }
                if (ddlDistrict.SelectedIndex > 0)
                {
                    tDashboard.DistrictId = Convert.ToInt32(ddlDistrict.SelectedItem.Value);
                }
                if (ddlULB.SelectedIndex > 0)
                {
                    tDashboard.ULBId = Convert.ToInt32(ddlULB.SelectedItem.Value);
                }
                if (ddlZone.SelectedIndex > 0)
                {
                    tDashboard.ZoneId = Convert.ToInt32(ddlZone.SelectedItem.Value);
                }


                if (!String.IsNullOrEmpty(txtFrom.Text))
                {
                    tDashboard.DateFrom = Convert.ToDateTime(txtFrom.Text.Trim());
                }
                if (!String.IsNullOrEmpty(txtTo.Text))
                {
                    tDashboard.DateTo = Convert.ToDateTime(txtTo.Text.Trim());
                }

                tDashboard.UserId = UIManager.CurrentUserSession().UserId;

                DataSet ds = tDashboard.Dashboard_Select();
                Session["dashboard"] = ds;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvInBoundCallStatus.DataSource = ds.Tables[0];
                    gvInBoundCallStatus.DataBind();
                    gvInBoundCallStatus.Visible = true;

                    int totalcalls = ds.Tables[0].AsEnumerable().Sum(row => row.Field<int>("TotalCalls"));
                    int Open = ds.Tables[0].AsEnumerable().Sum(row => row.Field<int>("Open"));
                    int Escalated = ds.Tables[0].AsEnumerable().Sum(row => row.Field<int>("Escalated"));
                    int Verified = ds.Tables[0].AsEnumerable().Sum(row => row.Field<int>("Verified"));
                    int NonVerified = ds.Tables[0].AsEnumerable().Sum(row => row.Field<int>("Non Verified"));
                    int Closed = ds.Tables[0].AsEnumerable().Sum(row => row.Field<int>("Closed"));
                    int Rectified = ds.Tables[0].AsEnumerable().Sum(row => row.Field<int>("Rectified"));

                    gvInBoundCallStatus.FooterRow.Cells[1].Text = "Total";
                    gvInBoundCallStatus.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallStatus.FooterRow.Cells[2].Text = totalcalls.ToString();
                    gvInBoundCallStatus.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallStatus.FooterRow.Cells[3].Text = Open.ToString();
                    gvInBoundCallStatus.FooterRow.Cells[3].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallStatus.FooterRow.Cells[4].Text = Escalated.ToString();
                    gvInBoundCallStatus.FooterRow.Cells[4].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallStatus.FooterRow.Cells[5].Text = Verified.ToString();
                    gvInBoundCallStatus.FooterRow.Cells[5].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallStatus.FooterRow.Cells[6].Text = NonVerified.ToString();
                    gvInBoundCallStatus.FooterRow.Cells[6].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallStatus.FooterRow.Cells[7].Text = Closed.ToString();
                    gvInBoundCallStatus.FooterRow.Cells[7].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallStatus.FooterRow.Cells[8].Text = Rectified.ToString();
                    gvInBoundCallStatus.FooterRow.Cells[8].HorizontalAlign = HorizontalAlign.Center;
                }
                else
                {
                    gvInBoundCallStatus.Visible = false;
                }

                if (ds.Tables[1].Rows.Count > 0)
                {
                    gvInBoundCallType.DataSource = ds.Tables[1];
                    gvInBoundCallType.DataBind();
                    gvInBoundCallType.Visible = true;

                    int totalcalls = ds.Tables[1].AsEnumerable().Sum(row => row.Field<int>("TotalCalls"));
                    int Query = ds.Tables[1].AsEnumerable().Sum(row => row.Field<int>("Query"));
                    int Registration = ds.Tables[1].AsEnumerable().Sum(row => row.Field<int>("Registration"));
                    int Complaint = ds.Tables[1].AsEnumerable().Sum(row => row.Field<int>("Complaint"));
                    int Other = ds.Tables[1].AsEnumerable().Sum(row => row.Field<int>("Other"));

                    gvInBoundCallType.FooterRow.Cells[1].Text = "Total";
                    gvInBoundCallType.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallType.FooterRow.Cells[2].Text = totalcalls.ToString();
                    gvInBoundCallType.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallType.FooterRow.Cells[3].Text = Query.ToString();
                    gvInBoundCallType.FooterRow.Cells[3].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallType.FooterRow.Cells[4].Text = Registration.ToString();
                    gvInBoundCallType.FooterRow.Cells[4].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallType.FooterRow.Cells[5].Text = Complaint.ToString();
                    gvInBoundCallType.FooterRow.Cells[5].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallType.FooterRow.Cells[6].Text = Other.ToString();
                    gvInBoundCallType.FooterRow.Cells[6].HorizontalAlign = HorizontalAlign.Center;
                }
                else
                {
                    gvInBoundCallType.Visible = false;
                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    gvInBoundCallScheme.DataSource = ds.Tables[2];
                    gvInBoundCallScheme.DataBind();
                    gvInBoundCallScheme.Visible = true;

                    int totalcalls = ds.Tables[2].AsEnumerable().Sum(row => row.Field<int>("TotalCalls"));
                    int SL = ds.Tables[2].AsEnumerable().Sum(row => row.Field<int>("Street Light"));
                    int Ujala = ds.Tables[2].AsEnumerable().Sum(row => row.Field<int>("Ujala"));
                    int AgDSM = ds.Tables[2].AsEnumerable().Sum(row => row.Field<int>("AGDSM"));
                    int Building = ds.Tables[2].AsEnumerable().Sum(row => row.Field<int>("Building"));
                    int Other = ds.Tables[2].AsEnumerable().Sum(row => row.Field<int>("Other"));

                    gvInBoundCallScheme.FooterRow.Cells[1].Text = "Total";
                    gvInBoundCallScheme.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallScheme.FooterRow.Cells[2].Text = totalcalls.ToString();   
                    gvInBoundCallStatus.FooterRow.Cells[3].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallScheme.FooterRow.Cells[3].Text = SL.ToString();
                    gvInBoundCallStatus.FooterRow.Cells[3].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallScheme.FooterRow.Cells[4].Text = Ujala.ToString();
                    gvInBoundCallStatus.FooterRow.Cells[4].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallScheme.FooterRow.Cells[5].Text = AgDSM.ToString();
                    gvInBoundCallStatus.FooterRow.Cells[5].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallScheme.FooterRow.Cells[6].Text = Building.ToString();
                    gvInBoundCallStatus.FooterRow.Cells[6].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallScheme.FooterRow.Cells[7].Text = Other.ToString();
                    gvInBoundCallStatus.FooterRow.Cells[7].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallScheme.Columns[3].Visible = false;
                    gvInBoundCallScheme.Columns[4].Visible = false;
                    gvInBoundCallScheme.Columns[5].Visible = false;
                    gvInBoundCallScheme.Columns[6].Visible = false;
                    gvInBoundCallScheme.Columns[7].Visible = false;


                    DataTable dtScheme = SQLDBHelper.GetDataInTable("Select SchemeAbbreviation from Scheme SCH inner join UserSchemeMapping USM on SCH.SchemeId = USM.SchemeId where USM.UserId = " + tDashboard.UserId + "");
                    if (dtScheme.Rows.Count > 0)
                    {
                        DataRow[] drSL = dtScheme.Select(string.Format("SchemeAbbreviation ='{0}' ", "SL"));
                        DataRow[] drUJ = dtScheme.Select(string.Format("SchemeAbbreviation ='{0}' ", "UJ"));
                        DataRow[] drAG = dtScheme.Select(string.Format("SchemeAbbreviation ='{0}' ", "AG"));
                        DataRow[] drBL = dtScheme.Select(string.Format("SchemeAbbreviation ='{0}' ", "BL"));
                        DataRow[] drOT = dtScheme.Select(string.Format("SchemeAbbreviation ='{0}' ", "OT"));

                        if (drSL.Length > 0)
                            gvInBoundCallScheme.Columns[3].Visible = true;

                        if (drUJ.Length > 0)
                            gvInBoundCallScheme.Columns[4].Visible = true;

                        if (drAG.Length > 0)
                            gvInBoundCallScheme.Columns[5].Visible = true;

                        if (drBL.Length > 0)
                            gvInBoundCallScheme.Columns[6].Visible = true;

                        if (drOT.Length > 0)
                            gvInBoundCallScheme.Columns[7].Visible = true;
                    }
                    else
                    {
                        gvInBoundCallScheme.Columns[3].Visible = true;
                        gvInBoundCallScheme.Columns[4].Visible = true;
                        gvInBoundCallScheme.Columns[5].Visible = true;
                        gvInBoundCallScheme.Columns[6].Visible = true;
                        gvInBoundCallScheme.Columns[7].Visible = true;
                    }
                }
                else
                {
                    gvInBoundCallScheme.Visible = false;
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlDistrict.Items.Clear();
            if (ddlState.SelectedIndex != 0)
            {
                SQLDBHelper.PopulateDropDownList(ddlDistrict, "District", "District", "DistrictId", true, "StateId=" + ddlState.SelectedValue);
            }
            else
            {
                ddlDistrict.Items.Insert(0, "--Select--");
            }
            btnSearch_Click(null, null);
            ddlDistrict.Focus();

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {

                if ((UIManager.CurrentUserSession().RoleName.ToUpper() == "ADMIN") || (UIManager.CurrentUserSession().RoleName.ToUpper() == "SUPERVISOR") || (UIManager.CurrentUserSession().RoleName.ToUpper() == "CUSTOMER"))
                {
                    Dashboard();
                }

            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                Session["IsReset"] = "YES";
                PopulateUI();
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        [WebMethod(EnableSession = true)]
        public static GraphData GetChartData()
        {
            List<SchemeWiseGraphData> schemeWiseDataList = new List<SchemeWiseGraphData>();
            List<CallStatusWiseGraphData> callStatusWiseDataList = new List<CallStatusWiseGraphData>();
            List<CallCategoryWiseGraphData> callCategoryWiseDataList = new List<CallCategoryWiseGraphData>();
            DataSet dashboardData = (DataSet)System.Web.HttpContext.Current.Session["dashboard"];

            if (dashboardData.Tables[2].Rows.Count > 0)
            {
                schemeWiseDataList = (from b in dashboardData.Tables[2].AsEnumerable()
                                      select new
                                      {
                                          CallDate = b.Field<DateTime>("CallDate"),
                                          TotalCalls = b.Field<int?>("TotalCalls"),
                                          StreetLight = b.Field<int?>("Street Light"),
                                          Ujala = b.Field<int?>("Ujala"),
                                          AGDSM = b.Field<int?>("AGDSM"),
                                          Building = b.Field<int?>("Building"),
                                          Other = b.Field<int?>("Other")
                                      }).ToList()
                            .Select(r => new SchemeWiseGraphData
                            {
                                Y = r.CallDate.ToString("yyyy-MM-dd"),
                                TOTALCALLS = r.TotalCalls.HasValue ? r.TotalCalls.Value : 0,
                                SL = r.StreetLight.HasValue ? r.StreetLight.Value : 0,
                                UJALA = r.Ujala.HasValue ? r.Ujala.Value : 0,
                                AGDSM = r.AGDSM.HasValue ? r.AGDSM.Value : 0,
                                BUILDING = r.Building.HasValue ? r.Building.Value : 0,
                                OTHERS = r.Other.HasValue ? r.Other.Value : 0
                            }).ToList();
            }

            if (dashboardData.Tables[1].Rows.Count > 0)
            {
                callCategoryWiseDataList = (from b in dashboardData.Tables[1].AsEnumerable()
                                            select new
                                            {
                                                CallDate = b.Field<DateTime>("CallDate"),
                                                TotalCalls = b.Field<int?>("TotalCalls"),
                                                Query = b.Field<int?>("Query"),
                                                Registration = b.Field<int?>("Registration"),
                                                Complaint = b.Field<int?>("Complaint"),
                                                Other = b.Field<int?>("Other")
                                            }).ToList()
                            .Select(r => new CallCategoryWiseGraphData
                            {
                                Y = r.CallDate.ToString("yyyy-MM-dd"),
                                TOTALCALLS = r.TotalCalls.HasValue ? r.TotalCalls.Value : 0,
                                QUERY = r.Query.HasValue ? r.Query.Value : 0,
                                REGISTRATION = r.Registration.HasValue ? r.Registration.Value : 0,
                                COMPLAINT = r.Complaint.HasValue ? r.Complaint.Value : 0,
                                OTHERS = r.Other.HasValue ? r.Other.Value : 0
                            }).ToList();
            }

            if (dashboardData.Tables[0].Rows.Count > 0)
            {
                callStatusWiseDataList = (from b in dashboardData.Tables[0].AsEnumerable()
                                          select new
                                          {
                                              CallDate = b.Field<DateTime>("CallDate"),
                                              TotalCalls = b.Field<int?>("TotalCalls"),
                                              Open = b.Field<int?>("Open"),
                                              Escalated = b.Field<int?>("Escalated"),
                                              Verified = b.Field<int?>("Verified"),
                                              NonVerified = b.Field<int?>("Non Verified"),
                                              Closed = b.Field<int?>("Closed"),
                                              Rectified = b.Field<int?>("Rectified")
                                          }).ToList()
                            .Select(r => new CallStatusWiseGraphData
                            {
                                Y = r.CallDate.ToString("yyyy-MM-dd"),
                                TOTALCALLS = r.TotalCalls.HasValue ? r.TotalCalls.Value : 0,
                                OPEN = r.Open.HasValue ? r.Open.Value : 0,
                                ESCALATED = r.Escalated.HasValue ? r.Escalated.Value : 0,
                                VERIFIED = r.Verified.HasValue ? r.Verified.Value : 0,
                                NOTVERIFIED = r.NonVerified.HasValue ? r.NonVerified.Value : 0,
                                CLOSED = r.Closed.HasValue ? r.Closed.Value : 0,
                                RECTIFIED = r.Rectified.HasValue ? r.Rectified.Value : 0
                            }).ToList();
            }

            return new GraphData { SchemeWiseData = schemeWiseDataList, CallCategoryWiseData = callCategoryWiseDataList, CallStatusWiseData = callStatusWiseDataList };
        }

        protected void ddlDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            int DistrictId = 0;
            int.TryParse(ddlDistrict.SelectedValue, out DistrictId);

            int StateId = 0;
            int.TryParse(ddlState.SelectedValue, out StateId);

            if (DistrictId > 0 && StateId > 0)
            {
                String strQuery = "select ULBMappingId, ULB from [ULBMapping] where StateId = " + StateId + " AND DistrictId = " + DistrictId + "";
                DataTable dt = SQLDBHelper.GetDataInTable(strQuery);
                if (dt != null && dt.Rows.Count > 0)
                {
                    ddlULB.DataTextField = "ULB";
                    ddlULB.DataValueField = "ULBMappingId";
                    ddlULB.DataSource = dt;
                    ddlULB.DataBind();
                }
            }
            ddlULB.Items.Insert(0, "--Select--");
            btnSearch_Click(null, null);
            ddlULB.Focus();
        }

        protected void ddlULB_SelectedIndexChanged(object sender, EventArgs e)
        {
            int ULBId = 0;
            int.TryParse(ddlULB.SelectedValue, out ULBId);

            if (ULBId > 0)
            {
                String strQuery = "Select ZoneMappingId,Zone from ZoneMapping where ULBMappingId = " + ULBId + "";
                DataTable dt = SQLDBHelper.GetDataInTable(strQuery);
                if (dt != null && dt.Rows.Count > 0)
                {
                    ddlZone.DataTextField = "Zone";
                    ddlZone.DataValueField = "ZoneMappingId";
                    ddlZone.DataSource = dt;
                    ddlZone.DataBind();
                }
            }
            ddlZone.Items.Insert(0, "--Select--");
            btnSearch_Click(null, null);
            ddlZone.Focus();
        }
    }
}