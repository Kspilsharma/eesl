﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRM.Models;
using System.IO;
using System.IO.Compression;

namespace CRM
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["UserName"] = null;
                if (UIManager.CurrentUserSession() != null)
                { UIManager.CurrentUserSession().EndSession(); }
                UserName.Focus();
            }
        }

        protected void LoginButton_Click(object sender, EventArgs e)
        {
            try
            {
                UserMaster tUserMaster = new UserMaster();
                tUserMaster.UserName = UserName.Text;
                tUserMaster.Password = Convert.ToString(Password.Text).Replace("'", "''");
                int tResult = tUserMaster.CheckUserLogin();
                if (tResult == 1)
                {
                    Session["hUserName"] = CommonUtils.EncryptString(UserName.Text);
                    Server.Transfer("~/View/frmNavigate.aspx", true);
                }
                else
                {
                    if (tResult == 2)
                    {
                        FailureText.Text = "Your a/c is diactive, Please Contact to Administrator.";
                    }
                    else
                    {
                        FailureText.Text = "Invalid Username / Password.";
                    }

                    Password.Focus();
                }
            }
            catch (Exception ex)
            {
                FailureText.Text = "Application Not Responding : Try Again later";
            }
        }
    }
}