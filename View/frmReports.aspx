﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="frmReports.aspx.cs" Inherits="CRM.View.frmReports" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <link rel="stylesheet" href="../Styles/jquery-ui.css" />
    <script type="text/javascript" src="../Scripts/jquery.min.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#<%= txtFrom.ClientID  %>").datepicker(
            {
                dateFormat: 'dd-mm-yy',
                //minDate: new Date(),
                changeMonth: true,
                numberOfMonths: 1,
                onSelect: function (selected) {
                    var arr = selected.split("-");
                    var dmy = arr[1] + "-" + arr[0] + "-" + arr[2];
                    var dt = new Date(dmy);
                    dt.setDate(dt.getDate());
                    $("#<%= txtTo.ClientID  %>").datepicker("option", "minDate", dt);
                }

            }).datepicker("setDate", new Date());


            $("#<%= txtTo.ClientID  %>").datepicker(
            {
                dateFormat: 'dd-mm-yy',
                //minDate: new Date(),
                changeMonth: true,
                numberOfMonths: 1,
                onSelect: function (selected) {
                    var arr = selected.split("-");
                    var dmy = arr[1] + "-" + arr[0] + "-" + arr[2];
                    var dt = new Date(dmy);
                    dt.setDate(dt.getDate());
                    $("#<%= txtFrom.ClientID  %>").datepicker("option", "maxDate", dt);
                }

            }).datepicker("setDate", new Date());

            $("#<%= ddlReportType.ClientID  %>").change(function () {
                if ($("#<%= ddlReportType.ClientID  %>").val() == 2) {
                    $("#<%= ddlScheme.ClientID  %>").attr("disabled", "disabled");
                    document.getElementById("rfvScheme").style.display = "none";
                    $("#<%= ddlScheme.ClientID  %>").val('--Select--');
                }
                else
                {
                    $("#<%= ddlScheme.ClientID  %>").attr("disabled", false);
                }
            });

        });

        function ValidateField()
        {
            var isValid = false;
            if ($("#<%= ddlReportType.ClientID  %>").val() == 0 && $("#<%= ddlScheme.ClientID  %>").val() == '--Select--') {
                document.getElementById("rfvScheme").style.display = "block";
                $("#<%= ddlScheme.ClientID  %>").focus();
                isValid = false;
            }
            else
            {
                document.getElementById("rfvScheme").style.display = "none";
                isValid = true;
            }
            return isValid;
        }
    </script>



    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="Heading" align="center" style="color: #101010">
                <asp:Label ID="lblPageHeader" runat="server" Text="MIS Report" CssClass="bold"></asp:Label>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
        <tr>
            <td align="left" valign="top" width="10%"></td>
            <td valign="top" width="80%">
                <fieldset class="fieldset">
                    <legend class="Legendheading" align="left">Report</legend>
                    <table cellpadding="1" cellspacing="5px" border="0" style="width: 100%;">
                        <tr>
                            <td width="100%" colspan="2" style="width: 100%" align="left">
                                <asp:Label ID="lblMessage" runat="server" SkinID="baseError" CssClass="failureNotification"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td width="30%" class="tdlabel">Date Range<span class="failureNotification">&nbsp; *</span>
                            </td>
                            <td width="70%" class="tddata" align="left" valign="top">
                                <asp:TextBox type="text" ID="txtFrom" runat="server" Width="80" />&nbsp;To&nbsp;
                                <asp:TextBox type="text" ID="txtTo" runat="server" Width="80" />
                            </td>
                        </tr>
                        <tr>
                            <td width="30%" class="tdlabel">Report Type
                            </td>
                            <td width="70%" class="tddata" align="left" valign="top">
                                <%--<asp:RadioButton ID="rdoInBoundMIS" runat="server" Checked="True" GroupName="ReportType"
                                    Text="InBound MIS" TabIndex="102" />
                                <asp:RadioButton ID="rdoOutBoundMIS" runat="server" Visible="false" GroupName="ReportType" Text="OutBound MIS"
                                    TabIndex="103" />--%>
                                <asp:DropDownList ID="ddlReportType" runat="server" Width="150px">
                                    <asp:ListItem Value="0">InBound MIS</asp:ListItem>
                                    <asp:ListItem Value="2">Productivity Report</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td width="30%" class="tdlabel"> 
                                Scheme
                                <span class="failureNotification">&nbsp; *</span>
                            </td>
                            <td width="70%" class="tddata" align="left" valign="top">
                                <asp:DropDownList ID="ddlScheme" runat="server" Width="150px" />
                                <label id="rfvScheme" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Select Scheme of Call.</label>
                            </td>
                        </tr>
                        <tr>
                            <td width="30%" class="tddata"></td>
                            <td width="70%" class="tddata" align="left" valign="top">
                                <asp:Button ID="btnSave" OnClientClick="return ValidateField();" runat="server" TabIndex="107" Text="Submit"
                                    ValidationGroup="Submit" OnClick="btnSave_Click" />
                                <asp:Button ID="btnReset" runat="server" TabIndex="108"
                                    Text="Reset" CausesValidation="False" OnClick="btnReset_Click" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
            <td align="left" valign="top" width="10%"></td>
        </tr>

    </table>
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>

</asp:Content>
