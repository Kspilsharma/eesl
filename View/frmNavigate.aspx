﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmNavigate.aspx.cs" Inherits="CRM.View.frmNavigate" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
    <div style="height: 400px">
        <center>
            <table width="100%">
                <tr>
                    <td align="center" valign="middle">
                        <div id="divSpinner" align="center" valign="middle" runat="server" style="visibility: visible;
                            vertical-align: middle; color: #0D6F93">
                            <div>
                                <img src="../Images/spinner.gif" alt="" height="100" width="100" />
                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </center>
    </div>
    </form>
</body>
</html>
