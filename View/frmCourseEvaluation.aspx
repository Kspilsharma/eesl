﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WithoutOuth.Master" AutoEventWireup="true" CodeBehind="frmCourseEvaluation.aspx.cs" Inherits="CRM.View.frmCourseEvaluation1" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.min.js"></script>
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $("#<%= txtTrainingFrom.ClientID  %>").datepicker(
            {
                dateFormat: 'dd-mm-yy',
                //minDate: new Date(),
                changeMonth: true,
                numberOfMonths: 1,
                onSelect: function (selected) {
                    var arr = selected.split("-");
                    var dmy = arr[1] + "-" + arr[0] + "-" + arr[2];
                    var dt = new Date(dmy);
                    dt.setDate(dt.getDate());
                    $("#<%= txtTrainingTo.ClientID  %>").datepicker("option", "minDate", dt);
                }

            }).datepicker("setDate", new Date());


            $("#<%= txtTrainingTo.ClientID  %>").datepicker(
            {
                dateFormat: 'dd-mm-yy',
                //minDate: new Date(),
                changeMonth: true,
                numberOfMonths: 1,
                onSelect: function (selected) {
                    var arr = selected.split("-");
                    var dmy = arr[1] + "-" + arr[0] + "-" + arr[2];
                    var dt = new Date(dmy);
                    dt.setDate(dt.getDate());
                    $("#<%= txtTrainingFrom.ClientID  %>").datepicker("option", "maxDate", dt);
                }

            }).datepicker("setDate", new Date());
        });
    </script>

    <script type="text/javascript">

        var pageUrl = '<%=ResolveUrl("~/View/frmCourseEvaluation.aspx")%>'
        function PopulateCities() {

            if ($('#<%=ddlState.ClientID%>').val() == "--Select--") {
                $('#<%=ddlDistrict.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            }
            else {
                $('#<%=ddlDistrict.ClientID %>').empty().append('<option selected="selected" value="0">Loading...</option>');
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/PopulateCities',
                    data: '{stateId: ' + $('#<%=ddlState.ClientID%>').val() + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnCitiesPopulated,
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
        }

        function OnCitiesPopulated(response) {
            PopulateControl(response.d, $("#<%=ddlDistrict.ClientID %>"));
        }

        function PopulateControl(list, control) {
            if (list.length > 0) {
                control.empty().append('<option selected="selected" value="0">--Select--</option>');
                $.each(list, function () {
                    control.append($("<option></option>").val(this['Value']).html(this['Text']));
                });
            }
            else {
                $('#<%=ddlDistrict.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            }
        }

        function DistrictChange() {

            if ($('#<%=ddlDistrict.ClientID%>').find('option:selected').text() == "Other") {
                $('#<%=trDistrictName.ClientID%>').show();
            }
            else {
                $('#<%=trDistrictName.ClientID%>').hide();
            }
        }

    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#<% =ddlDistrict.ClientID %>').change(function (e) {
                var districId = $('#<% =ddlDistrict.ClientID %>').val();
                $('#<% =hdnDistricId.ClientID %>').attr('value', districId);
            });
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="Heading" align="center" style="background-color: #808080; color: #FFFFFF">
                <asp:Label ID="lblPageHeader" runat="server" Text="Course Evaluation Form" CssClass="bold"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left">&nbsp;
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="lblMessage" runat="server" Font-Bold="False" CssClass="failureNotification"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left">&nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td align="right">
                <asp:LinkButton ID="btnFAQ" runat="server" OnClick="btnFAQ_Click" Text="FAQ" Visible="false" />
            </td>
        </tr>
    </table>

    <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
        <tr>
            <td align="left" valign="top" width="95%">
                <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
                    <tr>
                        <td align="left" valign="top">
                            <fieldset>
                                <legend></legend>
                                <table id="tblCourseDetail" runat="server" cellpadding="0" cellspacing="3" style="width: 100%; margin: 0px; padding: 0px;">
                                    <tr>
                                        <td colspan="4" style="width: 100%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%" class="tdlabel">Mobile No. <span style="color: red">*</span></td>
                                        <td style="width: 35%" class="tdlabel">

                                            <asp:TextBox ID="txtMobileNo" runat="server" Width="80%" MaxLength="15" TabIndex="101"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1No" runat="server" ControlToValidate="txtMobileNo" ErrorMessage="Enter Mobile." ForeColor="Maroon" ValidationGroup="Save" Font-Bold="True" Font-Size="X-Small"></asp:RequiredFieldValidator>
                                            <asp:FilteredTextBoxExtender ID="fteMobileNo" runat="server" Enabled="true" TargetControlID="txtMobileNo" FilterType="Custom,Numbers" />
                                            <br />

                                        </td>

                                        <td style="width: 15%" class="tdlabel">Training Date</td>
                                        <td style="width: 35%" class="tdlabel">&nbsp;<asp:TextBox type="text" ID="txtTrainingFrom" runat="server" Width="100px" TabIndex="105" />&nbsp;To
                                <asp:TextBox type="text" ID="txtTrainingTo" runat="server" Width="100px" TabIndex="106" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="width: 100%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%" class="tdlabel">Name <span style="color: red">*</span></td>
                                        <td style="width: 35%" class="tdlabel">

                                            <asp:TextBox ID="txtNameOfParticipant" runat="server" Width="80%" MaxLength="50" TabIndex="102"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvNameOfParticipant" runat="server" ControlToValidate="txtNameOfParticipant" ErrorMessage="Enter Name." ForeColor="Maroon" ValidationGroup="Save" Font-Bold="True" Font-Size="X-Small"></asp:RequiredFieldValidator>

                                        </td>
                                        <td style="width: 15%" class="tdlabel">Course</td>
                                        <td style="width: 35%" class="tdlabel">
                                            <asp:DropDownList ID="ddlCourseName" runat="server" Width="80%" TabIndex="107" />
                                            <br />
                                            <asp:RequiredFieldValidator ID="rfvCourseName" ForeColor="Maroon" TabIndex="107" ControlToValidate="ddlCourseName" InitialValue="--Select--" runat="server" ErrorMessage="Select Course Name." Display="Dynamic" ValidationGroup="Save" Font-Bold="True" Font-Size="X-Small"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="width: 100%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%" class="tdlabel">Phone No.</td>
                                        <td style="width: 35%" class="tdlabel">
                                            <asp:TextBox ID="txtTelephoneNo" runat="server" Width="80%" MaxLength="15" TabIndex="103"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="fteTelephoneNo" runat="server" Enabled="true" TargetControlID="txtTelephoneNo" FilterType="Custom,Numbers" FilterMode="ValidChars" ValidChars="-" />
                                        </td>
                                        <td style="width: 15%" class="tdlabel">Location(State/District)</td>
                                        <td style="width: 35%" class="tdlabel">
                                            <asp:DropDownList ID="ddlState" runat="server" Width="45%" onchange="PopulateCities();" TabIndex="108" />
                                            &nbsp;                                           
                                            <asp:DropDownList ID="ddlDistrict" runat="server" Width="50%" onchange="DistrictChange();" TabIndex="109" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="width: 100%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr id="trDistrictName" runat="server" class="tdlabel">
                                        <td style="width: 15%" class="tdlabel">&nbsp;</td>
                                        <td style="width: 35%" class="tdlabel">&nbsp;</td>
                                        <td style="width: 15%" class="tdlabel">District Name</td>
                                        <td style="width: 35%" class="tdlabel">
                                            <asp:TextBox ID="txtDistrictName" runat="server" Width="100%" MaxLength="100"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="width: 100%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%" class="tdlabel">Email</td>
                                        <td style="width: 35%" class="tdlabel">
                                            <asp:TextBox ID="txtEmail" runat="server" Width="80%" MaxLength="50" TabIndex="104"></asp:TextBox>
                                            <asp:RegularExpressionValidator ID="revEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="Invalid Email !" Font-Bold="True" Font-Size="X-Small" ForeColor="Maroon" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ValidationGroup="Save"></asp:RegularExpressionValidator>
                                        </td>
                                        <td style="width: 15%" class="tdlabel">Faculty</td>
                                        <td style="width: 35%" class="tdlabel">
                                            <asp:TextBox ID="txtFaculty" runat="server" Width="80%" MaxLength="50" TabIndex="110"></asp:TextBox>
                                        </td>
                                    </tr>

                                </table>
                            </fieldset>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top">
                            <fieldset>
                                <legend></legend>
                                <table id="tblQuestions" runat="server" cellpadding="0" cellspacing="3" style="width: 100%; margin: 0px; padding: 0px;">
                                    <tr>
                                        <td style="width: 60%" class="tdlabel">Relevant of topics with the overall aim of the course</td>
                                        <td style="width: 40%" class="tdlabel">
                                            <asp:DropDownList ID="ddlQ1" runat="server" TabIndex="111">
                                                <asp:ListItem Selected="True">Highly Relevant</asp:ListItem>
                                                <asp:ListItem>Relevant</asp:ListItem>
                                                <asp:ListItem>Very Relevant</asp:ListItem>
                                                <asp:ListItem>Not Relevant</asp:ListItem>
                                            </asp:DropDownList>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%">&nbsp;</td>
                                        <td style="width: 40%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%" class="tdlabel">Effectiveness and structure of the programme design to facilitate learning</td>
                                        <td style="width: 40%" class="tdlabel">
                                            <asp:DropDownList ID="ddlQ2" runat="server" TabIndex="112">
                                                <asp:ListItem Selected="True">Highly Relevant</asp:ListItem>
                                                <asp:ListItem>Relevant</asp:ListItem>
                                                <asp:ListItem>Very Relevant</asp:ListItem>
                                                <asp:ListItem>Not Relevant</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%">&nbsp;</td>
                                        <td style="width: 40%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%" class="tdlabel">Effectiveness of the course content to apply theory to practice</td>
                                        <td style="width: 40%" class="tdlabel">
                                            <asp:DropDownList ID="ddlQ3" runat="server" TabIndex="113">
                                                <asp:ListItem Selected="True">Excellent</asp:ListItem>
                                                <asp:ListItem>Good</asp:ListItem>
                                                <asp:ListItem>Very Good</asp:ListItem>
                                                <asp:ListItem>Average</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%">&nbsp;</td>
                                        <td style="width: 40%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%" class="tdlabel">Course Material provided for Participants</td>
                                        <td style="width: 40%" class="tdlabel">
                                            <asp:DropDownList ID="ddlQ4" runat="server" TabIndex="114">
                                                <asp:ListItem Selected="True">Excellent</asp:ListItem>
                                                <asp:ListItem>Good</asp:ListItem>
                                                <asp:ListItem>Very Good</asp:ListItem>
                                                <asp:ListItem>Average</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%">&nbsp;</td>
                                        <td style="width: 40%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%" class="tdlabel">Presentation of course content by Faculty</td>
                                        <td style="width: 40%" class="tdlabel">
                                            <asp:DropDownList ID="ddlQ5" runat="server" TabIndex="115">
                                                <asp:ListItem Selected="True">Excellent</asp:ListItem>
                                                <asp:ListItem>Good</asp:ListItem>
                                                <asp:ListItem>Very Good</asp:ListItem>
                                                <asp:ListItem>Average</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%">&nbsp;</td>
                                        <td style="width: 40%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%" class="tdlabel">Level of interaction between Faculty and Participant</td>
                                        <td style="width: 40%" class="tdlabel">
                                            <asp:DropDownList ID="ddlQ6" runat="server" TabIndex="116">
                                                <asp:ListItem Selected="True">Excellent</asp:ListItem>
                                                <asp:ListItem>Good</asp:ListItem>
                                                <asp:ListItem>Very Good</asp:ListItem>
                                                <asp:ListItem>Average</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%">&nbsp;</td>
                                        <td style="width: 40%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%" class="tdlabel">Facilities provided for Participants</td>
                                        <td style="width: 40%" class="tdlabel">
                                            <asp:DropDownList ID="ddlQ7" runat="server" TabIndex="117">
                                                <asp:ListItem Selected="True">Excellent</asp:ListItem>
                                                <asp:ListItem>Good</asp:ListItem>
                                                <asp:ListItem>Very Good</asp:ListItem>
                                                <asp:ListItem>Average</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%">&nbsp;</td>
                                        <td style="width: 40%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%" class="tdlabel">Overall grading of the Programme</td>
                                        <td style="width: 40%" class="tdlabel">
                                            <asp:DropDownList ID="ddlQ8" runat="server" TabIndex="118">
                                                <asp:ListItem Selected="True">Excellent</asp:ListItem>
                                                <asp:ListItem>Good</asp:ListItem>
                                                <asp:ListItem>Very Good</asp:ListItem>
                                                <asp:ListItem>Average</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%">&nbsp;</td>
                                        <td style="width: 40%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="tdlabel" colspan="2">What were the strengths of the Programme?</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtQuestion9" runat="server" Width="100%" TextMode="MultiLine" MaxLength="1000" TabIndex="119"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="tdlabel" colspan="2">Suggestions/recommendations, if any, for further improvement of this Programme?</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtQuestion10" runat="server" Width="100%" TextMode="MultiLine" MaxLength="1000" TabIndex="120"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%" class="tdlabel">Would you recommend this or similar Programme/Course conducted by KVIC to anyone seeking employment/selfemployment/entrepreneurship?</td>
                                        <td style="width: 40%" class="tdlabel">
                                            <asp:DropDownList ID="ddlQ11" runat="server" TabIndex="121">
                                                <asp:ListItem Selected="True">Yes</asp:ListItem>
                                                <asp:ListItem>No</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td class="tdlabel" colspan="2"></td>
                                    </tr>

                                    <tr>
                                        <td style="width: 60%">&nbsp;</td>
                                        <td style="width: 40%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%" class="tdlabel">Would you like to be intimated about courses conducted by KVIC in the future ?</td>
                                        <td style="width: 40%" class="tdlabel">
                                            <asp:DropDownList ID="ddlQ12" runat="server" TabIndex="122">
                                                <asp:ListItem Selected="True">Yes</asp:ListItem>
                                                <asp:ListItem>No</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%">&nbsp;</td>
                                        <td style="width: 40%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="tdlabel" colspan="2">Currently what you are doing after getting training from KVIC?</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtQuestion13" runat="server" Width="100%" TextMode="MultiLine" MaxLength="1000" TabIndex="123"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 60%">&nbsp;</td>
                                        <td style="width: 40%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td class="tdlabel" colspan="2">From where you took financial help to start ?</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:TextBox ID="txtQuestion14" runat="server" Width="100%" TextMode="MultiLine" MaxLength="1000" TabIndex="124"></asp:TextBox>
                                        </td>
                                    </tr>

                                </table>
                            </fieldset>
                        </td>
                    </tr>

                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center" valign="top" width="100%">&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center" valign="top" width="100%">
                <asp:Button ID="btnSave" runat="server" Text="Save" ValidationGroup="Save" AccessKey="S" UseSubmitBehavior="False" OnClick="btnSave_Click" TabIndex="125" />
                &nbsp;<asp:Button ID="btnReset" runat="server" Text="Reset" UseSubmitBehavior="false" OnClick="btnReset_Click" TabIndex="126" Visible="false" />
            </td>
        </tr>
    </table>


    <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
        <tr>
            <td>
                <asp:HiddenField ID="hdnCallEntryId" runat="server" Value="0" />
                <asp:HiddenField ID="hdnAgent" runat="server" />

                <asp:HiddenField ID="hdnDistricId" runat="server" Value="0" />

                &nbsp;
            </td>
        </tr>
        <tr>
            <td>&nbsp;&nbsp;</td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="gvCourseEvaluation" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="EvaluationId" EmptyDataText="No Record Found" ForeColor="#333333" GridLines="None" PageSize="50" TabIndex="106" Width="100%" OnPageIndexChanging="gvCourseEvaluation_PageIndexChanging">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:TemplateField HeaderText="Sr.">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EvaluationId" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="gvlblEvaluationId" runat="server" Text='<%# Bind("EvaluationId") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Unique ID">
                            <ItemTemplate>
                                <asp:Label ID="gvlblUID" runat="server" Text='<%# Bind("UID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Training From">
                            <ItemTemplate>
                                <asp:Label ID="gvlblDateOfTrainingFrom" runat="server" Text='<%# Bind("DateOfTrainingFrom") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Training To">
                            <ItemTemplate>
                                <asp:Label ID="gvlblDateOfTrainingTo" runat="server" Text='<%# Bind("DateOfTrainingTo") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Course Name">
                            <ItemTemplate>
                                <asp:Label ID="gvlblCourseName" runat="server" Text='<%# Bind("CourseName") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Question1" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="gvlblQuestion1" runat="server" Text='<%# Bind("Question1") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Question2" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="gvlblQuestion2" runat="server" Text='<%# Bind("Question2") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Question3" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="gvlblQuestion3" runat="server" Text='<%# Bind("Question3") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Question4" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="gvlblQuestion4" runat="server" Text='<%# Bind("Question4") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Question5" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="gvlblQuestion5" runat="server" Text='<%# Bind("Question5") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Question6" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="gvlblQuestion6" runat="server" Text='<%# Bind("Question6") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Question7" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="gvlblQuestion7" runat="server" Text='<%# Bind("Question7") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Question8" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="gvlblQuestion8" runat="server" Text='<%# Bind("Question8") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Question9" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="gvlblQuestion9" runat="server" Text='<%# Bind("Question9") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Question10" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="gvlblQuestion10" runat="server" Text='<%# Bind("Question10") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Question11" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="gvlblQuestion11" runat="server" Text='<%# Bind("Question11") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Question12" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="gvlblQuestion12" runat="server" Text='<%# Bind("Question12") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit" Visible="false">
                            <ItemTemplate>
                                <asp:Button ID="btnEdit" runat="server" Text="Edit" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#EFF3FB" HorizontalAlign="Center" VerticalAlign="Top" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
                &nbsp;&nbsp;
            </td>
        </tr>
    </table>

    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
</asp:Content>

