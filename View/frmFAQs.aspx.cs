﻿using CRM.Models;
using System;
using System.Configuration;
using System.Data;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CRM.View
{
    public partial class frmFAQs : System.Web.UI.Page
    {
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();
        string QSSchemeId = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                QSSchemeId = Convert.ToString(Request.QueryString["SID"]);
                SetUI(); 
            }
        }

        protected void SetUI()
        {
            SQLDBHelper.PopulateDropDownList(ddlScheme, "Scheme", "Scheme", "SchemeId", false, "");
            FillGrid();
        }

        protected void FillGrid()
        {
            try
            {
                FAQ tFAQ = new FAQ();
                DataSet ds = new DataSet();
                tFAQ.IsActive = true;
                if (!string.IsNullOrEmpty(QSSchemeId))
                {
                    if (QSSchemeId == "0")
                    {
                        tFAQ.SchemeId = 0;
                        ddlScheme.Visible = true;
                    }
                    else
                    {
                        tFAQ.SchemeId = Convert.ToInt64(QSSchemeId);
                        ddlScheme.Visible = false;
                    }
                        
                }
                else
                {
                    tFAQ.SchemeId = ddlScheme.SelectedIndex == 0 ? 0 : Convert.ToInt64(ddlScheme.SelectedItem.Value);
                    ddlScheme.Visible = true;
                }
                if (!string.IsNullOrEmpty(txtKeyword.Text))
                {
                    tFAQ.Keyword = Convert.ToString(txtKeyword.Text.Trim().ToLower());    
                }
                
                ds = tFAQ.Search();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvFAQs.DataSource = ds.Tables[0];
                    gvFAQs.DataBind();
                    gvFAQs.Visible = true;
                }
                else
                {
                    gvFAQs.Visible = false;
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void ddlScheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void txtKeyword_TextChanged(object sender, EventArgs e)
        {
            try
            {
                FillGrid();
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void gvFAQs_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvFAQs.PageIndex = e.NewPageIndex;
            FillGrid();
        }
    }
}