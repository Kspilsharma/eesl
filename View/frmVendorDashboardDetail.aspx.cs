﻿using CRM.Models;
using System;
using System.Configuration;
using System.Data;
using System.Reflection;
using System.Web.UI.WebControls;
using System.Web;


namespace EESL.View
{
    public partial class frmVendorDashboardDetail : System.Web.UI.Page
    {
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();
        DateTime date = new DateTime();
        string statusId = string.Empty;
        string callCategoryId = string.Empty;
        string schemeId = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if ((UIManager.CurrentUserSession().RoleName.ToUpper() == "VENDOR"))
                {
                    FillDashboardDetail();
                }
            }
        }

        protected void FillDashboardDetail()
        {
            try
            {
                if (Convert.ToString(Request.QueryString["Date"]) != null)
                {
                    date = Convert.ToDateTime(Request.QueryString["Date"]);
                }
                if (Convert.ToString(Request.QueryString["StatusId"]) != null)
                {
                    statusId = Convert.ToString(Request.QueryString["StatusId"]);
                }
                else
                {
                    statusId = "0";
                }
                if (Convert.ToString(Request.QueryString["CallCategoryId"]) != null)
                {
                    callCategoryId = Convert.ToString(Request.QueryString["CallCategoryId"]);
                }
                else
                {
                    callCategoryId = "0";
                }
                if (Convert.ToString(Request.QueryString["SchemeId"]) != null)
                {
                    schemeId = Convert.ToString(Request.QueryString["SchemeId"]);
                }
                else
                {
                    schemeId = "0";
                }

                Dashboard tDashboard = new Dashboard();
                tDashboard.Date = date;
                tDashboard.StatusOfCallId = Convert.ToInt32(statusId);
                tDashboard.CategoryOfCallId = Convert.ToInt32(callCategoryId);
                tDashboard.SchemeId = Convert.ToInt32(schemeId);
                tDashboard.VendorId = Convert.ToInt32(SQLDBHelper.getFieldValueByCondition("UserMaster", "VendorId", "UserId", Convert.ToString(UIManager.CurrentUserSession().UserId)));
                DataSet ds = tDashboard.DashboardDetailVendor_Select();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvDashboardDetail.DataSource = ds.Tables[0];
                    gvDashboardDetail.DataBind();
                    gvDashboardDetail.Visible = true;
                }
                else
                {
                    gvDashboardDetail.Visible = false;
                }

            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void ExportDashboardDetail()
        {
            try
            {
                string sFileName = "InBoundMISSummary-" + System.DateTime.Now.Date + ".xls";
                if (Convert.ToString(Request.QueryString["Date"]) != null)
                {
                    date = Convert.ToDateTime(Request.QueryString["Date"]);
                }
                if (Convert.ToString(Request.QueryString["StatusId"]) != null)
                {
                    statusId = Convert.ToString(Request.QueryString["StatusId"]);
                }
                else
                {
                    statusId = "0";
                }
                if (Convert.ToString(Request.QueryString["CallCategoryId"]) != null)
                {
                    callCategoryId = Convert.ToString(Request.QueryString["CallCategoryId"]);
                }
                else
                {
                    callCategoryId = "0";
                }
                if (Convert.ToString(Request.QueryString["SchemeId"]) != null)
                {
                    schemeId = Convert.ToString(Request.QueryString["SchemeId"]);
                }
                else
                {
                    schemeId = "0";
                }
                Dashboard tDashboard = new Dashboard();
                tDashboard.Date = date;
                tDashboard.StatusOfCallId = Convert.ToInt32(statusId);
                tDashboard.CategoryOfCallId = Convert.ToInt32(callCategoryId);
                tDashboard.SchemeId = Convert.ToInt32(schemeId);
                tDashboard.VendorId = Convert.ToInt32(SQLDBHelper.getFieldValueByCondition("UserMaster", "VendorId", "UserId", Convert.ToString(UIManager.CurrentUserSession().UserId)));

                DataSet ds = tDashboard.DashboardDetailVendor_Select();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ExportDatatableToExcel(ds.Tables[0], sFileName);
                }

            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        public void ExportDatatableToExcel(DataTable table, string sFileName)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + sFileName);

            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR><BR><BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> <TR>");
            //am getting my grid's column headers
            int columnscount = table.Columns.Count;

            for (int j = 0; j < columnscount; j++)
            {      //write in new column
                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(table.Columns[j].ColumnName.ToString());
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (DataRow row in table.Rows)
            {//write in new row
                HttpContext.Current.Response.Write("<TR>");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    HttpContext.Current.Response.Write("<Td>");
                    HttpContext.Current.Response.Write(row[i].ToString());
                    HttpContext.Current.Response.Write("</Td>");
                }

                HttpContext.Current.Response.Write("</TR>");
            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }
        protected void imgbtnBack_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            Response.Redirect("~/View/frmVendorDashboard.aspx", false);
        }

        protected void imgbtnExportExcel_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            ExportDashboardDetail();
        }

        protected void gvDashboardDetail_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvDashboardDetail.PageIndex = e.NewPageIndex;
            FillDashboardDetail();
        }

    }
}