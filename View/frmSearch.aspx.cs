﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRM.Models;
using System.Reflection;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace CRM.View
{
    public partial class frmSearch : System.Web.UI.Page
    {
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                PopulateUI();
            }

            if (Session["IsSearchPage"] != null)
            {
                if (Session["IsSearchPage"].ToString() == "YES")
                {
                    GetPageData();
                    Session["IsSearchPage"] = null;
                    btnSearch_Click(null, null);
                }
            }
        }

        private void PopulateUI()
        {
            txtNameOfCaller.Text = "";
            txtMobileNo.Text = "";
            txtFrom.Text = "";
            txtTo.Text = "";
            UserMaster userMaster = new UserMaster();
            userMaster.UserId= UIManager.CurrentUserSession().UserId;
            DataSet userStates = userMaster.GetUserStates();
            ddlState.DataTextField = "State";
            ddlState.DataValueField = "StateId";
            ddlState.DataSource = userStates;
            ddlState.DataBind();
            ddlState.Items.Insert(0, "--Select--");
            //SQLDBHelper.PopulateDropDownList(ddlState, "State", "State", "StateId", false, "");
            SQLDBHelper.PopulateDropDownList(ddlScheme, "Scheme", "Scheme", "SchemeId", false, "");
            SQLDBHelper.PopulateDropDownList(ddlSource, "Source", "Source", "SourceId", false, "");
            SQLDBHelper.PopulateDropDownList(ddlCategoryOfCall, "CallCategory", "CallCategory", "CallCategoryId", false, "");
            SQLDBHelper.PopulateDropDownList(ddlLanguage, "Languages", "Language", "LanguageId", false, "");
            SQLDBHelper.PopulateDropDownList(ddlStatusOfCall, "StatusOfCall", "StatusOfCall", "StatusOfCallId", false, "");
            SQLDBHelper.PopulateDropDownList(ddlComplaintHours, "ComplaintHours", "ComplaintHours", "ComplaintHours", false, "");
            SQLDBHelper.PopulateDropDownList(ddlSLComplaintFrom, "ComplaintFrom", "ComplaintFrom", "ComplaintFromId", false, "");
            ddlDistrict.Items.Insert(0, "--Select--");
            ddlSLULB.Items.Insert(0, "--Select--");
            ddlSLZone.Items.Insert(0, "--Select--");
            ddlSLComplaintFrom.Items.Insert(0, "--Select--");
            divStreetOptions.Visible = false;

        }

        protected void FillGrid()
        {
            try
            {
                Call tCall = new Call();
                DataSet ds = new DataSet();
                if (!String.IsNullOrEmpty(txtUID.Text))
                {
                    tCall.UID = Convert.ToString(txtUID.Text.Trim());
                }
                if (!String.IsNullOrEmpty(txtNameOfCaller.Text))
                {
                    tCall.CallerName = Convert.ToString(txtNameOfCaller.Text.Trim());
                }
                if (!String.IsNullOrEmpty(txtMobileNo.Text))
                {
                    tCall.CallerNumber = Convert.ToString(txtMobileNo.Text.Trim());
                }
                if (ddlSource.SelectedIndex > 0)
                {
                    tCall.SourceId = Convert.ToInt32(ddlSource.SelectedItem.Value);
                }
                if (ddlScheme.SelectedIndex > 0)
                {
                    tCall.SchemeId = Convert.ToInt32(ddlScheme.SelectedItem.Value);
                    if(tCall.SchemeId==1)
                    {
                        if (ddlSLULB.SelectedIndex > 0)
                        {
                            tCall.SL_ULBId = Convert.ToInt32(ddlSLULB.SelectedItem.Value);
                        }
                        if (ddlSLZone.SelectedIndex > 0)
                        {
                            tCall.SL_ZoneId = Convert.ToInt32(ddlSLZone.SelectedItem.Value);
                        }
                        if (!String.IsNullOrEmpty(txtSLWardNo.Text))
                        {
                            tCall.SL_WardNo = Convert.ToString(txtSLWardNo.Text.Trim());
                        }
                        if (ddlSLComplaintFrom.SelectedIndex > 0)
                        {
                            tCall.SL_ComplaintFromId = Convert.ToInt32(ddlSLComplaintFrom.SelectedItem.Value);
                        }
                    }
                }
                if (ddlStatusOfCall.SelectedIndex > 0)
                {
                    tCall.StatusOfCallId = Convert.ToInt32(ddlStatusOfCall.SelectedItem.Value);
                }
                if (ddlCategoryOfCall.SelectedIndex > 0)
                {
                    tCall.CategoryOfCallId = Convert.ToInt32(ddlCategoryOfCall.SelectedItem.Value);
                }
                if (ddlLanguage.SelectedIndex > 0)
                {
                    tCall.LanguageId = Convert.ToInt32(ddlLanguage.SelectedItem.Value);
                }
                if (ddlState.SelectedIndex > 0)
                {
                    tCall.StateId = Convert.ToInt32(ddlState.SelectedItem.Value);
                }
                if (ddlDistrict.SelectedIndex > 0)
                {
                    tCall.DistrictId = Convert.ToInt32(ddlDistrict.SelectedItem.Value);
                }

                if (!String.IsNullOrEmpty(txtFrom.Text))
                {
                    tCall.DateFrom = Convert.ToDateTime(txtFrom.Text.Trim());
                }
                if (!String.IsNullOrEmpty(txtTo.Text))
                {
                    tCall.DateTo = Convert.ToDateTime(txtTo.Text.Trim());
                }
                if (ddlComplaintHours.SelectedIndex > 0)
                {
                    tCall.ComplaintHours = Convert.ToInt32(ddlComplaintHours.SelectedItem.Value);
                }
                if (!String.IsNullOrEmpty(txtKeyword.Text))
                {
                    tCall.Keyword = Convert.ToString(txtKeyword.Text.Trim());
                }

                tCall.DateSearchBy = ddlDateSearchBy.SelectedValue;

                tCall.UserId = UIManager.CurrentUserSession().UserId;

                ds = tCall.Call_Search();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvCalllDetail.DataSource = ds.Tables[0];
                    lblRecordsFound.Text = ds.Tables[0].Rows.Count.ToString();
                    gvCalllDetail.DataBind();
                    gvCalllDetail.Visible = true;
                }
                else
                {
                    gvCalllDetail.Visible = false;
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void ExportSearchDetail()
        {
            //try
            //{
                string sFileName = "InBoundMISSummary-" + System.DateTime.Now.Date + ".xls";
                Call tCall = new Call();
                DataSet ds = new DataSet();
                if (!String.IsNullOrEmpty(txtUID.Text))
                {
                    tCall.UID = Convert.ToString(txtUID.Text.Trim());
                }
                if (!String.IsNullOrEmpty(txtNameOfCaller.Text))
                {
                    tCall.CallerName = Convert.ToString(txtNameOfCaller.Text.Trim());
                }
                if (!String.IsNullOrEmpty(txtMobileNo.Text))
                {
                    tCall.CallerNumber = Convert.ToString(txtMobileNo.Text.Trim());
                }
                if (ddlSource.SelectedIndex > 0)
                {
                    tCall.SourceId = Convert.ToInt32(ddlSource.SelectedItem.Value);
                }
                if (ddlScheme.SelectedIndex > 0)
                {
                    tCall.SchemeId = Convert.ToInt32(ddlScheme.SelectedItem.Value);
                }
                if (ddlStatusOfCall.SelectedIndex > 0)
                {
                    tCall.StatusOfCallId = Convert.ToInt32(ddlStatusOfCall.SelectedItem.Value);
                }
                if (ddlCategoryOfCall.SelectedIndex > 0)
                {
                    tCall.CategoryOfCallId = Convert.ToInt32(ddlCategoryOfCall.SelectedItem.Value);
                }
                if (ddlLanguage.SelectedIndex > 0)
                {
                    tCall.LanguageId = Convert.ToInt32(ddlLanguage.SelectedItem.Value);
                }
                if (ddlState.SelectedIndex > 0)
                {
                    tCall.StateId = Convert.ToInt32(ddlState.SelectedItem.Value);
                }
                if (ddlDistrict.SelectedIndex > 0)
                {
                    tCall.DistrictId = Convert.ToInt32(ddlDistrict.SelectedItem.Value);
                }

                if (!String.IsNullOrEmpty(txtFrom.Text))
                {
                    tCall.DateFrom = Convert.ToDateTime(txtFrom.Text.Trim());
                }
                if (!String.IsNullOrEmpty(txtTo.Text))
                {
                    tCall.DateTo = Convert.ToDateTime(txtTo.Text.Trim());
                }
                if (ddlComplaintHours.SelectedIndex > 0)
                {
                    tCall.ComplaintHours = Convert.ToInt32(ddlComplaintHours.SelectedItem.Value);
                }
                if (!String.IsNullOrEmpty(txtKeyword.Text))
                {
                    tCall.Keyword = Convert.ToString(txtKeyword.Text.Trim());
                }

                tCall.DateSearchBy = ddlDateSearchBy.SelectedValue;
                tCall.UserId = UIManager.CurrentUserSession().UserId;

                ds = tCall.Call_Search();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ExportDatatableToExcel(ds.Tables[0], sFileName);
                }
               
            //}
            //catch (Exception Ex)
            //{
            //    ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
            //    Response.Redirect("~/frmApplicationError.aspx", false);
            //}
        }

        public void ExportDatatableToExcel(DataTable table, string sFileName)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + sFileName);

            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR><BR><BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> <TR>");
            //am getting my grid's column headers
            int columnscount = table.Columns.Count;

            for (int j = 0; j < columnscount; j++)
            {      //write in new column
                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(table.Columns[j].ColumnName.ToString());
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (DataRow row in table.Rows)
            {//write in new row
                HttpContext.Current.Response.Write("<TR>");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    HttpContext.Current.Response.Write("<Td>");
                    HttpContext.Current.Response.Write(row[i].ToString());
                    HttpContext.Current.Response.Write("</Td>");
                }

                HttpContext.Current.Response.Write("</TR>");
            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlDistrict.Items.Clear();
            if (ddlState.SelectedIndex != 0)
            {
                SQLDBHelper.PopulateDropDownList(ddlDistrict, "District", "District", "DistrictId", true, "StateId=" + ddlState.SelectedValue);
            }
            else
            {
                ddlDistrict.Items.Insert(0, "--Select--");
            }
            ddlDistrict.Focus();

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {

                FillGrid();

            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                //Response.Redirect("/frmApplicationError.aspx", false);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                PopulateUI();
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                //Response.Redirect("/frmApplicationError.aspx", false);
            }
        }

        protected void btnHistory_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                Label gvlblCallId = (Label)gvr.FindControl("gvlblCallId");
                int callId = Convert.ToInt32(gvlblCallId.Text.Trim());
                FillCallDetailHistoryGrid(callId);
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void FillCallDetailHistoryGrid(int callId)
        {
            try
            {
                Call tCall = new Call();
                DataSet ds = new DataSet();
                tCall.CallId = callId;
                ds = tCall.GetCallHistory();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvCallDetailHistory.DataSource = ds.Tables[0];
                    gvCallDetailHistory.DataBind();
                    gvCallDetailHistory.Visible = true;
                    modelHistory.Show();
                }
                else
                {
                    gvCallDetailHistory.Visible = false;
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void btnResponse_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                Label gvlblMobileNo = (Label)gvr.FindControl("gvlblCallerNo");
                Label gvlblUID = (Label)gvr.FindControl("gvlblUID");
                string mobileNo = Convert.ToString(gvlblMobileNo.Text.Trim());
                string uId = Convert.ToString(gvlblUID.Text.Trim());
                Session["IsBackToPage"] = "YES";
                SetPageData();
                Response.Redirect("~/View/frmInBoundEntry.aspx?cid=" + mobileNo + "&UniqueID=" + uId, false);
                
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);

            }   
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {

        }

        protected void imgbtnExportExcel_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            ExportSearchDetail();
        }

        protected void gvCalllDetail_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvCalllDetail.PageIndex = e.NewPageIndex;
            FillGrid();
        }

        protected void ddlDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            string stateId, districtId;
            stateId = ddlState.SelectedItem.Value;
            districtId = ddlDistrict.SelectedItem.Value;

            String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            String strQuery = "select ULBMappingId, ULB from [ULBMapping] where StateId = @stateId AND DistrictId = @DistrictId";
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@StateId", stateId);
                    cmd.Parameters.AddWithValue("@DistrictId", districtId);
                    cmd.CommandText = strQuery;
                    cmd.Connection = con;
                    con.Open();

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    ddlSLULB.DataSource = ds;
                    ddlSLULB.DataTextField = "ULB";
                    ddlSLULB.DataValueField = "ULBMappingId";
                    ddlSLULB.DataBind();
                    con.Close();
                    ddlSLULB.Items.Insert(0, "--Select--");
                }
            }
        }

        protected void ddlScheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            divStreetOptions.Visible = false;
            if (ddlScheme.SelectedItem.Value=="1")
                divStreetOptions.Visible = true;
        }

        protected void ddlSLULB_SelectedIndexChanged(object sender, EventArgs e)
        {
            int stateId, districtId, ulbMappingId;
            stateId = Convert.ToInt32(ddlState.SelectedItem.Value);
            districtId= Convert.ToInt32(ddlDistrict.SelectedItem.Value);
            ulbMappingId = Convert.ToInt32(ddlSLULB.SelectedItem.Value);

            String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            String strQuery = "select ZoneMappingId, Zone from [ZoneMapping] where StateId = @stateId AND DistrictId = @DistrictId AND ULBMappingId = @ULBMappingId";
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@StateId", stateId);
                    cmd.Parameters.AddWithValue("@DistrictId", districtId);
                    cmd.Parameters.AddWithValue("@ULBMappingId", ulbMappingId);
                    cmd.CommandText = strQuery;
                    cmd.Connection = con;
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    ddlSLZone.DataSource = ds;
                    ddlSLZone.DataTextField = "Zone";
                    ddlSLZone.DataValueField = "ZoneMappingId";
                    ddlSLZone.DataBind();
                    con.Close();
                    ddlSLZone.Items.Insert(0, "--Select--");
                }
            }
        }
        public void SetPageData()
        {
            Session["frm_CallerNumber"] = txtMobileNo.Text;
            Session["frm_DateSearchBy"] = ddlDateSearchBy.SelectedIndex.ToString();
            Session["frm_DateFrom"] = txtFrom.Text;
            Session["frm_DateTo"] = txtTo.Text;
            Session["frm_UID"] = txtUID.Text;
            Session["frm_CallerName"] = txtNameOfCaller.Text;
            Session["frm_Language"] = ddlLanguage.SelectedIndex.ToString();
            Session["frm_CallStatus"] = ddlStatusOfCall.SelectedIndex.ToString();
            Session["frm_Scheme"] = ddlScheme.SelectedIndex.ToString();
            Session["frm_CallCategory"] = ddlCategoryOfCall.SelectedIndex.ToString();
            Session["frm_State"] = ddlState.SelectedIndex.ToString();
            Session["frm_District"] = ddlDistrict.SelectedIndex.ToString();
            Session["frm_Source"] = ddlSource.SelectedIndex.ToString();
            Session["frm_ComplaintHours"] = ddlComplaintHours.SelectedIndex.ToString();
            Session["frm_Keyword"] = txtKeyword.Text;

            Session["IsSearchPage"] = "YES";
        }
        public void GetPageData()
        {
            txtMobileNo.Text = Session["frm_CallerNumber"].ToString();
            ddlDateSearchBy.SelectedIndex = int.Parse(Session["frm_DateSearchBy"].ToString());
            txtFrom.Text = Session["frm_DateFrom"].ToString();
            txtTo.Text = Session["frm_DateTo"].ToString();
            txtUID.Text = Session["frm_UID"].ToString();
            txtNameOfCaller.Text = Session["frm_CallerName"].ToString();
            ddlLanguage.SelectedIndex = int.Parse(Session["frm_Language"].ToString());
            ddlStatusOfCall.SelectedIndex = int.Parse(Session["frm_CallStatus"].ToString());
            ddlScheme.SelectedIndex = int.Parse(Session["frm_Scheme"].ToString());
            ddlCategoryOfCall.SelectedIndex = int.Parse(Session["frm_CallCategory"].ToString());
            ddlState.SelectedIndex = int.Parse(Session["frm_State"].ToString());
            ddlDistrict.SelectedIndex = int.Parse(Session["frm_District"].ToString());
            ddlSource.SelectedIndex = int.Parse(Session["frm_Source"].ToString());
            ddlComplaintHours.SelectedIndex = int.Parse(Session["frm_ComplaintHours"].ToString());
            txtKeyword.Text = Session["frm_Keyword"].ToString();

        }
    }
}