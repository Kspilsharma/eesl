﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CRM.Models;
using System.Reflection;
using System.Configuration;
using System.Collections;
using System.Data.SqlClient;
using System.IO;
using System.Web.Services;

namespace EESL.View
{
    public partial class frmZoneMapping : System.Web.UI.Page
    {
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();
      
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!IsPostBack)
            { SetUI();
               
            }
           
           
        }

        protected void SetUI()
        {
            hdnId.Value = "-1";
            ddlState.Focus();
            txtZone.Text = "";
            SQLDBHelper.PopulateDropDownList(ddlState, "State", "State", "StateId", false, "");
            SQLDBHelper.PopulateDropDownList(ddlULB, "ULBMapping", "ULBMappingId", "ULB", true, "StateId=0 AND DistrictId=0");
            FillGrid();
            FillGridReturnDataTable();
        }

        protected void FillGrid()
        {
            try
            {
                Mapping tMapping = new Mapping();
                
                DataSet ds = new DataSet();
                ds = tMapping.ZoneMappingSelect();
                if (ds.Tables[0].Rows.Count > 0)
                {
                  
                    gvZoneMapping.DataSource = ds.Tables[0];
                    gvZoneMapping.DataBind();
                    gvZoneMapping.Visible = true;
                }
                else
                {
                    gvZoneMapping.Visible = false;
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void ExportDashboardDetail()
        {

            string sFileName = "ZoneList-" + System.DateTime.Now.Date + ".xls";
            try
            {
                Mapping tMapping = new Mapping();
                DataSet ds = new DataSet();
                ds = tMapping.ZoneMappingSelect();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ExportDatatableToExcel(ds.Tables[0], sFileName);
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        public void ExportDatatableToExcel(DataTable table, string sFileName)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + sFileName);

            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR><BR><BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> <TR>");
            //am getting my grid's column headers
            int columnscount = table.Columns.Count;

            for (int j = 0; j < columnscount; j++)
            {      //write in new column
                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(table.Columns[j].ColumnName.ToString());
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (DataRow row in table.Rows)
            {//write in new row
                HttpContext.Current.Response.Write("<TR>");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    HttpContext.Current.Response.Write("<Td>");
                    HttpContext.Current.Response.Write(row[i].ToString());
                    HttpContext.Current.Response.Write("</Td>");
                }

                HttpContext.Current.Response.Write("</TR>");
            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }

        protected void imgbtnExportExcel_Click(object sender, ImageClickEventArgs e)
        {
            ExportDashboardDetail();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Mapping tMapping = new Mapping();
                DataSet ds = new DataSet();

                tMapping.StateId = Convert.ToInt32(ddlState.SelectedItem.Value);
                tMapping.DistrictId = Convert.ToInt32(hdnDistricId.Value);
                tMapping.ULBMappingId = Convert.ToInt32(hdnULBMappingId.Value);
                tMapping.Zone = Convert.ToString(txtZone.Text.Trim());
                tMapping.LoginId = UIManager.CurrentUserSession().UserId;
                string tErrorMsg = string.Empty;
                if (hdnId.Value == "-1")
                {
                    if (tMapping.ZoneMappingInsert(out tErrorMsg))
                    { SetUI(); }
                }
                else
                {
                    tMapping.ZoneMappingId = Convert.ToInt32(hdnId.Value);
                    if (tMapping.ZoneMappingUpdate(out tErrorMsg))
                    { SetUI(); }
                    hdnId.Value = "-1";
                }

                lblMsg.Text = tErrorMsg;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            SetUI();
        }

        protected void gvZoneMapping_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {

                Mapping tMapping = new Mapping();
                DataSet ds = new DataSet();
                Label lblstatus = ((Label)gvZoneMapping.Rows[e.NewEditIndex].FindControl("gvlblIsActive"));
                string strIsActive = lblstatus.Text;
                hdnId.Value = (gvZoneMapping.DataKeys[e.NewEditIndex].Value.ToString());

                tMapping.IsActive = Convert.ToBoolean(strIsActive);
                tMapping.ZoneMappingId = Convert.ToInt32(hdnId.Value);
                ds = tMapping.ZoneMappingEdit();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    txtZone.Text = ds.Tables[0].Rows[0]["Zone"].ToString();

                    int stateId = Convert.ToInt32(ds.Tables[0].Rows[0]["StateId"]);
                    int ulbMappingId = Convert.ToInt32(ds.Tables[0].Rows[0]["ULBMappingId"]);
                    int districtId = Convert.ToInt32(ds.Tables[0].Rows[0]["DistrictId"]);
                    hdnDistricId.Value = Convert.ToString(districtId);
                    hdnULBMappingId.Value = Convert.ToString(ulbMappingId);
                    if (stateId > 0)
                    {
                        ddlState.SelectedIndex = -1;
                        ddlState.Items.FindByValue(Convert.ToString(stateId)).Selected = true;
                    }
                    else
                        ddlState.SelectedIndex = 0;
                    if (districtId > 0)
                    {
                        ddlDistrict.SelectedIndex = -1;
                        SQLDBHelper.PopulateDropDownList(ddlDistrict, "District", "District", "DistrictId", true, "StateId=" + stateId);
                        ddlDistrict.Items.FindByValue(Convert.ToString(districtId)).Selected = true;
                    }
                    else
                    {
                        ddlDistrict.Items.Insert(0, "--Select--");
                        ddlDistrict.SelectedIndex = 0;
                    }
                    if (ulbMappingId > 0)
                    {
                        ddlULB.SelectedIndex = -1;
                        SQLDBHelper.PopulateDropDownList(ddlULB, "ULBMapping", "ULB", "ULBMappingId", true, "StateId=" + stateId + " AND DistrictId=" + districtId);
                        ddlULB.Items.FindByValue(Convert.ToString(ulbMappingId)).Selected = true;
                    }
                    else
                    {
                        ddlULB.Items.Insert(0, "--Select--");
                        ddlULB.SelectedIndex = 0;
                    }
                        

                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void gvZoneMapping_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Mapping tMapping = new Mapping();
                tMapping.ZoneMappingId = Convert.ToInt32(gvZoneMapping.DataKeys[e.RowIndex].Value.ToString());
                Label lblstatus = ((Label)gvZoneMapping.Rows[e.RowIndex].FindControl("gvlblIsActive"));
                string strIsActive = lblstatus.Text;
                if (strIsActive == "False")
                {
                    tMapping.IsActive = true;
                }
                else
                {
                    tMapping.IsActive = false;
                }
                string tErrorMsg = string.Empty;
                if (tMapping.ZoneMappingStatusUpdate(out tErrorMsg))
                {
                    FillGrid();
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void gvZoneMapping_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            Mapping tMapping = new Mapping();
            Int32 ZoneMappingId = Convert.ToInt32(gvZoneMapping.DataKeys[e.RowIndex].Value.ToString());
            Label lblstatus = ((Label)gvZoneMapping.Rows[e.RowIndex].FindControl("gvlblIsActive"));
            string strIsActive = lblstatus.Text;
            try
            {
                tMapping.IsActive = Convert.ToBoolean(strIsActive);
                tMapping.ZoneMappingId = ZoneMappingId;
                string tErrorMsg = string.Empty;
                if (tMapping.ZoneMappingDelete(out tErrorMsg))
                {
                    SetUI();
                }

                lblMsg.Text = tErrorMsg;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void gvZoneMapping_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvZoneMapping.PageIndex = e.NewPageIndex;
            FillGrid();
        }
        protected void gvZoneMapping_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string STRIsActive = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "IsActive"));

                    ImageButton imgbtnstatus = (ImageButton)e.Row.FindControl("imgbtnStatus");
                    if (STRIsActive == "False")
                    {
                        imgbtnstatus.ImageUrl = "~/Images/block.Jpg";
                        imgbtnstatus.ToolTip = "Click to Active";
                    }
                    else
                    {
                        imgbtnstatus.ImageUrl = "~/Images/active.gif";
                        imgbtnstatus.ToolTip = "Click to Block";
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        [System.Web.Services.WebMethod]
        public static ArrayList PopulateCities(int stateId)
        {
            ArrayList list = new ArrayList();
            String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            String strQuery = "select DistrictID, District from [District] where StateId = @StateId";
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@StateId", stateId);
                    cmd.CommandText = strQuery;
                    cmd.Connection = con;
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        list.Add(new ListItem(
                       sdr["District"].ToString(),
                       sdr["DistrictID"].ToString()
                        ));
                    }
                    con.Close();
                    return list;
                }
            }
        }

        [System.Web.Services.WebMethod]
        public static ArrayList PopulateULB(int stateId,int districtId)
        {
            ArrayList list = new ArrayList();
            String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            String strQuery = "select ULBMappingId, ULB from [ULBMapping] where StateId = @stateId AND DistrictId = @DistrictId";
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@StateId", stateId);
                    cmd.Parameters.AddWithValue("@DistrictId", districtId);
                    cmd.CommandText = strQuery;
                    cmd.Connection = con;
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        list.Add(new ListItem(
                       sdr["ULB"].ToString(),
                       sdr["ULBMappingId"].ToString()
                        ));
                    }
                    con.Close();
                    return list;
                }
            }
        }

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            DataTable dt = FillGridReturnDataTable();
            DataView dv = new DataView(dt);
            string SearchExpression = null;
            if (!String.IsNullOrEmpty(txtSearch.Text))
            {
                SearchExpression = string.Format("{0} '%{1}%'",
                gvZoneMapping.SortExpression, txtSearch.Text);

            }
            if (!String.IsNullOrEmpty(txtSearch.Text))
            {
                dv.RowFilter = "State like" + SearchExpression + "or District like" + SearchExpression + "or ULB like" + SearchExpression + "or Zone like" + SearchExpression;
                gvZoneMapping.DataSource = dv;
                gvZoneMapping.DataBind();
            }
            else
            {
                gvZoneMapping.DataSource = dt;
                gvZoneMapping.DataBind();
            }

             

        }
        protected DataTable  FillGridReturnDataTable()
        {
            DataTable dt = new DataTable();
            try
            {
                Mapping tMapping = new Mapping();

                DataSet ds = new DataSet();
               
                ds = tMapping.ZoneMappingSelect();
                if (ds.Tables[0].Rows.Count > 0)
                {

                    dt = ds.Tables[0];
                }
                
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
            return dt;
        }
    }
}