﻿<%@ Page Title="Complaint Registration" Language="C#" MasterPageFile="~/WithoutOuth.Master" AutoEventWireup="true" CodeBehind="frmConsumerEntry.aspx.cs" Inherits="EESL.View.frmConsumerEntry" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="../Scripts/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.min.js"></script>
    <script src="../Scripts/bootstrap-3.3.7.min.js"></script>
    <link rel="stylesheet" href="../Styles/bootstrap-3.3.7.min.css" />
    <link rel="stylesheet" href="../Styles/bootstrap-theme-3.3.7.min.css" />
    <script type="text/javascript">

        $(document).ready(function () {
            $('#<% =ddlSLAssignedTo.ClientID %>').change(function (e) {
                var assignedToId = $('#<% =ddlSLAssignedTo.ClientID %>').val();
                $('#<% =hdnAssignedToId.ClientID %>').attr('value', assignedToId);
                var assignedToText = $("#<% =ddlSLAssignedTo.ClientID %> option:selected").text();
                if (assignedToText != "--Select--")
                    $('#<% =hdnAssignedToText.ClientID %>').attr('value', assignedToText);
                else
                    $('#<% =hdnAssignedToText.ClientID %>').attr('value', '');
            });
            $('#<% =ddlDistrict.ClientID %>').change(function (e) {
                var districId = $('#<% =ddlDistrict.ClientID %>').val();
                $('#<% =hdnDistricId.ClientID %>').attr('value', districId);
				
                var districText = $("#<% =ddlDistrict.ClientID %> option:selected").text();
                if (districText != "--Select--")
                    $('#<% =hdnDistricText.ClientID %>').attr('value', districText);
                else
                    $('#<% =hdnDistricText.ClientID %>').attr('value', '');

            });

            $('#<% =ddlSLULB.ClientID %>').change(function (e) {
                var ulbMappingId = $('#<% =ddlSLULB.ClientID %>').val();
                $('#<% =hdnULBId.ClientID %>').attr('value', ulbMappingId);
                var ulbMappingText = $('#<% =ddlSLULB.ClientID %> option:selected').text();
                if (ulbMappingText != "--Select--")
                    $('#<% =hdnULBText.ClientID %>').attr('value', ulbMappingText);
                else
                    $('#<% =hdnULBText.ClientID %>').attr('value', '');
            });

            $('#<% =ddlSLZone.ClientID %>').change(function (e) {
                var zoneMappingId = $('#<% =ddlSLZone.ClientID %>').val();
                $('#<% =hdnZoneId.ClientID %>').attr('value', zoneMappingId);
                var zoneMappingText = $('#<% =ddlSLZone.ClientID %> option:selected').text();
                if (zoneMappingText != "--Select--")
                    $('#<% =hdnZoneText.ClientID %>').attr('value', zoneMappingText);
                else
                    $('#<% =hdnZoneText.ClientID %>').attr('value', '');
            });

            $('#<% =ddlUJDiscom.ClientID %>').change(function (e) {
                var discomMappingId = $('#<% =ddlUJDiscom.ClientID %>').val();
                $('#<% =hdnDiscomId.ClientID %>').attr('value', discomMappingId);

                var discomMappingText = $('#<% =ddlUJDiscom.ClientID %> option:selected').text();
                if (discomMappingText != "--Select--")
                    $('#<% =hdnDiscomText.ClientID %>').attr('value', discomMappingText);
                else
                    $('#<% =hdnDiscomText.ClientID %>').attr('value', '');
            });

            $('#<% =ddlBLBuilding.ClientID %>').change(function (e) {
                var buildingMappingId = $('#<% =ddlBLBuilding.ClientID %>').val();
                $('#<% =hdnBuildingId.ClientID %>').attr('value', buildingMappingId);
                var buildingMappingText = $('#<% =ddlBLBuilding.ClientID %> option:selected').text();
                if (buildingMappingText != "--Select--")
                    $('#<% =hdnBuildingText.ClientID %>').attr('value', buildingMappingText);
                else
                    $('#<% =hdnBuildingText.ClientID %>').attr('value', '');
            });

        });

        var pageUrl = '<%=ResolveUrl("~/View/frmConsumerEntry.aspx")%>'

        function PopulateCities() {

            $('#<% =hdnDistricId.ClientID %>').attr('value', '0');
            $('#<% =hdnDistricText.ClientID %>').attr('value', '0');
            $('#<% =hdnULBId.ClientID %>').attr('value', '0');
            $('#<% =hdnULBText.ClientID %>').attr('value', '');
            $('#<% =hdnZoneId.ClientID %>').attr('value', '0');
            $('#<% =hdnZoneText.ClientID %>').attr('value', '');
            $('#<% =hdnAssignedToId.ClientID %>').attr('value', '0');
            $('#<% =hdnAssignedToText.ClientID %>').attr('value', '');
            $('#<% =hdnDiscomId.ClientID %>').attr('value', '0');
            $('#<% =hdnDiscomText.ClientID %>').attr('value', '');
            $('#<% =hdnBuildingId.ClientID %>').attr('value', '0');
            $('#<% =hdnBuildingText.ClientID %>').attr('value', '');
            $('#<% =hdnEsclatedId.ClientID %>').attr('value', '0');
            $('#<% =hdnEsclatedText.ClientID %>').attr('value', '');

            $('#<%=ddlSLULB.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            $('#<%=ddlSLZone.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            $('#<%=ddlSLAssignedTo.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            $('#<%=ddlBLBuilding.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            $('#<%=ddlUJDiscom.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');

            if ($('#<%=ddlState.ClientID%>').val() == "--Select--") {
                $('#<%=ddlDistrict.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            }
            else {
                $('#<%=ddlDistrict.ClientID %>').empty().append('<option selected="selected" value="0">Loading...</option>');
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/PopulateCities',
                    data: '{stateId: ' + $('#<%=ddlState.ClientID%>').val() + ', schemeid: ' + $('#<%=ddlScheme.ClientID%>').val() + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnCitiesPopulated,
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
        }

        function OnCitiesPopulated(response) {
            PopulateControl(response.d, $("#<%=ddlDistrict.ClientID %>"));
        }

        function PopulateULBDiscomBuilding() {

            $('#<% =hdnULBId.ClientID %>').attr('value', '0');
            $('#<% =hdnULBText.ClientID %>').attr('value', '');
            $('#<% =hdnZoneId.ClientID %>').attr('value', '0');
            $('#<% =hdnZoneText.ClientID %>').attr('value', '');
            $('#<% =hdnAssignedToId.ClientID %>').attr('value', '0');
            $('#<% =hdnAssignedToText.ClientID %>').attr('value', '');
            $('#<% =hdnDiscomId.ClientID %>').attr('value', '0');
            $('#<% =hdnDiscomText.ClientID %>').attr('value', '');
            $('#<% =hdnBuildingId.ClientID %>').attr('value', '0');
            $('#<% =hdnBuildingText.ClientID %>').attr('value', '');
            $('#<% =hdnEsclatedId.ClientID %>').attr('value', '0');
            $('#<% =hdnEsclatedText.ClientID %>').attr('value', '');

            $('#<%=ddlSLZone.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            $('#<%=ddlSLAssignedTo.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            $('#<%=ddlAGDSMAssignedTo.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');

            PopulateULB();
            PopulateDiscom();
            PopulateBuilding();
            PopulateAGDSMVendor();
        }


        function PopulateULB() {

            if ($('#<%=ddlDistrict.ClientID%>').val() == "--Select--") {
                $('#<%=ddlSLULB.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            }
            else {
                $('#<%=ddlSLULB.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/PopulateULB',
                    data: '{stateId: ' + $('#<%=ddlState.ClientID%>').val() + ',districtId: ' + $('#<%=ddlDistrict.ClientID%>').val() + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnULBPopulated,
                    failure: function (response) {
                        alert(response.d);
                    }
                });

            }
        }

        function OnULBPopulated(response) {
            if (response.d.length > 0) {
                PopulateControl(response.d, $("#<%=ddlSLULB.ClientID %>"), "ULB");
            }            
        }

        function PopulateZoneAndVendor() {

            if ($('#<%=ddlSLULB.ClientID%>').val() == "--Select--") {
                $('#<%=ddlSLZone.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            }
            else {

                $('#<%=ddlSLZone.ClientID %>').empty().append('<option selected="selected" value="0">Loading...</option>');
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/PopulateZone',
                    data: '{stateId: ' + $('#<%=ddlState.ClientID%>').val() + ',districtId: ' + $('#<%=ddlDistrict.ClientID%>').val() + ',ulbMappingId: ' + $('#<%=ddlSLULB.ClientID%>').val() + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnZonePopulated,
                    failure: function (response) {
                        alert(response.d);
                    }
                });

                //Vendor
                PopulateVendor();
            }
        }

        function OnZonePopulated(response) {
            PopulateControl(response.d, $("#<%=ddlSLZone.ClientID %>"), "Zone");
          }

          function PopulateVendor() {

              if ($('#<%=ddlSLULB.ClientID%>').val() == "--Select--") {
                $('#<%=ddlSLAssignedTo.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            }
            else {

                $('#<%=ddlSLAssignedTo.ClientID %>').empty().append('<option selected="selected" value="0">Loading...</option>');
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/PopulateVendor',
                    data: '{stateId: ' + $('#<%=ddlState.ClientID%>').val() + ',districtId: ' + $('#<%=ddlDistrict.ClientID%>').val() + ',ulbMappingId: ' + $('#<%=ddlSLULB.ClientID%>').val() + ',zoneId: ' + $('#<%=ddlSLZone.ClientID%>').val() + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnVendorPopulated,
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
        }

        function OnVendorPopulated(response) {
            PopulateControl(response.d, $("#<%=ddlSLAssignedTo.ClientID %>"), "Vendor");
        }



        function PopulateDiscom() {

            if ($('#<%=ddlDistrict.ClientID%>').val() == "--Select--") {
                $('#<%=ddlUJDiscom.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            }
            else {
                $('#<%=ddlUJDiscom.ClientID %>').empty().append('<option selected="selected" value="0">Loading...</option>');
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/PopulateDiscom',
                    data: '{stateId: ' + $('#<%=ddlState.ClientID%>').val() + ',districtId: ' + $('#<%=ddlDistrict.ClientID%>').val() + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnDiscomPopulated,
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
        }

        function OnDiscomPopulated(response) {
            PopulateControl(response.d, $("#<%=ddlUJDiscom.ClientID %>"), "Discom");
        }

        function PopulateBuilding() {

            if ($('#<%=ddlDistrict.ClientID%>').val() == "--Select--") {
                $('#<%=ddlBLBuilding.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            }
            else {
                $('#<%=ddlBLBuilding.ClientID %>').empty().append('<option selected="selected" value="0">Loading...</option>');
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/PopulateBuilding',
                    data: '{stateId: ' + $('#<%=ddlState.ClientID%>').val() + ',districtId: ' + $('#<%=ddlDistrict.ClientID%>').val() + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnBuildingPopulated,
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
        }

        function OnBuildingPopulated(response) {
            PopulateControl(response.d, $("#<%=ddlBLBuilding.ClientID %>"), "Building");
        }


        function PopulateControl(list, control, type) {
            var AgDSMDistrictId = 0;
            if (list.length > 0) {
                control.empty().append('<option selected="selected" value="0">--Select--</option>');
                $.each(list, function () {
                    if (this['Text'] == $('#<%=hdnAgDSMDistrict.ClientID%>').val())
                    {
                        AgDSMDistrictId = this['Value'];
                        $('#<%=hdnAgDSMDistrictId.ClientID%>').val(this['Value']);
                    }
                    control.append($("<option></option>").val(this['Value']).html(this['Text']));
                });
                if (type == "Vendor") {
                    $('#<%=ddlSLAssignedTo.ClientID %>').prop('selectedIndex', 1);
                    $('#<%=ddlSLAssignedTo.ClientID %>').attr('selectedIndex', 1);
                    var assignedToId = $('#<% =ddlSLAssignedTo.ClientID %>').val();
                    $('#<% =hdnAssignedToId.ClientID %>').attr('value', assignedToId);
                    var assignedToText = $("#<% =ddlSLAssignedTo.ClientID %> option:selected").text();
                    $('#<% =hdnAssignedToText.ClientID %>').attr('value', assignedToText);
                }
            }
            else {
                if (type == "District") {
                    $('#<%=ddlDistrict.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
                }
                else if (type == "ULB") {
                    $('#<%=ddlSLULB.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
                }
                else if (type == "Zone") {
                    $('#<%=ddlSLZone.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
                }
                else if (type == "Discom") {
                    $('#<%=ddlUJDiscom.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
                }
                else if (type == "Building") {
                    $('#<%=ddlBLBuilding.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
                }
                else if (type == "Vendor") {
                    $('#<%=ddlSLAssignedTo.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
                 }

            }
            var schemename = $('#<%=ddlScheme.ClientID%> option:selected').text();
            if (schemename == "AgDSM") {
                var districtname = $('#<%=hdnAgDSMDistrict.ClientID%>').val();
                SetDistrict(AgDSMDistrictId, districtname);
            }      
          }


function SchemeChange() {

    $('#<% =hdnULBId.ClientID %>').attr('value', '0');
    $('#<% =hdnULBText.ClientID %>').attr('value', '');
    $('#<% =hdnZoneId.ClientID %>').attr('value', '0');
    $('#<% =hdnZoneText.ClientID %>').attr('value', '');
    $('#<% =hdnAssignedToId.ClientID %>').attr('value', '0');
    $('#<% =hdnAssignedToText.ClientID %>').attr('value', '');
    $('#<% =hdnDiscomId.ClientID %>').attr('value', '0');
    $('#<% =hdnDiscomText.ClientID %>').attr('value', '');
    $('#<% =hdnBuildingId.ClientID %>').attr('value', '0');
    $('#<% =hdnBuildingText.ClientID %>').attr('value', '');
    $('#<% =hdnEsclatedId.ClientID %>').attr('value', '0');
    $('#<% =hdnEsclatedText.ClientID %>').attr('value', '');

    $('#<%=ddlSLULB.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
    $('#<%=ddlSLZone.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
    $('#<%=ddlSLAssignedTo.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
    $('#<%=ddlBLBuilding.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
    $('#<%=ddlUJDiscom.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
    $('#<%=ddlAGDSMAssignedTo.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');

    $('#MainContent_LightTypeTr').hide();
    $('#<%=AGDSMTr_USCNo0.ClientID %>').hide();
    $('#<%=AGDSMTr_USCNo.ClientID %>').hide();
    $('#<%=AGDSMTr_PanelBoradMobNo0.ClientID %>').hide();
    $('#<%=AGDSMTr_PanelBoradMobNo.ClientID %>').hide();

    if ($('#<%=ddlScheme.ClientID%>').find('option:selected').text() == "Street Light") {
        $('#<%=fsStreetLight.ClientID%>').show();
        $('#MainContent_LightTypeTr').show();
        PopulateULB();
    }
    else {
        $('#<%=fsStreetLight.ClientID%>').hide();
    }
    if ($('#<%=ddlScheme.ClientID%>').find('option:selected').text() == "UJALA/NEEFP") {
        $('#<%=fsUjala.ClientID%>').show();
        PopulateDiscom();
    }
    else {
        $('#<%=fsUjala.ClientID%>').hide();
    }
    if ($('#<%=ddlScheme.ClientID%>').find('option:selected').text().toUpperCase() == "AGDSM") {
        $('#<%=fsAGDSM.ClientID%>').show();
        $('#<%=AGDSMTr_USCNo0.ClientID %>').show();
        $('#<%=AGDSMTr_USCNo.ClientID %>').show();
         $('#<%=AGDSMTr_PanelBoradMobNo0.ClientID %>').show();
        $('#<%=AGDSMTr_PanelBoradMobNo.ClientID %>').show();
    }
    else {
        $('#<%=fsAGDSM.ClientID%>').hide();
    }
    if ($('#<%=ddlScheme.ClientID%>').find('option:selected').text() == "Building") {
        $('#<%=fsBuilding.ClientID%>').show();
        PopulateBuilding();
    }
    else {
        $('#<%=fsBuilding.ClientID%>').hide();
    }
}


    </script>

    <script type="text/javascript">

        function PopupCenter(url, title, w, h) {
            // Fixes dual-screen position                         Most browsers      Firefox
            var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
            var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.TOP;

            var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
            var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

            var left = ((width / 2) - (w / 2)) + dualScreenLeft;
            var TOP = ((height / 2) - (h / 2)) + dualScreenTop;
            var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

            // Puts focus on the newWindow
            if (window.focus) {
                newWindow.focus();
            }
        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {
            SearchWardNo();
            SearchPoleNo();
            SearchConsumerNo();
            SearchUJTown();
            SearchAGDSMTown();
            SearchAGDSMDistributionCenter();
        });

        var pageUrl = '<%=ResolveUrl("~/View/frmConsumerEntry.aspx")%>'

        function SearchWardNo() {
            $("#<%=txtSLWardNo.ClientID %>").autocomplete({
                source: function (request, response) {
                    request.term = request.term.replace("'", "\\'");
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: pageUrl + "/GetWardNo",
                        data: "{'WardNo':'" + request.term + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                }
            });
        }

        function SearchPoleNo() {
            $("#<%=txtSLPoleNo.ClientID %>").autocomplete({
                source: function (request, response) {
                    request.term = request.term.replace("'", "\\'");
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: pageUrl + "/GetPoleNo",
                        data: "{'PoleNo':'" + request.term + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                }
            });
        }

        function SearchConsumerNo() {
            $("#<%=txtUJConsumerNo.ClientID %>").autocomplete({
                source: function (request, response) {
                    request.term = request.term.replace("'", "\\'");
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: pageUrl + "/GetConsumerNo",
                        data: "{'Consumer':'" + request.term + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                }
            });
        }
        function SearchUJTown() {
            $("#<%=txtUJTown.ClientID %>").autocomplete({
                source: function (request, response) {
                    request.term = request.term.replace("'", "\\'");
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: pageUrl + "/GetUJTown",
                        data: "{'Town':'" + request.term + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                }
            });
        }

        function SearchAGDSMTown() {
            $("#<%=txtAGDSMTown.ClientID %>").autocomplete({
                source: function (request, response) {
                    request.term = request.term.replace("'", "\\'");
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: pageUrl + "/GetAGDSMTown",
                        data: "{'Town':'" + request.term + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                }
            });
        }

        function SearchAGDSMDistributionCenter() {
            $("#<%=txtAGDSMDistributionCenter.ClientID %>").autocomplete({
                source: function (request, response) {
                    request.term = request.term.replace("'", "\\'");
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: pageUrl + "/GetDistributionCenter",
                        data: "{'DistributionCenter':'" + request.term + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                }
            });
        }        
        
        function ValidateField()
        {
            var selectedSchemeType = $('#<% =ddlScheme.ClientID %>').val();
            
            document.getElementById("rfvMobileNo").style.display = "none";

            document.getElementById("rfvState").style.display = "none";
            document.getElementById("rfvDistrict").style.display = "none";
            document.getElementById("rfvScheme").style.display = "none";

            document.getElementById("rfvULB").style.display = "none";
            document.getElementById("rfvZone").style.display = "none";
            document.getElementById("rfvSLAreaType").style.display = "none";
            if (selectedSchemeType == "1")
                document.getElementById("rfvLightType").style.display = "none";
            
            document.getElementById("rfvCaptcha").style.display = "none";
            document.getElementById("rfvAddress").style.display = "none";

            document.getElementById("rfvLandmark").style.display = "none";
            document.getElementById("rfvAssignedTo").style.display = "none";

            var fieldToFocus = '';
            var isValid=true;

            if ($('#<% =txtNumberOfCaller.ClientID %>').val() == '' )
            {
                isValid=false;
                if(fieldToFocus=='')
                    fieldToFocus=<% =txtNumberOfCaller.ClientID%>;
                document.getElementById("rfvMobileNo").style.display = "block";
            }
            
            if ($('#<% =ddlState.ClientID %>').val() == '--Select--' || $('#<% =ddlState.ClientID %>').val() == '0')
            {
                isValid=false;
                if(fieldToFocus=='')
                    fieldToFocus=<% =ddlState.ClientID%>;
                document.getElementById("rfvState").style.display = "block";
            }
            if ($('#<% =ddlDistrict.ClientID %>').val() == '--Select--' || $('#<% =ddlDistrict.ClientID %>').val() == '0' || $('#<% =ddlDistrict.ClientID %>').text().trim() == '')
            {
                isValid=false;
                if(fieldToFocus=='')
                    fieldToFocus=<% =ddlDistrict.ClientID%>;
                document.getElementById("rfvDistrict").style.display = "block";
            }
            if ($('#<% =ddlScheme.ClientID %>').val() == '--Select--' || $('#<% =ddlScheme.ClientID %>').val() == '0')
            {
                isValid=false;
                if(fieldToFocus=='')
                    fieldToFocus=<% =ddlScheme.ClientID%>;
                document.getElementById("rfvScheme").style.display = "block";
            }
            
            if (selectedSchemeType == 1) // Validation for Street Light
            {
                if ($('#<% =ddlSLULB.ClientID %>').val() == '--Select--' || $('#<% =ddlSLULB.ClientID %>').val() == '0')
                {
                    isValid=false;
                    if(fieldToFocus=='')
                        fieldToFocus=<% =ddlSLULB.ClientID%>;
                           document.getElementById("rfvULB").style.display = "block";
                       }                
                       if ($('#<% =ddlSLZone.ClientID %>').val() == '--Select--' || $('#<% =ddlSLZone.ClientID %>').val() == '0')
                {
                    isValid=false;
                    if(fieldToFocus=='')
                        fieldToFocus=<% =ddlSLZone.ClientID %>;
                           document.getElementById("rfvZone").style.display = "block";
                       }
                       if ($('#<% =ddlSLAreaType.ClientID %>').val() == '--Select--' || $('#<% =ddlSLAreaType.ClientID %>').val() == '0')
                {
                    isValid=false;
                    if(fieldToFocus=='')
                        fieldToFocus=<% =ddlSLAreaType.ClientID %>;
                           document.getElementById("rfvSLAreaType").style.display = "block";
                       }                
                       if ($('#<% =ddlSLLightType.ClientID %>').val() == '--Select--' || $('#<% =ddlSLLightType.ClientID %>').val() == '0')
                {
                    isValid=false;
                    if(fieldToFocus=='')
                        fieldToFocus=<% =ddlSLLightType.ClientID %>;
                           document.getElementById("rfvLightType").style.display = "block";
                       }
                   }
            
                   if (selectedSchemeType == 4) // Validation for Building
                   {
                       var isBLProductValid = false;
                       var chkListBLProductTypes= document.getElementById ('<%= dlBLProductTypes.ClientID %>');
                var chkListinputs = chkListBLProductTypes.getElementsByTagName("input");
                for (var i=0;i<chkListinputs .length;i++)
                {
                    if (chkListinputs [i].checked)
                    {
                        isBLProductValid = true;
                    }
                }
                
                if (!isBLProductValid) {
                    isValid = false;
                    if(fieldToFocus=='')
                        fieldToFocus = <%= dlBLProductTypes.ClientID %>;
                    alert('Please select atleast one product');
                }
            }

            if ($('#<% =txtCaptcha.ClientID %>').val() == '' )
            {
                isValid=false;
                if(fieldToFocus=='')
                    fieldToFocus=<% =txtCaptcha.ClientID%>;
                document.getElementById("rfvCaptcha").style.display = "block";
            }

            if ($("#<% =txtNameOfCaller.ClientID%>").val() == '') {
                isValid = false;
                if (fieldToFocus == '')
                    fieldToFocus =<% =txtNameOfCaller.ClientID%>;
                document.getElementById("rfvName").style.display = "block";
            }

            if ($("#<% =txtAddress.ClientID%>").val() == '') {
                isValid = false;
                if (fieldToFocus == '')
                    fieldToFocus =<% =txtAddress.ClientID%>;
                document.getElementById("rfvAddress").style.display = "block";
            }

            if ($("#<% =txtLandmark.ClientID%>").val() == '') {
                isValid = false;
                if (fieldToFocus == '')
                    fieldToFocus =<% =txtLandmark.ClientID%>;
                document.getElementById("rfvLandmark").style.display = "block";
            }

            if ($('#<%=ddlScheme.ClientID%>').val() == "3") {
                if ($('#<% =ddlAGDSMAssignedTo.ClientID %>').val() == '--Select--' || $('#<% =ddlAGDSMAssignedTo.ClientID %>').val() == '0') {
                    isValid = false;
                    if (fieldToFocus == '')
                        fieldToFocus =<% =ddlAGDSMAssignedTo.ClientID%>;
                    document.getElementById("rfvAGDSMAssignedTo").style.display = "block";
                }
            }


            $('#'+fieldToFocus.id).focus();                

            return isValid;
        }


        $(document).ready(function () {
            
            $('#<% =txtNumberOfCaller.ClientID %>').change(function()
            {
                if ($('#<% =txtNumberOfCaller.ClientID %>').val().trim() == '' )
                {
                    isValid=false;
                    if(fieldToFocus=='')
                        fieldToFocus=<% =txtNumberOfCaller.ClientID%>;
                    document.getElementById("rfvMobileNo").style.display = "block";
                }
                else
                {
                    document.getElementById("rfvMobileNo").style.display = "none";
                }
            });

            $('#<% =ddlState.ClientID %>').change(function()
            {
                if ($('#<% =ddlState.ClientID %>').val() == '--Select--' || $('#<% =ddlState.ClientID %>').val() == '0')
                {
                    isValid=false;
                    if(fieldToFocus=='')
                        fieldToFocus=<% =ddlState.ClientID%>;
                           document.getElementById("rfvState").style.display = "block";
                       }
                       else
                       {
                           document.getElementById("rfvState").style.display = "none";
                       }
            });

            $('#<% =ddlDistrict.ClientID %>').change(function()
            {
                if ($('#<% =ddlDistrict.ClientID %>').val() == '--Select--' || $('#<% =ddlDistrict.ClientID %>').val() == '0')
                {
                    isValid=false;
                    if(fieldToFocus=='')
                        fieldToFocus=<% =ddlDistrict.ClientID%>;
                           document.getElementById("rfvDistrict").style.display = "block";
                       }
                       else
                       {
                           document.getElementById("rfvDistrict").style.display = "none";
                       }
            });

            $('#<% =ddlScheme.ClientID %>').change(function()
            {
                if ($('#<% =ddlScheme.ClientID %>').val() == '--Select--' || $('#<% =ddlScheme.ClientID %>').val() == '0')
                {
                    isValid=false;
                    if(fieldToFocus=='')
                        fieldToFocus=<% =ddlScheme.ClientID%>;
                           document.getElementById("rfvScheme").style.display = "block";
                       }
                       else
                       {
                           document.getElementById("rfvScheme").style.display = "none";
                       }
            });           

            $('#<% =ddlSLULB.ClientID %>').change(function()
            {
                if ($('#<% =ddlSLULB.ClientID %>').val() == '--Select--' || $('#<% =ddlSLULB.ClientID %>').val() == '0')
                {
                    isValid=false;
                    if(fieldToFocus=='')
                        fieldToFocus=<% =ddlSLULB.ClientID%>;
                           document.getElementById("rfvULB").style.display = "block";
                       }
                       else
                       {
                           document.getElementById("rfvULB").style.display = "none";
                       }
            });

            $('#<% =ddlSLZone.ClientID %>').change(function()
            {
                if ($('#<% =ddlSLZone.ClientID %>').val() == '--Select--' || $('#<% =ddlSLZone.ClientID %>').val() == '0')
                {
                    isValid=false;
                    if(fieldToFocus=='')
                        fieldToFocus=<% =ddlSLZone.ClientID%>;
                           document.getElementById("rfvZone").style.display = "block";
                       }
                       else
                       {
                           document.getElementById("rfvZone").style.display = "none";
                       }
            });

            $('#<% =ddlSLAreaType.ClientID %>').change(function()
            {
                if ($('#<% =ddlSLAreaType.ClientID %>').val() == '--Select--' || $('#<% =ddlSLAreaType.ClientID %>').val() == '0')
                {
                    isValid=false;
                    if(fieldToFocus=='')
                        fieldToFocus=<% =ddlSLAreaType.ClientID%>;
                           document.getElementById("rfvSLAreaType").style.display = "block";
                       }
                       else
                       {
                           document.getElementById("rfvSLAreaType").style.display = "none";
                       }
            });

            $('#<% =ddlSLLightType.ClientID %>').change(function()
            {
                if ($('#<% =ddlSLLightType.ClientID %>').val() == '--Select--' || $('#<% =ddlSLLightType.ClientID %>').val() == '0')
                {
                    isValid=false;
                    if(fieldToFocus=='')
                        fieldToFocus=<% =ddlSLLightType.ClientID%>;
                           document.getElementById("rfvLightType").style.display = "block";
                       }
                       else
                       {
                           document.getElementById("rfvLightType").style.display = "none";
                       }
            });

            $('#<% =txtNameOfCaller.ClientID %>').change(function () {
                if ($('#<% =txtNameOfCaller.ClientID %>').val().trim() == '') {
                    isValid = false;
                    if (fieldToFocus == '')
                        fieldToFocus =<% =txtNameOfCaller.ClientID%>;
                           document.getElementById("rfvName").style.display = "block";
                       }
                       else {
                           document.getElementById("rfvName").style.display = "none";
                       }
            });

            $('#<% =txtAddress.ClientID %>').change(function () {
                if ($('#<% =txtAddress.ClientID %>').val().trim() == '') {
                    isValid = false;
                    if (fieldToFocus == '')
                        fieldToFocus =<% =txtAddress.ClientID%>;
                           document.getElementById("rfvAddress").style.display = "block";
                       }
                       else {
                           document.getElementById("rfvAddress").style.display = "none";
                       }
            });

            $('#<% =txtLandmark.ClientID %>').change(function () {
                if ($('#<% =txtLandmark.ClientID %>').val().trim() == '') {
                    isValid = false;
                    if (fieldToFocus == '')
                        fieldToFocus =<% =txtLandmark.ClientID%>;
                           document.getElementById("rfvLandmark").style.display = "block";
                       }
                       else {
                           document.getElementById("rfvLandmark").style.display = "none";
                       }
            });

            $('#<% =txtCaptcha.ClientID %>').change(function()
            {
                if ($('#<% =txtCaptcha.ClientID %>').val() == '' )
                {
                    isValid=false;
                    if(fieldToFocus=='')
                        fieldToFocus=<% =txtCaptcha.ClientID%>;
                           document.getElementById("rfvCaptcha").style.display = "block";
                       }
                       else
                       {
                           document.getElementById("rfvCaptcha").style.display = "none";
                       }
            });

            $("#<%=dlBLProductTypes.ClientID %>").find('input[type="checkbox"]').click(function () {
                var txt = $(this).closest('td').find("input[type=text]");
                if (txt) {
                    if (this.checked) {
                        txt.show();
                        txt.focus();
                    }
                    else {
                        txt.hide();
                    }
                }
                
                checkUncheckBLProductVendors(); 
            });
                       
            $( ".BLProductQty" ).blur(function() {                
                if (!$(this).val() || parseInt($(this).val()) === 0){                    
                    $(this).val('1');
                }
            });
        })


        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;            
            key = String.fromCharCode(charCode);
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        function PopulateBLVendor() {

            $.ajax({
                type: "POST",
                url: pageUrl + '/PopulateVendors',
                data: '{BuildingId: ' + $('#<%=ddlBLBuilding.ClientID%>').val() + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnBLVendorsPopulate,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function GetDetails() {
            $.ajax({
                type: "POST",
                url: pageUrl + '/GetBuildingDetails',
                data: '{BuildingId: ' + $('#<%=ddlBLBuilding.ClientID%>').val() + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var address = (response.d[0]);
                    var landmark = (response.d[1]);
                    $('#<%=txtAddress.ClientID%>').val(address);
                    $('#<%=txtLandmark.ClientID%>').val(landmark);
                    $('#<%=hdnAddress.ClientID%>').val(address);
                    $('#<%=hdnLandmark.ClientID%>').val(landmark);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }          

        function OnBLVendorsPopulate(response) {
            
            var checkboxlistItems = JSON.parse(response.d);
            
            var table = $('<table></table>');
            var counter = 0;
            
            $(checkboxlistItems).each(function () {
                
                if (this.IsChecked == "true") {
                    table.append($('<tr></tr>').append($('<td></td>').append($('<input>').attr('checked', true).attr({
                        type: 'checkbox', class: 'chkVendors', name: 'chklistitem', runat: 'server', value: this.Value, id: 'chklistitem_' + this.Value
                    })).append(
                        $('<label>').attr({
                            for: 'chklistitem_' + this.Value
                        }).text(this.Name))));
                }
                else {
                    table.append($('<tr></tr>').append($('<td></td>').append($('<input>').attr('checked', false).attr({
                        type: 'checkbox', class: 'chkVendors', name: 'chklistitem', runat: 'server', value: this.Value, id: 'chklistitem_' + this.Value
                    })).append(
                        $('<label>').attr({
                            for: 'chklistitem_' + this.Value
                        }).text(this.Name))));
                }                
            });

            $('#chklstProducts').html(table); 
            
            checkUncheckBLProductVendors();
        }

        function checkUncheckBLProductVendors() {

            var prodIds = $("#<%=dlBLProductTypes.ClientID %>").find('input[type="checkbox"]:checked').closest('td').find("input[type=hidden]").map(function() {return this.value;}).get().join(',');  
            $.ajax({
                type: "POST",
                url: pageUrl + '/GetProductVendros',
                data: '{stateId: ' + $('#<%=ddlState.ClientID%>').val() + ',districtId: ' + $('#<%=ddlDistrict.ClientID%>').val() + ',productTypeIds: "' + prodIds + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    $('#chklstProducts input[type=checkbox]').each(function () {
                        if($.inArray(parseInt(this.value), response.d) >= 0){
                            this.checked = true;
                        }
                        else {
                            this.checked = false;
                        }
                    });
                        
                    var blVendorIds = $('#chklstProducts input[type=checkbox]:checked').map(function() {return this.value;}).get().join(',');
                    $('#<% =hdnBLVendors.ClientID %>').val(blVendorIds);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function PopulateAGDSMVendor() {
            if ($('#<%=ddlScheme.ClientID%>').val() == "--Select--" || $('#<%=ddlState.ClientID%>').val() == "--Select--" || $('#<%=ddlDistrict.ClientID%>').val() == "--Select--") {
                 $('#<%=ddlAGDSMAssignedTo.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            }
            else {
                $('#<%=ddlAGDSMAssignedTo.ClientID %>').empty().append('<option selected="selected" value="0">Loading...</option>');
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/PopulateAGDSMVendor',
                    data: '{schemeId: ' + $('#<%=ddlScheme.ClientID%>').val() + ',stateId: ' + $('#<%=ddlState.ClientID%>').val() + ',districtId: ' + $('#<%=ddlDistrict.ClientID%>').val() + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        if (response.d.length > 0) {
                            $('#<%=ddlAGDSMAssignedTo.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
                            $.each(response.d, function () {
                                $('#<%=ddlAGDSMAssignedTo.ClientID %>').append($("<option></option>").val(this['Value']).html(this['Text']));
                            });

                            $('#<%=ddlAGDSMAssignedTo.ClientID %>').prop('selectedIndex', 1);
                            $('#<%=ddlAGDSMAssignedTo.ClientID %>').attr('selectedIndex', 1);
                            var assignedToId = $('#<% =ddlAGDSMAssignedTo.ClientID %>').val();
                            $('#<% =hdnAGDSMAssignedToId.ClientID %>').attr('value', assignedToId);
                            var assignedToText = $("#<% =ddlAGDSMAssignedTo.ClientID %> option:selected").text();
                            $('#<% =hdnAGDSMAssignedToText.ClientID %>').attr('value', assignedToText);
                        }
                        else {
                            $('#<%=ddlAGDSMAssignedTo.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
                        }
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
        }

        function GetAgDSMLogin(typeid) {
            var SearchText = "";
            if (typeid == "1")
                SearchText = $('#<%=txtAGDSMUSCORServiceConnNo.ClientID%>').val();
            else
                SearchText = $('#<%=txtAGDSMPanelBoardMobNo.ClientID%>').val();

            if (SearchText.length > 0) {
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/GetAgDSMLoginDetails',
                    data: '{Test: 1}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var obj = $.parseJSON(response.d);
                        var sessionid = obj.Results.JSUsers[0].JSUser.SessionID;
                        var userid = obj.Results.JSUsers[0].JSUser.UserId;
                        GetAgDSMConsumerDetails(userid, sessionid, typeid)
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
         }

         function GetAgDSMConsumerDetails(UserId, SessionId, TypeId) {
             var SearchType = TypeId;
             var SearchText = "";
             if (TypeId == "1")
                 SearchText = $('#<%=txtAGDSMUSCORServiceConnNo.ClientID%>').val();
            else
                SearchText = $('#<%=txtAGDSMPanelBoardMobNo.ClientID%>').val();
            $.ajax({
                type: "POST",
                url: pageUrl + '/GetAgDSMDetails',
                data: '{UserID: ' + UserId + ',SessionID: ' + SessionId + ',SearchText: ' + SearchText + ',SearchType: ' + SearchType + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var obj = $.parseJSON(response.d);
                    var message = obj.Results.message;
                    //if (message == "0")
                    {
                        var FarmerName = obj.Results.JSUsers[0].JSUser[0].FarmerName;
                        var FarmerMobileNo = obj.Results.JSUsers[0].JSUser[0].FarmerMobileNo;
                        var Division = obj.Results.JSUsers[0].JSUser[0].Division;
                        var Village = obj.Results.JSUsers[0].JSUser[0].Village;
                        var Taluka = obj.Results.JSUsers[0].JSUser[0].Taluk;
                        var District = obj.Results.JSUsers[0].JSUser[0].Circle;
                        var USCNo = obj.Results.JSUsers[0].JSUser[0].USCNo;
                        if (Division == "")
                            Division = "NA";
                        if (Village == "")
                            Village = "NA";
                        if (Taluka == "")
                            Taluka = "NA";
                        var Address = "Division-" + Division + ",Village-" + Village + ",Taluk-" + Taluka + "";
                        $('#<%=txtNameOfCaller.ClientID%>').val(FarmerName);
                        $('#<%=txtAGDSMPanelBoardMobNo.ClientID%>').val(FarmerMobileNo);
                        $('#<%=txtAGDSMUSCORServiceConnNo.ClientID%>').val(USCNo);
                        $('#<%=txtAddress.ClientID%>').val(Address);
                        $('#<%=txtLandmark.ClientID%>').val(Division);
                        $('#<%=ddlState.ClientID%>').val('2');
                        $('#<%=hdnAgDSMDistrict.ClientID%>').val(District.toLowerCase());
                        PopulateCities();
                    }
                    <%--else {
                        alert(message);
                        $('#<%=txtAGDSMUSCORServiceConnNo.ClientID%>').val('');
                    }--%>
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

         function SetDistrict(DistrictId, DistrictName) {
            $('#<%=ddlDistrict.ClientID%>').val(DistrictId);
            PopulateAGDSMVendor();
            $('#<%=ddlDistrict.ClientID%>').attr('disabled', true);
            $('#<%=hdnDistricText.ClientID%>').val(DistrictName);
            $('#<%=hdnDistricId.ClientID%>').val(DistrictId);
         }

         function ShowAlert() {
             alert('No Vendor is Found');
             window.location.href = "../View/frmConsumerEntry.aspx";
         }
    </script>

    <style type="text/css">
        .wrapper {
            overflow: hidden;
        }

        .uppercase {
            text-transform: uppercase;
        }

        .lowercase {
            text-transform: lowercase;
        }

        .normalcase {
            text-transform: inherit;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
     <asp:HiddenField ID="hdnAddress" runat="server" />
     <asp:HiddenField ID="hdnLandmark" runat="server" />
     <asp:HiddenField ID="hdnAgDSMDistrict" runat="server" />
     <asp:HiddenField ID="hdnAgDSMDistrictId" runat="server" />
    <button id="btnModel" type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal" style="display: none">
        Launch demo modal
    </button>
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                </div>
                <div class="modal-body">
                    Currently services are not available in this Area!
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="Heading" align="center" style="color: #101010">
                <asp:Label ID="lblPageHeader" runat="server" Text="Consumer Complaint Registration" CssClass="bold"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left">&nbsp;
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="lblMessage" runat="server" Font-Bold="False" Visible="false" CssClass="failureNotification"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:LinkButton ID="btnFAQ" runat="server" Text="FAQ" OnClick="btnFAQ_Click" />
                &nbsp;</td>
        </tr>
        <tr>
            <td align="left">&nbsp;&nbsp;</td>
        </tr>
    </table>

    <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
        <tr>
            <td align="left" valign="top" width="50%">
                <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
                    <tr>
                        <td align="left" valign="top">
                            <fieldset class="fieldset">
                                <legend class="Legendheading">Basic Info</legend>
                                <table id="tblBasicInfo" runat="server" cellpadding="0" cellspacing="3" style="width: 100%; margin: 0px; padding: 0px; height: 475px;">
                                    <tr valign="top">
                                        <td colspan="1" style="width: 30%" class="tdlabel">&nbsp;</td>
                                        <td colspan="3" style="width: 70%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr valign="top">
                                        <td colspan="1" style="width: 30%" class="tdlabel">Phone Number<span style="color: red">*</span></td>
                                        <td colspan="3" style="width: 70%" class="tddata">
                                            <asp:TextBox ID="txtNumberOfCaller" runat="server" Width="100%" MaxLength="12"></asp:TextBox>
                                            <label id="rfvMobileNo" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Enter Mobile.</label>
                                            <asp:FilteredTextBoxExtender ID="fteMobileNo" runat="server" Enabled="true" TargetControlID="txtNumberOfCaller" FilterType="Custom,Numbers" FilterMode="ValidChars" ValidChars="-" />
                                            <label id="rfvMobileNo" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Enter Mobile.</label>
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td style="width: 10%" class="tdlabel">&nbsp;</td>
                                        <td class="tddata" colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr runat="server">
                                        <td style="width: 10%" class="tdlabel">Scheme<span style="color: red">&nbsp;*</span></td>
                                        <td class="tddata" colspan="3">
                                            <asp:DropDownList ID="ddlScheme" runat="server" Width="100%" OnSelectedIndexChanged="ddlScheme_SelectedIndexChanged" AutoPostBack="true" />
                                            <br />
                                            <label id="rfvScheme" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Select Scheme of Call.</label>
                                        </td>
                                    </tr>
                                    <tr runat="server">
                                        <td style="width: 10%" class="tdlabel">&nbsp;</td>
                                        <td class="tddata" colspan="3">
                                            <label id="rfvCategoryOfCall" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Select Category of Call.</label>
                                        </td>
                                    </tr>
                                    <tr id="AGDSMTr_USCNo0" runat="server">
                                        <td style="width: 10%" class="tdlabel">&nbsp;</td>
                                        <td class="tddata" colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr id="AGDSMTr_USCNo" runat="server">
                                        <td style="width: 10%" class="tdlabel">USC No./Service Connection No.<span style="color: red">&nbsp;*</span></td>
                                        <td class="tddata" colspan="3">
                                            <asp:TextBox ID="txtAGDSMUSCORServiceConnNo" runat="server" Width="150px" MaxLength="20" onblur="GetAgDSMLogin(1);"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="fteAGDSMUSCORServiceConnNo" runat="server" Enabled="true" TargetControlID="txtAGDSMUSCORServiceConnNo" FilterType="Numbers" FilterMode="ValidChars" ValidChars="" />
                                        </td>
                                    </tr>
                                    <tr id="AGDSMTr_PanelBoradMobNo0" runat="server">
                                        <td style="width: 10%" class="tdlabel">&nbsp;</td>
                                        <td class="tddata" colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr id="AGDSMTr_PanelBoradMobNo" runat="server">
                                        <td style="width: 10%" class="tdlabel">Panel Board Mobile No<span style="color: red">&nbsp;</span></td>
                                        <td class="tddata" colspan="3">
                                            <asp:TextBox ID="txtAGDSMPanelBoardMobNo" runat="server" Width="150px" MaxLength="12" onblur="GetAgDSMLogin(3);"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="fteAGDSMPanelBoardMobNo" runat="server" Enabled="true" TargetControlID="txtAGDSMPanelBoardMobNo" FilterType="Numbers" FilterMode="ValidChars" ValidChars="" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" style="width: 100%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr valign="top">
                                        <td colspan="4" style="width: 100%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr valign="top">
                                        <td colspan="1" style="width: 30%" class="tdlabel">Name<span style="color: red;">&nbsp;*</span></td>
                                        <td colspan="3" style="width: 70%" class="tddata">
                                            <asp:TextBox ID="txtNameOfCaller" runat="server" Width="100%" MaxLength="50"></asp:TextBox>
                                            <asp:FilteredTextBoxExtender ID="fteNameOfCaller" runat="server" Enabled="true" TargetControlID="txtNameOfCaller" FilterType="Custom,LowercaseLetters,UppercaseLetters" FilterMode="ValidChars" ValidChars=" " />
                                            <br />
                                            <label id="rfvName" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Select Name.</label>
                                        </td>
                                    </tr>

                                    <tr valign="top">
                                        <td colspan="4" style="width: 100%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr id="LightTypeTr">
                                        <td style="width: 10%" class="tdlabel">Light Type<span style="color: red">&nbsp;*</span></td>
                                        <td colspan="3" style="width: 70%" class="tddata">
                                            <asp:DropDownList ID="ddlSLLightType" runat="server" Width="100%">
                                                <asp:ListItem Value="0">--Select--</asp:ListItem>
                                                <asp:ListItem Value="1">LED</asp:ListItem>
                                                <asp:ListItem Value="2">Non-LED</asp:ListItem>
                                            </asp:DropDownList>
                                            <br />
                                            <label id="rfvLightType" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Select Light Type.</label>
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td colspan="4" style="width: 100%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr valign="top">
                                        <td colspan="1" style="width: 30%" class="tdlabel">Address (Complaint Location)<span style="color: red">&nbsp;*</span></td>
                                        <td colspan="3" style="width: 70%" class="tddata">
                                            <asp:TextBox ID="txtAddress" runat="server" Width="98%" MaxLength="500" TextMode="MultiLine" Style="resize: none;" CssClass="uppercase"></asp:TextBox>
                                            <br />
                                            <label id="rfvAddress" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Select Address (Complaint Location).</label>
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td colspan="4" style="width: 100%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr valign="top">
                                        <td style="width: 10%" class="tdlabel">Landmark (Complaint Location)<span style="color: red">&nbsp;*</span></td>
                                        <td class="tddata" colspan="3">
                                            <asp:TextBox ID="txtLandmark" runat="server" Width="100%" MaxLength="100"></asp:TextBox>
                                            <br />
                                            <label id="rfvLandmark" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Select Landmark (Complaint Location).</label>
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td colspan="4" style="width: 100%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr valign="top">
                                        <td style="width: 10%" class="tdlabel">State <span style="color: red">*</span></td>
                                        <td style="width: 40%" class="tddata">
                                            <asp:DropDownList ID="ddlState" runat="server" Width="100%" onchange="PopulateCities();" />
                                            <label id="rfvState" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Select State.</label>
                                        </td>
                                        <td style="width: 11%" class="tdlabel">District <span style="color: red">*</span></td>
                                        <td style="width: 40%" class="tddata">
                                            <asp:DropDownList ID="ddlDistrict" runat="server" onchange="PopulateULBDiscomBuilding();" Width="100%" />
                                            <label id="rfvDistrict" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Select District.</label>
                                        </td>
                                    </tr>
                                    <tr valign="top">
                                        <td style="width: 10%" class="tdlabel">&nbsp;</td>
                                        <td style="width: 40%" class="tddata"></td>
                                        <td style="width: 10%" class="tdlabel">&nbsp;</td>
                                        <td style="width: 40%" class="tddata"></td>
                                    </tr>
                                    <tr valign="top">
                                        <td style="width: 10%" class="tdlabel">&nbsp;</td>
                                        <td class="tddata" colspan="3">&nbsp;</td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>

                </table>
            </td>
            <td align="left" valign="top" width="50%">
                <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%; height: 100%;">
                    <tr>
                        <td align="left" valign="top">
                            <fieldset class="fieldset">
                                <legend class="Legendheading">Query/Registration/Complaint Info</legend>
                                <table id="tblCallInfo" runat="server" cellpadding="0" cellspacing="3" style="width: 100%; margin: 0px; padding: 0px; max-height: 475px; vertical-align: top">
                                    <tr>
                                        <td colspan="2" style="width: 100%" class="" valign="top">
                                            <fieldset id="fsStreetLight" runat="server" class="fieldset">
                                                <legend class="Legendheading">Street Light</legend>
                                                <table class="ui-accordion" style="width: 100%;">
                                                    <tr>
                                                        <td style="width: 30%; vertical-align: top;" class="tdlabel">ULB<span style="color: red">&nbsp;*</span>
                                                        </td>
                                                        <td style="width: 70%; vertical-align: top;" class="tddata">
                                                            <asp:DropDownList ID="ddlSLULB" runat="server" Width="100%" onchange="PopulateZoneAndVendor();" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%">&nbsp;</td>
                                                        <td style="width: 70%">
                                                            <label id="rfvULB" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Select ULB.</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; vertical-align: top;" class="tdlabel">Zone<span style="color: red">&nbsp;*</span></td>
                                                        <td style="width: 70%; vertical-align: top;" class="tddata">
                                                            <asp:DropDownList ID="ddlSLZone" runat="server" Width="100%" onchange="PopulateVendor();" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%">&nbsp;</td>
                                                        <td style="width: 70%">
                                                            <label id="rfvZone" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Select Zone.</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; vertical-align: top;" class="tdlabel">Ward/Sector/Society</td>
                                                        <td style="width: 70%; vertical-align: top;" class="tddata">
                                                            <asp:TextBox ID="txtSLWardNo" runat="server" Width="100%" MaxLength="112"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%">&nbsp;</td>
                                                        <td style="width: 70%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; vertical-align: top;" class="tdlabel">Pole No.</td>
                                                        <td style="width: 70%; vertical-align: top;" class="tddata">
                                                            <asp:TextBox ID="txtSLPoleNo" runat="server" Width="100%" MaxLength="113"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%">&nbsp;</td>
                                                        <td style="width: 70%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; vertical-align: top;" class="tdlabel">Area Type<span style="color: red">&nbsp;*</span></td>
                                                        <td style="width: 70%; vertical-align: top;" class="tddata">
                                                            <asp:DropDownList ID="ddlSLAreaType" runat="server" Width="100%">
                                                                <asp:ListItem>--Select--</asp:ListItem>
                                                                <asp:ListItem Value="Individual">Single (Only 1 Light)</asp:ListItem>
                                                                <asp:ListItem Value="Multiple">Multiple (3-4 Light)</asp:ListItem>
                                                                <asp:ListItem Value="WholeArea">Complete street</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%">&nbsp;</td>
                                                        <td style="width: 70%">
                                                            <label id="rfvSLAreaType" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Select Area Type.</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; vertical-align: top;" class="tdlabel">Ticket Type</td>
                                                        <td style="width: 70%; vertical-align: top;" class="tddata">
                                                            <asp:DropDownList ID="ddlSLTicketType" runat="server" Width="100%" />
                                                        </td>
                                                    </tr>
                                                    <tr style="display: none">
                                                        <td style="width: 30%">&nbsp;</td>
                                                        <td style="width: 70%">&nbsp;</td>
                                                    </tr>
                                                    <tr style="display: none">
                                                        <td style="width: 30%; vertical-align: top;" class="tdlabel">Assigned To</td>
                                                        <td style="width: 70%; vertical-align: top;" class="tddata">
                                                            <asp:DropDownList ID="ddlSLAssignedTo" runat="server" Width="100%" />
                                                            <label id="rfvAssignedTo" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Select Assigned To.</label>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="width: 100%" class="">
                                            <fieldset id="fsUjala" runat="server" class="fieldset">
                                                <legend class="Legendheading">UJALA/NEEFP</legend>
                                                <table class="ui-accordion">
                                                    <tr>
                                                        <td style="width: 30%; vertical-align: top;" class="tdlabel">Discom</td>
                                                        <td style="width: 70%; vertical-align: top;" class="tddata">
                                                            <asp:DropDownList ID="ddlUJDiscom" runat="server" Width="100%" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%">&nbsp;</td>
                                                        <td style="width: 70%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; vertical-align: top;" class="tdlabel">Email</td>
                                                        <td style="width: 70%; vertical-align: top;" class="tddata">
                                                            <asp:TextBox ID="txtUJEmail" runat="server" Width="100%" MaxLength="100"></asp:TextBox>
                                                            <br />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; vertical-align: top;" class="tdlabel">&nbsp;</td>
                                                        <td style="width: 70%; vertical-align: top;" class="tddata">
                                                            <asp:RegularExpressionValidator ID="revUJEmail" runat="server" ForeColor="Maroon" ControlToValidate="txtUJEmail" ErrorMessage="Invalid Email." Display="Dynamic" ValidationGroup="Save" Font-Bold="True" Font-Size="X-Small" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; vertical-align: top;" class="tdlabel">Town</td>
                                                        <td style="width: 70%; margin-left: 120px; vertical-align: top;" class="tddata">
                                                            <asp:TextBox ID="txtUJTown" runat="server" Width="100%" MaxLength="100"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%">&nbsp;</td>
                                                        <td style="width: 70%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; vertical-align: top;" class="tdlabel">Consumer No.</td>
                                                        <td style="width: 70%; vertical-align: top;" class="tddata">
                                                            <asp:TextBox ID="txtUJConsumerNo" runat="server" Width="100%" MaxLength="100"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%">&nbsp;</td>
                                                        <td style="width: 70%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; vertical-align: top;" class="tdlabel">Product</td>
                                                        <td style="width: 70%; vertical-align: top;" class="tddata">
                                                            <asp:CheckBoxList ID="chklstUJProducts" runat="server"></asp:CheckBoxList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="width: 100%" class="">
                                            <fieldset id="fsAGDSM" runat="server" class="fieldset">
                                                <legend class="Legendheading">AGDSM</legend>
                                                <table class="ui-accordion">
                                                    <tr>
                                                        <td style="width: 30%; vertical-align: top" class="tdlabel">HPkW Rating</td>
                                                        <td style="width: 70%; vertical-align: top" class="tddata">
                                                            <asp:TextBox ID="txtAGDSMHPkWRating" runat="server" Width="100%" MaxLength="100" Text="5"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%">&nbsp;</td>
                                                        <td style="width: 70%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; vertical-align: top" class="tdlabel">Reason Of Complaint<span style="color: red">&nbsp;*</span></td>
                                                        <td style="width: 70%; vertical-align: top" class="tddata">
                                                            <asp:DropDownList ID="ddlAGDSMReasonOfComplaint" runat="server" Width="100%">
                                                                <asp:ListItem>--Select--</asp:ListItem>
                                                                <asp:ListItem Value="PUMPSET">Submersible Pump Set</asp:ListItem>
                                                                <asp:ListItem Value="CONTROLPANEL">Smart Control Panel</asp:ListItem>
                                                                <asp:ListItem Value="BOTH">Both</asp:ListItem>
                                                            </asp:DropDownList>
                                                            <label id="rfvAGDSMReasonOfComplaint" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Select Reason Of Complaint.</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%">&nbsp;</td>
                                                        <td style="width: 70%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; vertical-align: top" class="tdlabel">Assigned To<span id="spnAGDSMAssignedTo" style="color: red;">&nbsp;*</span></td>
                                                        <td style="width: 70%; vertical-align: top" class="tddata">
                                                            <asp:DropDownList ID="ddlAGDSMAssignedTo" runat="server" Width="100%" />
                                                            <label id="rfvAGDSMAssignedTo" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Select Assigned To.</label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%">&nbsp;</td>
                                                        <td style="width: 70%">&nbsp;</td>
                                                    </tr>
                                                    <tr style="display:none">
                                                        <td style="width: 30%; vertical-align: top" class="tdlabel">Town</td>
                                                        <td style="width: 70%; vertical-align: top" class="tddata">
                                                            <asp:TextBox ID="txtAGDSMTown" runat="server" Width="100%" MaxLength="100"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%">&nbsp;</td>
                                                        <td style="width: 70%">&nbsp;</td>
                                                    </tr>
                                                    <tr style="display:none">
                                                        <td style="width: 30%; vertical-align: top" class="tdlabel">Distribution Center</td>
                                                        <td style="width: 70%; vertical-align: top" class="tddata">
                                                            <asp:TextBox ID="txtAGDSMDistributionCenter" runat="server" Width="100%" MaxLength="100"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%">&nbsp;</td>
                                                        <td style="width: 70%">&nbsp;</td>
                                                    </tr>
                                                    <tr style="display:none">
                                                        <td style="width: 30%; vertical-align: top" class="tdlabel">Consumer No</td>
                                                        <td style="width: 70%; vertical-align: top" class="tddata">
                                                            <asp:TextBox ID="txtAGDSMConsumerNo" runat="server" Width="100%" MaxLength="100"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="width: 100%" class="">
                                            <fieldset id="fsBuilding" runat="server" class="fieldset">
                                                <legend class="Legendheading">Building</legend>
                                                <table class="ui-accordion">
                                                    <tr>
                                                        <td style="width: 30%; vertical-align: top;" class="tdlabel">Building</td>
                                                        <td style="width: 70%; vertical-align: top;" class="tddata">
                                                            <asp:DropDownList ID="ddlBLBuilding" onchange="PopulateBLVendor();GetDetails();" runat="server" Width="100%" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%">&nbsp;</td>
                                                        <td style="width: 70%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; vertical-align: top;" class="tdlabel">Reference No.</td>
                                                        <td style="width: 70%; vertical-align: top;" class="tddata">
                                                            <asp:TextBox ID="txtBLReferenceNo" runat="server" Width="100%" MaxLength="100"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%">&nbsp;</td>
                                                        <td style="width: 70%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; vertical-align: top;" class="tdlabel">Floor/Block</td>
                                                        <td style="width: 70%; vertical-align: top;" class="tddata">
                                                            <asp:TextBox ID="txtBLFlatNo" runat="server" Width="100%" MaxLength="100"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%">&nbsp;</td>
                                                        <td style="width: 70%">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; vertical-align: top" class="tdlabel">Product</td>
                                                        <td style="width: 70%; vertical-align: top" class="tddata">
                                                            <div style="width: 380px; border-style: solid; overflow: auto; border-width: 1px; height: 190px;">
                                                                <asp:DataList ID="dlBLProductTypes" runat="server" DataKeyField="ProductTypeId" RepeatDirection="Vertical" CaptionAlign="Left" OnItemDataBound="dlBLProductTypes_ItemDataBound">
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="chkProductType" CssClass='<%# Eval("ProductTypeId") %>' Text='<%# Eval("ProductType") %>' runat="server" />
                                                                        <asp:HiddenField ID="hdnProductTypeQuantity" Value='<%# Eval("ProductTypeId") %>' runat="server" />
                                                                        <asp:TextBox ID="txtProductTypeQuantity" Width="50" runat="server" Text="1" CssClass="BLProductQty" onkeypress="return isNumber(event)" MaxLength="2" />
                                                                    </ItemTemplate>
                                                                </asp:DataList>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%">&nbsp;</td>
                                                        <td style="width: 70%">&nbsp;</td>
                                                    </tr>
                                                    <tr style="display: none;">
                                                        <td style="width: 30%; vertical-align: top" class="tdlabel">Vendor
                                                        </td>
                                                        <td style="width: 70%; vertical-align: top" class="tddata">
                                                            <div id="chklstProducts" style="width: 380px; border-style: solid; overflow: auto; border-width: 1px; height: 190px; display: none;">
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="width: 100%" class="">
                                            <fieldset class="fieldset" runat="server">
                                                <legend class="Legendheading">Other Info</legend>
                                                <table class="ui-accordion">
                                                    <tr>
                                                        <td style="width: 30%; vertical-align: top" class="tdlabel" valign="top">Remarks</td>
                                                        <td style="width: 70%; vertical-align: top" class="tddata" valign="top">
                                                            <asp:TextBox type="text" ID="txtRemark" TextMode="MultiLine" Style="resize: none;" Rows="4" Width="98%" runat="server" CssClass="uppercase" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; vertical-align: top" class="tdlabel" valign="top">&nbsp;&nbsp;</td>
                                                        <td style="width: 70%; vertical-align: top" class="tddata" valign="top">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; vertical-align: top" class="tdlabel" valign="top">Captcha</td>
                                                        <td style="width: 70%; vertical-align: top" class="tddata" valign="top">
                                                            <asp:TextBox ID="txtCaptcha" runat="server" Width="50%" MaxLength="10" Style="text-transform: none"></asp:TextBox>
                                                            <%--<asp:RequiredFieldValidator ID="rfvCaptcha" runat="server" ControlToValidate="txtCaptcha" ErrorMessage="Enter Captcha Code." ForeColor="Maroon" ValidationGroup="Save" Font-Bold="True" Font-Size="X-Small"></asp:RequiredFieldValidator>--%>
                                                            <label id="rfvCaptcha" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Enter Captcha Code.</label>
                                                            <br />
                                                            <asp:Label ID="lblCaptchaMsg" runat="server" Font-Bold="True" ForeColor="Red" Text=""></asp:Label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width: 30%; vertical-align: top" class="tdlabel" valign="top">&nbsp;</td>
                                                        <td style="width: 70%; vertical-align: top" class="tddata" valign="top">
                                                            <asp:Image ID="imgCaptcha" runat="server" ImageUrl="~/View/CaptchaImage.aspx" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="vertical-align: top" class="tdlabel" valign="top" colspan="2">&nbsp;</td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>


        <tr>
            <td colspan="2" align="center">
                <asp:Button ID="btnSave" OnClientClick="return ValidateField();" runat="server" Text="Save" ValidationGroup="Save" AccessKey="S" OnClick="btnSave_Click" />
                &nbsp;<asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" />
                &nbsp;
            </td>
        </tr>


        <tr>
            <td colspan="2" align="center">&nbsp;</td>
        </tr>


        <tr>
            <td colspan="2" align="center" style="margin-left: 120px">&nbsp;&nbsp;</td>
        </tr>

        <tr>
            <td colspan="2" align="center">
                <asp:ModalPopupExtender ID="modelMessage" runat="server" BackgroundCssClass="modalBackground" CancelControlID="btnMessageOk" DropShadow="true" Enabled="True" OnOkScript="onOk()" PopupControlID="modelMessagePanel" RepositionMode="RepositionOnWindowResize " TargetControlID="btnMessageOk">
                </asp:ModalPopupExtender>
                <asp:Panel ID="modelMessagePanel" runat="server" CssClass="ModelPanel">
                    <div style="text-align: center; width: 100%">
                        <asp:Label ID="Label2" runat="server" CssClass="modalHeading" Text="Message"></asp:Label>
                    </div>
                    <br />
                    <asp:Label ID="Label3" runat="server" Style="font-weight: 500" Text="Message :-"></asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="lblPopUpMessage" runat="server" Style="font-weight: 900" Text="Message"></asp:Label>
                    <br />
                    <br />
                    <br />
                    <asp:Button ID="btnMessageOk" runat="server" Text="Ok" />
                    <br />
                </asp:Panel>
                <asp:ModalPopupExtender ID="modelUID" runat="server" BackgroundCssClass="modalBackground" CancelControlID="hButton" DropShadow="true" Enabled="True" OnOkScript="onOk()" PopupControlID="modelPanelUID" RepositionMode="RepositionOnWindowResize " TargetControlID="hButton">
                </asp:ModalPopupExtender>
                <asp:Panel ID="modelPanelUID" runat="server" CssClass="ModelPanel">
                    <div style="text-align: center; width: 100%">
                        <asp:Label ID="Label1" runat="server" CssClass="modalHeading" Text="Message"></asp:Label>
                    </div>
                    <br />
                    <asp:Label ID="lblStaticText" runat="server" Style="font-weight: 800" Text="Your Complaint is registered with UNIQ ID :-"></asp:Label>
                    <br />
                    <br />
                    <asp:Label ID="lblModelUID" runat="server" Style="font-weight: 1000" Text="UID"></asp:Label>
                    <br />
                    <br />
                    <br />
                    <asp:Button ID="btnUIDhide" runat="server" Text="Ok" CausesValidation="false" OnClick="btnUIDhide_Click" />
                    <asp:Button ID="hButton" runat="server" Style="display: none;" />
                    <br />
                </asp:Panel>
            </td>
        </tr>
    </table>


    <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
    </table>
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
    <asp:HiddenField ID="hdnAgent" runat="server" />
    <asp:HiddenField ID="hdnCallEntryId" runat="server" Value="0" />
    <asp:HiddenField ID="hdnUID" runat="server" />
    <asp:HiddenField ID="hdnDistricId" runat="server" />
    <asp:HiddenField ID="hdnULBId" runat="server" Value="0" />
    <asp:HiddenField ID="hdnZoneId" runat="server" Value="0" />
    <asp:HiddenField ID="hdnDiscomId" runat="server" Value="0" />
    <asp:HiddenField ID="hdnBuildingId" runat="server" Value="0" />
    <asp:HiddenField ID="hdnEsclatedId" runat="server" Value="0" />
    <asp:HiddenField ID="hdnAssignedToId" runat="server" Value="0" />
    <asp:HiddenField ID="hdnDistricText" runat="server" Value="" />
    <asp:HiddenField ID="hdnULBText" runat="server" Value="" />
    <asp:HiddenField ID="hdnZoneText" runat="server" Value="" />
    <asp:HiddenField ID="hdnDiscomText" runat="server" Value="" />
    <asp:HiddenField ID="hdnBuildingText" runat="server" Value="" />
    <asp:HiddenField ID="hdnEsclatedText" runat="server" Value="" />
    <asp:HiddenField ID="hdnAssignedToText" runat="server" Value="" />
    <asp:HiddenField ID="hdnBLVendors" runat="server" Value="" />
    <asp:HiddenField ID="hdnAGDSMAssignedToId" runat="server" Value="0" />
    <asp:HiddenField ID="hdnAGDSMAssignedToText" runat="server" Value="" />
</asp:Content>
