﻿using CRM.Models;
using System;
using System.Configuration;
using System.Data;
using System.Reflection;
using System.Web;

namespace CRM.View
{
    public partial class frmReports : UIBase
    {
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = string.Empty;
            if (!IsPostBack)
            {
                SQLDBHelper.PopulateDropDownList(ddlScheme, "Scheme", "Scheme", "SchemeId", false, "");
            }
        }

        private bool PopulateGrid(DateTime dFrom, DateTime dTo)
        {
            try
            {
                Reports rpt = new Reports();
                rpt.DateFrom = dFrom;
                rpt.DateUpTo = dTo;
                rpt.Scheme = Convert.ToString(ddlScheme.SelectedItem.Text.Trim());
                DataSet ds = new DataSet();
                string sFileName = string.Empty;

                rpt.UserId= UIManager.CurrentUserSession().UserId;

                //if (rdoInBoundMIS.Checked)
                if(ddlReportType.SelectedValue=="0")
                {
                    rpt.ReportType = "InBound";
                    sFileName = "InBoundMIS-" + System.DateTime.Now.ToString("dd_MM_yyyy") + ".xls";
                    ds = rpt.InBoundOutBoundMISReport();
                }
                else if (ddlReportType.SelectedValue == "1")
                {
                    rpt.ReportType = "OutBound";
                    sFileName = "OutBoundMIS-" + System.DateTime.Now.ToString("dd_MM_yyyy") + ".xls";
                    ds = rpt.InBoundOutBoundMISReport();
                }
                else if (ddlReportType.SelectedValue == "2")
                {
                    rpt.ReportType = "LoginLogout";
                    sFileName = "ProductivityReport-" + System.DateTime.Now.ToString("dd_MM_yyyy") + ".xls";
                    ds = rpt.LoginLogoutReport();
                }

                if (ds != null && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ExportDatatableToExcel(ds.Tables[0], sFileName);
                    }
                }

                return true;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                //Response.Redirect(AppBaseURL + "frmApplicationError.aspx", false);
                return true;
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            DateTime dFrom;
            DateTime dTo;
            if (DateTime.TryParseExact(txtFrom.Text, "dd-MM-yyyy", null, System.Globalization.DateTimeStyles.None, out dFrom) && DateTime.TryParseExact(txtTo.Text, "dd-MM-yyyy", null, System.Globalization.DateTimeStyles.None, out dTo))
            {
                if (!PopulateGrid(dFrom, dTo))
                { return; }
            }
            else
            {
                lblMessage.Text = "Invalid Date !";
                lblMessage.Focus();
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/View/frmReports.aspx");
        }

        public void ExportDatatableToExcel(DataTable table, string sFileName)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + sFileName);

            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR><BR><BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> <TR>");
            //am getting my grid's column headers
            int columnscount = table.Columns.Count;

            for (int j = 0; j < columnscount; j++)
            {      //write in new column
                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(table.Columns[j].ColumnName.ToString());
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (DataRow row in table.Rows)
            {//write in new row
                HttpContext.Current.Response.Write("<TR>");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    HttpContext.Current.Response.Write("<Td>");
                    HttpContext.Current.Response.Write(row[i].ToString());
                    HttpContext.Current.Response.Write("</Td>");
                }

                HttpContext.Current.Response.Write("</TR>");
            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }
    }
}