﻿using CRM.Models;
using System;
using System.Configuration;
using System.Data;
using System.Reflection;

namespace CRM.View
{
    public partial class frmConfigurationSetting : UIBase
    {
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = "";
            txtImageFolderPath.Focus();
            if (!IsPostBack)
            {
                SetUI();
            }
        }

        private void SetUI()
        {
            try
            {
                DataSet ds = new DataSet();
                ConfigurationSetting tConfigurationSetting = new ConfigurationSetting();
                ds = tConfigurationSetting.Select();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    txtImageFolderPath.Text = ds.Tables[0].Rows[0]["ImageFolderPath"].ToString();
                }
                else
                {
                    txtImageFolderPath.Text = "";
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                ConfigurationSetting tConfigurationSetting = new ConfigurationSetting();

                tConfigurationSetting.ImageFolderPath = txtImageFolderPath.Text.Trim();
                string tErrorMsg = string.Empty;
                if (tConfigurationSetting.Update(out tErrorMsg))
                {
                    lblMessage.Text = tErrorMsg;
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }
    }
}