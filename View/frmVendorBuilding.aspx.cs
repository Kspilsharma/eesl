﻿using CRM.Models;
using EESL.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EESL.View
{
    public partial class frmVendorBuilding : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!IsPostBack)
            {
                SetUI();
            }
        }

        protected void SetUI()
        {
            chklstVendors.Items.Clear();
            ddlState.Items.Clear();
            ddlDistrict.Items.Clear();
            ddlBuilding.Items.Clear();
            ddlDistrict.Items.Insert(0, new ListItem { Value = "0", Text = "--Select--" });
            ddlBuilding.Items.Insert(0, new ListItem { Value = "0", Text = "--Select--" });
            SQLDBHelper.PopulateDropDownList(ddlState, "State", "State", "StateId", false, "");            
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlDistrict.Items.Clear();
            if (ddlState.SelectedItem.Value != "--Select--")
            {
                String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                String strQuery = "select DistrictID, District from [District] where StateId = @StateId";
                using (SqlConnection con = new SqlConnection(strConnString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.AddWithValue("@StateId", ddlState.SelectedItem.Value);
                        cmd.CommandText = strQuery;
                        cmd.Connection = con;
                        con.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        ddlDistrict.DataSource = ds;
                        ddlDistrict.DataTextField = "District";
                        ddlDistrict.DataValueField = "DistrictID";
                        ddlDistrict.DataBind();
                        ddlDistrict.Items.Insert(0, new ListItem { Value = "0", Text = "--Select--" });

                        con.Close();
                    }
                }
            }
        }

        protected void ddlDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlBuilding.Items.Clear();
            if (ddlDistrict.SelectedItem.Value != "--Select--")
            {
                String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                String strQuery = "SELECT [BuildingMappingId], [Building] FROM [BuildingMapping] where [StateId]=@StateId and [DistrictId]=@DistrictId and [IsActive] = 1";

                using (SqlConnection con = new SqlConnection(strConnString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.AddWithValue("@StateId", ddlState.SelectedItem.Value);
                        cmd.Parameters.AddWithValue("@DistrictId", ddlDistrict.SelectedItem.Value);
                        cmd.CommandText = strQuery;
                        cmd.Connection = con;
                        con.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        ddlBuilding.DataSource = ds;
                        ddlBuilding.DataTextField = "Building";
                        ddlBuilding.DataValueField = "BuildingMappingId";
                        ddlBuilding.DataBind();
                        ddlBuilding.Items.Insert(0, new ListItem { Value = "0", Text = "--Select--" });

                        con.Close();
                    }
                }
            }

        }

        protected void ddlBuilding_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlBuilding.SelectedItem.Value != "0")
            {
                String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

                String strQuery = "SELECT[VendorMappingId], [VendorName]+' ( '+VT.VendorType+' Vendor )' as VendorName, "
                                + " CASE WHEN EXISTS(SELECT 1 FROM[BuildingVendorMapping] WHERE [VendorMappingId]= VM.[VendorMappingId] and BuildingMappingId = @BuildingMappingId) THEN 1 ELSE 0 END as BuildingVendors "
                                + " FROM[VendorMapping] VM join VendorType VT on VT.VendorTypeValue=VM.VendorType and VT.SchemeId= VM.SchemeId "
                                + " Where[StateId]= @StateId and[DistrictId]= @DistrictId and[IsActive]= 1 and VM.[SchemeId]= 4 ";

                chklstVendors.Items.Clear();

                using (SqlConnection con = new SqlConnection(strConnString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.AddWithValue("@StateId", ddlState.SelectedItem.Value);
                        cmd.Parameters.AddWithValue("@DistrictId", ddlDistrict.SelectedItem.Value);
                        cmd.Parameters.AddWithValue("@BuildingMappingId", ddlBuilding.SelectedItem.Value);
                        cmd.CommandText = strQuery;
                        cmd.Connection = con;
                        con.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            ListItem item = new ListItem();
                            item.Text = row["VendorName"].ToString();
                            item.Value = row["VendorMappingId"].ToString();
                            if (row["BuildingVendors"].ToString() == "1")
                            {
                                item.Selected = true;
                                item.Enabled = false;
                            }
                            else
                            {
                                item.Enabled = true;
                            }
                            chklstVendors.Items.Add(item);
                        }
                        con.Close();
                    }
                }
            }
            else
            {
                chklstVendors.Items.Clear();
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string msgitem = string.Empty;
            VendorBuildingProducts products = new VendorBuildingProducts();

            foreach (ListItem item in chklstVendors.Items)
            {
                if (item.Selected == true && item.Enabled==true)
                {
                    products.VendorMappingId = Convert.ToInt32(item.Value);
                    products.BuildingMappingId = Convert.ToInt32(ddlBuilding.SelectedItem.Value);
                    products.VendorBuildingInsert(out msgitem);
                }
            }
            SetUI();
            lblMsg.Text = msgitem;
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            SetUI();
        }
    }
}