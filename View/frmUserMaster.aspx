﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="frmUserMaster.aspx.cs" Inherits="CRM.View.frmUserMaster" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.min.js"></script>
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">

        function roleChange() {

            if ($('#<%=ddlRole.ClientID%>').find('option:selected').text() == "Agent") {
                $('#<%=trAgentType.ClientID%>').show();
                $('#<%=trVendor.ClientID%>').hide();
            }
            else if ($('#<%=ddlRole.ClientID%>').find('option:selected').text() == "Vendor") {
                $('#<%=trVendor.ClientID%>').show();
                $('#<%=trAgentType.ClientID%>').hide();
            }
            else {
                $('#<%=trVendor.ClientID%>').hide();
                $('#<%=trAgentType.ClientID%>').hide();
            }
    }
    </script>
    <style>
        .CheckboxAllState {
            padding-left: 4px;
        }
    </style>
    <script type="text/javascript">
        function CheckAll() {
            var intIndex = 0;
            var rowCount = document.getElementById('MainContent_chkListStates').getElementsByTagName("input").length;
            for (i = 0; i < rowCount; i++) {
                if (document.getElementById('MainContent_chkSelectAllStates').checked == true) {
                    if (document.getElementById("MainContent_chkListStates" + "_" + i)) {
                        if (document.getElementById("MainContent_chkListStates" + "_" + i).disabled != true)
                            document.getElementById("MainContent_chkListStates" + "_" + i).checked = true;
                    }
                }
                else {
                    if (document.getElementById("MainContent_chkListStates" + "_" + i)) {
                        if (document.getElementById("MainContent_chkListStates" + "_" + i).disabled != true)
                            document.getElementById("MainContent_chkListStates" + "_" + i).checked = false;
                    }
                }
            }
        }

        function UnCheckAll() {
            var intIndex = 0;
            var flag = 0;
            var rowCount = document.getElementById('MainContent_chkListStates').getElementsByTagName("input").length;
            for (i = 0; i < rowCount; i++) {
                if (document.getElementById("MainContent_chkListStates" + "_" + i)) {
                    if (document.getElementById("MainContent_chkListStates" + "_" + i).checked == true) {
                        flag = 1;
                    }
                    else {
                        flag = 0;
                        break;
                    }
                }
            }
            if (flag == 0)
                document.getElementById('MainContent_chkSelectAllStates').checked = false;
            else
                document.getElementById('MainContent_chkSelectAllStates').checked = true;

        }

        function ValidateModuleList(source, args) {
            var chkListModules = document.getElementById('<%= chkListStates.ClientID %>');
            var chkListinputs = chkListModules.getElementsByTagName("input");
            for (var i = 0; i < chkListinputs.length; i++) {
                if (chkListinputs[i].checked) {
                    args.IsValid = true;
                    return;
                }
            }
            alert('Please select atleast one State');
            args.IsValid = false;
        }

        function CheckAllSchemes() {
            var intIndex = 0;
            var rowCount = document.getElementById('MainContent_chkListSchemes').getElementsByTagName("input").length;
            for (i = 0; i < rowCount; i++) {
                if (document.getElementById('MainContent_chkSelectAllSchemes').checked == true) {
                    if (document.getElementById("MainContent_chkListSchemes" + "_" + i)) {
                        if (document.getElementById("MainContent_chkListSchemes" + "_" + i).disabled != true)
                            document.getElementById("MainContent_chkListSchemes" + "_" + i).checked = true;
                    }
                }
                else {
                    if (document.getElementById("MainContent_chkListSchemes" + "_" + i)) {
                        if (document.getElementById("MainContent_chkListSchemes" + "_" + i).disabled != true)
                            document.getElementById("MainContent_chkListSchemes" + "_" + i).checked = false;
                    }
                }
            }
        }

        function UnCheckAllSchemes() {
            var intIndex = 0;
            var flag = 0;
            var rowCount = document.getElementById('MainContent_chkListSchemes').getElementsByTagName("input").length;
            for (i = 0; i < rowCount; i++) {
                if (document.getElementById("MainContent_chkListSchemes" + "_" + i)) {
                    if (document.getElementById("MainContent_chkListSchemes" + "_" + i).checked == true) {
                        flag = 1;
                    }
                    else {
                        flag = 0;
                        break;
                    }
                }
            }
            if (flag == 0)
                document.getElementById('MainContent_chkListSchemes').checked = false;
            else
                document.getElementById('MainContent_chkListSchemes').checked = true;

        }

        function ValidateModuleScheme(source, args) {
            var chkListModules = document.getElementById('<%= chkListSchemes.ClientID %>');
            var chkListinputs = chkListModules.getElementsByTagName("input");
            for (var i = 0; i < chkListinputs.length; i++) {
                if (chkListinputs[i].checked) {
                    args.IsValid = true;
                    return;
                }
            }
            alert('Please select atleast one Scheme');
            args.IsValid = false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="Heading" align="center" style="color: #101010">
                <asp:Label ID="lblPageHeader" runat="server" Text="User Master" CssClass="bold"></asp:Label>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
        <tr>
            <td align="left" valign="top" width="15%"></td>
            <td valign="top" width="70%">
                <fieldset class="fieldset">
                    <legend class="Legendheading" align="left">User Master</legend>
                    <table width="100%">
                        <tr>
                            <td valign="top" style="width: 100px;">
                                <table cellpadding="0" cellspacing="3px" width="100%" style="margin: 0px; padding: 0px;">
                                    <tr>
                                        <td colspan="2" align="left" valign="top">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="failureNotification"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 80px;" align="left" class="tdlabel" valign="top">
                                            <asp:Label ID="lblRole" runat="server" Text="Role"></asp:Label>
                                        </td>
                                        <td valign="top" align="left" class="tddata">
                                            <asp:DropDownList ID="ddlRole" runat="server" TabIndex="101" Width="155px" onChange="roleChange();">
                                            </asp:DropDownList>
                                            <span>
                                                <asp:RequiredFieldValidator ID="RFVUSERTYPE" runat="server" ControlToValidate="ddlRole"
                                                    ForeColor="Red" InitialValue="Please-Select" ErrorMessage="*"></asp:RequiredFieldValidator>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr id="trAgentType" runat="server">
                                        <td align="left" class="tdlabel" valign="top">Agent Type</td>
                                        <td valign="top" align="left" class="tddata">
                                            <asp:DropDownList ID="ddlAgentType" runat="server" TabIndex="101" Width="155px">
                                                <asp:ListItem Selected="True" Value="1">InBound</asp:ListItem>
                                                <asp:ListItem Value="2">OutBound</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr id="trVendor" runat="server">
                                        <td align="left" class="tdlabel" valign="top">Vendor<span style="color: red">&nbsp;*</span> &nbsp;:</td>
                                        <td valign="top" align="left" class="tddata">
                                            <asp:DropDownList ID="ddlVendor" runat="server" Width="75%" TabIndex="104" />
                                            &nbsp;<asp:RequiredFieldValidator ID="rfvVendor" ForeColor="Maroon" ControlToValidate="ddlVendor" InitialValue="--Select--" runat="server" ErrorMessage="Select Vendor." Display="Dynamic" ValidationGroup="Save" Font-Bold="True" Font-Size="X-Small"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="tdlabel" valign="top">
                                            <asp:Label ID="lblUserName" runat="server" Text="User Name"></asp:Label>
                                        </td>
                                        <td valign="top" align="left" class="tddata">
                                            <asp:TextBox ID="txtUserName" runat="server" TabIndex="102" Style="text-transform: none"
                                                MaxLength="20" Width="150px"></asp:TextBox>
                                            <span>
                                                <asp:RequiredFieldValidator ID="rfvUserName" runat="server" ControlToValidate="txtUserName"
                                                    ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                                &nbsp;<asp:RegularExpressionValidator ID="revUserName" runat="server" ControlToValidate="txtUserName"
                                                    ErrorMessage="Minimum 3 characters required. !" ValidationExpression="^[a-zA-Z0-9]{3,20}$"
                                                    CssClass="failureNotification"></asp:RegularExpressionValidator>
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="tdlabel" valign="top">
                                            <asp:Label ID="lblPassword" runat="server" Text="Password"></asp:Label>
                                        </td>
                                        <td valign="top" align="left" class="tddata">
                                            <asp:TextBox ID="txtPassword" runat="server" onblur="return passwordLenCheck();"
                                                TabIndex="103" Style="text-transform: none" MaxLength="20" Width="150px"></asp:TextBox>
                                            <span>
                                                <asp:RequiredFieldValidator ID="rfvPassword" runat="server" ControlToValidate="txtPassword"
                                                    ForeColor="Red" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revPassword" runat="server" ErrorMessage="Password length must be between 6 to 20 characters.!"
                                                    ControlToValidate="txtPassword" CssClass="failureNotification" ValidationExpression="^[a-zA-Z0-9'@&#._\s]{6,12}$" />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">&nbsp;
                                        </td>
                                        <td align="left" class="tddata">
                                            <asp:Button ID="btnSubmit" Text="Submit" TabIndex="104" runat="server" OnClick="btnSubmit_Click" />
                                            <asp:Button ID="btnReset" Text="Reset" TabIndex="105" runat="server" OnClick="btnReset_Click" CausesValidation="False" />
                                            <br />
                                            <asp:HiddenField ID="hdnDistricId" runat="server" Value="0" />
                                            <asp:HiddenField ID="hdnULBMappingId" runat="server" Value="0" />
                                            <asp:HiddenField ID="hdnZoneMappingId" runat="server" Value="0" />
                                            <asp:HiddenField ID="hdnVendorMappingId" runat="server" Value="0" />
                                            <asp:HiddenField ID="hdnId" runat="server" Value="-1" />
                                            <asp:ScriptManager ID="ScriptManager1" runat="server">
                                            </asp:ScriptManager>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left">&nbsp;</td>
                                        <td align="left" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" align="left">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" style="width: 150px;">
                                <table width="100%">
                                    <tr>
                                        <td>States</td>
                                        <td>
                                            <asp:CheckBox runat="server" ID="chkSelectAllStates" onclick="CheckAll();" CssClass="CheckboxAllState" Text="Select All States" />
                                            <div style="height: 150px; border-style: solid; border-width: 1px; border-color: #CCCCCC; overflow: auto; width: 250px;">
                                                <asp:CheckBoxList runat="server" ID="chkListStates" onclick="UnCheckAll();">
                                                </asp:CheckBoxList>
                                                <asp:CustomValidator runat="server" ID="cvmodulelist" ClientValidationFunction="ValidateModuleList" ErrorMessage=""></asp:CustomValidator>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top" style="width: 100px;">
                                <table width="100%">
                                    <tr>
                                        <td>Schemes</td>
                                        <td>
                                            <asp:CheckBox ID="chkSelectAllSchemes" runat="server" onclick="CheckAllSchemes();" CssClass="CheckboxAllState" Text="Select All Schemes" />
                                            <div style="height: 150px; border-style: solid; border-width: 1px; border-color: #CCCCCC; overflow: auto; width: 250px;">
                                                <asp:CheckBoxList ID="chkListSchemes" runat="server" onclick="UnCheckAllSchemes();">
                                                </asp:CheckBoxList>
                                                <asp:CustomValidator runat="server" ID="cvmodulescheme" ClientValidationFunction="ValidateModuleScheme" ErrorMessage=""></asp:CustomValidator>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>

                </fieldset>
            </td>
            <td align="left" valign="top" width="15%"></td>
        </tr>
        <tr>
            <td align="left" valign="top" width="15%"></td>
            <td valign="top" width="70%">
                <table border="0" width="100%">
                    <tr>
                        <td align="center">
                            <asp:GridView ID="gvUser" PageSize="100" TabIndex="106" runat="server" Width="100%"
                                AutoGenerateColumns="False" DataKeyNames="UserId" OnPageIndexChanging="gvUser_PageIndexChanging"
                                OnRowDataBound="gvUser_RowDataBound" OnRowDeleting="gvUser_RowDeleting"
                                OnRowEditing="gvUser_RowEditing" CellPadding="4" ForeColor="#333333"
                                GridLines="None" OnRowUpdating="gvUser_RowUpdating" class="tabStyle">
                                <AlternatingRowStyle BackColor="" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr.">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Role">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblRoleName" runat="server" Text='<%# Bind("RoleName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Agent Type">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblAgentType" runat="server" Text='<%# Bind("AgentType") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblVendor" runat="server" Text='<%# Bind("VendorName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Name">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblUserName" runat="server" Text='<%# Bind("UserName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Password">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblPassword" runat="server" Text='<%# Bind("Password") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnEdit" CommandName="Edit" runat="server" ImageUrl="~/Images/edit.png"
                                                OnClientClick="return confirm('Are you sure! You want to Edit Seleted Row?');" CausesValidation="False" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnStatus" CommandName="Update" runat="server" ImageUrl="~/Images/active.gif"
                                                OnClientClick="return confirm('Are you sure! You want to change status?');" CausesValidation="False" />
                                            <asp:Label ID="gvlblIsActive" runat="server" Text='<%# Bind("IsActive") %>' Visible="False" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete" Visible="false">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnDelete" CommandName="Delete" runat="server" ImageUrl="~/Images/delete.gif"
                                                OnClientClick="return confirm('Are you sure! You want to Delete Seleted Row?');"
                                                Enabled='<%# (Convert.ToString(Eval("UserName"))=="admin" ? false : true) %>' CausesValidation="False" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle BackColor="#2461BF" />
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="Black" HorizontalAlign="Center" />
                                <RowStyle HorizontalAlign="Center" VerticalAlign="Top" BackColor="" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
            <td align="left" valign="top" width="15%"></td>
        </tr>
    </table>
</asp:Content>
