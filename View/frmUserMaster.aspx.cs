﻿using CRM.Models;
using System;
using System.Configuration;
using System.Data;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CRM.View
{
    public partial class frmUserMaster : UIBase
    {
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!IsPostBack)
            { SetUI(); }
        }
        protected void SetUI()
        {
            hdnId.Value = "-1";
            ddlRole.Focus();
            txtUserName.Text = "";
            txtPassword.Text = "";
            FillRoleddl();
            FillVendorddl();
            FillGrid();
            ddlRole.Focus();
            trAgentType.Style.Add("display", "none");
            trVendor.Style.Add("display", "none");
            FillStates();
            FillSchemes();
        }

        private void FillStates()
        {
            UserMaster tUserMaster = new UserMaster();
            DataSet dsStates = tUserMaster.GetStates();
            chkListStates.DataTextField = "State";
            chkListStates.DataValueField = "StateId";
            chkListStates.DataSource = dsStates;
            chkListStates.DataBind();
        }

        private void FillSchemes()
        {
            DataTable dt = SQLDBHelper.GetDataInTable("Select SchemeId, Scheme from Scheme");
            chkListSchemes.DataTextField = "Scheme";
            chkListSchemes.DataValueField = "SchemeId";
            chkListSchemes.DataSource = dt;
            chkListSchemes.DataBind();
        }

        protected void FillRoleddl()
        {
            try
            {
                RoleMaster tRoleMaster = new RoleMaster();
                DataSet ds = new DataSet();
                ddlRole.Items.Clear();
                ds = tRoleMaster.Select();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlRole.DataValueField = "RoleId";
                    ddlRole.DataTextField = "RoleName";
                    ddlRole.DataSource = ds;
                    ddlRole.DataBind();
                    ddlRole.Items.Insert(0, "Please-Select");
                }
                else
                {
                    ddlRole.Items.Insert(0, "Please-Select");
                }

                
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }
        protected void FillVendorddl()
        {
            try
            {
                Mapping tMapping = new Mapping();
                DataSet ds = new DataSet();
                ds = tMapping.VendorMappingSelect();
                ddlVendor.Items.Clear();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlVendor.DataValueField = "VendorMappingId";
                    ddlVendor.DataTextField = "VendorDetail";
                    ddlVendor.DataSource = ds;
                    ddlVendor.DataBind();
                    ddlVendor.Items.Insert(0, "Please-Select");
                }
                else
                {
                    ddlVendor.Items.Insert(0, "Please-Select");
                }
                
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }
        protected void FillGrid()
        {
            try
            {
                UserMaster tUserMaster = new UserMaster();
                DataSet ds = new DataSet();
                ds = tUserMaster.Select();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvUser.DataSource = ds.Tables[0];
                    gvUser.DataBind();
                    gvUser.Visible = true;
                }
                else
                {
                    gvUser.Visible = false;
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                UserMaster tUserMaster = new UserMaster();
                DataSet ds = new DataSet();

                tUserMaster.RoleId = Convert.ToInt32(ddlRole.SelectedItem.Value);
                if (ddlRole.SelectedItem.Text == "Agent")
                {
                    tUserMaster.AgentTypeId = Convert.ToInt32(ddlAgentType.SelectedItem.Value);
                }
                else
                {
                    tUserMaster.AgentTypeId = 0;
                }

                if (ddlRole.SelectedItem.Text == "Vendor")
                {
                    tUserMaster.VendorId = Convert.ToInt32(ddlVendor.SelectedItem.Value);
                }
                else
                {
                    tUserMaster.VendorId = 0;
                }

                tUserMaster.UserName = Convert.ToString(txtUserName.Text.Trim());
                tUserMaster.Password = Convert.ToString(txtPassword.Text.Trim());
                tUserMaster.LoginId = UIManager.CurrentUserSession().UserId;
                string tErrorMsg = string.Empty;
                if (hdnId.Value == "-1")
                {
                    if (tUserMaster.Insert(out tErrorMsg))
                    {
                        tUserMaster.UserId = tUserMaster.GetUserId();
                        InsertUserStates(tUserMaster);
                        InsertUserSchemes(tUserMaster);
                        SetUI();
                    }
                }
                else
                {
                    tUserMaster.UserId = Convert.ToInt32(hdnId.Value);
                    if (tUserMaster.Update(out tErrorMsg))
                    {
                        InsertUserStates(tUserMaster);
                        InsertUserSchemes(tUserMaster);
                        SetUI();
                    }
                }

                lblMsg.Text = tErrorMsg;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        private void InsertUserStates(UserMaster tUserMaster)
        {
            tUserMaster.DeleteUserState();
            foreach (ListItem item in chkListStates.Items)
            {
                if (item.Selected)
                {
                    tUserMaster.StateId = Convert.ToInt32(item.Value);
                    tUserMaster.InsertUserState();
                }
            }
        }

        private void InsertUserSchemes(UserMaster tUserMaster)
        {
            tUserMaster.DeleteUserScheme();
            foreach (ListItem item in chkListSchemes.Items)
            {
                if (item.Selected)
                {
                    tUserMaster.SchemeId = Convert.ToInt32(item.Value);
                    tUserMaster.InsertUserScheme();
                }
            }            
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            SetUI();
        }
        protected void gvUser_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                UserMaster tUserMaster = new UserMaster();
                DataSet ds = new DataSet();
                Label lblstatus = ((Label)gvUser.Rows[e.NewEditIndex].FindControl("gvlblIsActive"));
                string strIsActive = lblstatus.Text;
                hdnId.Value = (gvUser.DataKeys[e.NewEditIndex].Value.ToString());

                tUserMaster.IsActive = Convert.ToBoolean(strIsActive);
                tUserMaster.UserId = Convert.ToInt32(hdnId.Value);
                ds = tUserMaster.Select();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    txtUserName.Text = ds.Tables[0].Rows[0]["UserName"].ToString();
                    txtPassword.Text = ds.Tables[0].Rows[0]["Password"].ToString();
                    ddlRole.SelectedIndex = -1;
                    ddlRole.Items.FindByValue(ds.Tables[0].Rows[0]["RoleId"].ToString()).Selected = true;
                    if (ddlRole.SelectedItem.Text == "Agent")
                    {
                        trAgentType.Attributes.Remove("display");
                        trAgentType.Style["display"] = "";
                        trVendor.Style.Add("display", "none");
                        ddlAgentType.SelectedIndex = -1;
                        ddlAgentType.Items.FindByValue(ds.Tables[0].Rows[0]["AgentTypeId"].ToString()).Selected = true;
                    }
                    if (ddlRole.SelectedItem.Text == "Vendor")
                    {
                        trVendor.Attributes.Remove("display");
                        trVendor.Style["display"] = "";
                        trAgentType.Style.Add("display", "none");
                        ddlVendor.SelectedIndex = -1;
                        ddlVendor.Items.FindByValue(ds.Tables[0].Rows[0]["VendorId"].ToString()).Selected = true;
                    }
                    else
                    {
                        trAgentType.Style.Add("display", "none");
                        trVendor.Style.Add("display", "none");
                        txtUserName.Focus();
                    }

                    UserStates(tUserMaster.UserId);
                    UserSchemes(tUserMaster.UserId);
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        private void UserStates(int userid)
        {
            DataSet dsUserStates = new DataSet();
            UserMaster tUserMaster = new UserMaster();
            tUserMaster.UserId = userid;

            dsUserStates = tUserMaster.GetUserStates();
            foreach(DataRow row in dsUserStates.Tables[0].Rows)
            {
                chkListStates.Items.FindByValue(row["StateId"].ToString()).Selected = true;
            }
        }

        private void UserSchemes(int userid)
        {
            DataSet dsUserSchemes = new DataSet();
            UserMaster tUserMaster = new UserMaster();
            tUserMaster.UserId = userid;

            dsUserSchemes = tUserMaster.GetUserSchemes();
            foreach (DataRow row in dsUserSchemes.Tables[0].Rows)
            {
                chkListSchemes.Items.FindByValue(row["SchemeId"].ToString()).Selected = true;
            }
        }

        protected void gvUser_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            UserMaster tUserMaster = new UserMaster();
            int userId = Convert.ToInt32(gvUser.DataKeys[e.RowIndex].Value.ToString());
            Label lblstatus = ((Label)gvUser.Rows[e.RowIndex].FindControl("gvlblIsActive"));
            string strIsActive = lblstatus.Text;
            try
            {
                tUserMaster.IsActive = Convert.ToBoolean(strIsActive);
                tUserMaster.UserId = userId;
                string tErrorMsg = string.Empty;
                if (tUserMaster.Delete(out tErrorMsg))
                {
                    SetUI();
                }

                lblMsg.Text = tErrorMsg;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }
        protected void gvUser_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvUser.PageIndex = e.NewPageIndex;
            FillGrid();
        }
        protected void gvUser_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string STRIsActive = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "IsActive"));

                    ImageButton imgbtnstatus = (ImageButton)e.Row.FindControl("imgbtnStatus");
                    if (STRIsActive == "False")
                    {
                        imgbtnstatus.ImageUrl = "~/Images/block.Jpg";
                        imgbtnstatus.ToolTip = "Click to Active";
                    }
                    else
                    {
                        imgbtnstatus.ImageUrl = "~/Images/active.gif";
                        imgbtnstatus.ToolTip = "Click to Block";
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }
        protected void gvUser_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                UserMaster tUserMaster = new UserMaster();
                tUserMaster.UserId = Convert.ToInt32(gvUser.DataKeys[e.RowIndex].Value.ToString());
                Label lblstatus = ((Label)gvUser.Rows[e.RowIndex].FindControl("gvlblIsActive"));
                string strIsActive = lblstatus.Text;
                if (strIsActive == "False")
                {
                    tUserMaster.IsActive = true;
                }
                else
                {
                    tUserMaster.IsActive = false;
                }
                string tErrorMsg = string.Empty;
                if (tUserMaster.StatusUpdate(out tErrorMsg))
                {
                    FillGrid();
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }
    }
}