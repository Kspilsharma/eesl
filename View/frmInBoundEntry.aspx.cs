﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CRM.Models;
using System.Reflection;
using System.Configuration;
using System.Collections;
using System.Data.SqlClient;
using System.IO;
using System.Web.Services;
using System.Web.Script.Serialization;
using System.Text;
using System.Net.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CRM.View
{
    public partial class frmInBoundEntry : UIBase
    {
        private Object thisLock = new Object();
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();
        string ariaUID = string.Empty;
        DataSet dsNOEmailSMS = new DataSet();
        DataSet dsVendorEmailSMS = new DataSet();

        protected void Page_Load(object sender, EventArgs e)
        {
            string ariaCallerId = string.Empty;
            string ariaUniqueId = string.Empty;
            string ariaAgentId = string.Empty;
            string ariaAgentPhone = string.Empty;
            string ariaVoiceFile = string.Empty;

            if (!IsPostBack)
            {
                PopulateUI();
                ariaCallerId = Convert.ToString(Request.QueryString["cid"]);
                ariaUniqueId = Convert.ToString(Request.QueryString["uid"]);
                ariaAgentId = Convert.ToString(Request.QueryString["aid"]);
                ariaAgentPhone = Convert.ToString(Request.QueryString["ap"]);
                ariaVoiceFile = Convert.ToString(Request.QueryString["vfn"]);
                ariaUID = Convert.ToString(Request.QueryString["UniqueID"]);

                if (!string.IsNullOrEmpty(ariaCallerId))
                {
                    LightTypeTr.Visible = false;
                    if (!string.IsNullOrEmpty(ariaUID))
                    {
                        txtNumberOfCaller.Text = ariaCallerId;
                        hdnUID.Value = ariaUID;
                        FillExistingCallDetailByUID(ariaUID);
                    }
                    else
                    {
                        txtNumberOfCaller.Text = ariaCallerId;
                        hdnAgent.Value = ariaAgentId;
                        hdnUID.Value = ariaUID;
                        FillExistingCallDetailByMobileNo(ariaCallerId);
                    }

                    if (ddlScheme.SelectedValue == "4")
                    {
                        txtAddress.Enabled = false;
                        txtLandmark.Enabled = false;
                    }
                    else if (ddlScheme.SelectedValue == "1")
                    {
                        LightTypeTr.Visible = true;
                    }

                    if (Session["IsBackToPage"] != null && IsPostBack == false)
                    {
                        if (Session["IsBackToPage"].ToString() == "YES")
                        {
                            if (Session["EntryView"] == null)
                            {
                                if (Session["PreviousUrl"] == null)
                                    Session["PreviousUrl"] = Request.UrlReferrer.AbsoluteUri;
                            }
                            imgbtnBack.Visible = true;
                        }
                        else
                        {
                            imgbtnBack.Visible = false;
                        }
                    }
                }
            }

            
        }

        private void PopulateUI()
        {
            txtNumberOfCaller.Focus();
            txtNumberOfCaller.Text = "";
            txtNameOfCaller.Text = "";
            txtAddress.Text = "";
            txtLandmark.Text = "";
            txtRemark.Text = "";
            txtSLWardNo.Text = "";
            txtSLPoleNo.Text = "";
            txtUJEmail.Text = "";
            txtUJTown.Text = "";
            txtUJConsumerNo.Text = "";
            txtAGDSMUSCORServiceConnNo.Text = "";
            txtAGDSMPanelBoardMobNo.Text = "";
            txtAGDSMHPkWRating.Text = "5";
            txtAGDSMTown.Text = "";
            txtAGDSMDistributionCenter.Text = "";
            txtAGDSMConsumerNo.Text = "";
            txtBLFlatNo.Text = "";
            txtBLReferenceNo.Text = "";
            lblUID.Text = "";

            SQLDBHelper.PopulateDropDownList(ddlState, "State", "State", "StateId", false, "");
            SQLDBHelper.PopulateDropDownList(ddlScheme, "Scheme", "Scheme", "SchemeId", false, "");
            SQLDBHelper.PopulateDropDownList(ddlSource, "Source", "Source", "SourceId", false, "");
            SQLDBHelper.PopulateDropDownList(ddlCategoryOfCall, "CallCategory", "CallCategory", "CallCategoryId", false, "");
            SQLDBHelper.PopulateDropDownList(ddlLanguage, "Languages", "Language", "LanguageId", false, "");
            SQLDBHelper.PopulateDropDownList(ddlStatusOfCall, "StatusOfCall", "StatusOfCall", "StatusOfCallId", false, "");
            SQLDBHelper.PopulateDropDownList(ddlSLTicketType, "TicketType", "TicketType", "TicketTypeId", false, "");
            SQLDBHelper.PopulateDropDownList(ddlSLComplaintFrom, "ComplaintFrom", "ComplaintFrom", "ComplaintFromId", false, "");
            SQLDBHelper.PopulateCheckboxList(chklstUJProducts, "Product", "Product", "ProductId", false, "");
            SQLDBHelper.PopulateDataList(dlBLProductTypes, "ProductType", "ProductType", "ProductTypeId", false, "");

            fsStreetLight.Style.Add("display", "none");
            fsUjala.Style.Add("display", "none");
            fsAGDSM.Style.Add("display", "none");
            fsBuilding.Style.Add("display", "none");
            trEscalate.Style.Add("display", "none");
            trUID.Visible = false;

            AGDSMTr_USCNo0.Style.Add("display", "none");
            AGDSMTr_USCNo.Style.Add("display", "none");
            AGDSMTr_PanelBoradMobNo0.Style.Add("display", "none");
            AGDSMTr_PanelBoradMobNo.Style.Add("display", "none");

            //ddlStatusOfCall.SelectedIndex = 1;
            ddlStatusOfCall.SelectedIndex = -1;
            ddlStatusOfCall.Items.FindByValue(Convert.ToString(1)).Selected = true;

            //ddlSource.SelectedIndex = 1;
            ddlSource.SelectedIndex = -1;
            ddlSource.Items.FindByValue(Convert.ToString(1)).Selected = true;

            ddlAGDSMReasonOfComplaint.SelectedIndex = -1;

            hdnAgent.Value = string.Empty;
            hdnCallEntryId.Value = "0";
            hdnUID.Value = "0";
            hdnDistricId.Value = "0";
            hdnDistricText.Value = string.Empty;
            hdnULBId.Value = "0";
            hdnULBText.Value = string.Empty;
            hdnZoneId.Value = "0";
            hdnZoneText.Value = string.Empty;
            hdnDiscomId.Value = "0";
            hdnDiscomText.Value = string.Empty;
            hdnBuildingId.Value = "0";
            hdnBuildingText.Value = string.Empty;
            hdnEsclatedId.Value = "0";
            hdnEsclatedText.Value = string.Empty;
            hdnAssignedToId.Value = "0";
            hdnAssignedToText.Value = string.Empty;
            hdnBLVendors.Value = string.Empty;
            hdnAGDSMAssignedToId.Value = "0";
            hdnAGDSMAssignedToText.Value = string.Empty;

            EnableControls();
        }

        protected void dlBLProductTypes_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            if ((e.Item.ItemType == ListItemType.Item) || (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                TextBox txt = (TextBox)e.Item.FindControl("txtProductTypeQuantity");
                txt.Text = "1";
                txt.Style.Add("display", "none");
            }
        }

        private void ResetUI()
        {
            txtNumberOfCaller.Text = "";
            btnNewComplain.Visible = false;
            txtNameOfCaller.Text = "";
            txtAddress.Text = "";
            txtLandmark.Text = "";
            txtRemark.Text = "";
            txtSLWardNo.Text = "";
            txtSLPoleNo.Text = "";
            txtUJEmail.Text = "";
            txtUJTown.Text = "";
            txtUJConsumerNo.Text = "";
            txtAGDSMUSCORServiceConnNo.Text = "";
            txtAGDSMPanelBoardMobNo.Text = "";
            txtAGDSMHPkWRating.Text = "5";
            txtAGDSMTown.Text = "";
            txtAGDSMDistributionCenter.Text = "";
            txtAGDSMConsumerNo.Text = "";
            txtBLFlatNo.Text = "";
            txtBLReferenceNo.Text = "";
            lblUID.Text = "";

            ddlState.SelectedIndex = 0;
            ddlDistrict.Items.Clear();
            ddlScheme.SelectedIndex = 0;

            //ddlSource.SelectedIndex = 1;
            ddlSource.SelectedIndex = -1;
            ddlSource.Items.FindByValue(Convert.ToString(1)).Selected = true;

            ddlCategoryOfCall.SelectedIndex = 0;
            ddlLanguage.SelectedIndex = 0;

            ddlStatusOfCall.SelectedIndex = -1;
            ddlStatusOfCall.Items.FindByValue(Convert.ToString(1)).Selected = true;

            ddlSLTicketType.SelectedIndex = 0;
            ddlSLAssignedTo.Items.Clear();
            ddlSLComplaintFrom.SelectedIndex = 0;

            ddlSLAreaType.SelectedIndex = -1;
            ddlSLAreaType.SelectedIndex = 0;

            ddlSLLightType.SelectedIndex = -1;
            ddlSLLightType.SelectedIndex = 0;

            ddlAGDSMReasonOfComplaint.SelectedIndex = -1;

            foreach (DataListItem item in dlBLProductTypes.Items)
            {
                CheckBox chk = (CheckBox)item.FindControl("chkProductType");
                chk.Checked = false;

                TextBox txt = (TextBox)item.FindControl("txtProductTypeQuantity");
                txt.Text = "1";
                txt.Style.Add("display", "none");
            }

            foreach (ListItem item in chklstUJProducts.Items)
            {
                item.Selected = false;
            }

            fsStreetLight.Style.Add("display", "none");
            fsUjala.Style.Add("display", "none");
            fsAGDSM.Style.Add("display", "none");
            fsBuilding.Style.Add("display", "none");
            trEscalate.Style.Add("display", "none");
            trUID.Visible = false;

            AGDSMTr_USCNo0.Style.Add("display", "none");
            AGDSMTr_USCNo.Style.Add("display", "none");
            AGDSMTr_PanelBoradMobNo0.Style.Add("display", "none");
            AGDSMTr_PanelBoradMobNo.Style.Add("display", "none");

            hdnAgent.Value = string.Empty;
            hdnCallEntryId.Value = "0";
            hdnUID.Value = "0";
            hdnDistricId.Value = "0";
            hdnDistricText.Value = string.Empty;
            hdnULBId.Value = "0";
            hdnULBText.Value = string.Empty;
            hdnZoneId.Value = "0";
            hdnZoneText.Value = string.Empty;
            hdnDiscomId.Value = "0";
            hdnDiscomText.Value = string.Empty;
            hdnBuildingId.Value = "0";
            hdnBuildingText.Value = string.Empty;
            hdnEsclatedId.Value = "0";
            hdnEsclatedText.Value = string.Empty;
            hdnAssignedToId.Value = "0";
            hdnAssignedToText.Value = string.Empty;
            hdnBLVendors.Value = string.Empty;
            hdnAGDSMAssignedToId.Value = "0";
            hdnAGDSMAssignedToText.Value = string.Empty;

            gvCallDetailHistoryCurrent.Visible = false;
            gvCalllDetail.Visible = false;
            gvCallDetailHistory.Visible = false;
            gvBLCallAssignedHistory.Visible = false;

            ddlStatusOfCall.Enabled = true;

            txtNumberOfCaller.Focus();
            EnableControls();
        }

        private void EnableControls()
        {
            txtNumberOfCaller.Enabled = true;
            ddlState.Enabled = true;
            ddlScheme.Enabled = true;
        }

        private void DisableControls()
        {
            txtNumberOfCaller.Enabled = false;
            ddlState.Enabled = false;
            ddlScheme.Enabled = false;

        }

        protected void FillExistingCallDetailByMobileNo(string mobileNo)
        {
            try
            {

                ResetUI();

                txtNumberOfCaller.Text = mobileNo;

                Call tCall = new Call();
                tCall.CallerNumber = mobileNo;
                DataSet ds = tCall.GetCallDetailsByMobileNo();

                int stateId = 0;
                int districtId = 0;
                if (ds.Tables[0].Rows.Count > 0)
                {

                    txtNameOfCaller.Text = Convert.ToString(ds.Tables[0].Rows[0]["CallerName"]);
                    txtAddress.Text = Convert.ToString(ds.Tables[0].Rows[0]["Address"]);
                    txtLandmark.Text = Convert.ToString(ds.Tables[0].Rows[0]["Landmark"]);


                    if (Convert.ToString(ds.Tables[0].Rows[0]["LanguageId"]) != "0")
                    {
                        ddlLanguage.SelectedIndex = -1;
                        ddlLanguage.Items.FindByValue(Convert.ToString(ds.Tables[0].Rows[0]["LanguageId"])).Selected = true;
                    }
                    stateId = Convert.ToInt32(ds.Tables[0].Rows[0]["StateId"]);
                    districtId = Convert.ToInt32(ds.Tables[0].Rows[0]["DistrictId"]);

                    hdnDistricText.Value = Convert.ToString(ds.Tables[0].Rows[0]["District"]);

                    hdnDistricId.Value = Convert.ToString(districtId);
                    if (stateId > 0)
                    {
                        ddlState.SelectedIndex = -1;
                        ddlState.Items.FindByValue(Convert.ToString(stateId)).Selected = true;
                    }
                    else
                        ddlState.SelectedIndex = 0;

                    ddlDistrict.SelectedIndex = -1;
                    SQLDBHelper.PopulateDropDownList(ddlDistrict, "District", "District", "DistrictId", true, "StateId=" + stateId);
                    if (districtId > 0 && (ddlDistrict.Items.FindByValue(Convert.ToString(districtId)) != null))
                    {
                        ddlDistrict.Items.FindByValue(Convert.ToString(districtId)).Selected = true;
                    }
                    DisableControls();
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    trUID.Visible = true;
                    if (string.IsNullOrEmpty(ariaUID))
                    {
                        lblUID.Text = Convert.ToString(ds.Tables[1].Rows[0]["UID"]);
                    }
                    int callId = Convert.ToInt32(ds.Tables[1].Rows[0]["CallID"]);
                    int statusOfCallId = Convert.ToInt32(ds.Tables[1].Rows[0]["CurrentStatusId"]);
                    int sourceId = Convert.ToInt32(ds.Tables[1].Rows[0]["SourceId"]);
                    int SL_ULBId = Convert.ToInt32(ds.Tables[1].Rows[0]["SL_ULBId"]);
                    int SL_ZoneId = Convert.ToInt32(ds.Tables[1].Rows[0]["SL_ZoneId"]);
                    int SL_TicketTypeId = Convert.ToInt32(ds.Tables[1].Rows[0]["SL_TicketTypeId"]);
                    int SL_AssignedToId = Convert.ToInt32(ds.Tables[1].Rows[0]["SL_AssignedToId"]);
                    int SL_ComplaintFromId = Convert.ToInt32(ds.Tables[1].Rows[0]["SL_ComplaintFromId"]);
                    int UJ_DiscomId = Convert.ToInt32(ds.Tables[1].Rows[0]["UJ_DiscomId"]);
                    int BL_BuildingId = Convert.ToInt32(ds.Tables[1].Rows[0]["BL_BuildingId"]);
                    int AGDSM_AssignedToId = Convert.ToInt32(ds.Tables[1].Rows[0]["AGDSM_AssignedToId"]);

                    int schemeId = Convert.ToInt32(ds.Tables[1].Rows[0]["SchemeId"]);
                    int callCategoryId = Convert.ToInt32(ds.Tables[1].Rows[0]["CallCategoryId"]);

                    hdnULBId.Value = Convert.ToString(SL_ULBId);
                    hdnZoneId.Value = Convert.ToString(SL_ZoneId);
                    hdnDiscomId.Value = Convert.ToString(UJ_DiscomId);
                    hdnBuildingId.Value = Convert.ToString(BL_BuildingId);
                    hdnAssignedToId.Value = Convert.ToString(SL_AssignedToId);
                    hdnAGDSMAssignedToId.Value = Convert.ToString(AGDSM_AssignedToId);

                    hdnULBText.Value = Convert.ToString(ds.Tables[1].Rows[0]["ULB"]);
                    hdnZoneText.Value = Convert.ToString(ds.Tables[1].Rows[0]["Zone"]);
                    hdnDiscomText.Value = Convert.ToString(ds.Tables[1].Rows[0]["Discom"]);
                    hdnBuildingText.Value = Convert.ToString(ds.Tables[1].Rows[0]["Building"]);

                    txtSLWardNo.Text = Convert.ToString(ds.Tables[1].Rows[0]["SL_WardNo"]);
                    txtSLPoleNo.Text = Convert.ToString(ds.Tables[1].Rows[0]["SL_PoleNo"]);
                    txtUJEmail.Text = Convert.ToString(ds.Tables[1].Rows[0]["UJ_Email"]);
                    txtUJTown.Text = Convert.ToString(ds.Tables[1].Rows[0]["UJ_Town"]);
                    txtUJConsumerNo.Text = Convert.ToString(ds.Tables[1].Rows[0]["UJ_ConsumerNo"]);
                    txtAGDSMTown.Text = Convert.ToString(ds.Tables[1].Rows[0]["AGDSM_Town"]);
                    txtAGDSMDistributionCenter.Text = Convert.ToString(ds.Tables[1].Rows[0]["AGDSM_DistributionCenter"]);
                    txtAGDSMConsumerNo.Text = Convert.ToString(ds.Tables[1].Rows[0]["AGDSM_ConsumerNo"]);


                    txtBLFlatNo.Text = Convert.ToString(ds.Tables[1].Rows[0]["BL_FlatNo"]);
                    txtBLReferenceNo.Text = Convert.ToString(ds.Tables[1].Rows[0]["BL_ReferenceNo"]);
                    hdnBLVendors.Value = Convert.ToString(ds.Tables[1].Rows[0]["BL_AssignedToIds"]);

                    if (sourceId > 0)
                    {
                        ddlSource.SelectedIndex = -1;
                        ddlSource.Items.FindByValue(Convert.ToString(sourceId)).Selected = true;
                    }
                    else
                        ddlSource.SelectedIndex = 0;


                    if (schemeId > 0)
                    {
                        ddlScheme.SelectedIndex = -1;
                        ddlScheme.Items.FindByValue(Convert.ToString(schemeId)).Selected = true;
                        LightTypeTr.Visible = false;
                        if (schemeId == 1)
                        {
                            LightTypeTr.Visible = true;
                        }
                        if (schemeId == 4)
                        {
                            int TotalCount = Convert.ToInt32(SQLDBHelper.GetTableCount("Select * from CallAssignedToHistory where CallId = " + callId + ""));
                            int ValidCount = Convert.ToInt32(SQLDBHelper.GetTableCount("Select * from CallAssignedToHistory where CallId = " + callId + " AND ISNUll(ServiceReportURL,'') <> '' "));

                            if (TotalCount == ValidCount)
                                ViewState["ServiceReportUrl"] = null;
                            else
                                ViewState["ServiceReportUrl"] = "Invalid";
                        }
                    }
                    else
                        ddlScheme.SelectedIndex = 0;

                    if (callCategoryId > 0)
                    {
                        ddlCategoryOfCall.SelectedIndex = -1;
                        ddlCategoryOfCall.Items.FindByValue(Convert.ToString(callCategoryId)).Selected = true;
                    }
                    else
                        ddlCategoryOfCall.SelectedIndex = 0;

                    if (statusOfCallId > 0)
                    {
                        ddlStatusOfCall.SelectedIndex = -1;
                        ddlStatusOfCall.Items.FindByValue(Convert.ToString(statusOfCallId)).Selected = true;
                        if (ddlStatusOfCall.SelectedItem.Text == "Closed")
                            ddlStatusOfCall.Enabled = false;
                    }
                    else
                        ddlStatusOfCall.SelectedIndex = 0;


                    ddlSLULB.SelectedIndex = -1;
                    SQLDBHelper.PopulateDropDownList(ddlSLULB, "ULBMapping", "ULB", "ULBMappingId", true, "StateId=" + stateId + " AND DistrictId=" + districtId);
                    if (SL_ULBId > 0 && (ddlSLULB.Items.FindByValue(Convert.ToString(SL_ULBId)) != null))
                    {
                        ddlSLULB.Items.FindByValue(Convert.ToString(SL_ULBId)).Selected = true;
                    }

                    ddlSLZone.SelectedIndex = -1;
                    SQLDBHelper.PopulateDropDownList(ddlSLZone, "ZoneMapping", "Zone", "ZoneMappingId", true, "StateId=" + stateId + " AND DistrictId=" + districtId + " AND ULBMappingId=" + SL_ULBId);
                    if (SL_ZoneId > 0 && (ddlSLZone.Items.FindByValue(Convert.ToString(SL_ZoneId)) != null))
                    {
                        ddlSLZone.Items.FindByValue(Convert.ToString(SL_ZoneId)).Selected = true;
                    }

                    ddlSLAreaType.SelectedIndex = -1;
                    string slAreaType = Convert.ToString(ds.Tables[1].Rows[0]["SL_AreaType"]);
                    if (ddlSLAreaType.Items.FindByValue(Convert.ToString(slAreaType)) != null)
                    {
                        ddlSLAreaType.Items.FindByValue(Convert.ToString(slAreaType)).Selected = true;
                    }

                    ddlSLLightType.SelectedIndex = -1;

                    int SL_LightTypeId;
                    if (ds.Tables[1].Rows[0]["SL_LightTypeId"].ToString() == string.Empty)
                        SL_LightTypeId = 0;
                    else
                        SL_LightTypeId = Convert.ToInt32(ds.Tables[1].Rows[0]["SL_LightTypeId"]);

                    if (SL_LightTypeId > 0 && (ddlSLLightType.Items.FindByValue(Convert.ToString(SL_LightTypeId)) != null))
                    {
                        ddlSLLightType.Items.FindByValue(Convert.ToString(SL_LightTypeId)).Selected = true;
                    }

                    //ddlSLWard.SelectedIndex = -1;
                    //PopulateWardDropDownList(SL_ZoneId);
                    //if (SL_ZoneId > 0 && (ddlSLWard.Items.FindByValue(Convert.ToString(SL_ZoneId)) != null))
                    //{
                    //    ddlSLWard.Items.FindByValue(Convert.ToString(SL_ZoneId)).Selected = true;
                    //}

                    if (SL_TicketTypeId > 0)
                    {
                        ddlSLTicketType.SelectedIndex = -1;
                        ddlSLTicketType.Items.FindByValue(Convert.ToString(SL_TicketTypeId)).Selected = true;
                    }
                    else
                        ddlSLTicketType.SelectedIndex = 0;

                    ddlSLAssignedTo.SelectedIndex = -1;
                    string condition = string.Empty;
                    if (SL_ZoneId > 0)
                    {
                        condition = "StateId=" + stateId + " AND DistrictId=" + districtId + " AND ULBMappingId=" + SL_ULBId + " AND ZoneMappingId=" + SL_ZoneId;
                    }
                    else
                    {
                        condition = "StateId=" + stateId + " AND DistrictId=" + districtId + " AND ULBMappingId=" + SL_ULBId;
                    }
                    SQLDBHelper.PopulateDropDownList(ddlSLAssignedTo, "VendorMapping", "VendorName", "VendorMappingId", true, condition);

                    if (SL_AssignedToId > 0 && (ddlSLAssignedTo.Items.FindByValue(Convert.ToString(SL_AssignedToId)) != null))
                    {
                        ddlSLAssignedTo.Items.FindByValue(Convert.ToString(SL_AssignedToId)).Selected = true;
                    }

                    if (SL_ComplaintFromId > 0)
                    {
                        ddlSLComplaintFrom.SelectedIndex = -1;
                        ddlSLComplaintFrom.Items.FindByValue(Convert.ToString(SL_ComplaintFromId)).Selected = true;
                    }
                    else
                        ddlSLComplaintFrom.SelectedIndex = 0;

                    ddlUJDiscom.SelectedIndex = -1;
                    SQLDBHelper.PopulateDropDownList(ddlUJDiscom, "DiscomMapping", "Discom", "DiscomMappingId", true, "StateId=" + stateId + " AND DistrictId=" + districtId);

                    if (UJ_DiscomId > 0 && (ddlUJDiscom.Items.FindByValue(Convert.ToString(UJ_DiscomId)) != null))
                    {
                        ddlUJDiscom.Items.FindByValue(Convert.ToString(UJ_DiscomId)).Selected = true;
                    }


                    ddlBLBuilding.SelectedIndex = -1;
                    SQLDBHelper.PopulateDropDownList(ddlBLBuilding, "BuildingMapping", "Building", "BuildingMappingId", true, "StateId=" + stateId + " AND DistrictId=" + districtId);

                    if (BL_BuildingId > 0 && (ddlBLBuilding.Items.FindByValue(Convert.ToString(BL_BuildingId)) != null))
                    {
                        ddlBLBuilding.Items.FindByValue(Convert.ToString(BL_BuildingId)).Selected = true;
                    }




                    if (ddlScheme.SelectedItem.Text == "Street Light")
                    {
                        fsStreetLight.Attributes.Remove("display");
                        fsStreetLight.Style["display"] = "";
                        fsUjala.Style.Add("display", "none");
                        fsAGDSM.Style.Add("display", "none");
                        fsBuilding.Style.Add("display", "none");
                        ddlSLZone.Focus();
                    }
                    else if (ddlScheme.SelectedItem.Text == "UJALA/NEEFP")
                    {
                        fsUjala.Attributes.Remove("display");
                        fsUjala.Style["display"] = "";
                        fsStreetLight.Style.Add("display", "none");
                        fsAGDSM.Style.Add("display", "none");
                        fsBuilding.Style.Add("display", "none");
                        ddlDistrict.Focus();
                    }
                    else if (ddlScheme.SelectedItem.Text.ToUpper() == "AGDSM")
                    {
                        fsAGDSM.Style.Remove("display");
                        fsAGDSM.Style["display"] = "";
                        fsStreetLight.Style.Add("display", "none");
                        fsUjala.Style.Add("display", "none");
                        fsBuilding.Style.Add("display", "none");

                        AGDSMTr_USCNo0.Style.Remove("display");
                        AGDSMTr_USCNo.Style.Remove("display");
                        AGDSMTr_PanelBoradMobNo0.Style.Remove("display");
                        AGDSMTr_PanelBoradMobNo.Style.Remove("display");

                        txtAGDSMUSCORServiceConnNo.Text = Convert.ToString(ds.Tables[1].Rows[0]["AGDSM_USCORServiceConnNo"]);
                        txtAGDSMPanelBoardMobNo.Text = Convert.ToString(ds.Tables[1].Rows[0]["AGDSM_PanelBoardMobNo"]);
                        txtAGDSMHPkWRating.Text = Convert.ToString(ds.Tables[1].Rows[0]["AGDSM_HPkWRating"]);

                        ddlAGDSMReasonOfComplaint.SelectedIndex = -1;
                        if (ddlAGDSMReasonOfComplaint.Items.FindByValue(Convert.ToString(ds.Tables[1].Rows[0]["AGDSM_ComplaintReason"]).ToUpper()) != null)
                        {
                            ddlAGDSMReasonOfComplaint.Items.FindByValue(Convert.ToString(ds.Tables[1].Rows[0]["AGDSM_ComplaintReason"]).ToUpper()).Selected = true;
                        }

                        ddlAGDSMAssignedTo.SelectedIndex = -1;
                        SQLDBHelper.PopulateDropDownList(ddlAGDSMAssignedTo, "VendorMapping", "VendorName", "VendorMappingId", true, "StateId=" + stateId + " AND DistrictId=" + districtId);

                        if (AGDSM_AssignedToId > 0 && (ddlAGDSMAssignedTo.Items.FindByValue(Convert.ToString(AGDSM_AssignedToId)) != null))
                        {
                            ddlAGDSMAssignedTo.Items.FindByValue(Convert.ToString(AGDSM_AssignedToId)).Selected = true;
                        }
                    }
                    else if (ddlScheme.SelectedItem.Text == "Building")
                    {
                        fsBuilding.Attributes.Remove("display");
                        fsBuilding.Style["display"] = "";
                        fsStreetLight.Style.Add("display", "none");
                        fsUjala.Style.Add("display", "none");
                        fsAGDSM.Style.Add("display", "none");

                        if (ds.Tables[4].Rows.Count > 0)
                        {
                            gvBLCallAssignedHistory.DataSource = ds.Tables[4];
                            gvBLCallAssignedHistory.DataBind();
                            gvBLCallAssignedHistory.Visible = true;
                        }

                        ddlBLBuilding.Focus();
                    }
                    else
                    {
                        btnSave.Focus();
                    }

                    if (ddlStatusOfCall.SelectedItem.Text == "Escalated")
                    {

                        ddlEscalatedTo.SelectedIndex = -1;
                        SQLDBHelper.PopulateDropDownList(ddlEscalatedTo, "NodalOfficerMapping", "NOName", "NOMappingId", true, "StateId=" + stateId + " AND DistrictId=" + districtId + " AND SchemeId=" + schemeId);
                        trEscalate.Attributes.Remove("display");
                        trEscalate.Style["display"] = "";
                        ddlEscalatedTo.Focus();
                    }
                    else
                    {
                        trEscalate.Style.Add("display", "none");
                        btnSave.Focus();
                    }

                    if (ddlCategoryOfCall.SelectedItem.Text == "Query")
                    {

                        ddlStatusOfCall.Items[2].Attributes["disabled"] = "disabled";
                        ddlStatusOfCall.Items[3].Attributes["disabled"] = "disabled";
                        ddlStatusOfCall.Items[4].Attributes["disabled"] = "disabled";
                        ddlStatusOfCall.Items[6].Attributes["disabled"] = "disabled";
                    }
                    else
                    {
                        ddlStatusOfCall.Items[2].Enabled = true; //.Attributes["disabled"] = "";
                        ddlStatusOfCall.Items[3].Enabled = true; //.Attributes["disabled"] = "";
                        ddlStatusOfCall.Items[4].Enabled = true; //.Attributes["disabled"] = "";
                        ddlStatusOfCall.Items[6].Enabled = true; //.Attributes["disabled"] = "";
                    }

                    string sProducts = Convert.ToString(ds.Tables[1].Rows[0]["UJ_Products"]);
                    if (sProducts != "")
                    {
                        string[] arrProducts;
                        arrProducts = sProducts.Split(',');
                        for (int i = 0; i < arrProducts.Length; i++)
                        {
                            chklstUJProducts.Items.FindByValue(arrProducts[i]).Selected = true;
                        }
                    }

                    string sProductTypes = Convert.ToString(ds.Tables[1].Rows[0]["BL_ProductTypes"]);
                    string sProductTypesQty = Convert.ToString(ds.Tables[1].Rows[0]["BL_ProductTypesQty"]);
                    if (!string.IsNullOrEmpty(sProductTypes) && !string.IsNullOrEmpty(sProductTypesQty))
                    {
                        string[] arrProductTypes = sProductTypes.Split(',');
                        string[] arrProductTypesQty = sProductTypesQty.Split(',');
                        if (arrProductTypes.Length == arrProductTypesQty.Length)
                        {
                            for (int i = 0; i < arrProductTypes.Length; i++)
                            {
                                foreach (DataListItem item in dlBLProductTypes.Items)
                                {
                                    HiddenField hdn = (HiddenField)item.FindControl("hdnProductTypeQuantity");
                                    if (hdn.Value.Trim() == arrProductTypes[i].Trim())
                                    {
                                        TextBox txt = (TextBox)item.FindControl("txtProductTypeQuantity");
                                        txt.Text = arrProductTypesQty[i].Trim();
                                        txt.Style.Remove("display");
                                        CheckBox chk = (CheckBox)item.FindControl("chkProductType");
                                        chk.Checked = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    FillCurrentCallDetailHistoryGrid(callId);
                }

                if (ds.Tables[2].Rows.Count > 0)
                {
                    txtRemark.Text = Convert.ToString(ds.Tables[2].Rows[0]["Remark"]);

                    int escalatedToId = Convert.ToInt32(ds.Tables[2].Rows[0]["EscalatedToId"]);
                    hdnEsclatedId.Value = Convert.ToString(escalatedToId);
                    if (escalatedToId > 0 && (ddlEscalatedTo.Items.FindByValue(Convert.ToString(escalatedToId)) != null))
                    {
                        ddlEscalatedTo.Items.FindByValue(Convert.ToString(escalatedToId)).Selected = true;
                    }
                }

                if (ds.Tables[3].Rows.Count > 0)
                {

                    gvCalllDetail.DataSource = ds.Tables[3];
                    gvCalllDetail.DataBind();
                    gvCalllDetail.Visible = true;
                }


            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void FillExistingCallDetailByUID(string UID)
        {
            try
            {
                Call tCall = new Call();
                tCall.UID = UID;
                DataSet ds = tCall.GetCallDetailsByUID();
                int stateId = 0;
                int districtId = 0;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    trUID.Visible = true;
                    int callId = Convert.ToInt32(ds.Tables[0].Rows[0]["CallID"]);
                    lblUID.Text = Convert.ToString(ds.Tables[0].Rows[0]["UID"]);
                    hdnAgent.Value = Convert.ToString(ds.Tables[0].Rows[0]["UserName"]);
                    txtNameOfCaller.Text = Convert.ToString(ds.Tables[0].Rows[0]["CallerName"]);
                    txtAddress.Text = Convert.ToString(ds.Tables[0].Rows[0]["Address"]);
                    txtLandmark.Text = Convert.ToString(ds.Tables[0].Rows[0]["Landmark"]);

                    if (Convert.ToString(ds.Tables[0].Rows[0]["LanguageId"]) != "0")
                    {
                        ddlLanguage.SelectedIndex = -1;
                        ddlLanguage.Items.FindByValue(Convert.ToString(ds.Tables[0].Rows[0]["LanguageId"])).Selected = true;
                    }
                    stateId = Convert.ToInt32(ds.Tables[0].Rows[0]["StateId"]);
                    districtId = Convert.ToInt32(ds.Tables[0].Rows[0]["DistrictId"]);

                    hdnDistricText.Value = Convert.ToString(ds.Tables[0].Rows[0]["District"]);
                    hdnULBText.Value = Convert.ToString(ds.Tables[0].Rows[0]["ULB"]);
                    hdnZoneText.Value = Convert.ToString(ds.Tables[0].Rows[0]["Zone"]);
                    hdnDiscomText.Value = Convert.ToString(ds.Tables[0].Rows[0]["Discom"]);
                    hdnBuildingText.Value = Convert.ToString(ds.Tables[0].Rows[0]["Building"]);

                    hdnDistricId.Value = Convert.ToString(districtId);
                    if (stateId > 0)
                    {
                        ddlState.SelectedIndex = -1;
                        ddlState.Items.FindByValue(Convert.ToString(stateId)).Selected = true;
                    }
                    else
                        ddlState.SelectedIndex = 0;

                    ddlDistrict.SelectedIndex = -1;
                    SQLDBHelper.PopulateDropDownList(ddlDistrict, "District", "District", "DistrictId", true, "StateId=" + stateId);
                    if (districtId > 0 && (ddlDistrict.Items.FindByValue(Convert.ToString(districtId)) != null))
                    {
                        ddlDistrict.Items.FindByValue(Convert.ToString(districtId)).Selected = true;
                    }

                    if (string.IsNullOrEmpty(ariaUID))
                    {
                        lblUID.Text = Convert.ToString(ds.Tables[0].Rows[0]["UID"]);
                    }

                    int callCategoryId = Convert.ToInt32(ds.Tables[0].Rows[0]["CallCategoryId"]);
                    int sourceId = Convert.ToInt32(ds.Tables[0].Rows[0]["SourceId"]);
                    int schemeId = Convert.ToInt32(ds.Tables[0].Rows[0]["SchemeId"]);
                    int statusOfCallId = Convert.ToInt32(ds.Tables[0].Rows[0]["CurrentStatusId"]);

                    int SL_ULBId = Convert.ToInt32(ds.Tables[0].Rows[0]["SL_ULBId"]);
                    int SL_ZoneId = Convert.ToInt32(ds.Tables[0].Rows[0]["SL_ZoneId"]);
                    int SL_TicketTypeId = Convert.ToInt32(ds.Tables[0].Rows[0]["SL_TicketTypeId"]);
                    int SL_AssignedToId = Convert.ToInt32(ds.Tables[0].Rows[0]["SL_AssignedToId"]);
                    int SL_ComplaintFromId = Convert.ToInt32(ds.Tables[0].Rows[0]["SL_ComplaintFromId"]);
                    int UJ_DiscomId = Convert.ToInt32(ds.Tables[0].Rows[0]["UJ_DiscomId"]);
                    int BL_BuildingId = Convert.ToInt32(ds.Tables[0].Rows[0]["BL_BuildingId"]);
                    int AGDSM_AssignedToId = Convert.ToInt32(ds.Tables[0].Rows[0]["AGDSM_AssignedToId"]);
                    
                    hdnULBId.Value = Convert.ToString(SL_ULBId);
                    hdnZoneId.Value = Convert.ToString(SL_ZoneId);
                    hdnDiscomId.Value = Convert.ToString(UJ_DiscomId);
                    hdnBuildingId.Value = Convert.ToString(BL_BuildingId);
                    hdnAssignedToId.Value = Convert.ToString(SL_AssignedToId);
                    hdnAGDSMAssignedToId.Value = Convert.ToString(AGDSM_AssignedToId);

                    txtSLWardNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["SL_WardNo"]);
                    txtSLPoleNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["SL_PoleNo"]);
                    txtUJEmail.Text = Convert.ToString(ds.Tables[0].Rows[0]["UJ_Email"]);
                    txtUJTown.Text = Convert.ToString(ds.Tables[0].Rows[0]["UJ_Town"]);
                    txtUJConsumerNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["UJ_ConsumerNo"]);
                    txtAGDSMTown.Text = Convert.ToString(ds.Tables[0].Rows[0]["AGDSM_Town"]);
                    txtAGDSMDistributionCenter.Text = Convert.ToString(ds.Tables[0].Rows[0]["AGDSM_DistributionCenter"]);
                    txtAGDSMConsumerNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["AGDSM_ConsumerNo"]);
                    txtBLFlatNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["BL_FlatNo"]);
                    txtBLReferenceNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["BL_ReferenceNo"]);
                    hdnBLVendors.Value = Convert.ToString(ds.Tables[0].Rows[0]["BL_AssignedToIds"]);


                    ddlSLULB.SelectedIndex = -1;
                    SQLDBHelper.PopulateDropDownList(ddlSLULB, "ULBMapping", "ULB", "ULBMappingId", true, "StateId=" + stateId + " AND DistrictId=" + districtId);
                    if (SL_ULBId > 0 && (ddlSLULB.Items.FindByValue(Convert.ToString(SL_ULBId)) != null))
                    {
                        ddlSLULB.Items.FindByValue(Convert.ToString(SL_ULBId)).Selected = true;
                    }

                    ddlSLZone.SelectedIndex = -1;
                    SQLDBHelper.PopulateDropDownList(ddlSLZone, "ZoneMapping", "Zone", "ZoneMappingId", true, "StateId=" + stateId + " AND DistrictId=" + districtId + " AND ULBMappingId=" + SL_ULBId);
                    if (SL_ZoneId > 0 && (ddlSLZone.Items.FindByValue(Convert.ToString(SL_ZoneId)) != null))
                    {
                        ddlSLZone.Items.FindByValue(Convert.ToString(SL_ZoneId)).Selected = true;
                    }

                    ddlSLAreaType.SelectedIndex = -1;
                    string slAreaType = Convert.ToString(ds.Tables[0].Rows[0]["SL_AreaType"]);
                    if (ddlSLAreaType.Items.FindByValue(Convert.ToString(slAreaType)) != null)
                    {
                        ddlSLAreaType.Items.FindByValue(Convert.ToString(slAreaType)).Selected = true;
                    }

                    ddlSLLightType.SelectedIndex = -1;

                    int SL_LightTypeId;
                    if (ds.Tables[0].Rows[0]["SL_LightTypeId"].ToString() == string.Empty)
                        SL_LightTypeId = 0;
                    else
                        SL_LightTypeId = Convert.ToInt32(ds.Tables[0].Rows[0]["SL_LightTypeId"]);

                    if (SL_LightTypeId > 0 && (ddlSLLightType.Items.FindByValue(Convert.ToString(SL_LightTypeId)) != null))
                    {
                        ddlSLLightType.Items.FindByValue(Convert.ToString(SL_LightTypeId)).Selected = true;
                    }

                    if (SL_TicketTypeId > 0)
                    {
                        ddlSLTicketType.SelectedIndex = -1;
                        ddlSLTicketType.Items.FindByValue(Convert.ToString(SL_TicketTypeId)).Selected = true;
                    }
                    else
                        ddlSLTicketType.SelectedIndex = 0;

                    ddlSLAssignedTo.SelectedIndex = -1;
                    string condition = string.Empty;
                    if (SL_ZoneId > 0)
                    {
                        condition = "StateId=" + stateId + " AND DistrictId=" + districtId + " AND ULBMappingId=" + SL_ULBId + " AND ZoneMappingId=" + SL_ZoneId;
                    }
                    else
                    {
                        condition = "StateId=" + stateId + " AND DistrictId=" + districtId + " AND ULBMappingId=" + SL_ULBId;
                    }
                    //ddlSLWard.SelectedIndex = -1;
                    //PopulateWardDropDownList(SL_ZoneId);

                    SQLDBHelper.PopulateDropDownList(ddlSLAssignedTo, "VendorMapping", "VendorName", "VendorMappingId", true, condition);

                    if (SL_AssignedToId > 0 && (ddlSLAssignedTo.Items.FindByValue(Convert.ToString(SL_AssignedToId)) != null))
                    {
                        ddlSLAssignedTo.Items.FindByValue(Convert.ToString(SL_AssignedToId)).Selected = true;
                    }

                    if (SL_ComplaintFromId > 0)
                    {
                        ddlSLComplaintFrom.SelectedIndex = -1;
                        ddlSLComplaintFrom.Items.FindByValue(Convert.ToString(SL_ComplaintFromId)).Selected = true;
                    }
                    else
                        ddlSLComplaintFrom.SelectedIndex = 0;

                    ddlUJDiscom.SelectedIndex = -1;
                    SQLDBHelper.PopulateDropDownList(ddlUJDiscom, "DiscomMapping", "Discom", "DiscomMappingId", true, "StateId=" + stateId + " AND DistrictId=" + districtId);

                    if (UJ_DiscomId > 0 && (ddlUJDiscom.Items.FindByValue(Convert.ToString(UJ_DiscomId)) != null))
                    {
                        ddlUJDiscom.Items.FindByValue(Convert.ToString(UJ_DiscomId)).Selected = true;
                    }

                    ddlBLBuilding.SelectedIndex = -1;
                    SQLDBHelper.PopulateDropDownList(ddlBLBuilding, "BuildingMapping", "Building", "BuildingMappingId", true, "StateId=" + stateId + " AND DistrictId=" + districtId);

                    if (BL_BuildingId > 0 && (ddlBLBuilding.Items.FindByValue(Convert.ToString(BL_BuildingId)) != null))
                    {
                        ddlBLBuilding.Items.FindByValue(Convert.ToString(BL_BuildingId)).Selected = true;
                    }

                    if (sourceId > 0)
                    {
                        ddlSource.SelectedIndex = -1;
                        ddlSource.Items.FindByValue(Convert.ToString(sourceId)).Selected = true;
                    }
                    else
                        ddlSource.SelectedIndex = 0;

                    if (schemeId > 0)
                    {
                        ddlScheme.SelectedIndex = -1;
                        ddlScheme.Items.FindByValue(Convert.ToString(schemeId)).Selected = true;
                    }
                    else
                        ddlScheme.SelectedIndex = 0;
                    if (statusOfCallId > 0)
                    {
                        ddlStatusOfCall.SelectedIndex = -1;
                        ddlStatusOfCall.Items.FindByValue(Convert.ToString(statusOfCallId)).Selected = true;
                        if (ddlStatusOfCall.SelectedItem.Text == "Closed")
                            ddlStatusOfCall.Enabled = false;
                    }
                    else
                        ddlStatusOfCall.SelectedIndex = 0;
                    if (callCategoryId > 0)
                    {
                        ddlCategoryOfCall.SelectedIndex = -1;
                        ddlCategoryOfCall.Items.FindByValue(Convert.ToString(callCategoryId)).Selected = true;
                    }
                    else
                        ddlCategoryOfCall.SelectedIndex = 0;

                    if (ddlStatusOfCall.SelectedItem.Text == "Escalated")
                    {

                        ddlEscalatedTo.SelectedIndex = -1;
                        SQLDBHelper.PopulateDropDownList(ddlEscalatedTo, "NodalOfficerMapping", "NOName", "NOMappingId", true, "StateId=" + stateId + " AND DistrictId=" + districtId + " AND SchemeId=" + schemeId);
                        trEscalate.Attributes.Remove("display");
                        trEscalate.Style["display"] = "";
                        ddlEscalatedTo.Focus();

                    }
                    else
                    {
                        trEscalate.Style.Add("display", "none");
                        btnSave.Focus();
                    }

                    if (ddlCategoryOfCall.SelectedItem.Text == "Query")
                    {

                        ddlStatusOfCall.Items[2].Attributes["disabled"] = "disabled";
                        ddlStatusOfCall.Items[3].Attributes["disabled"] = "disabled";
                        ddlStatusOfCall.Items[4].Attributes["disabled"] = "disabled";
                        ddlStatusOfCall.Items[6].Attributes["disabled"] = "disabled";
                    }
                    else
                    {
                        ddlStatusOfCall.Items[2].Enabled = true; //.Attributes["disabled"] = "";
                        ddlStatusOfCall.Items[3].Enabled = true; //.Attributes["disabled"] = "";
                        ddlStatusOfCall.Items[4].Enabled = true; //.Attributes["disabled"] = "";
                        ddlStatusOfCall.Items[6].Enabled = true; //.Attributes["disabled"] = "";
                    }
                    string sProducts = Convert.ToString(ds.Tables[0].Rows[0]["UJ_Products"]);
                    if (sProducts != "")
                    {
                        string[] arrProducts;
                        arrProducts = sProducts.Split(',');
                        for (int i = 0; i < arrProducts.Length; i++)
                        {
                            chklstUJProducts.Items.FindByValue(arrProducts[i]).Selected = true;
                        }
                    }

                    string sProductTypes = Convert.ToString(ds.Tables[0].Rows[0]["BL_ProductTypes"]);
                    string sProductTypesQty = Convert.ToString(ds.Tables[0].Rows[0]["BL_ProductTypesQty"]);
                    if (!string.IsNullOrEmpty(sProductTypes) && !string.IsNullOrEmpty(sProductTypesQty))
                    {
                        string[] arrProductTypes = sProductTypes.Split(',');
                        string[] arrProductTypesQty = sProductTypesQty.Split(',');
                        if (arrProductTypes.Length == arrProductTypesQty.Length)
                        {
                            for (int i = 0; i < arrProductTypes.Length; i++)
                            {
                                foreach (DataListItem item in dlBLProductTypes.Items)
                                {
                                    HiddenField hdn = (HiddenField)item.FindControl("hdnProductTypeQuantity");
                                    if (hdn.Value.Trim() == arrProductTypes[i].Trim())
                                    {
                                        TextBox txt = (TextBox)item.FindControl("txtProductTypeQuantity");
                                        txt.Text = arrProductTypesQty[i].Trim();
                                        txt.Style.Remove("display");
                                        CheckBox chk = (CheckBox)item.FindControl("chkProductType");
                                        chk.Checked = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    FillCurrentCallDetailHistoryGrid(callId);

                    if (ddlScheme.SelectedItem.Text == "Street Light")
                    {
                        fsStreetLight.Attributes.Remove("display");
                        fsStreetLight.Style["display"] = "";
                        fsUjala.Style.Add("display", "none");
                        fsAGDSM.Style.Add("display", "none");
                        fsBuilding.Style.Add("display", "none");
                        ddlSLZone.Focus();
                    }
                    else if (ddlScheme.SelectedItem.Text == "UJALA/NEEFP")
                    {
                        fsUjala.Attributes.Remove("display");
                        fsUjala.Style["display"] = "";
                        fsStreetLight.Style.Add("display", "none");
                        fsAGDSM.Style.Add("display", "none");
                        fsBuilding.Style.Add("display", "none");
                        ddlDistrict.Focus();
                    }
                    else if (ddlScheme.SelectedItem.Text.ToUpper() == "AGDSM")
                    {
                        fsAGDSM.Attributes.Remove("display");
                        fsAGDSM.Style["display"] = "";
                        fsStreetLight.Style.Add("display", "none");
                        fsUjala.Style.Add("display", "none");
                        fsBuilding.Style.Add("display", "none");
                        txtAGDSMTown.Focus();

                        AGDSMTr_USCNo0.Style.Remove("display");
                        AGDSMTr_USCNo.Style.Remove("display");
                        AGDSMTr_PanelBoradMobNo0.Style.Remove("display");
                        AGDSMTr_PanelBoradMobNo.Style.Remove("display");

                        txtAGDSMUSCORServiceConnNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["AGDSM_USCORServiceConnNo"]);
                        txtAGDSMPanelBoardMobNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["AGDSM_PanelBoardMobNo"]);
                        txtAGDSMHPkWRating.Text = Convert.ToString(ds.Tables[0].Rows[0]["AGDSM_HPkWRating"]);

                        ddlAGDSMReasonOfComplaint.SelectedIndex = -1;
                        if (ddlAGDSMReasonOfComplaint.Items.FindByValue(Convert.ToString(ds.Tables[0].Rows[0]["AGDSM_ComplaintReason"]).ToUpper()) != null)
                        {
                            ddlAGDSMReasonOfComplaint.Items.FindByValue(Convert.ToString(ds.Tables[0].Rows[0]["AGDSM_ComplaintReason"]).ToUpper()).Selected = true;
                        }

                        ddlAGDSMAssignedTo.SelectedIndex = -1;
                        SQLDBHelper.PopulateDropDownList(ddlAGDSMAssignedTo, "VendorMapping", "VendorName", "VendorMappingId", true, "StateId=" + stateId + " AND DistrictId=" + districtId);

                        if (AGDSM_AssignedToId > 0 && (ddlAGDSMAssignedTo.Items.FindByValue(Convert.ToString(AGDSM_AssignedToId)) != null))
                        {
                            ddlAGDSMAssignedTo.Items.FindByValue(Convert.ToString(AGDSM_AssignedToId)).Selected = true;
                        }
                    }
                    else if (ddlScheme.SelectedItem.Text == "Building")
                    {
                        fsBuilding.Attributes.Remove("display");
                        fsBuilding.Style["display"] = "";
                        fsStreetLight.Style.Add("display", "none");
                        fsUjala.Style.Add("display", "none");
                        fsAGDSM.Style.Add("display", "none");

                        if (ds.Tables[3].Rows.Count > 0)
                        {
                            gvBLCallAssignedHistory.DataSource = ds.Tables[3];
                            gvBLCallAssignedHistory.DataBind();
                            gvBLCallAssignedHistory.Visible = true;
                        }

                        ddlBLBuilding.Focus();
                    }
                    else
                    {
                        btnSave.Focus();
                    }

                    FillCurrentCallDetailHistoryGrid(callId);
                    DisableControls();
                }


                if (ds.Tables[1].Rows.Count > 0)
                {
                    gvCalllDetail.DataSource = ds.Tables[1];
                    gvCalllDetail.DataBind();
                    gvCalllDetail.Visible = true;
                }

                if (ds.Tables[2].Rows.Count > 0)
                {
                    txtRemark.Text = Convert.ToString(ds.Tables[2].Rows[0]["Remark"]);

                    int escalatedToId = Convert.ToInt32(ds.Tables[2].Rows[0]["EscalatedToId"]);
                    hdnEsclatedId.Value = Convert.ToString(escalatedToId);
                    if (escalatedToId > 0 && (ddlEscalatedTo.Items.FindByValue(Convert.ToString(escalatedToId)) != null))
                    {
                        ddlEscalatedTo.Items.FindByValue(Convert.ToString(escalatedToId)).Selected = true;
                    }
                }


            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ViewState["ServiceReportUrl"] != null && ddlStatusOfCall.SelectedItem.Text == "Closed")
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Please Upload Image')", true);
                    return;
                }
                string strAddress = string.Empty;
                string strAddressBL = string.Empty;
                string strSMSAddress = string.Empty;
                string tErrorMsg = string.Empty;
                string tDMLType = string.Empty;
                Call objCall = new Call();
                objCall.UID = lblUID.Text;
                objCall.CallerName = Convert.ToString(txtNameOfCaller.Text.Trim().ToUpper());
               // 
                string addressvalue = hdnAddress.Value;
                if (!string.IsNullOrEmpty(addressvalue))
                    objCall.Address = addressvalue;
                else
                    objCall.Address = Convert.ToString(txtAddress.Text.Trim());

                string landmarkvalue = hdnLandmark.Value;
                if (!string.IsNullOrEmpty(landmarkvalue))
                    objCall.Landmark = landmarkvalue;
                else
                    objCall.Landmark = Convert.ToString(txtLandmark.Text.Trim());

                objCall.CallerNumber = Convert.ToString(txtNumberOfCaller.Text.Trim());
                objCall.Remark = Convert.ToString(txtRemark.Text.Trim()).ToUpper();
                objCall.LanguageId = ddlLanguage.SelectedIndex == 0 ? 0 : Convert.ToInt32(ddlLanguage.SelectedItem.Value);
                objCall.StateId = ddlState.SelectedIndex == 0 ? 0 : Convert.ToInt32(ddlState.SelectedItem.Value);
                objCall.DistrictId = Convert.ToInt32(hdnDistricId.Value);
                objCall.SourceId = ddlSource.SelectedIndex == 0 ? 0 : Convert.ToInt32(ddlSource.SelectedItem.Value);
                objCall.SchemeId = ddlScheme.SelectedIndex == 0 ? 0 : Convert.ToInt32(ddlScheme.SelectedItem.Value);
                if (objCall.SchemeId == 1)
                {
                    if (ddlSLLightType.SelectedValue == "2")
                    {
                        objCall.CallCategoryId = 1;
                        objCall.StatusOfCallId = 5;
                    }
                    else
                    {
                        objCall.CallCategoryId = ddlCategoryOfCall.SelectedIndex == 0 ? 0 : Convert.ToInt32(ddlCategoryOfCall.SelectedItem.Value);
                        objCall.StatusOfCallId = Convert.ToInt32(ddlStatusOfCall.SelectedItem.Value);
                    }
                }
                else
                {
                    objCall.CallCategoryId = ddlCategoryOfCall.SelectedIndex == 0 ? 0 : Convert.ToInt32(ddlCategoryOfCall.SelectedItem.Value);
                    objCall.StatusOfCallId = Convert.ToInt32(ddlStatusOfCall.SelectedItem.Value);
                }


                //objCall.EscalatedToId = ddlEscalatedTo.SelectedIndex == 0 ? 0 : Convert.ToInt32(ddlEscalatedTo.SelectedItem.Value);
                objCall.EscalatedToId = Convert.ToInt32(hdnEsclatedId.Value);
                objCall.UserId = UIManager.CurrentUserSession().UserId;

                objCall.SL_ULBId = Convert.ToInt32(hdnULBId.Value);
                objCall.SL_ZoneId = Convert.ToInt32(hdnZoneId.Value);

                objCall.SL_WardNo = Convert.ToString(txtSLWardNo.Text.Trim());
                objCall.SL_PoleNo = Convert.ToString(txtSLPoleNo.Text.Trim());

                objCall.SL_AreaType = ddlSLAreaType.SelectedItem.Value;
                objCall.SL_LightTypeId = ddlSLLightType.SelectedIndex == 0 ? 0 : Convert.ToInt32(ddlSLLightType.SelectedItem.Value);

                objCall.SL_TicketTypeId = ddlSLTicketType.SelectedIndex == 0 ? 0 : Convert.ToInt32(ddlSLTicketType.SelectedItem.Value);
                objCall.SL_AssignedToId = Convert.ToInt32(hdnAssignedToId.Value);

                objCall.SL_ComplaintFromId = ddlSLComplaintFrom.SelectedIndex == 0 ? 0 : Convert.ToInt32(ddlSLComplaintFrom.SelectedItem.Value);

                objCall.UJ_DiscomId = Convert.ToInt32(hdnDiscomId.Value);
                objCall.UJ_Email = Convert.ToString(txtUJEmail.Text.Trim());
                objCall.UJ_Town = Convert.ToString(txtUJTown.Text.Trim());
                objCall.UJ_ConsumerNo = Convert.ToString(txtUJConsumerNo.Text.Trim());
                string sProducts = "";
                string strProducts = "";
                for (int i = 0; i < chklstUJProducts.Items.Count; i++)
                {
                    if (chklstUJProducts.Items[i].Selected == true)
                    {
                        sProducts += chklstUJProducts.Items[i].Value.ToString() + ",";
                        strProducts += chklstUJProducts.Items[i].Text.ToString() + ",";
                    }

                }
                if (sProducts != "")
                {
                    sProducts = sProducts.Substring(0, sProducts.Length - 1);
                    strProducts = strProducts.Substring(0, strProducts.Length - 1);
                }
                objCall.UJ_Products = sProducts;

                objCall.AGDSM_Town = Convert.ToString(txtAGDSMTown.Text.Trim());
                objCall.AGDSM_DistributionCenter = Convert.ToString(txtAGDSMDistributionCenter.Text.Trim());
                objCall.AGDSM_ConsumerNo = Convert.ToString(txtAGDSMConsumerNo.Text.Trim());

                objCall.AGDSM_USCORServiceConnNo = Convert.ToString(txtAGDSMUSCORServiceConnNo.Text).Trim();
                objCall.AGDSM_PanelBoardMobNo = Convert.ToString(txtAGDSMPanelBoardMobNo.Text).Trim();
                objCall.AGDSM_HPkWRating = Convert.ToString(txtAGDSMHPkWRating.Text).Trim();
                objCall.AGDSM_ComplaintReason = Convert.ToString(ddlAGDSMReasonOfComplaint.SelectedItem.Value).Trim();
                objCall.AGDSM_AssignedToId = Convert.ToInt32(hdnAGDSMAssignedToId.Value);

                objCall.BL_BuildingId = Convert.ToInt32(hdnBuildingId.Value);
                objCall.BL_FlatNo = Convert.ToString(txtBLFlatNo.Text.Trim());
                objCall.BL_ReferenceNo = Convert.ToString(txtBLReferenceNo.Text.Trim());
                objCall.BL_AssignedToIds = Convert.ToString(hdnBLVendors.Value.Trim());
                string sProductTypes = "";
                string sProductTypesQty = "";
                string strProductTypes = "";
                string sBLProductTypes = "";
                string strBLProductTypes = "";
                string sBLACProductTypes = "";
                string strBLACProductTypes = "";
                string producttype = string.Empty, producttypeId = string.Empty, producttypeQty = string.Empty;
                DataTable productTypes = GetProductTypes();
                foreach (DataListItem item in dlBLProductTypes.Items)
                {
                    CheckBox chk = (CheckBox)item.FindControl("chkProductType");
                    if (chk.Checked)
                    {
                        producttypeQty = ((TextBox)item.FindControl("txtProductTypeQuantity")).Text;
                        producttype = chk.Text;
                        producttypeId = dlBLProductTypes.DataKeys[item.ItemIndex].ToString();

                        DataRow[] prodTypes = productTypes.Select("[ProductTypeId] = " + Convert.ToInt32(producttypeId));
                        if (prodTypes.Count() > 0)
                        {
                            if (Convert.ToInt32(prodTypes[0]["VendorTypeId"]) == 3)
                            {
                                sBLProductTypes += producttypeId + ",";
                                strBLProductTypes += producttype + ",";
                            }
                            else if (Convert.ToInt32(prodTypes[0]["VendorTypeId"]) == 4)
                            {
                                sBLACProductTypes += producttypeId + ",";
                                strBLACProductTypes += producttype + ",";
                            }
                        }

                        sProductTypes += producttypeId + ",";
                        strProductTypes += producttype + ",";
                        sProductTypesQty += producttypeQty + ",";
                    }
                }

                if (sProductTypes != "")
                {
                    sProductTypes = sProductTypes.Substring(0, sProductTypes.Length - 1);
                    strProductTypes = strProductTypes.Substring(0, strProductTypes.Length - 1);
                    sProductTypesQty = sProductTypesQty.Substring(0, sProductTypesQty.Length - 1);
                }

                if (sBLProductTypes != "")
                {
                    sBLProductTypes = sBLProductTypes.Substring(0, sBLProductTypes.Length - 1);
                    strBLProductTypes = strBLProductTypes.Substring(0, strBLProductTypes.Length - 1);
                }

                if (sBLACProductTypes != "")
                {
                    sBLACProductTypes = sBLACProductTypes.Substring(0, sBLACProductTypes.Length - 1);
                    strBLACProductTypes = strBLACProductTypes.Substring(0, strBLACProductTypes.Length - 1);
                }

                objCall.BL_ProductTypes = sProductTypes;
                objCall.BL_ProductTypesQty = sProductTypesQty;
                objCall.BL_LEDProductTypes = sBLProductTypes;
                objCall.BL_ACProductTypes = sBLACProductTypes;

                //Common Address
                strAddress = txtAddress.Text.Trim().ToUpper() + "<br />" +
                            "Landmark : " + txtLandmark.Text.Trim().ToUpper() + "<br />" +
                            "State : " + ddlState.SelectedItem.Text.Trim() + "<br />" +
                            "District : " + hdnDistricText.Value.Trim() + "<br/>";
                strAddressBL = strAddress;

                //Scheme Detail Address Start (Street Light,Ujala,AGDSM,Building)
                if (ddlScheme.SelectedItem.Text == "Street Light")
                {
                    strAddress += "ULB : " + hdnULBText.Value.Trim() + "<br />" +
                           "Zone : " + hdnZoneText.Value.Trim() + "<br />" +
                           "Ward No : " + txtSLWardNo.Text.Trim().ToUpper() + "<br />" +
                           "Pole No : " + txtSLPoleNo.Text.Trim().ToUpper() + "<br />";

                    strSMSAddress = "Landmark : " + txtLandmark.Text.Trim().ToUpper() + ", Pole No : " + txtSLPoleNo.Text.Trim().ToUpper();
                }
                else if (ddlScheme.SelectedItem.Text == "UJALA/NEEFP")
                {
                    strAddress += "Discom : " + hdnDiscomText.Value.Trim() + "<br />" +
                             "Email : " + txtUJEmail.Text.Trim().ToUpper() + "<br />" +
                             "Town : " + txtUJTown.Text.Trim().ToUpper() + "<br />" +
                             "Consumer No : " + txtUJConsumerNo.Text.Trim().ToUpper() + "<br />" +
                             "Products : " + strProducts + "<br />";
                    strSMSAddress = "";
                }
                else if (ddlScheme.SelectedItem.Text.ToUpper() == "AGDSM")
                {
                    strAddress += "Town : " + txtAGDSMTown.Text.Trim().ToUpper() + "<br />" +
                             "Distribution Center : " + txtAGDSMDistributionCenter.Text.Trim().ToUpper() + "<br />" +
                             "Consumer No : " + txtAGDSMConsumerNo.Text.Trim().ToUpper();
                    strSMSAddress = "";
                }
                else if (ddlScheme.SelectedItem.Text == "Building")
                {
                    strAddress += "Building : " + hdnBuildingText.Value.Trim() + "<br />" +
                             "Flat No : " + txtBLFlatNo.Text.Trim().ToUpper() + "<br />" +
                             "Reference No : " + txtBLReferenceNo.Text.Trim().ToUpper() + "<br />" +
                             "Product Type : " + strBLProductTypes;

                    strAddressBL += "Building : " + hdnBuildingText.Value.Trim() + "<br />" +
                             "Flat No : " + txtBLFlatNo.Text.Trim().ToUpper() + "<br />" +
                             "Reference No : " + txtBLReferenceNo.Text.Trim().ToUpper() + "<br />" +
                             "Product Type : " + strBLACProductTypes;

                    strSMSAddress = "";
                }

                // Scheme Detail Address End

                lock (thisLock)
                {
                    if (objCall.InBoundCall_Insert(out tErrorMsg, out tDMLType))
                    {
                        int callStatus = Convert.ToInt32(ddlStatusOfCall.SelectedValue);
                        string strCallStatus = ddlStatusOfCall.SelectedItem.Text.Trim();

                        DataTable allNOEmailMobileNos = GetNOEmailMobileNo();
                        List<string> noToEmail = new List<string>();
                        List<string> noToMobileNo = new List<string>();
                        if (allNOEmailMobileNos.Rows.Count > 0)
                        {
                            DataRow[] noEmailMobileNos = allNOEmailMobileNos.Select("[SchemeId] = " + objCall.SchemeId + " AND [StateId] = " + objCall.StateId + " AND [DistrictId] = " + objCall.DistrictId);
                            foreach (DataRow row in noEmailMobileNos)
                            {
                                string noEmail = Convert.ToString(row["NOEmail"]);
                                if (!string.IsNullOrEmpty(noEmail)) { noToEmail.Add(noEmail); }
                                string noMobileNo = Convert.ToString(row["NOMobileNo"]);
                                if (!string.IsNullOrEmpty(noMobileNo)) { noToMobileNo.Add(noMobileNo); }
                            }
                        }

                        DataTable allVendorEmailMobileNos = GetVendorEmailMobileNo();
                        List<string> vendorToEmail = new List<string>();
                        List<string> vendorToMobileNo = new List<string>();
                        List<string> vendorToEmailBLAC = new List<string>();
                        List<string> vendorToMobileNoBLAC = new List<string>();
                        if (allVendorEmailMobileNos.Rows.Count > 0)
                        {
                            DataRow[] vendorEmailMobileNos;
                            if (ddlScheme.SelectedItem.Text.ToUpper() == "BUILDING")
                            {
                                string selectedVendorIds = string.Join("','", objCall.BL_AssignedToIds.Split(','));
                                vendorEmailMobileNos = allVendorEmailMobileNos.Select("VendorMappingId IN ('" + selectedVendorIds + "')");
                                foreach (DataRow row in vendorEmailMobileNos)
                                {

                                    if (Convert.ToString(row["VendorType"]) == "3")
                                    {
                                        string vendorEmail = Convert.ToString(row["VendorEmail"]);
                                        if (!string.IsNullOrEmpty(vendorEmail)) { vendorToEmail.Add(vendorEmail); }
                                        string vendorMobileNo = Convert.ToString(row["VendorMobileNo"]);
                                        if (!string.IsNullOrEmpty(vendorMobileNo)) { vendorToMobileNo.Add(vendorMobileNo); }
                                    }
                                    else if (Convert.ToString(row["VendorType"]) == "4")
                                    {
                                        string vendorEmail = Convert.ToString(row["VendorEmail"]);
                                        if (!string.IsNullOrEmpty(vendorEmail)) { vendorToEmailBLAC.Add(vendorEmail); }
                                        string vendorMobileNo = Convert.ToString(row["VendorMobileNo"]);
                                        if (!string.IsNullOrEmpty(vendorMobileNo)) { vendorToMobileNoBLAC.Add(vendorMobileNo); }
                                    }
                                }
                            }
                            else
                            {
                                if (ddlScheme.SelectedItem.Text.ToUpper() == "AGDSM")
                                {
                                    vendorEmailMobileNos = allVendorEmailMobileNos.Select("VendorMappingId = " + objCall.AGDSM_AssignedToId);
                                }
                                else
                                {
                                    vendorEmailMobileNos = allVendorEmailMobileNos.Select("VendorMappingId = " + objCall.SL_AssignedToId);
                                }      
                                                          
                                foreach (DataRow row in vendorEmailMobileNos)
                                {
                                    string vendorEmail = Convert.ToString(row["VendorEmail"]);
                                    if (!string.IsNullOrEmpty(vendorEmail)) { vendorToEmail.Add(vendorEmail); }
                                    string vendorMobileNo = Convert.ToString(row["VendorMobileNo"]);
                                    if (!string.IsNullOrEmpty(vendorMobileNo)) { vendorToMobileNo.Add(vendorMobileNo); }
                                }
                            }
                        }

                        string roleContactNo = UIManager.CurrentUserSession().ContactNo;

                        if (ddlCategoryOfCall.SelectedItem.Text == "Complaint")
                        {
                            DataTable smsEmailSetting = GetSMSEmailSettings();
                            //Nodal Officer
                            DataRow[] noSetting = smsEmailSetting.Select("[StatusOfCallId] = " + callStatus + " AND [SettingTypeId] = " + Convert.ToInt32(SettingType.NodalOfficer));
                            if (noSetting.Count() > 0)
                            {
                                if (Convert.ToBoolean(noSetting[0]["SendSMS"]) && noToMobileNo.Count > 0)
                                {
                                    SendSMS(Convert.ToInt32(SettingType.NodalOfficer), objCall.SchemeId, "", callStatus, string.Join(";", noToMobileNo), tErrorMsg.Trim(), roleContactNo, strSMSAddress);
                                }
                                if (Convert.ToBoolean(noSetting[0]["SendEmail"]) && noToEmail.Count > 0)
                                {
                                    SendEmail(Convert.ToInt32(SettingType.NodalOfficer), objCall.SchemeId, "", callStatus, string.Join(";", noToEmail), strCallStatus, tErrorMsg.Trim(), txtNameOfCaller.Text.Trim().ToUpper(), txtNumberOfCaller.Text.Trim(), ddlCategoryOfCall.SelectedItem.Text.Trim(), ddlScheme.SelectedItem.Text.Trim(), Convert.ToString(txtRemark.Text.Trim()).ToUpper(), strAddress);
                                }
                            }

                            //Vendor
                            DataRow[] vendorSetting = smsEmailSetting.Select("[StatusOfCallId] = " + callStatus + " AND [SettingTypeId] = " + Convert.ToInt32(SettingType.Vendor));
                            if (vendorSetting.Count() > 0)
                            {
                                if (ddlScheme.SelectedItem.Text == "Building")
                                {
                                    if (Convert.ToBoolean(vendorSetting[0]["SendSMS"]) && vendorToMobileNo.Count > 0)
                                    {
                                        SendSMS(Convert.ToInt32(SettingType.Vendor), objCall.SchemeId, "3", callStatus, string.Join(";", vendorToMobileNo), tErrorMsg.Trim(), roleContactNo, strSMSAddress);
                                    }
                                    if (Convert.ToBoolean(vendorSetting[0]["SendEmail"]) && vendorToEmail.Count > 0)
                                    {
                                        SendEmail(Convert.ToInt32(SettingType.Vendor), objCall.SchemeId, "3", callStatus, string.Join(";", vendorToEmail), strCallStatus, tErrorMsg.Trim(), txtNameOfCaller.Text.Trim().ToUpper(), txtNumberOfCaller.Text.Trim(), ddlCategoryOfCall.SelectedItem.Text.Trim(), ddlScheme.SelectedItem.Text.Trim(), Convert.ToString(txtRemark.Text.Trim()).ToUpper(), strAddress);
                                    }

                                    if (Convert.ToBoolean(vendorSetting[0]["SendSMS"]) && vendorToMobileNoBLAC.Count > 0)
                                    {
                                        SendSMS(Convert.ToInt32(SettingType.Vendor), objCall.SchemeId, "4", callStatus, string.Join(";", vendorToMobileNoBLAC), tErrorMsg.Trim(), roleContactNo, strSMSAddress);
                                    }
                                    if (Convert.ToBoolean(vendorSetting[0]["SendEmail"]) && vendorToEmailBLAC.Count > 0)
                                    {
                                        SendEmail(Convert.ToInt32(SettingType.Vendor), objCall.SchemeId, "4", callStatus, string.Join(";", vendorToEmailBLAC), strCallStatus, tErrorMsg.Trim(), txtNameOfCaller.Text.Trim().ToUpper(), txtNumberOfCaller.Text.Trim(), ddlCategoryOfCall.SelectedItem.Text.Trim(), ddlScheme.SelectedItem.Text.Trim(), Convert.ToString(txtRemark.Text.Trim()).ToUpper(), strAddressBL);
                                    }
                                }
                                else
                                {
                                    if (Convert.ToBoolean(vendorSetting[0]["SendSMS"]) && vendorToMobileNo.Count > 0)
                                    {
                                        SendSMS(Convert.ToInt32(SettingType.Vendor), objCall.SchemeId, "", callStatus, string.Join(";", vendorToMobileNo), tErrorMsg.Trim(), roleContactNo, strSMSAddress);
                                    }
                                    if (Convert.ToBoolean(vendorSetting[0]["SendEmail"]) && vendorToEmail.Count > 0)
                                    {
                                        SendEmail(Convert.ToInt32(SettingType.Vendor), objCall.SchemeId, "", callStatus, string.Join(";", vendorToEmail), strCallStatus, tErrorMsg.Trim(), txtNameOfCaller.Text.Trim().ToUpper(), txtNumberOfCaller.Text.Trim(), ddlCategoryOfCall.SelectedItem.Text.Trim(), ddlScheme.SelectedItem.Text.Trim(), Convert.ToString(txtRemark.Text.Trim()).ToUpper(), strAddress);
                                    }
                                }
                            }

                            //Customer
                            DataRow[] customerSetting = smsEmailSetting.Select("[StatusOfCallId] = " + callStatus + " AND [SettingTypeId] = " + Convert.ToInt32(SettingType.Customer));
                            if (customerSetting.Count() > 0)
                            {
                                if (Convert.ToBoolean(customerSetting[0]["SendSMS"]) && !string.IsNullOrEmpty(txtNumberOfCaller.Text))
                                {
                                    SendSMS(Convert.ToInt32(SettingType.Customer), objCall.SchemeId, "", callStatus, string.Join(";", txtNumberOfCaller.Text.Trim()), tErrorMsg.Trim(), roleContactNo, strSMSAddress);
                                }
                                if (Convert.ToBoolean(customerSetting[0]["SendEmail"]) && !string.IsNullOrEmpty(txtUJEmail.Text))
                                {
                                    SendEmail(Convert.ToInt32(SettingType.Customer), objCall.SchemeId, "", callStatus, string.Join(";", txtUJEmail.Text.Trim()), strCallStatus, tErrorMsg.Trim(), txtNameOfCaller.Text.Trim().ToUpper(), txtNumberOfCaller.Text.Trim(), ddlCategoryOfCall.SelectedItem.Text.Trim(), ddlScheme.SelectedItem.Text.Trim(), Convert.ToString(txtRemark.Text.Trim()).ToUpper(), strAddress);
                                }
                            }
                        }

                        lblModelUID.Text = tErrorMsg;
                        modelUID.Show();
                        ResetUI();
                    }
                    else
                    {
                        lblPopUpMessage.Text = tErrorMsg;
                        modelMessage.Show();
                        ResetUI();
                    }

                }
                Session["IsBackToPage"] = null;
                lblMessage.Text = tErrorMsg;

            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                ResetUI();
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        private DataTable GetNOEmailMobileNo()
        {
            DataTable emailMobileNo = new DataTable();
            try
            {
                if (HttpRuntime.Cache["NodalOfficerEmailMobileNo"] == null)
                {
                    Call tCall = new Call();
                    HttpRuntime.Cache["NodalOfficerEmailMobileNo"] = tCall.NodalOfficerEmailMobileSelectAll().Tables[0];
                }
                emailMobileNo = (DataTable)HttpRuntime.Cache["NodalOfficerEmailMobileNo"];
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
            }
            return emailMobileNo;
        }

        private DataTable GetVendorEmailMobileNo()
        {
            DataTable emailMobileNo = new DataTable();
            try
            {
                if (HttpRuntime.Cache["VendorEmailMobileNo"] == null)
                {
                    Call tCall = new Call();
                    HttpRuntime.Cache["VendorEmailMobileNo"] = tCall.VendorEmailMobileSelectAll().Tables[0];
                }
                emailMobileNo = (DataTable)HttpRuntime.Cache["VendorEmailMobileNo"];
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
            }
            return emailMobileNo;
        }

        private DataTable GetProductTypes()
        {
            DataTable productTypes = new DataTable();
            try
            {
                if (HttpRuntime.Cache["ProductTypes"] == null)
                {
                    Call tCall = new Call();
                    HttpRuntime.Cache["ProductTypes"] = tCall.ProductTypesSelectAll().Tables[0];
                }
                productTypes = (DataTable)HttpRuntime.Cache["ProductTypes"];
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
            }
            return productTypes;
        }

        private DataTable GetSMSEmailSettings()
        {
            DataTable setting = new DataTable();
            try
            {
                if (HttpRuntime.Cache["SMSEmailConfigurationSettings"] == null)
                {
                    SMSEmailConfigurationSetting tSMSEmailConfigurationSetting = new SMSEmailConfigurationSetting();
                    HttpRuntime.Cache["SMSEmailConfigurationSettings"] = tSMSEmailConfigurationSetting.SelectAll().Tables[0];
                }
                setting = (DataTable)HttpRuntime.Cache["SMSEmailConfigurationSettings"];
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
            }
            return setting;
        }

        private void SendEmail(int settingType, int schemeId, string vendorType, int callStatus, string toEmail, string strCallStatus, string uid, string customerName, string customerNo, string complaintType, string schemeName, string remark, string address)
        {
            try
            {
                if (HttpRuntime.Cache["SMSEmailTemplates"] == null)
                {
                    SMSEmailTemplates tSMSEmailTemplates = new SMSEmailTemplates();
                    HttpRuntime.Cache["SMSEmailTemplates"] = tSMSEmailTemplates.SelectAll().Tables[0];
                }

                DataTable templates = (DataTable)HttpRuntime.Cache["SMSEmailTemplates"];
                DataRow[] result = templates.Select("[StatusOfCallId] = " + callStatus + " AND [SettingTypeId] = " + settingType);

                string subject = string.Empty, body = string.Empty;
                subject = Convert.ToString(result[0]["EmailTemplateSubject"]);
                subject = subject.Replace("{Status}", strCallStatus);
                subject = subject.Replace("{ComplaintType}", complaintType);
                subject = subject.Replace("{SchemeName}", schemeName);
                subject = subject.Replace("{UID}", uid);

                body = Convert.ToString(result[0]["EmailTemplateBody"]);
                body = body.Replace("{UID}", uid);
                body = body.Replace("{CustomerName}", customerName);
                body = body.Replace("{CustomerNumber}", customerNo);
                body = body.Replace("{ComplaintType}", complaintType);
                body = body.Replace("{CallDescription}", remark);
                body = body.Replace("{CallDescription}", remark);
                body = body.Replace("{Address}", address);

                EmailUtils.SendEmail(uid, callStatus, settingType, schemeId, vendorType, subject, body, toEmail);
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
            }
        }

        private void SendSMS(int settingType, int schemeId, string vendorType, int callStatus, string toSMS, string uid, string contactNo, string address)
        {
            try
            {
                if (HttpRuntime.Cache["SMSEmailTemplates"] == null)
                {
                    SMSEmailTemplates tSMSEmailTemplates = new SMSEmailTemplates();
                    HttpRuntime.Cache["SMSEmailTemplates"] = tSMSEmailTemplates.SelectAll().Tables[0];
                }

                DataTable templates = (DataTable)HttpRuntime.Cache["SMSEmailTemplates"];
                DataRow[] result = templates.Select("[StatusOfCallId] = " + callStatus + " AND [SettingTypeId] = " + settingType);

                string body = string.Empty;
                body = Convert.ToString(result[0]["SMSTemplateBody"]);
                body = body.Replace("{UID}", uid);
                body = body.Replace("{ContactNo}", contactNo);
                body = body.Replace("{Address}", address);

                if (!string.IsNullOrEmpty(toSMS))
                {
                    string[] arrMobileNos = new string[] { };
                    arrMobileNos = toSMS.Split(';');
                    for (int i = 0; i < arrMobileNos.Length; i++)
                    {
                        EmailUtils.SendSMS(uid, callStatus, settingType, schemeId, vendorType, body, arrMobileNos[i]);
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
            }
        }

        [System.Web.Services.WebMethod]
        public static ArrayList PopulateCities(int stateId, int schemeid)
        {
            ArrayList list = new ArrayList();
            String strQuery = "select DistrictID, District from [District] where StateId = @StateId order by District";
            if (schemeid.ToString() == "4")
            {
                strQuery = string.Empty;
                strQuery = "Select distinct DST.DistrictID, DST.District from BuildingMapping BMG inner join District DST on BMG.DistrictId = DST.DistrictId where DST.StateId = @StateId";
            }
            else if (schemeid.ToString() == "1")
            {
                strQuery = string.Empty;
                strQuery = "Select distinct DST.DistrictID, DST.District from ULBMapping ULB inner join District DST on ULB.DistrictId = DST.DistrictId inner join ZoneMapping ZMG on ZMG.ULBMappingId = ULB.ULBMappingId where DST.StateId = @StateId and DST.DistrictId not in(137,740)";
            }
            else if (schemeid.ToString() == "2")
            {
                strQuery = string.Empty;
                strQuery = "Select distinct DST.DistrictID, DST.District from DiscomMapping DSM inner join District DST on DSM.DistrictId = DST.DistrictId where DST.StateId = @StateId";
            }
            else if (schemeid.ToString() == "3")
            {
                strQuery = string.Empty;
                strQuery = "select DistrictID, lower(District) as District from [District] where StateId = @StateId order by District";
            }
            String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@StateId", stateId);
                    cmd.CommandText = strQuery;
                    cmd.Connection = con;
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        list.Add(new ListItem(
                       sdr["District"].ToString(),
                       sdr["DistrictID"].ToString()
                        ));
                    }
                    con.Close();
                    return list;
                }
            }
        }

        protected void btnHistory_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                Label gvlblCallId = (Label)gvr.FindControl("gvlblCallId");
                int callId = Convert.ToInt32(gvlblCallId.Text.Trim());
                FillCallDetailHistoryGrid(callId);
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);

            }
        }

        protected void FillCallDetailHistoryGrid(int callId)
        {
            try
            {
                Call tCall = new Call();
                DataSet ds = new DataSet();
                tCall.CallId = callId;
                ds = tCall.GetCallHistory();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvCallDetailHistory.DataSource = ds.Tables[0];
                    gvCallDetailHistory.DataBind();
                    gvCallDetailHistory.Visible = true;
                    modelHistory.Show();
                }
                else
                {
                    gvCallDetailHistory.Visible = false;
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void FillCurrentCallDetailHistoryGrid(int callId)
        {
            try
            {
                Call tCall = new Call();
                DataSet ds = new DataSet();
                tCall.CallId = callId;
                ds = tCall.GetCallHistory();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvCallDetailHistoryCurrent.DataSource = ds.Tables[0];
                    gvCallDetailHistoryCurrent.DataBind();
                    gvCallDetailHistoryCurrent.Visible = true;
                    btnNewComplain.Visible = true;
                }
                else
                {
                    gvCallDetailHistoryCurrent.Visible = false;
                    btnNewComplain.Visible = false;
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void gvCalllDetail_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvCalllDetail.PageIndex = e.NewPageIndex;
            if (!string.IsNullOrEmpty(ariaUID))
            {
                FillExistingCallDetailByUID(lblUID.Text.Trim());
            }
            else
            {
                FillExistingCallDetailByMobileNo(txtNumberOfCaller.Text.Trim());
            }

        }

        protected void btnFAQ_Click(object sender, EventArgs e)
        {
            string schemeId = ddlScheme.SelectedIndex == 0 ? "0" : ddlScheme.SelectedItem.Value;
            string url = "../View/frmFAQs.aspx?SID=" + schemeId;
            ClientScript.RegisterStartupScript(this.GetType(), "OpenWin", "<script>PopupCenter('" + url + "','FAQ','900','500');</script>");

        }

        protected void txtNumberOfCaller_TextChanged(object sender, EventArgs e)
        {
            try
            {
                FillExistingCallDetailByMobileNo(txtNumberOfCaller.Text);
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                GridViewRow gvr = (GridViewRow)btn.NamingContainer;
                Label gvlblUID = (Label)gvr.FindControl("gvlblUID");
                string mobileNo = Convert.ToString(txtNumberOfCaller.Text.Trim());
                string uId = Convert.ToString(gvlblUID.Text.Trim());
                Session["EntryView"] = "YES";
                Response.Redirect("~/View/frmInBoundEntry.aspx?cid=" + mobileNo + "&UniqueID=" + uId, false);

            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);

            }
        }

        //public void PopulateWardDropDownList(int zoneId)
        //{
        //    //ArrayList list = new ArrayList();
        //    DataSet ds = new DataSet();
        //    String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        //    string strQuery = "SELECT WardMappingId, WardName +' - ( ' + WardNo + ' )' FROM WardMapping WHERE ZoneMappingId = @ZoneMappingId ORDER BY WardName";
        //    ddlSLWard.Items.Clear();
        //    using (SqlConnection con = new SqlConnection(strConnString))
        //    {
        //        using (SqlCommand cmd = new SqlCommand())
        //        {
        //            cmd.CommandType = CommandType.Text;
        //            cmd.Parameters.AddWithValue("@ZoneMappingId", zoneId);
        //            cmd.CommandText = strQuery;
        //            cmd.Connection = con;
        //            con.Open();
        //            SqlDataAdapter da = new SqlDataAdapter(cmd);
        //            da.Fill(ds);
        //            con.Close();
        //            ddlSLWard.DataSource = ds.Tables[0];
        //            ddlSLWard.DataBind();
        //            ddlSLWard.Items.Insert(0, "--Select--");
        //        }
        //    }
        //}

        [WebMethod]
        public static List<string> GetWardNo(string WardNo)
        {
            List<string> result = new List<string>();
            using (SqlConnection con = new SqlConnection(SQLDBHelper.GetConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select Top 100 WardNo from Ward where WardNo LIKE ''+@SearchName+'%' Order By WardNo";
                    cmd.Connection = con;
                    con.Open();
                    cmd.Parameters.AddWithValue("@SearchName", WardNo);
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        result.Add(dr["WardNo"].ToString());
                    }
                    con.Close();
                    return result;
                }
            }
        }

        [WebMethod]
        public static List<string> GetPoleNo(string PoleNo)
        {
            List<string> result = new List<string>();
            using (SqlConnection con = new SqlConnection(SQLDBHelper.GetConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select Top 100 PoleNo from Pole where PoleNo LIKE ''+@SearchName+'%' Order By PoleNo";
                    cmd.Connection = con;
                    con.Open();
                    cmd.Parameters.AddWithValue("@SearchName", PoleNo);
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        result.Add(dr["PoleNo"].ToString());
                    }
                    con.Close();
                    return result;
                }
            }
        }

        [WebMethod]
        public static List<string> GetConsumerNo(string Consumer)
        {
            List<string> result = new List<string>();
            using (SqlConnection con = new SqlConnection(SQLDBHelper.GetConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select Top 100 Consumer from Consumer where Consumer LIKE ''+@SearchName+'%' Order By Consumer";
                    cmd.Connection = con;
                    con.Open();
                    cmd.Parameters.AddWithValue("@SearchName", Consumer);
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        result.Add(dr["Consumer"].ToString());
                    }
                    con.Close();
                    return result;
                }
            }
        }

        [WebMethod]
        public static List<string> GetUJTown(string Town)
        {
            List<string> result = new List<string>();
            using (SqlConnection con = new SqlConnection(SQLDBHelper.GetConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select Top 100 Town from Town where Town LIKE ''+@SearchName+'%' Order By Town";
                    cmd.Connection = con;
                    con.Open();
                    cmd.Parameters.AddWithValue("@SearchName", Town);
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        result.Add(dr["Town"].ToString());
                    }
                    con.Close();
                    return result;
                }
            }
        }

        [WebMethod]
        public static List<string> GetAGDSMTown(string Town)
        {
            List<string> result = new List<string>();
            using (SqlConnection con = new SqlConnection(SQLDBHelper.GetConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select Top 100 Town from Town where Town LIKE ''+@SearchName+'%' Order By Town";
                    cmd.Connection = con;
                    con.Open();
                    cmd.Parameters.AddWithValue("@SearchName", Town);
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        result.Add(dr["Town"].ToString());
                    }
                    con.Close();
                    return result;
                }
            }
        }

        [WebMethod]
        public static List<string> GetDistributionCenter(string DistributionCenter)
        {
            List<string> result = new List<string>();
            using (SqlConnection con = new SqlConnection(SQLDBHelper.GetConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "select Top 100 DistributionCenter from DistributionCenter where DistributionCenter LIKE ''+@SearchName+'%' Order By DistributionCenter";
                    cmd.Connection = con;
                    con.Open();
                    cmd.Parameters.AddWithValue("@SearchName", DistributionCenter);
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        result.Add(dr["DistributionCenter"].ToString());
                    }
                    con.Close();
                    return result;
                }
            }
        }

        [WebMethod]
        public static ArrayList PopulateNodalOfficer(int stateId, int districtId, int schemeId)
        {
            ArrayList list = new ArrayList();
            String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            String strQuery = "select NOMappingId, NOName from [NodalOfficerMapping] where StateId = @stateId AND DistrictId = @DistrictId AND SchemeId = @SchemeId";
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@StateId", stateId);
                    cmd.Parameters.AddWithValue("@DistrictId", districtId);
                    cmd.Parameters.AddWithValue("@SchemeId", schemeId);
                    cmd.CommandText = strQuery;
                    cmd.Connection = con;
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        list.Add(new ListItem(
                       sdr["NOName"].ToString(),
                       sdr["NOMappingId"].ToString()
                        ));
                    }
                    con.Close();
                    return list;
                }
            }
        }

        [WebMethod]
        public static ArrayList PopulateULB(int stateId, int districtId)
        {
            ArrayList list = new ArrayList();
            String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            String strQuery = "select ULBMappingId, ULB from [ULBMapping] where StateId = @stateId AND DistrictId = @DistrictId AND IsActive = 1";
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@StateId", stateId);
                    cmd.Parameters.AddWithValue("@DistrictId", districtId);
                    cmd.CommandText = strQuery;
                    cmd.Connection = con;
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        list.Add(new ListItem(
                       sdr["ULB"].ToString(),
                       sdr["ULBMappingId"].ToString()
                        ));
                    }
                    con.Close();
                    return list;
                }
            }
        }

        [WebMethod]
        public static ArrayList PopulateZone(int stateId, int districtId, int ulbMappingId)
        {
            ArrayList list = new ArrayList();
            String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            String strQuery = "select ZoneMappingId, Zone from [ZoneMapping] where StateId = @stateId AND DistrictId = @DistrictId AND ULBMappingId = @ULBMappingId AND IsActive = 1";
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@StateId", stateId);
                    cmd.Parameters.AddWithValue("@DistrictId", districtId);
                    cmd.Parameters.AddWithValue("@ULBMappingId", ulbMappingId);
                    cmd.CommandText = strQuery;
                    cmd.Connection = con;
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        list.Add(new ListItem(
                       sdr["Zone"].ToString(),
                       sdr["ZoneMappingId"].ToString()
                        ));
                    }
                    con.Close();
                    return list;
                }
            }
        }

        [WebMethod]
        public static ArrayList PopulateVendor(int stateId, int districtId, int ulbMappingId, int zoneId)
        {
            ArrayList list = new ArrayList();
            String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string strQuery = "SELECT VendorMappingId, VendorName FROM VendorMapping VM inner join UserMaster UM on VM.VendorMappingId = UM.VendorId WHERE StateId = @stateId AND DistrictId = @DistrictId AND ULBMappingId = @ULBMappingId AND (@ZoneMappingId = 0 OR ZoneMappingId = @ZoneMappingId) AND VM.IsActive = 1 ORDER BY VendorType DESC, VendorName ASC";
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@StateId", stateId);
                    cmd.Parameters.AddWithValue("@DistrictId", districtId);
                    cmd.Parameters.AddWithValue("@ULBMappingId", ulbMappingId);
                    cmd.Parameters.AddWithValue("@ZoneMappingId", zoneId);
                    cmd.CommandText = strQuery;
                    cmd.Connection = con;
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        list.Add(new ListItem(
                       sdr["VendorName"].ToString(),
                       sdr["VendorMappingId"].ToString()
                        ));
                    }
                    con.Close();
                    return list;
                }
            }
        }

        [WebMethod]
        public static ArrayList PopulateWard(int stateId, int districtId, int ulbMappingId, int zoneId)
        {
            ArrayList list = new ArrayList();
            String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string strQuery = "SELECT WardMappingId, WardName, WardNo FROM WardMapping WHERE ZoneMappingId = @ZoneMappingId ORDER BY WardName";
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@ZoneMappingId", zoneId);
                    cmd.CommandText = strQuery;
                    cmd.Connection = con;
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        list.Add(new ListItem(
                       sdr["WardName"].ToString() + " - ( " + sdr["WardNo"].ToString() + " )",
                       sdr["WardMappingId"].ToString()
                        ));
                    }
                    con.Close();
                    return list;
                }
            }
        }

        [WebMethod]
        public static ArrayList PopulateDiscom(int stateId, int districtId)
        {
            ArrayList list = new ArrayList();
            String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            String strQuery = "select DiscomMappingId, Discom from [DiscomMapping] where StateId = @stateId AND DistrictId = @DistrictId AND IsActive = 1";
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@StateId", stateId);
                    cmd.Parameters.AddWithValue("@DistrictId", districtId);
                    cmd.CommandText = strQuery;
                    cmd.Connection = con;
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        list.Add(new ListItem(
                       sdr["Discom"].ToString(),
                       sdr["DiscomMappingId"].ToString()
                        ));
                    }
                    con.Close();
                    return list;
                }
            }
        }

        [WebMethod]
        public static ArrayList PopulateBuilding(int stateId, int districtId)
        {
            ArrayList list = new ArrayList();
            String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            String strQuery = "select BuildingMappingId, Building from [BuildingMapping] where StateId = @stateId AND DistrictId = @DistrictId AND IsActive = 1";
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@StateId", stateId);
                    cmd.Parameters.AddWithValue("@DistrictId", districtId);
                    cmd.CommandText = strQuery;
                    cmd.Connection = con;
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        list.Add(new ListItem(
                       sdr["Building"].ToString(),
                       sdr["BuildingMappingId"].ToString()
                        ));
                    }
                    con.Close();
                    return list;
                }
            }
        }

        protected void btnNewComplain_Click(object sender, EventArgs e)
        {
            string mobileNo;
            mobileNo = txtNumberOfCaller.Text;

            PopulateUI();
            ResetUI();
            LightTypeTr.Visible = true;
            txtAddress.Enabled = true;
            txtLandmark.Enabled = true;
            txtNumberOfCaller.Text = mobileNo;
        }

        [WebMethod]
        public static string PopulateVendors(string BuildingId)
        {
            String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

            string query = "SELECT VM.[VendorMappingId],[VendorName]+' ('+VT.VendorType+' Vendor )' as VendorName " +
            " FROM BuildingVendorMapping BVM " +
            " join VendorMapping VM on VM.VendorMappingId=BVM.VendorMappingId " +
            " join VendorType VT on VT.VendorTypeValue=VM.VendorType and VT.SchemeId= VM.SchemeId " +
            " WHERE BuildingMappingId = " + BuildingId;

            List<CheckBoxItem> vendors = new List<CheckBoxItem>();
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand(query))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Connection = con;
                        sda.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            con.Open();
                            using (SqlDataReader sdr = cmd.ExecuteReader())
                            {
                                while (sdr.Read())
                                {
                                    vendors.Add(new CheckBoxItem
                                    {
                                        Name = sdr["VendorName"].ToString(),
                                        Value = sdr["VendorMappingId"].ToString(),
                                        IsChecked = "false"
                                    });
                                }
                            }
                            con.Close();
                        }
                    }
                }
            }
            JavaScriptSerializer ser = new JavaScriptSerializer();
            return ser.Serialize(vendors);
        }

        [WebMethod]
        public static List<int> GetProductVendros(int stateId, int districtId, string productTypeIds)
        {
            string[] prodIds = productTypeIds.Split(',');
            List<int> result = new List<int>();
            String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            String strQuery = "SELECT DISTINCT VPM.VendorMappingId"
                             + " FROM[dbo].[VendorMapping] VM INNER JOIN[dbo].[VendorProductMapping] VPM"
                             + " ON VM.VendorMappingId = VPM.VendorMappingId "
                             + " WHERE StateId = @StateId AND DistrictId = @DistrictId AND VPM.ProductTypeId IN ({0})";

            string[] paramNames = prodIds.Select((s, i) => "@prodId" + i.ToString()).ToArray();
            string inClause = string.Join(", ", paramNames);

            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@StateId", stateId);
                    cmd.Parameters.AddWithValue("@DistrictId", districtId);
                    for (int i = 0; i < paramNames.Length; i++)
                    {
                        cmd.Parameters.AddWithValue(paramNames[i], prodIds[i]);
                    }
                    cmd.CommandText = string.Format(strQuery, inClause);
                    cmd.Connection = con;
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        result.Add(Convert.ToInt32(sdr["VendorMappingId"]));
                    }
                    con.Close();
                    return result;
                }
            }
        }

        [WebMethod]
        public static ArrayList PopulateAGDSMVendor(int schemeId, int stateId, int districtId)
        {
            ArrayList list = new ArrayList();
            String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            string strQuery = "SELECT VendorMappingId, VendorName FROM VendorMapping WHERE StateId = @stateId AND DistrictId = @DistrictId AND SchemeId = @SchemeId ORDER BY VendorType DESC, VendorName ASC";
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@StateId", stateId);
                    cmd.Parameters.AddWithValue("@DistrictId", districtId);
                    cmd.Parameters.AddWithValue("@SchemeId", schemeId);
                    cmd.CommandText = strQuery;
                    cmd.Connection = con;
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        list.Add(new ListItem(
                       sdr["VendorName"].ToString(),
                       sdr["VendorMappingId"].ToString()
                        ));
                    }
                    con.Close();
                    return list;
                }
            }
        }
        
        protected void imgbtnBack_Click(object sender, ImageClickEventArgs e)
        {
            Session["IsBackToPage"] = null;
            Session["EntryView"] = null;
            string Url = string.Empty;
            if (Session["PreviousUrl"] != null)
                Url = Session["PreviousUrl"].ToString();
            else
                Url = Request.UrlReferrer.AbsoluteUri;
            Session["PreviousUrl"] = null;
            Response.Redirect(Url);
        }

        protected void ddlScheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            string schemeid = ddlScheme.SelectedValue;
            txtAddress.Enabled = true;
            txtLandmark.Enabled = true;
            txtAGDSMHPkWRating.Enabled = true;

            if (schemeid == "4")
            {
                string Query = "Select distinct STE.StateId,STE.State from BuildingMapping BMG inner join State STE on BMG.StateId = STE.StateId";
                DataTable dt = SQLDBHelper.GetDataInTable(Query);
                if (dt.Rows.Count > 0)
                {
                    ddlState.DataSource = dt;
                    ddlState.DataValueField = "StateId";
                    ddlState.DataTextField = "State";
                    ddlState.DataBind();
                    ddlState.Items.Insert(0, "--Select--");
                }
                txtAddress.Enabled = false;
                txtLandmark.Enabled = false;
            }
            else if (schemeid == "1")
            {
                string Query = "Select distinct STE.StateId, STE.State from ULBMapping ULB inner join State STE on ULB.StateId = STE.StateId inner join ZoneMapping ZMG on ZMG.ULBMappingId = ULB.ULBMappingId";
                DataTable dt = SQLDBHelper.GetDataInTable(Query);
                if (dt.Rows.Count > 0)
                {
                    ddlState.DataSource = dt;
                    ddlState.DataValueField = "StateId";
                    ddlState.DataTextField = "State";
                    ddlState.DataBind();
                    ddlState.Items.Insert(0, "--Select--");
                }
            }
            else if (schemeid == "2")
            {
                string Query = "Select distinct STE.StateId,STE.State from DiscomMapping DSM inner join State STE on DSM.StateId = STE.StateId";
                DataTable dt = SQLDBHelper.GetDataInTable(Query);
                if (dt.Rows.Count > 0)
                {
                    ddlState.DataSource = dt;
                    ddlState.DataValueField = "StateId";
                    ddlState.DataTextField = "State";
                    ddlState.DataBind();
                    ddlState.Items.Insert(0, "--Select--");
                }
            }
            else if (schemeid == "3")
            {
                txtAGDSMHPkWRating.Enabled = false;
                SQLDBHelper.PopulateDropDownList(ddlState, "State", "State", "StateId", false, "");
            }
            else
                SQLDBHelper.PopulateDropDownList(ddlState, "State", "State", "StateId", false, "");

            ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "SchemeChange();CheckStreetLight();", true);
        }

        [WebMethod]
        public static List<string> GetBuildingDetails(string BuildingId)
        {
            List<string> result = new List<string>();
            using (SqlConnection con = new SqlConnection(SQLDBHelper.GetConnectionString()))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = "Select * from BuildingMapping where BuildingMappingId = @BuildingId";
                    cmd.Connection = con;
                    con.Open();
                    cmd.Parameters.AddWithValue("@BuildingId", BuildingId);
                    SqlDataReader dr = cmd.ExecuteReader();
                    while (dr.Read())
                    {
                        result.Add(dr["Address"].ToString());
                        result.Add(dr["Landmark"].ToString());
                    }
                    con.Close();
                    return result;
                }
            }
        }

        [WebMethod]
        public static string GetAgDSMLoginDetails(string Test)
        {
            string output = string.Empty;
            HttpResponseMessage response = new HttpResponseMessage();
            string ResponseCode = "";
            string ResponseString = "";
            HttpClient client = new HttpClient();
            APILoginDetails objAPILoginDetails = new APILoginDetails();

            objAPILoginDetails.Privatekey = "g1Sm@D";
            objAPILoginDetails.UserName = "CC101";
            objAPILoginDetails.Password = "abcd1234";
            objAPILoginDetails.IPAddress = "127.0.0.1";

            string FullJson = JsonConvert.SerializeObject(objAPILoginDetails);

            response = client.PostAsJsonAsync("http://agdsmtest.cruxbytes.com/api/common/Login", FullJson).Result;

            if (response.IsSuccessStatusCode)
            {
                ResponseCode = response.StatusCode.ToString();
                var users = response.Content.ReadAsStringAsync();
                ResponseString = users.Result;
                dynamic parsedJson = JsonConvert.DeserializeObject(ResponseString);
                output = parsedJson;
            }
            return output;
        }

        public class APILoginDetails
        {
            public string Privatekey { get; set; }
            public string UserName { get; set; }
            public string Password { get; set; }
            public string IPAddress { get; set; }
        }

        [WebMethod]
        public static string GetAgDSMDetails(string UserID, string SessionID, string SearchText, string SearchType)
        {
            string output = string.Empty;
            string ResponseCode = "";
            string ResponseString = "";
            HttpResponseMessage response = new HttpResponseMessage();
            HttpClient client = new HttpClient();

            APIGetDetails details = new APIGetDetails();
            details.UserID = UserID;
            details.SessionID = SessionID;
            details.SearchText = SearchText;
            details.SearchType = SearchType;

            string FullJson = JsonConvert.SerializeObject(details);

            response = client.PostAsJsonAsync("http://agdsmtest.cruxbytes.com/api/common/GetConsumerDetails", FullJson).Result;

            if (response.IsSuccessStatusCode)
            {
                ResponseCode = response.StatusCode.ToString();
                var users = response.Content.ReadAsStringAsync();
                ResponseString = users.Result;
                dynamic parsedJson = JsonConvert.DeserializeObject(ResponseString);
                output = parsedJson;
            }
            return output;
            
        }

        public class APIGetDetails
        {
            public string UserID { get; set; }
            public string SessionID { get; set; }
            public string SearchText { get; set; }
            public string SearchType { get; set; }
        }

        //protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    string schemeid = ddlScheme.SelectedValue;
        //    string stateid = ddlState.SelectedValue;
        //    if (schemeid == "4")
        //    {
        //        string Query = "Select DST.DistrictId, DST.District from BuildingMapping BMG inner join District DST on BMG.DistrictId = DST.DistrictId where DST.StateId = " + stateid + "";
        //        DataTable dt = SQLDBHelper.GetDataInTable(Query);
        //        if (dt.Rows.Count > 0)
        //        {
        //            ddlDistrict.DataSource = dt;
        //            ddlDistrict.DataValueField = "DistrictId";
        //            ddlDistrict.DataTextField = "District";
        //            ddlDistrict.DataBind();
        //            ddlDistrict.Items.Insert(0, "--Select--");
        //        }
        //    }
        //    else
        //        SQLDBHelper.PopulateDropDownList(ddlDistrict, "District", "District", "DistrictId", false, "");

        //    ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "script", "PopulateCities();", true);

        //}
    }
}