﻿using CRM.Models;
using EESL.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace EESL.View
{
    public partial class frmVendorProducts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!IsPostBack)
            {
                SetUI();
            }
        }

        protected void SetUI()
        {
            chklstProducts.Items.Clear();

            ddlVendorType.Items.Clear();
            ddlVendorType.Items.Insert(0, new ListItem { Value = "0", Text = "--Select--" });
            ddlVendorType.SelectedIndex = 0;

            ddlState.Items.Clear();
            ddlDistrict.Items.Clear();
            ddlDistrict.Items.Insert(0, new ListItem { Value = "0", Text = "--Select--" });

            ddlVendor.Items.Clear();
            ddlVendor.Items.Insert(0, new ListItem { Value = "0", Text = "--Select--" });

            SQLDBHelper.PopulateDropDownList(ddlState, "State", "State", "StateId", false, "");
            SQLDBHelper.PopulateDropDownList(ddlScheme, "Scheme", "Scheme", "SchemeId", false, "");

            //ddlScheme.Items.FindByValue("--Select--").Enabled = false;
            ddlScheme.Items.FindByValue("1").Enabled = false;
            ddlScheme.Items.FindByValue("2").Enabled = false;
            ddlScheme.Items.FindByValue("3").Enabled = false;
            ddlScheme.Items.FindByValue("5").Enabled = false;
        }

        protected void ddlScheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlVendorType.Items.Clear();
            ddlVendor.Items.Clear();
            chklstProducts.Items.Clear();

            if (ddlScheme.SelectedItem.Value != "--Select--")
            {
                String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                String strQuery = "SELECT VendorTypeId,VendorType,VendorTypeValue,SchemeId From VendorType where SchemeId = @SchemeId";
                using (SqlConnection con = new SqlConnection(strConnString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.AddWithValue("@SchemeId", ddlScheme.SelectedItem.Value);
                        cmd.CommandText = strQuery;
                        cmd.Connection = con;
                        con.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        ddlVendorType.DataSource = ds;
                        ddlVendorType.DataTextField = "VendorType";
                        ddlVendorType.DataValueField = "VendorTypeValue";
                        ddlVendorType.DataBind();
                        ddlVendorType.Items.Insert(0, new ListItem { Value = "0", Text = "--Select--" });
                        con.Close();
                    }
                }
                PopulateVendor(ddlScheme.SelectedItem.Value, ddlVendorType.SelectedItem.Value, ddlState.SelectedItem.Value, ddlDistrict.SelectedItem.Value);
            }
            else
            {
                ddlVendorType.Items.Insert(0, new ListItem { Value = "0", Text = "--Select--" });
            }
        }

        protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlDistrict.Items.Clear();
            ddlVendor.Items.Clear();
            if (ddlState.SelectedItem.Value != "--Select--")
            {
                String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                String strQuery = "select DistrictID, District from [District] where StateId = @StateId";
                using (SqlConnection con = new SqlConnection(strConnString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.AddWithValue("@StateId", ddlState.SelectedItem.Value);
                        cmd.CommandText = strQuery;
                        cmd.Connection = con;
                        con.Open();
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        ddlDistrict.DataSource = ds;
                        ddlDistrict.DataTextField = "District";
                        ddlDistrict.DataValueField = "DistrictID";
                        ddlDistrict.DataBind();
                        ddlDistrict.Items.Insert(0, new ListItem { Value = "0", Text = "--Select--" });

                        con.Close();
                    }
                }

                PopulateVendor(ddlScheme.SelectedItem.Value, ddlVendorType.SelectedItem.Value, ddlState.SelectedItem.Value, ddlDistrict.SelectedItem.Value);
            }
            else
            {
                ddlDistrict.Items.Clear();
                ddlDistrict.Items.Insert(0, new ListItem { Value = "0", Text = "--Select--" });
                ddlVendor.Items.Clear();
                ddlVendor.Items.Insert(0, new ListItem { Value = "0", Text = "--Select--" });
            }
            VendorProductList();
        }

        protected void ddlDistrict_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateVendor(ddlScheme.SelectedItem.Value, ddlVendorType.SelectedItem.Value, ddlState.SelectedItem.Value, ddlDistrict.SelectedItem.Value);
            VendorProductList();
        }

        public void PopulateVendor(string schemeId, string vendortypeId, string stateId, string districtId)
        {
            ddlVendor.Items.Clear();
            if (schemeId == "--Select--")
                schemeId = "0";
            if (vendortypeId == "--Select--")
                vendortypeId = "0";
            if (stateId == "--Select--")
                stateId = "0";
            if (districtId == "--Select--")
                districtId = "0";
            String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            String strQuery = "SELECT VendorMappingId, VendorName From VendorMapping where IsActive=1 and SchemeId=@SchemeId and VendorType = @VendorTypeId and StateId= @StateId and (@DistrictId=0 or DistrictId= @DistrictId) ";
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@SchemeId", schemeId);
                    cmd.Parameters.AddWithValue("@VendorTypeId", vendortypeId);
                    cmd.Parameters.AddWithValue("@StateId", stateId);
                    cmd.Parameters.AddWithValue("@DistrictId", districtId);
                    cmd.CommandText = strQuery;
                    cmd.Connection = con;
                    con.Open();

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    ddlVendor.DataSource = ds;
                    ddlVendor.DataTextField = "VendorName";
                    ddlVendor.DataValueField = "VendorMappingId";
                    ddlVendor.DataBind();
                    ddlVendor.Items.Insert(0, new ListItem { Value = "0", Text = "--Select--" });
                    
                    con.Close();
                }
            }
            
        }

        protected void ddlVendorType_SelectedIndexChanged(object sender, EventArgs e)
        {
            PopulateVendor(ddlScheme.SelectedItem.Value, ddlVendorType.SelectedItem.Value, ddlState.SelectedItem.Value, ddlDistrict.SelectedItem.Value);
            VendorProductList();
        }

        private void VendorProductList()
        {

            if (ddlVendorType.SelectedItem.Value != "0")
            {
                chklstProducts.Items.Clear();
                String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                
                String strQuery = "SELECT[ProductTypeId], [ProductType], CASE WHEN EXISTS(" +
                " select 1 from dbo.VendorProductMapping vpm where vpm.ProductTypeId= py.ProductTypeId " +
                " and VendorMappingId = " + ddlVendor.SelectedItem.Value + " ) THEN 1 ELSE 0 END as VendorProduct " +
                " FROM[ProductType] py where py.VendorTypeId = @VendorTypeId";

                //" CASE WHEN EXISTS( select 1 from dbo.CallDetailMaster CDM " +
                //" join VendorProductMapping VPM on VPM.VendorMappingId = CDM.SL_AssignedToId " +
                //" where cdm.SchemeId= " + ddlScheme.SelectedItem.Value + " ) THEN 1 ELSE 0 END as VendorProductAssigned

                using (SqlConnection con = new SqlConnection(strConnString))
                {
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        cmd.CommandType = CommandType.Text;
                        cmd.Parameters.AddWithValue("@VendorTypeId", ddlVendorType.SelectedItem.Value);
                        cmd.CommandText = strQuery;
                        cmd.Connection = con;
                        con.Open();

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        foreach (DataRow row in ds.Tables[0].Rows)
                        {
                            ListItem item = new ListItem();
                            item.Text = row["ProductType"].ToString();
                            item.Value = row["ProductTypeId"].ToString();
                            if (row["VendorProduct"].ToString() == "1")
                            {
                                item.Selected = true;
                                item.Enabled = false;
                                //editflag = false;
                            }
                            else
                                item.Selected = false;
                            //if (row["VendorProductAssigned"].ToString() == "1")
                            //    item.Enabled = false;
                            //else
                            //    item.Enabled = true;
                            chklstProducts.Items.Add(item);
                        }
                        

                        con.Close();
                    }
                }
            }
            else
            {
                chklstProducts.Items.Clear();
            }
            //chklstProducts.Enabled = editflag;
            //btnSave.Enabled = editflag;
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            string msgitem = string.Empty;
            VendorBuildingProducts products = new VendorBuildingProducts();

            foreach(ListItem item in chklstProducts.Items)
            {
                if (item.Selected == true && item.Enabled==true)
                {
                    products.VendorMappingId = Convert.ToInt32(ddlVendor.SelectedItem.Value);
                    products.ProductTypeId = Convert.ToInt32(item.Value);
                    products.VendorProductInsert(out msgitem);
                }
            }
            SetUI();
            lblMsg.Text = msgitem;
        }

        protected void ddlVendor_SelectedIndexChanged(object sender, EventArgs e)
        {
            VendorProductList();
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            SetUI();
        }
    }

    public class CheckBoxItem
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public string IsChecked { get; set; }
    }
}