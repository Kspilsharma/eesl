﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteNew.Master" AutoEventWireup="true" CodeBehind="frmZoneMapping.aspx.cs" Inherits="EESL.View.frmZoneMapping" EnableEventValidation="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.min.js"></script>
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        $(document).ready(function () {
            $('#<% =ddlDistrict.ClientID %>').change(function (e) {
                var districId = $('#<% =ddlDistrict.ClientID %>').val();
                $('#<% =hdnDistricId.ClientID %>').attr('value', districId);
            });
            $('#<% =ddlULB.ClientID %>').change(function (e) {
                var ulbMappingId = $('#<% =ddlULB.ClientID %>').val();
                $('#<% =hdnULBMappingId.ClientID %>').attr('value', ulbMappingId);
            });
        });

        var pageUrl = '<%=ResolveUrl("~/View/frmZoneMapping.aspx")%>'
        function PopulateCities() {

            if ($('#<%=ddlState.ClientID%>').val() == "--Select--") {
                $('#<%=ddlDistrict.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            }
            else {
                $('#<%=ddlDistrict.ClientID %>').empty().append('<option selected="selected" value="0">Loading...</option>');
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/PopulateCities',
                    data: '{stateId: ' + $('#<%=ddlState.ClientID%>').val() + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnCitiesPopulated,
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
        }

        function OnCitiesPopulated(response) {
            PopulateControl(response.d, $("#<%=ddlDistrict.ClientID %>"),"District");
        }

        function PopulateULB() {

            if ($('#<%=ddlDistrict.ClientID%>').val() == "--Select--") {
                $('#<%=ddlULB.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            }
            else {
                $('#<%=ddlULB.ClientID %>').empty().append('<option selected="selected" value="0">Loading...</option>');
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/PopulateULB',
                    data: '{stateId: ' + $('#<%=ddlState.ClientID%>').val() + ',districtId: ' + $('#<%=ddlDistrict.ClientID%>').val() + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnULBPopulated,
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
        }

        function OnULBPopulated(response) {
            PopulateControl(response.d, $("#<%=ddlULB.ClientID %>"),"ULB");
        }

        function PopulateControl(list, control,type) {
            if (list.length > 0) {
                control.empty().append('<option selected="selected" value="0">--Select--</option>');
                $.each(list, function () {
                    control.append($("<option></option>").val(this['Value']).html(this['Text']));
                });
            }
            else {
                if (type == "District") {
                    $('#<%=ddlDistrict.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
                }
                else if (type == "ULB") {
                    $('#<%=ddlULB.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
                }
                
            }
        }



    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="Heading" align="center" style="color: #101010">
                <asp:Label ID="lblPageHeader" runat="server" Text="Zone Mapping" CssClass="bold"></asp:Label>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
        <tr>
            <td align="left" valign="top" width="15%"></td>
            <td valign="top" width="70%">
                <fieldset class="fieldset">
                    <legend class="Legendheading" align="left">Zone Mapping</legend>
                    <table cellpadding="0" cellspacing="3px" width="100%" style="margin: 0px; padding: 0px;">
                        <tr>
                            <td colspan="2" align="left">
                                <asp:Label ID="lblMsg" runat="server" CssClass="failureNotification"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="30%" class="tdlabel" valign="top">State<span style="color: red">&nbsp;*</span> &nbsp;:</td>
                            <td valign="top" align="left" class="tddata" width="70%">
                                <asp:DropDownList ID="ddlState" runat="server" Width="50%" onchange="PopulateCities();" TabIndex="101" />
                                &nbsp;<asp:RequiredFieldValidator ID="rfvState" ForeColor="Maroon" ControlToValidate="ddlState" InitialValue="--Select--" runat="server" ErrorMessage="Select State." Display="Dynamic" ValidationGroup="Save" Font-Bold="True" Font-Size="X-Small"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td align="left" width="30%" class="tdlabel" valign="top">District<span style="color: red">&nbsp;*</span> &nbsp;:</td>
                            <td valign="top" align="left" class="tddata" width="70%">
                                <asp:DropDownList ID="ddlDistrict" runat="server" Width="50%" onchange="PopulateULB();" TabIndex="102" />
                                &nbsp;<asp:RequiredFieldValidator ID="rfvDistrict" ForeColor="Maroon" ControlToValidate="ddlDistrict" InitialValue="--Select--" runat="server" ErrorMessage="Select District." Display="Dynamic" ValidationGroup="Save" Font-Bold="True" Font-Size="X-Small"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td align="left" class="tdlabel" valign="top" width="30%">ULB<span style="color: red">&nbsp;*</span> &nbsp;:</td>

                            <td valign="top" align="left" class="tddata" width="70%">
                                <asp:DropDownList ID="ddlULB" runat="server" Width="50%" TabIndex="103"/>
                                &nbsp;<asp:RequiredFieldValidator ID="rfvULB" ForeColor="Maroon" ControlToValidate="ddlULB" InitialValue="--Select--" runat="server" ErrorMessage="Select ULB." Display="Dynamic" ValidationGroup="Save" Font-Bold="True" Font-Size="X-Small"></asp:RequiredFieldValidator>
                            </td>
                        </tr>                       

                        <tr>
                            <td align="left" class="tdlabel" valign="top" width="30%">Zone<span style="color: red">&nbsp;*</span> &nbsp;:</td>

                            <td valign="top" align="left" class="tddata" width="70%">
                                <asp:TextBox ID="txtZone" runat="server" TabIndex="104" Width="50%" Style="text-transform: none" MaxLength="50"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="rfvZone" ErrorMessage="Enter Zone." runat="server" ForeColor="Maroon" Font-Bold="True" Font-Size="X-Small" ControlToValidate="txtZone" ValidationGroup="Save"></asp:RequiredFieldValidator>
                                <asp:FilteredTextBoxExtender ID="fteZone" runat="server" Enabled="true" TargetControlID="txtZone" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" FilterMode="ValidChars" ValidChars=" " />
                            </td>
                        </tr>                       
                        <tr>
                            <td align="left">&nbsp;
                            </td>
                            <td align="left">
                                <asp:Button ID="btnSave" Text="Save" TabIndex="105" runat="server" ValidationGroup="Save"
                                    OnClick="btnSave_Click" />
                                <asp:Button ID="btnReset" Text="Reset" TabIndex="106" runat="server" OnClick="btnReset_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left">
                                <asp:HiddenField ID="hdnId" runat="server" Value="-1" />
    <asp:HiddenField ID="hdnDistricId" runat="server" Value="0" />
    <asp:HiddenField ID="hdnULBMappingId" runat="server" Value="0" />
                                <asp:ScriptManager ID="ScriptManager1" runat="server">
                                </asp:ScriptManager>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
            <td align="left" valign="top" width="15%"></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="left" valign="top" width="15%"></td>
            <td valign="top" width="70%">
                <table border="0" width="100%">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="imgbtnExportExcel" ToolTip="Export to Excel" ImageUrl="~/Images/excel.jpg" Width="25px" Height="25px" runat="server" OnClick="imgbtnExportExcel_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                          <text>Search:</text> <asp:TextBox ID="txtSearch" runat="server" Width="15%" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged"></asp:TextBox>
                            <hr/>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:GridView ID="gvZoneMapping" TabIndex="106" PageSize="50" Width="100%" runat="server"
                                AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="ZoneMappingId" OnPageIndexChanging="gvZoneMapping_PageIndexChanging"
                                OnRowDataBound="gvZoneMapping_RowDataBound" OnRowDeleting="gvZoneMapping_RowDeleting" OnRowEditing="gvZoneMapping_RowEditing"
                                OnRowUpdating="gvZoneMapping_RowUpdating" EmptyDataText="No Record Found" CellPadding="4"
                                ForeColor="#333333" class="tabStyle" GridLines="None">
                                <AlternatingRowStyle BackColor="" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr.">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="State">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblState" runat="server" Text='<%# Bind("State") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="District">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblDistrict" runat="server" Text='<%# Bind("District") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="ULB">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblULB" runat="server" Text='<%# Bind("ULB") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Zone">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblZone" runat="server" Text='<%# Bind("Zone") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnEdit" CommandName="Edit" runat="server" ImageUrl="~/Images/edit.png"
                                                OnClientClick="return confirm('Are you sure! You want to Edit Seleted Row?');" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnStatus" CommandName="Update" runat="server" ImageUrl="~/Images/active.gif"
                                                OnClientClick="return confirm('Are you sure! You want to change status?');" />
                                            <asp:Label ID="gvlblIsActive" runat="server" Text='<%# Bind("IsActive") %>' Visible="False" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnDelete" CommandName="Delete" runat="server" ImageUrl="~/Images/delete.gif"
                                                OnClientClick="return confirm('Are you sure! You want to Delete Seleted Row?');" />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                                <EditRowStyle BackColor="#2461BF" />
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="Black" HorizontalAlign="Center" />
                                <RowStyle HorizontalAlign="Center" VerticalAlign="Top" BackColor="" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                             <br />
                            <br />
                            <br />
                        </td>
                    </tr>
                </table>
            </td>
            <td align="left" valign="top" width="15%"></td>
        </tr>
    </table>
</asp:Content>