﻿using CRM.Models;
using System;
using System.Configuration;
using System.Data;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CRM.View
{
    public partial class frmFAQMaster : System.Web.UI.Page
    {
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!IsPostBack)
            { SetUI(); }
        }

        protected void SetUI()
        {
            hdnId.Value = "-1";
            ddlScheme.Focus();
            txtQuestion.Text = "";
            txtAnswer.Text = string.Empty;
            chkIsActive.Checked = true;
            SQLDBHelper.PopulateDropDownList(ddlScheme, "Scheme", "Scheme", "SchemeId", false, "");
            FillGrid();
        }

        protected void FillGrid()
        {
            try
            {
                FAQ tFAQ = new FAQ();
                DataSet ds = new DataSet();
                ds = tFAQ.Select();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvFAQ.DataSource = ds.Tables[0];
                    gvFAQ.DataBind();
                    gvFAQ.Visible = true;
                }
                else
                {
                    gvFAQ.Visible = false;
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                FAQ tFAQ = new FAQ();
                DataSet ds = new DataSet();

                tFAQ.SchemeId = Convert.ToInt32(ddlScheme.SelectedItem.Value);
                tFAQ.Question = Convert.ToString(txtQuestion.Text.Trim());

                if (txtAnswer.Text != "")
                {
                    tFAQ.Answer = Convert.ToString(txtAnswer.Text.Trim());
                }
                else
                {
                    tFAQ.Answer = "";
                }
                tFAQ.IsActive = chkIsActive.Checked;

                tFAQ.LoginId = UIManager.CurrentUserSession().UserId;
                string tErrorMsg = string.Empty;
                if (hdnId.Value == "-1")
                {
                    if (tFAQ.Insert(out tErrorMsg))
                    { SetUI(); }
                }
                else
                {
                    tFAQ.QuestionId = Convert.ToInt64(hdnId.Value);
                    if (tFAQ.Update(out tErrorMsg))
                    { SetUI(); }
                    hdnId.Value = "-1";
                }

                lblMsg.Text = tErrorMsg;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            SetUI();
        }

        protected void gvFAQ_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                FAQ tFAQ = new FAQ();
                DataSet ds = new DataSet();
                Label lblstatus = ((Label)gvFAQ.Rows[e.NewEditIndex].FindControl("gvlblIsActive"));
                string strIsActive = lblstatus.Text;
                hdnId.Value = (gvFAQ.DataKeys[e.NewEditIndex].Value.ToString());

                tFAQ.IsActive = Convert.ToBoolean(strIsActive);
                tFAQ.QuestionId = Convert.ToInt64(hdnId.Value);
                ds = tFAQ.Edit();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    txtQuestion.Text = ds.Tables[0].Rows[0]["Question"].ToString();
                    txtAnswer.Text = ds.Tables[0].Rows[0]["Answer"].ToString();
                    chkIsActive.Checked = Convert.ToBoolean(ds.Tables[0].Rows[0]["IsActive"]);
                    int schemeId = Convert.ToInt32(ds.Tables[0].Rows[0]["SchemeId"]);
                    if (schemeId > 0)
                    {
                        ddlScheme.SelectedIndex = -1;
                        ddlScheme.Items.FindByValue(Convert.ToString(schemeId)).Selected = true;
                    }
                    else
                        ddlScheme.SelectedIndex = 0;

                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void gvFAQ_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                FAQ tFAQ = new FAQ();
                tFAQ.QuestionId = Convert.ToInt64(gvFAQ.DataKeys[e.RowIndex].Value.ToString());
                Label lblstatus = ((Label)gvFAQ.Rows[e.RowIndex].FindControl("gvlblIsActive"));
                string strIsActive = lblstatus.Text;
                if (strIsActive == "False")
                {
                    tFAQ.IsActive = true;
                }
                else
                {
                    tFAQ.IsActive = false;
                }
                string tErrorMsg = string.Empty;
                if (tFAQ.StatusUpdate(out tErrorMsg))
                {
                    FillGrid();
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void gvFAQ_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            FAQ tFAQ = new FAQ();
            Int64 FAQId = Convert.ToInt64(gvFAQ.DataKeys[e.RowIndex].Value.ToString());
            Label lblstatus = ((Label)gvFAQ.Rows[e.RowIndex].FindControl("gvlblIsActive"));
            string strIsActive = lblstatus.Text;
            try
            {
                tFAQ.IsActive = Convert.ToBoolean(strIsActive);
                tFAQ.QuestionId = FAQId;
                string tErrorMsg = string.Empty;
                if (tFAQ.Delete(out tErrorMsg))
                {
                    SetUI();
                }

                lblMsg.Text = tErrorMsg;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void gvFAQ_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvFAQ.PageIndex = e.NewPageIndex;
            FillGrid();
        }

        protected void gvFAQ_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string STRIsActive = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "IsActive"));

                    ImageButton imgbtnstatus = (ImageButton)e.Row.FindControl("imgbtnStatus");
                    if (STRIsActive == "False")
                    {
                        imgbtnstatus.ImageUrl = "~/Images/block.Jpg";
                        imgbtnstatus.ToolTip = "Click to Active";
                    }
                    else
                    {
                        imgbtnstatus.ImageUrl = "~/Images/active.gif";
                        imgbtnstatus.ToolTip = "Click to Block";
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }
    }
}