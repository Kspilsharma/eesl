﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="frmUploadInfo.aspx.cs" Inherits="CRM.View.frmUploadInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="Heading" align="center" style="color: #101010">
                <asp:Label ID="lblPageHeader" runat="server" Text="Upload Basic Customer Detail" CssClass="bold"></asp:Label>
            </td>
        </tr>
    </table>

    <table border="0" width="100%">
        <tr>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td>

                <asp:FileUpload ID="fuUpload" runat="server" />
                &nbsp;<asp:Button ID="btnUpload" runat="server" Text="Upload" OnClick="btnUpload_Click" />
                &nbsp;&nbsp;&nbsp;
                <asp:Label Text="Read and Display Data From an Excel File (.xsl or .xlsx) " ForeColor="Green" Font-Bold="true" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblMsg" ForeColor="Red" Font-Bold="true" runat="server" /></td>
        </tr>
        <tr>
            <td>
                <asp:GridView ID="grvExcelData" runat="server">
                    <HeaderStyle BackColor="#df5015" Font-Bold="true" ForeColor="White" />
                </asp:GridView>
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
