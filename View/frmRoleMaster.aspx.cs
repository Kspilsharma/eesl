﻿using CRM.Models;
using System;
using System.Configuration;
using System.Data;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CRM.View
{
    public partial class frmRoleMaster : UIBase
    {
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMessage.Text = "";

            if (!IsPostBack)
            { SetUI(); }
        }

        protected void SetUI()
        {
            hdnId.Value = "-1";
            txtRole.Text = "";
            txtContactNo.Text = "";
            FillGrid();
            txtRole.Focus();
        }

        protected void FillGrid()
        {
            try
            {
                RoleMaster tRoleMaster = new RoleMaster();
                DataSet ds = new DataSet();
                ds = tRoleMaster.Select();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvRole.DataSource = ds.Tables[0];
                    gvRole.DataBind();
                    gvRole.Visible = true;
                }
                else
                {
                    gvRole.Visible = false;
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                RoleMaster tRoleMaster = new RoleMaster();
                DataSet ds = new DataSet();

                tRoleMaster.RoleName = Convert.ToString(txtRole.Text.Trim());
                tRoleMaster.ContactNo = Convert.ToString(txtContactNo.Text.Trim());
                tRoleMaster.LoginId = UIManager.CurrentUserSession().UserId;
                string tErrorMsg = string.Empty;
                if (hdnId.Value == "-1")
                {
                    if (tRoleMaster.Insert(out tErrorMsg))
                    { SetUI(); }
                }
                else
                {
                    tRoleMaster.RoleId = Convert.ToInt32(hdnId.Value);
                    if (tRoleMaster.Update(out tErrorMsg))
                    { SetUI(); }
                    hdnId.Value = "-1";
                }
                lblMessage.Text = tErrorMsg;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            SetUI();
        }

        protected void gvRole_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                RoleMaster tRoleMaster = new RoleMaster();
                DataSet ds = new DataSet();
                Label lblstatus = ((Label)gvRole.Rows[e.NewEditIndex].FindControl("gvlblIsActive"));
                string strIsActive = lblstatus.Text;
                hdnId.Value = (gvRole.DataKeys[e.NewEditIndex].Value.ToString());

                tRoleMaster.IsActive = Convert.ToBoolean(strIsActive);
                tRoleMaster.RoleId = Convert.ToInt32(hdnId.Value);
                ds = tRoleMaster.Edit();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    txtRole.Text = ds.Tables[0].Rows[0]["RoleName"].ToString();
                    txtContactNo.Text = ds.Tables[0].Rows[0]["ContactNo"].ToString();
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void gvRole_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                RoleMaster tRoleMaster = new RoleMaster();
                tRoleMaster.RoleId = Convert.ToInt32(gvRole.DataKeys[e.RowIndex].Value.ToString());
                Label lblstatus = ((Label)gvRole.Rows[e.RowIndex].FindControl("gvlblIsActive"));
                string strIsActive = lblstatus.Text;
                if (strIsActive == "False")
                {
                    tRoleMaster.IsActive = true;
                }
                else
                {
                    tRoleMaster.IsActive = false;
                }
                string tErrorMsg = string.Empty;
                if (tRoleMaster.StatusUpdate(out tErrorMsg))
                { FillGrid(); }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void gvRole_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            try
            {
                RoleMaster tRoleMaster = new RoleMaster();
                int roleId = Convert.ToInt32(gvRole.DataKeys[e.RowIndex].Value.ToString());
                Label lblstatus = ((Label)gvRole.Rows[e.RowIndex].FindControl("gvlblIsActive"));
                string strIsActive = lblstatus.Text;

                tRoleMaster.IsActive = Convert.ToBoolean(strIsActive);
                tRoleMaster.RoleId = roleId;
                string tErrorMsg = string.Empty;
                if (tRoleMaster.Delete(out tErrorMsg))
                {
                    FillGrid();
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void gvRole_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvRole.PageIndex = e.NewPageIndex;
            FillGrid();
        }

        protected void gvRole_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string STRIsActive = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "IsActive"));

                    ImageButton imgbtnstatus = (ImageButton)e.Row.FindControl("imgbtnStatus");
                    if (STRIsActive == "False")
                    {
                        imgbtnstatus.ImageUrl = "~/Images/block.Jpg";
                        imgbtnstatus.ToolTip = "Click to Active";
                    }
                    else
                    {
                        imgbtnstatus.ImageUrl = "~/Images/active.gif";
                        imgbtnstatus.ToolTip = "Click to Block";
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }
    }
}