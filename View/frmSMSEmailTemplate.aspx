﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="frmSMSEmailTemplate.aspx.cs" ValidateRequest="false" Inherits="EESL.View.frmSMSEmailTemplate" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .unselectable.ajax__html_editor_extender_container {
            margin-bottom: 25px;
        }

        .ajax__html_editor_extender_button {
            background-color: transparent!important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <br />
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="Heading" align="center" style="color: #101010">
                <asp:Label ID="lblPageHeader" runat="server" Text="SMS Email Template" CssClass="bold"></asp:Label>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
        <tr>
            <td align="left" valign="top" width="25%"></td>
            <td valign="top" width="55%">
                <fieldset class="fieldset">
                    <legend class="Legendheading" align="left">SMS Email Configuration</legend>
                    <table cellpadding="0" cellspacing="3px" width="100%" style="margin: 0px; padding: 0px;">
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblMsg" ForeColor="Red" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top" style="width: 30%">
                                <asp:Label ID="lblRole" runat="server" Text="Type"></asp:Label>
                            </td>
                            <td valign="top" align="left" class="tddata" style="width: 70%">
                                <asp:DropDownList ID="ddlSettingType" runat="server" TabIndex="101"
                                    Width="300px" AutoPostBack="True" OnSelectedIndexChanged="ddlSettingType_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top" style="width: 30%"></td>
                            <td valign="top" align="left" class="tddata" style="width: 70%">
                                <asp:RequiredFieldValidator ID="RFVUSERTYPE" runat="server" ControlToValidate="ddlSettingType"
                                    InitialValue="--Select--" ErrorMessage="Select Type" ForeColor="Red" ValidationGroup="save" Font-Bold="True" Font-Size="X-Small" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top" style="width: 30%">Call Status</td>
                            <td valign="top" align="left" class="tddata" style="width: 70%">
                                <asp:DropDownList ID="ddlStatusOfCall" runat="server" Width="300px" AutoPostBack="True" OnSelectedIndexChanged="ddlStatusOfCall_SelectedIndexChanged" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top" style="width: 30%">&nbsp;</td>
                            <td valign="top" align="left" class="tddata" style="width: 70%">
                                <asp:RequiredFieldValidator ID="RFVUSERTYPE0" runat="server" ControlToValidate="ddlStatusOfCall"
                                    InitialValue="--Select--" ErrorMessage="Select Status" ForeColor="Red" ValidationGroup="save" Font-Bold="True" Font-Size="X-Small" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        
                        <tr>
                            <td align="left" class="tdlabel" valign="top" style="width: 30%">Email Subject</td>
                            <td valign="top" align="left" class="tddata" style="width: 70%">
                                <asp:TextBox ID="txtEmailSubject" runat="server" Width="99%" MaxLength="200" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top" style="width: 30%">&nbsp;</td>
                            <td valign="top" align="left" class="tddata" style="width: 70%">
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtEmailSubject"
                                    ErrorMessage="Please enter email subject" ForeColor="Red" ValidationGroup="save" Font-Bold="True" Font-Size="X-Small" Display="Dynamic"></asp:RequiredFieldValidator>
                                
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top" style="width: 30%">Email Body</td>
                            <td valign="top" align="left" class="tddata" style="width: 70%">
                                <asp:TextBox ID="txtEmailBody" runat="server" Width="500" Height="500px"></asp:TextBox>
                                <asp:HtmlEditorExtender ID="HtmlEditorExtender1" runat="server" TargetControlID="txtEmailBody">
                                </asp:HtmlEditorExtender>
                                <asp:ScriptManager ID="ScriptManager1" runat="server">
                                </asp:ScriptManager>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top" style="width: 30%">&nbsp;</td>
                            <td valign="top" align="left" class="tddata" style="width: 70%">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtEmailBody" ErrorMessage="Please enter email body" ForeColor="Red" ValidationGroup="save" Font-Bold="True" Font-Size="X-Small" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top" style="width: 30%">SMS Body</td>
                            <td valign="top" align="left" class="tddata" style="width: 70%">
                                <asp:TextBox ID="txtSMSBody" runat="server" Width="99%" MaxLength="140" TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top" style="width: 30%">&nbsp;</td>
                            <td valign="top" align="left" class="tddata" style="width: 70%">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtSMSBody" ErrorMessage="Please enter sms body" ForeColor="Red" ValidationGroup="save" Font-Bold="True" Font-Size="X-Small" Display="Dynamic"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">&nbsp;
                            </td>
                            <td align="left" class="tddata">
                                <asp:Button ID="btnSubmit" Text="Update" TabIndex="103" runat="server" ValidationGroup="save"
                                    OnClick="btnSubmit_Click" />
                                &nbsp;<asp:Button ID="btnReset" Text="Reset" TabIndex="104" runat="server"
                                    OnClick="btnReset_Click" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
            <td align="left" valign="top" width="25%"></td>
        </tr>
    </table>
</asp:Content>
