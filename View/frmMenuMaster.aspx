﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="frmMenuMaster.aspx.cs" Inherits="CRM.View.frmMenuMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="Heading" align="center" style="background-color: #000000; color: #FFFFFF">
                <asp:Label ID="lblPageHeader" runat="server" Text="Menu Master" CssClass="bold"></asp:Label>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
        <tr>
            <td align="left" valign="top" width="15%">
            </td>
            <td valign="top" width="70%">
                <fieldset class="fieldset">
                    <legend class="Legendheading" align="left">Menu Master</legend>
                    <table cellpadding="0" cellspacing="3px" width="100%" style="margin: 0px; padding: 0px;">
                        <tr>
                            <td colspan="2" align="left">
                                <asp:Label ID="lblMsg" runat="server" CssClass="failureNotification"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top">
                                <asp:Label ID="lblParentMenu" runat="server" Text="Parent Menu"></asp:Label>&nbsp;
                            </td>
                            <td valign="top" align="left" class="tddata">
                                <asp:DropDownList ID="ddlParentMenu" runat="server" TabIndex="101" Width="155px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top">
                                <asp:Label ID="lblMenuTitle" runat="server" Text="Menu Title"></asp:Label>&nbsp;&nbsp;
                            </td>
                            <td valign="top" align="left" class="tddata">
                                <asp:TextBox ID="txtMenuTitle" runat="server" TabIndex="102" Width="150px" 
                                    Style="text-transform: none" MaxLength="50"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                                    ID="rfvMenuTitle" ErrorMessage="*" runat="server" ForeColor="Red" ControlToValidate="txtMenuTitle"
                                    ValidationGroup="sub"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top">
                                Description
                            </td>
                            <td valign="top" align="left" class="tddata">
                                <asp:TextBox ID="txtDescription" runat="server" TabIndex="103" Width="150px" 
                                    Style="text-transform: none" MaxLength="50"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top">
                                <asp:Label ID="lblMenuUrl" runat="server" Text="Url"></asp:Label>
                            </td>
                            <td valign="top" align="left" class="tddata">
                                <asp:TextBox ID="txtMenuUrl" runat="server" TabIndex="104" Width="150px" 
                                    Style="text-transform: none" MaxLength="100"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                &nbsp;
                            </td>
                            <td align="left">
                                <asp:Button ID="btnSubmit" Text="Submit" TabIndex="105" runat="server" ValidationGroup="sub"
                                    OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnReset" Text="Reset" TabIndex="106" runat="server" OnClick="btnReset_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left">
                                <asp:HiddenField ID="hdnId" runat="server" Value="-1" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
            <td align="left" valign="top" width="15%">
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" width="15%">
            </td>
            <td valign="top" width="70%">
                <table border="0" width="100%">
                    <tr>
                        <td align="center">
                            <asp:GridView ID="gvMenu" TabIndex="106" PageSize="50" Width="100%" runat="server"
                                AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="MenuId" OnPageIndexChanging="gvMenu_PageIndexChanging"
                                OnRowDataBound="gvMenu_RowDataBound" OnRowDeleting="gvMenu_RowDeleting" OnRowEditing="gvMenu_RowEditing"
                                OnRowUpdating="gvMenu_RowUpdating" EmptyDataText="No Record Found" CellPadding="4"
                                ForeColor="#333333" GridLines="None" CssClass="tabStyle">
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr.">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Menu Title">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblMenuTitle" runat="server" Text='<%# Bind("MenuTitle") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Parent Menu">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblParentMenu" runat="server" Text='<%# Bind("ParentName") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Menu Url">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblMenuUrl" runat="server" Text='<%# Bind("MenuUrl") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Id">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblMenuId" runat="server" Text='<%# Bind("MenuId") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnEdit" CommandName="Edit" runat="server" ImageUrl="~/Images/edit.png"
                                                OnClientClick="return confirm('Are you sure! You want to Edit Seleted Row?');" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnStatus" CommandName="Update" runat="server" ImageUrl="~/Images/active.gif"
                                                OnClientClick="return confirm('Are you sure! You want to change status?');" />
                                            <asp:Label ID="gvlblIsActive" runat="server" Text='<%# Bind("IsActive") %>' Visible="False" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete" Visible="false">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnDelete" CommandName="Delete" runat="server" ImageUrl="~/Images/delete.gif"
                                                OnClientClick="return confirm('Are you sure! You want to Delete Seleted Row?');" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle BackColor="#2461BF" />
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="Black" HorizontalAlign="Center" />
                                <RowStyle HorizontalAlign="Center" VerticalAlign="Top" BackColor="#EFF3FB" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
            <td align="left" valign="top" width="15%">
            </td>
        </tr>
    </table>
</asp:Content>
