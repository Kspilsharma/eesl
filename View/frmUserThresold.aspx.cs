﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRM.Models;
using System.Reflection;
using System.Data;
using System.Configuration;

namespace CRM.View
{
    public partial class frmUserThresold : UIBase
    {
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SetUI();
            }
        }

        private void SetUI()
        {
            FillUserddl();
            ddlUserQA.SelectedIndex = 0;
            txtQAThresold.Text = "";
            txtQA2Thresold.Text = "";
            FillGrid();
        }

        protected void FillUserddl()
        {
            try
            {
                UserMaster tUserMaster = new UserMaster();
                DataSet ds = new DataSet();
                ddlUserQA.Items.Clear();
                if (rdolstRole.SelectedIndex == 0)
                {
                    lblQAThresold.Text = "Threshold For QA-1";
                    QA2.Visible = false;
                    tUserMaster.RoleName = "OPERATOR";
                }
                else if (rdolstRole.SelectedIndex == 1)
                {
                    QA2.Visible = true;
                    lblQAThresold.Text = "Threshold For QA-2 (Verified Records in QA-1)";
                    lblQA2Thresold.Text = "Threshold For QA-2 (Not Verified Records)";
                    tUserMaster.RoleName = "QA";
                }
                else
                {
                    tUserMaster.RoleName = "";
                }

                ds = tUserMaster.Select();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlUserQA.DataValueField = "UserId";
                    ddlUserQA.DataTextField = "UserName";
                    ddlUserQA.DataSource = ds;
                    ddlUserQA.DataBind();
                    ddlUserQA.Items.Insert(0, "Please-Select");
                }
                else
                {
                    ddlUserQA.Items.Insert(0, "Please-Select");
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void rdolstRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillUserddl();
            FillGrid();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                UserThresold tUserThresold = new UserThresold();

                if (rdolstRole.SelectedIndex == 0)
                {
                    tUserThresold.QA2Thresold = 0;
                }
                else
                {
                    if (txtQA2Thresold.Text.Trim() == string.Empty)
                    {
                        lblMsg.Text = "Please Enter Threshold For QA-2 (Not Verified Records)";
                        return;
                    }
                    else
                    {
                        tUserThresold.QA2Thresold = Convert.ToInt32(txtQA2Thresold.Text.Trim());
                    }
                }

                tUserThresold.RoleId = Convert.ToInt32(rdolstRole.SelectedItem.Value);
                tUserThresold.UserId = Convert.ToInt32(ddlUserQA.SelectedItem.Value.Trim());
                tUserThresold.QAThresold = Convert.ToInt32(txtQAThresold.Text.Trim());
                tUserThresold.LoginId = UIManager.CurrentUserSession().UserId;

                string tErrorMsg = string.Empty;
                if (tUserThresold.Insert(out tErrorMsg))
                { SetUI(); }

                lblMsg.Text = tErrorMsg;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            SetUI();
        }

        protected void FillGrid()
        {
            try
            {
                UserThresold tUserThresold = new UserThresold();
                DataSet ds = new DataSet();
                tUserThresold.RoleId = Convert.ToInt32(rdolstRole.SelectedValue.Trim());
                ds = tUserThresold.Select();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvUserThresold.DataSource = ds.Tables[0];
                    gvUserThresold.DataBind();
                    gvUserThresold.Visible = true;
                    if (rdolstRole.SelectedIndex == 1)
                    {
                        gvUserThresold.HeaderRow.Cells[4].Visible = true;
                        gvUserThresold.HeaderRow.Cells[3].Text = "Verified Records Thresold";
                        gvUserThresold.HeaderRow.Cells[4].Text = "Not Verified Records Thresold";
                    }
                    else if (rdolstRole.SelectedIndex == 0)
                    {
                        gvUserThresold.HeaderRow.Cells[3].Text = "Threshold For QA-1";
                        gvUserThresold.HeaderRow.Cells[4].Visible = false;
                    }

                }
                else
                {
                    gvUserThresold.Visible = false;
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void gvUserThresold_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                UserThresold tUserThresold = new UserThresold();
                DataSet ds = new DataSet();
                Label lblstatus = ((Label)gvUserThresold.Rows[e.NewEditIndex].FindControl("gvlblIsActive"));
                string strIsActive = lblstatus.Text;
                hdnId.Value = (gvUserThresold.DataKeys[e.NewEditIndex].Value.ToString());

                tUserThresold.IsActive = Convert.ToBoolean(strIsActive);
                tUserThresold.ID = Convert.ToInt32(hdnId.Value);
                ds = tUserThresold.Edit();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    txtQAThresold.Text = ds.Tables[0].Rows[0]["QAThresold"].ToString();
                    txtQA2Thresold.Text = ds.Tables[0].Rows[0]["QA2Thresold"].ToString();
                    rdolstRole.SelectedIndex = -1;
                    rdolstRole.Items.FindByValue(ds.Tables[0].Rows[0]["RoleId"].ToString()).Selected = true;
                    FillUserddl();
                    ddlUserQA.SelectedIndex = -1;
                    ddlUserQA.Items.FindByValue(ds.Tables[0].Rows[0]["UserId"].ToString()).Selected = true;
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void gvUserThresold_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

            UserThresold tUserThresold = new UserThresold();
            int Id = Convert.ToInt32(gvUserThresold.DataKeys[e.RowIndex].Value.ToString());
            Label lblstatus = ((Label)gvUserThresold.Rows[e.RowIndex].FindControl("gvlblIsActive"));
            string strIsActive = lblstatus.Text;
            try
            {
                tUserThresold.IsActive = Convert.ToBoolean(strIsActive);
                tUserThresold.ID = Id;
                string tErrorMsg = string.Empty;
                if (tUserThresold.Delete(out tErrorMsg))
                {
                    SetUI();
                }

                lblMsg.Text = tErrorMsg;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void gvUserThresold_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvUserThresold.PageIndex = e.NewPageIndex;
            FillGrid();
        }

        protected void gvUserThresold_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string STRIsActive = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "IsActive"));

                    ImageButton imgbtnstatus = (ImageButton)e.Row.FindControl("imgbtnStatus");
                    if (STRIsActive == "False")
                    {
                        imgbtnstatus.ImageUrl = "~/Images/block.Jpg";
                        imgbtnstatus.ToolTip = "Click to Active";
                    }
                    else
                    {
                        imgbtnstatus.ImageUrl = "~/Images/active.gif";
                        imgbtnstatus.ToolTip = "Click to Block";
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void gvUserThresold_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                UserThresold tUserThresold = new UserThresold();
                tUserThresold.ID = Convert.ToInt32(gvUserThresold.DataKeys[e.RowIndex].Value.ToString());
                Label lblstatus = ((Label)gvUserThresold.Rows[e.RowIndex].FindControl("gvlblIsActive"));
                string strIsActive = lblstatus.Text;
                if (strIsActive == "False")
                {
                    tUserThresold.IsActive = true;
                }
                else
                {
                    tUserThresold.IsActive = false;
                }
                string tErrorMsg = string.Empty;
                if (tUserThresold.StatusUpdate(out tErrorMsg))
                {
                    FillGrid();
                }

            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

    }
}