﻿using CRM.Models;
using System;
using System.Configuration;
using System.Data;
using System.Reflection;
using System.Web;
using System.Web.UI.WebControls;

namespace EESL.View
{
    public partial class frmSMSEmailTemplate : UIBase
    {
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!IsPostBack)
            {
                SetUI();
            }
        }

        protected void SetUI()
        {
            SQLDBHelper.PopulateDropDownList(ddlSettingType, "SettingTypeMaster", "Name", "Id", false, "");
            SQLDBHelper.PopulateDropDownList(ddlStatusOfCall, "StatusOfCall", "StatusOfCall", "StatusOfCallId", false, "");
            txtEmailSubject.Text = "";
            txtEmailBody.Text = "";
            txtSMSBody.Text = "";
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                SMSEmailTemplates tSMSEmailTemplates = new SMSEmailTemplates();
                tSMSEmailTemplates.SettingTypeId = Convert.ToInt32(ddlSettingType.SelectedItem.Value);
                tSMSEmailTemplates.StatusOfCallId = Convert.ToInt32(ddlStatusOfCall.SelectedItem.Value);
                tSMSEmailTemplates.SMSTemplateBody = txtSMSBody.Text.Trim();
                tSMSEmailTemplates.EmailTemplateSubject = txtEmailSubject.Text.Trim();
                tSMSEmailTemplates.EmailTemplateBody = txtEmailBody.Text.Trim();
                string tErrorMsg = string.Empty;
                bool tResult = false;
                tResult = tSMSEmailTemplates.UpdateSMSEmailTemplate(out tErrorMsg);
                if (tResult)
                {
                    ddlStatusOfCall.SelectedIndex = 0;
                    txtEmailSubject.Text = "";
                    txtEmailBody.Text = "";
                    txtSMSBody.Text = "";
                    HttpRuntime.Cache.Remove("SMSEmailTemplates");
                }
                lblMsg.Text = tErrorMsg;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            Response.Redirect(Request.RawUrl);
        }

        protected void ddlStatusOfCall_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSettingType.SelectedIndex == 0)
            {
                lblMsg.Text = "Please select Setting Type";
            }
            else if (ddlStatusOfCall.SelectedIndex == 0)
            {
                lblMsg.Text = "Please select Call Status";
            }
            else
            {
                SetExistingSavedTemplates();
            }
        }

        protected void ddlSettingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSettingType.SelectedIndex > 0 && ddlStatusOfCall.SelectedIndex > 0)
            {
                SetExistingSavedTemplates();
            }
        }

        private void SetExistingSavedTemplates()
        {
            txtEmailSubject.Text = "";
            txtEmailBody.Text = "";
            txtSMSBody.Text = "";

            try
            {
                SMSEmailTemplates tSMSEmailTemplates = new SMSEmailTemplates();
                tSMSEmailTemplates.SettingTypeId = Convert.ToInt32(ddlSettingType.SelectedItem.Value);
                tSMSEmailTemplates.StatusOfCallId = Convert.ToInt32(ddlStatusOfCall.SelectedItem.Value);
                DataSet ds = tSMSEmailTemplates.Select();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    txtEmailSubject.Text = Convert.ToString(ds.Tables[0].Rows[0]["EmailTemplateSubject"]);
                    txtEmailBody.Text = Convert.ToString(ds.Tables[0].Rows[0]["EmailTemplateBody"]);
                    txtSMSBody.Text = Convert.ToString(ds.Tables[0].Rows[0]["SMSTemplateBody"]);
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }
    }
}