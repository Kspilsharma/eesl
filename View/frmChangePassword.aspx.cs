﻿using CRM.Models;
using System;
using System.Configuration;
using System.Reflection;

namespace CRM.View
{
    public partial class frmChangePassword : UIBase
    {
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtOldPassword.Focus();
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                UserMaster tUserMaster = new UserMaster();
                tUserMaster.UserId = UIManager.CurrentUserSession().UserId;
                tUserMaster.Password = txtOldPassword.Text.Replace("'", "''");
                tUserMaster.NewPassword = txtNewPassword.Text.Replace("'", "''");
                int tResult = tUserMaster.ChangeUserPassword();
                if (tResult == 1)
                {
                    lblMessage.Text = "Password Change Successfully";
                }
                else
                {
                    lblMessage.Text = "Entered Old Password Not Matched";
                }

                btnOk.OnClientClick = null;
                modelSubmit.Show();
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            ResetUI();
        }

        /// <summary>
        /// Reset UI Controls
        /// </summary>
        private void ResetUI()
        {
            txtOldPassword.Text = "";
            txtNewPassword.Text = "";
            txtConfirmPassword.Text = "";
        }
    }
}