﻿using CRM.Models;
using System;
using System.Configuration;
using System.Data;
using System.Reflection;
using System.Web.UI.WebControls;

namespace CRM.View
{
    public partial class frmUserProfile : UIBase
    {
        private DataTable DT_Main = new DataTable();
        private DataSet dtcommon = new DataSet();
        private DataTable DT_Sub = new DataTable();

        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            tvUserProfile.Attributes.Add("onclick", "OnCheckBoxCheckChanged(event)");
            lblMsg.Text = "";
            if (!IsPostBack)
            {
                SetUI();
            }
        }

        protected void SetUI()
        {
            FillRoleddl();
            GetMenuHierarchy();
            ddlRole.Focus();
        }

        protected void FillRoleddl()
        {
            try
            {
                RoleMaster tRoleMaster = new RoleMaster();
                DataSet ds = new DataSet();
                ddlRole.Items.Clear();
                ds = tRoleMaster.Select();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlRole.DataValueField = "RoleId";
                    ddlRole.DataTextField = "RoleName";
                    ddlRole.DataSource = ds;
                    ddlRole.DataBind();
                }

                ddlRole.Items.Insert(0, "Please-Select");
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        private void GetMenuHierarchy()
        {
            tvUserProfile.Visible = true;
            TreeNode ParentItem = new TreeNode();
            TreeNode ChildItem = new TreeNode();
            TreeNode BaseItem = new TreeNode();
            DataRow[] Drsub = null;

            UserProfile tUserProfile = new UserProfile();
            int i = 0;
            int j = 0;
            try
            {
                dtcommon = tUserProfile.HierarchySelect();

                DT_Sub = dtcommon.Tables[0];
                DT_Main = dtcommon.Tables[1];
                tvUserProfile.Nodes.Clear();
                if (DT_Main.Rows.Count > 0)
                {
                    for (j = 0; j <= DT_Main.Rows.Count - 1; j++)
                    {
                        ParentItem = new TreeNode();
                        ParentItem.Text = DT_Main.Rows[j][2].ToString();
                        //ParentItem.NavigateUrl = DT_Sub.Rows[0][1].ToString();
                        ParentItem.Value = DT_Main.Rows[j][0].ToString();
                        if (ParentItem.Value != "83")
                        {
                            tvUserProfile.Nodes.Add(ParentItem);
                            if (DT_Sub.Rows.Count > 0)
                            {
                                Drsub = DT_Sub.Select("ParentId='" + DT_Main.Rows[j][0].ToString() + "'");

                                if (Drsub.Length > 0)
                                {
                                    for (i = 0; i <= Drsub.GetUpperBound(0); i++)
                                    {
                                        ChildItem = new TreeNode();
                                        ChildItem.Text = Drsub[i].ItemArray[2].ToString();
                                        //ChildItem.NavigateUrl = Drsub[i].ItemArray[3].ToString();
                                        ChildItem.Value = Drsub[i].ItemArray[0].ToString();

                                        ParentItem.ChildNodes.Add(ChildItem);
                                        GetChildNode(ChildItem, Drsub[i]);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        private TreeNode GetChildNode(TreeNode pItem, DataRow pDR)
        {
            int i = 0;
            DataRow[] tDrSub = null;
            TreeNode tItem = new TreeNode();
            try
            {
                tDrSub = DT_Sub.Select("ParentId='" + pDR[0] + "'");
                for (i = 0; i <= tDrSub.GetUpperBound(0); i++)
                {
                    tItem = new TreeNode();
                    tItem.Text = tDrSub[i].ItemArray[2].ToString();
                    //tItem.NavigateUrl = tDrSub[i].ItemArray[3].ToString();
                    tItem.Value = tDrSub[i].ItemArray[0].ToString();

                    pItem.ChildNodes.Add(tItem);
                    tItem = GetChildNode(tItem, tDrSub[i]);
                }
                return null;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return null;
            }
        }

        public void callCheckbocEdit(int editcat)
        {
            foreach (TreeNode node in tvUserProfile.Nodes)
            {
                if (node.Value == Convert.ToString(editcat))
                {
                    node.Checked = true;
                }
                else
                {
                    if (node.ChildNodes.Count > 0)
                    {
                        foreach (TreeNode cTn in node.ChildNodes)
                        {
                            if (cTn.Value == Convert.ToString(editcat))
                            {
                                cTn.Checked = true;
                            }
                            else
                            {
                                if (cTn.ChildNodes.Count > 0)
                                {
                                    foreach (TreeNode cTnn in cTn.ChildNodes)
                                    {
                                        if (cTnn.Value == Convert.ToString(editcat))
                                        {
                                            cTnn.Checked = true;
                                        }
                                        else
                                        {
                                            if (cTnn.ChildNodes.Count > 0)
                                            {
                                                foreach (TreeNode cTnnn in cTnn.ChildNodes)
                                                {
                                                    if (cTnnn.Value == Convert.ToString(editcat))
                                                    { cTnnn.Checked = true; }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlRole.SelectedIndex > 0)
            {
                UserProfile tUserProfile = new UserProfile();
                tUserProfile.RoleId = Convert.ToInt64(ddlRole.SelectedItem.Value);
                GetMenuHierarchy();
                tvUserProfile.ExpandAll();

                try
                {
                    tUserProfile.RoleId = Convert.ToInt64(ddlRole.SelectedItem.Value);
                    dtcommon = tUserProfile.Edit();

                    if (dtcommon.Tables[0].Rows.Count > 0)
                    {
                        string pagelist = dtcommon.Tables[0].Rows[0][0].ToString();
                        if (pagelist != "")
                        {
                            string[] arr = pagelist.Split(',');
                            for (int k = 0; k < arr.Length; k++)
                            {
                                if (arr[k].ToString() == "")
                                {
                                }
                                else
                                {
                                    int getparentedit = Convert.ToInt32(arr[k].ToString());
                                    callCheckbocEdit(getparentedit);
                                }
                            }
                        }
                    }

                    //else
                    //{
                    //    lblMsg.Text = "Still Profile Is Not Set By Administrator";
                    //}
                }
                catch (Exception Ex)
                {
                    ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                    Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
                }
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                UserProfile tUserProfile = new UserProfile();
                tUserProfile.RoleId = Convert.ToInt64(ddlRole.SelectedItem.Value);
                tUserProfile.LoginId = UIManager.CurrentUserSession().UserId;

                string permit = "";
                int count = tvUserProfile.CheckedNodes.Count;
                if (count > 0)
                {
                    for (int i = 0; i < count; i++)
                    {
                        permit = permit + tvUserProfile.CheckedNodes[i].Value.ToString() + ',';

                        if (permit == "")
                        {
                            tUserProfile.UserProfiles = "";
                        }
                        else
                        {
                            tUserProfile.UserProfiles = permit.Substring(0, (permit.Length) - 1);
                        }
                    }

                    string tErrorMsg = string.Empty;
                    if (tUserProfile.InsertUpdate(out tErrorMsg))
                    {
                        ddlRole.SelectedIndex = 0;
                        lblMsg.Text = tErrorMsg;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            SetUI();
        }
    }
}