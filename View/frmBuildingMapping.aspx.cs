﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CRM.Models;
using System.Reflection;
using System.Configuration;
using System.Collections;
using System.Data.SqlClient;
using System.IO;
using System.Web.Services;

namespace EESL.View
{
    public partial class frmBuildingMapping : System.Web.UI.Page
    {
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!IsPostBack)
            { SetUI(); }
        }

        protected void SetUI()
        {
            hdnId.Value = "-1";
            ddlState.Focus();
            txtBuilding.Text = "";
            txtAddress.Text = string.Empty;
            txtLandmark.Text = string.Empty;
            SQLDBHelper.PopulateDropDownList(ddlState, "State", "State", "StateId", false, "");
            FillGrid();
            FillGridReturnDataTable();
        }

        protected void FillGrid()
        {
            try
            {
                Mapping tMapping = new Mapping();
                DataSet ds = new DataSet();
                ds = tMapping.BuildingMappingSelect();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvBuildingMapping.DataSource = ds.Tables[0];
                    gvBuildingMapping.DataBind();
                    gvBuildingMapping.Visible = true;
                }
                else
                {
                    gvBuildingMapping.Visible = false;
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void ExportDashboardDetail()
        {

            string sFileName = "BuildingList-" + System.DateTime.Now.Date + ".xls";
            try
            {
                Mapping tMapping = new Mapping();
                DataSet ds = new DataSet();
                ds = tMapping.BuildingMappingSelect();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ExportDatatableToExcel(ds.Tables[0], sFileName);
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        public void ExportDatatableToExcel(DataTable table, string sFileName)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + sFileName);

            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR><BR><BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> <TR>");
            //am getting my grid's column headers
            int columnscount = table.Columns.Count;

            for (int j = 0; j < columnscount; j++)
            {      //write in new column
                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(table.Columns[j].ColumnName.ToString());
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (DataRow row in table.Rows)
            {//write in new row
                HttpContext.Current.Response.Write("<TR>");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    HttpContext.Current.Response.Write("<Td>");
                    HttpContext.Current.Response.Write(row[i].ToString());
                    HttpContext.Current.Response.Write("</Td>");
                }

                HttpContext.Current.Response.Write("</TR>");
            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }

        protected void imgbtnExportExcel_Click(object sender, ImageClickEventArgs e)
        {
            ExportDashboardDetail();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Mapping tMapping = new Mapping();
                DataSet ds = new DataSet();

                tMapping.StateId = Convert.ToInt32(ddlState.SelectedItem.Value);
                tMapping.DistrictId = Convert.ToInt32(hdnDistricId.Value);
                tMapping.Building = Convert.ToString(txtBuilding.Text.Trim());
                tMapping.LoginId = UIManager.CurrentUserSession().UserId;
                tMapping.Address = Convert.ToString(txtAddress.Text.Trim());
                tMapping.Landmark = Convert.ToString(txtLandmark.Text.Trim());

                string tErrorMsg = string.Empty;
                if (hdnId.Value == "-1")
                {
                    if (tMapping.BuildingMappingInsert(out tErrorMsg))
                    { SetUI(); }
                }
                else
                {
                    tMapping.BuildingMappingId = Convert.ToInt32(hdnId.Value);
                    if (tMapping.BuildingMappingUpdate(out tErrorMsg))
                    { SetUI(); }
                    hdnId.Value = "-1";
                }

                lblMsg.Text = tErrorMsg;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            SetUI();
        }

        protected void gvBuildingMapping_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {

                Mapping tMapping = new Mapping();
                DataSet ds = new DataSet();
                Label lblstatus = ((Label)gvBuildingMapping.Rows[e.NewEditIndex].FindControl("gvlblIsActive"));
                string strIsActive = lblstatus.Text;
                hdnId.Value = (gvBuildingMapping.DataKeys[e.NewEditIndex].Value.ToString());

                tMapping.IsActive = Convert.ToBoolean(strIsActive);
                tMapping.BuildingMappingId = Convert.ToInt32(hdnId.Value);
                ds = tMapping.BuildingMappingEdit();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    txtBuilding.Text = ds.Tables[0].Rows[0]["Building"].ToString();
                    txtAddress.Text = ds.Tables[0].Rows[0]["Address"].ToString();
                    txtLandmark.Text = ds.Tables[0].Rows[0]["Landmark"].ToString();

                    int stateId = Convert.ToInt32(ds.Tables[0].Rows[0]["StateId"]);
                    int districtId = Convert.ToInt32(ds.Tables[0].Rows[0]["DistrictId"]);
                    hdnDistricId.Value = Convert.ToString(districtId);
                    if (stateId > 0)
                    {
                        ddlState.SelectedIndex = -1;
                        ddlState.Items.FindByValue(Convert.ToString(stateId)).Selected = true;
                    }
                    else
                        ddlState.SelectedIndex = 0;
                    if (districtId > 0)
                    {
                        ddlDistrict.SelectedIndex = -1;
                        SQLDBHelper.PopulateDropDownList(ddlDistrict, "District", "District", "DistrictId", true, "StateId=" + stateId);
                        ddlDistrict.Items.FindByValue(Convert.ToString(districtId)).Selected = true;
                    }
                    else
                    {
                        ddlDistrict.Items.Insert(0, "--Select--");
                        ddlDistrict.SelectedIndex = 0;
                    }

                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void gvBuildingMapping_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Mapping tMapping = new Mapping();
                tMapping.BuildingMappingId = Convert.ToInt32(gvBuildingMapping.DataKeys[e.RowIndex].Value.ToString());
                Label lblstatus = ((Label)gvBuildingMapping.Rows[e.RowIndex].FindControl("gvlblIsActive"));
                string strIsActive = lblstatus.Text;
                if (strIsActive == "False")
                {
                    tMapping.IsActive = true;
                }
                else
                {
                    tMapping.IsActive = false;
                }
                string tErrorMsg = string.Empty;
                if (tMapping.BuildingMappingStatusUpdate(out tErrorMsg))
                {
                    FillGrid();
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void gvBuildingMapping_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            Mapping tMapping = new Mapping();
            Int32 BuildingMappingId = Convert.ToInt32(gvBuildingMapping.DataKeys[e.RowIndex].Value.ToString());
            Label lblstatus = ((Label)gvBuildingMapping.Rows[e.RowIndex].FindControl("gvlblIsActive"));
            string strIsActive = lblstatus.Text;
            try
            {
                tMapping.IsActive = Convert.ToBoolean(strIsActive);
                tMapping.BuildingMappingId = BuildingMappingId;
                string tErrorMsg = string.Empty;
                if (tMapping.BuildingMappingDelete(out tErrorMsg))
                {
                    SetUI();
                }

                lblMsg.Text = tErrorMsg;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void gvBuildingMapping_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvBuildingMapping.PageIndex = e.NewPageIndex;
            FillGrid();
        }
        protected void gvBuildingMapping_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string STRIsActive = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "IsActive"));

                    ImageButton imgbtnstatus = (ImageButton)e.Row.FindControl("imgbtnStatus");
                    if (STRIsActive == "False")
                    {
                        imgbtnstatus.ImageUrl = "~/Images/block.Jpg";
                        imgbtnstatus.ToolTip = "Click to Active";
                    }
                    else
                    {
                        imgbtnstatus.ImageUrl = "~/Images/active.gif";
                        imgbtnstatus.ToolTip = "Click to Block";
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        [System.Web.Services.WebMethod]
        public static ArrayList PopulateCities(int stateId)
        {
            ArrayList list = new ArrayList();
            String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            String strQuery = "select DistrictID, District from [District] where StateId = @StateId";
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@StateId", stateId);
                    cmd.CommandText = strQuery;
                    cmd.Connection = con;
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        list.Add(new ListItem(
                       sdr["District"].ToString(),
                       sdr["DistrictID"].ToString()
                        ));
                    }
                    con.Close();
                    return list;
                }
            }
        }
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            DataTable dt = FillGridReturnDataTable();
            DataView dv = new DataView(dt);
            string SearchExpression = null;
            if (!String.IsNullOrEmpty(txtSearch.Text))
            {
                SearchExpression = string.Format("{0} '%{1}%'",
                gvBuildingMapping.SortExpression, txtSearch.Text);

            }
            if (!String.IsNullOrEmpty(txtSearch.Text))
            {
                dv.RowFilter = "State like" + SearchExpression + "or District like" + SearchExpression + "or Building like" + SearchExpression + "or Address like" + SearchExpression + "or Landmark like" + SearchExpression;
                gvBuildingMapping.DataSource = dv;
                gvBuildingMapping.DataBind();
            }
            else
            {
                gvBuildingMapping.DataSource = dt;
                gvBuildingMapping.DataBind();
            }



        }
        protected DataTable FillGridReturnDataTable()
        {
            DataTable dt = new DataTable();
            try
            {
                Mapping tMapping = new Mapping();

                DataSet ds = new DataSet();

                ds = tMapping.BuildingMappingSelect();
                if (ds.Tables[0].Rows.Count > 0)
                {

                    dt = ds.Tables[0];
                }

            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
            return dt;
        }
    }
}