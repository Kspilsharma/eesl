﻿using CRM.Models;
using System;
using System.Configuration;
using System.Data;

namespace CRM.View
{
    public partial class frmNavigate : System.Web.UI.Page
    {
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            string strUserName = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(Session["hUserName"].ToString()))
                {
                    strUserName = Session["hUserName"].ToString();
                    UserMaster tUserMaster = new UserMaster();
                    tUserMaster.UserName = CommonUtils.DecryptString(strUserName);
                    DataSet tDataSet = tUserMaster.Select();
                    SessionUserContext tSessionUserContext = new SessionUserContext();
                    tSessionUserContext.RoleId = Convert.ToInt32(tDataSet.Tables[0].Rows[0]["RoleId"]);
                    tSessionUserContext.RoleName = Convert.ToString(tDataSet.Tables[0].Rows[0]["RoleName"]);
                    tSessionUserContext.ContactNo = Convert.ToString(tDataSet.Tables[0].Rows[0]["ContactNo"]);
                    tSessionUserContext.UserId = Convert.ToInt32(tDataSet.Tables[0].Rows[0]["UserId"]);
                    tSessionUserContext.UserName = Convert.ToString(tDataSet.Tables[0].Rows[0]["UserName"]);
                    tSessionUserContext.UserProfile = Convert.ToString(tDataSet.Tables[0].Rows[0]["UserProfile"]);
                    tSessionUserContext.LastLogin = Convert.ToDateTime(tDataSet.Tables[0].Rows[0]["LastLoginDate"]);
                    tSessionUserContext.VendorSchemeName = Convert.ToString(tDataSet.Tables[0].Rows[0]["VendorSchemeName"]);
                    
                    tSessionUserContext.StartSession(); // Start Current Session
                    strUserName = string.Empty;
                    Response.Redirect("~/Default.aspx", false);
                }
                else
                { Response.Redirect(AppBaseURL + "/frmSessionExpired.aspx", false); }
            }
            catch (Exception)
            { Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false); }
        }
    }
}