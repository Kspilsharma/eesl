﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CRM.View
{
    public partial class frmCallEnded : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                string UID = Convert.ToString(Request.QueryString["UID"]);
                string callType = Convert.ToString(Request.QueryString["CT"]);
                if (!string.IsNullOrEmpty(UID))
                {
                    if (callType.ToUpper() == "InBound".ToUpper())
                    {
                        lblCallEnded.Text = "INBOUND CALL UPDATED WITH UNIQ ID " + UID;
                    }
                    else if (callType.ToUpper() == "OutBound".ToUpper())
                    {
                        lblCallEnded.Text = "OUTBOUND CALL UPDATED WITH UNIQ ID " + UID;
                    }
                }
            }
        }
    }
}