﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="frmFAQMaster.aspx.cs" Inherits="CRM.View.frmFAQMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="Heading" align="center" style="color: #101010">
                <asp:Label ID="lblPageHeader" runat="server" Text="FAQ Master" CssClass="bold"></asp:Label>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
        <tr>
            <td align="left" valign="top" width="15%"></td>
            <td valign="top" width="70%">
                <fieldset class="fieldset">
                    <legend class="Legendheading" align="left">FAQ Master</legend>
                    <table cellpadding="0" cellspacing="3px" width="100%" style="margin: 0px; padding: 0px;">
                        <tr>
                            <td colspan="2" align="left">
                                <asp:Label ID="lblMsg" runat="server" CssClass="failureNotification"></asp:Label>
                            </td>
                        </tr>

                        <tr>
                            <td align="left" width="100" class="tdlabel" valign="top">Scheme<span style="color: red">&nbsp;*</span> &nbsp;:</td>
                            <td valign="top" align="left" class="tddata">
                                <asp:DropDownList ID="ddlScheme" runat="server" />
                                <asp:RequiredFieldValidator ID="rfvScheme" ForeColor="Maroon" ControlToValidate="ddlScheme" InitialValue="--Select--" runat="server" ErrorMessage="Select Scheme of Call." Display="Dynamic" ValidationGroup="sub" Font-Bold="True" Font-Size="X-Small"></asp:RequiredFieldValidator>
                            </td>
                        </tr>

                        <tr>
                            <td align="left" class="tdlabel" valign="top">
                                <asp:Label ID="lblQuestion" runat="server" Text="Question"></asp:Label>&nbsp;<span style="color: red">*</span>&nbsp;:
                            </td>

                            <td valign="top" align="left" class="tddata">
                                <asp:TextBox ID="txtQuestion" runat="server" TabIndex="102" Width="80.5%"
                                    Style="text-transform: none" MaxLength="500"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator
                                        ID="rfvQuestion" ErrorMessage="*" runat="server" ForeColor="Red" ControlToValidate="txtQuestion"
                                        ValidationGroup="sub"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top">Answer
                            &nbsp;:</td>
                            <td valign="top" align="left" class="tddata">
                                <asp:TextBox ID="txtAnswer" runat="server" TextMode="MultiLine" TabIndex="103" Width="80%" Style="text-transform: none"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top">
                                <asp:Label ID="lblIsActive" runat="server" Text="Is Active &nbsp;:"></asp:Label>
                            </td>
                            <td valign="top" align="left" class="tddata">
                                <asp:CheckBox ID="chkIsActive" Text="Is Active" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">&nbsp;
                            </td>
                            <td align="left">
                                <asp:Button ID="btnSubmit" Text="Submit" TabIndex="105" runat="server" ValidationGroup="sub"
                                    OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnReset" Text="Reset" TabIndex="106" runat="server" OnClick="btnReset_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left">
                                <asp:HiddenField ID="hdnId" runat="server" Value="-1" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
            <td align="left" valign="top" width="15%"></td>
        </tr>
<tr><td>&nbsp;</td></tr>
        <tr>
            <td align="left" valign="top" width="15%"></td>
            <td valign="top" width="70%">
                <table border="0" width="100%">
                    <tr>
                        <td align="center">
                            <asp:GridView ID="gvFAQ" TabIndex="106" PageSize="50" Width="100%" runat="server"
                                AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="QuestionId" OnPageIndexChanging="gvFAQ_PageIndexChanging"
                                OnRowDataBound="gvFAQ_RowDataBound" OnRowDeleting="gvFAQ_RowDeleting" OnRowEditing="gvFAQ_RowEditing"
                                OnRowUpdating="gvFAQ_RowUpdating" EmptyDataText="No Record Found" CellPadding="4"
                                ForeColor="#333333" class="tabStyle" GridLines="None">
                                <AlternatingRowStyle BackColor="" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr.">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Scheme">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblScheme" runat="server" Text='<%# Bind("Scheme") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Question">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblQuestion" runat="server" Text='<%# Bind("Question") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Answer">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblAnswer" runat="server" Text='<%# Bind("Answer") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnEdit" CommandName="Edit" runat="server" ImageUrl="~/Images/edit.png"
                                                OnClientClick="return confirm('Are you sure! You want to Edit Seleted Row?');" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnStatus" CommandName="Update" runat="server" ImageUrl="~/Images/active.gif"
                                                OnClientClick="return confirm('Are you sure! You want to change status?');" />
                                            <asp:Label ID="gvlblIsActive" runat="server" Text='<%# Bind("IsActive") %>' Visible="False" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete" Visible="false">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnDelete" CommandName="Delete" runat="server" ImageUrl="~/Images/delete.gif"
                                                OnClientClick="return confirm('Are you sure! You want to Delete Seleted Row?');" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle BackColor="#2461BF" />
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="Black" HorizontalAlign="Center" />
                                <RowStyle HorizontalAlign="Center" VerticalAlign="Top" BackColor="" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
            <td align="left" valign="top" width="15%"></td>
        </tr>
    </table>
</asp:Content>
