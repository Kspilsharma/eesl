﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CRM.Models;
using System.Reflection;
using System.Configuration;
using System.Collections;
using System.Data.SqlClient;
using System.IO;
using System.Web.Services;

namespace EESL.View
{
    public partial class frmVendorMapping : System.Web.UI.Page
    {
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!IsPostBack)
            { SetUI(); }
        }

        protected void SetUI()
        {
            hdnId.Value = "-1";
            hdnVendorType.Value = "0";

            //ddlVendorType.Focus();
            ddlVendorType.Items.Clear();
            ddlVendorType.Items.Insert(0,new ListItem { Value = "0", Text = "--Select--" });
            ddlVendorType.SelectedIndex = 0;
            
            txtVendorName.Text = "";
            txtVendorEmail.Text = "";
            txtVendorMobileNo.Text = "";
            ddlState.Items.Clear();

            ddlDistrict.Items.Clear();
            ddlDistrict.Items.Insert(0, new ListItem { Value = "0", Text = "--Select--" });

            ddlULB.Items.Clear();
            ddlULB.Items.Insert(0, new ListItem { Value = "0", Text = "--Select--" });

            ddlZone.Items.Clear();
            ddlZone.Items.Insert(0, new ListItem { Value = "0", Text = "--Select--" });

            SQLDBHelper.PopulateDropDownList(ddlState, "State", "State", "StateId", false, "");
            SQLDBHelper.PopulateDropDownList(ddlScheme, "Scheme", "Scheme", "SchemeId", false, "");
            ddlScheme.Items.FindByValue("2").Enabled = false;
            //ddlScheme.Items.FindByValue("3").Enabled = false;
            ddlScheme.Items.FindByValue("5").Enabled = false;

            ddlScheme.Enabled = true;
            ddlVendorType.Enabled = true;
            ddlState.Enabled = true;
            ddlDistrict.Enabled = true;
            ddlULB.Enabled = true;
            ddlZone.Enabled = true;

            FillGrid();
            FillGridReturnDataTable();
        }

        protected void FillGrid()
        {
            try
            {
                Mapping tMapping = new Mapping();
                DataSet ds = new DataSet();
                ds = tMapping.VendorMappingSelect();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvVendorMapping.DataSource = ds.Tables[0];
                    gvVendorMapping.DataBind();
                    gvVendorMapping.Visible = true;
                }
                else
                {
                    gvVendorMapping.Visible = false;
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void ExportDashboardDetail()
        {

            string sFileName = "VendorList-" + System.DateTime.Now.Date + ".xls";
            try
            {
                Mapping tMapping = new Mapping();
                DataSet ds = new DataSet();
                ds = tMapping.VendorMappingSelect();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ExportDatatableToExcel(ds.Tables[0], sFileName);
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        public void ExportDatatableToExcel(DataTable table, string sFileName)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + sFileName);

            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR><BR><BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> <TR>");
            //am getting my grid's column headers
            int columnscount = table.Columns.Count;

            for (int j = 0; j < columnscount; j++)
            {      //write in new column
                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(table.Columns[j].ColumnName.ToString());
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (DataRow row in table.Rows)
            {//write in new row
                HttpContext.Current.Response.Write("<TR>");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    HttpContext.Current.Response.Write("<Td>");
                    HttpContext.Current.Response.Write(row[i].ToString());
                    HttpContext.Current.Response.Write("</Td>");
                }

                HttpContext.Current.Response.Write("</TR>");
            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }

        protected void imgbtnExportExcel_Click(object sender, ImageClickEventArgs e)
        {
            ExportDashboardDetail();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Mapping tMapping = new Mapping();
                DataSet ds = new DataSet();

                tMapping.StateId = Convert.ToInt32(ddlState.SelectedItem.Value);
                tMapping.DistrictId = Convert.ToInt32(hdnDistricId.Value);
                if (ddlScheme.SelectedItem.Value == "1")
                {
                    tMapping.ULBMappingId = Convert.ToInt32(hdnULBMappingId.Value);
                    tMapping.ZoneMappingId = Convert.ToInt32(hdnZoneMappingId.Value);
                }
                tMapping.VendorName = Convert.ToString(txtVendorName.Text.Trim());
                tMapping.VendorEmail = Convert.ToString(txtVendorEmail.Text.Trim());
                tMapping.VendorMobileNo = Convert.ToString(txtVendorMobileNo.Text.Trim());
                tMapping.VendorType = Convert.ToString(hdnVendorType.Value);
                tMapping.SchemeId = Convert.ToInt32(ddlScheme.SelectedItem.Value);
                tMapping.LoginId = UIManager.CurrentUserSession().UserId;
                string tErrorMsg = string.Empty;
                if (hdnId.Value == "-1")
                {
                    if (tMapping.VendorMappingInsert(out tErrorMsg))
                    {
                        HttpRuntime.Cache.Remove("VendorEmailMobileNo");
                        SetUI();
                    }
                }
                else
                {
                    tMapping.VendorMappingId = Convert.ToInt32(hdnId.Value);
                    if (tMapping.VendorMappingUpdate(out tErrorMsg))
                    {
                        HttpRuntime.Cache.Remove("VendorEmailMobileNo");
                        SetUI();
                    }
                    hdnId.Value = "-1";
                }

                lblMsg.Text = tErrorMsg;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            SetUI();
        }

        protected void gvVendorMapping_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                Mapping tMapping = new Mapping();
                DataSet ds = new DataSet();
                Label lblstatus = ((Label)gvVendorMapping.Rows[e.NewEditIndex].FindControl("gvlblIsActive"));
                string strIsActive = lblstatus.Text;
                hdnId.Value = (gvVendorMapping.DataKeys[e.NewEditIndex].Value.ToString());
                int ulbMappingId=0, zoneMappingId=0;
                tMapping.IsActive = Convert.ToBoolean(strIsActive);
                tMapping.VendorMappingId = Convert.ToInt32(hdnId.Value);
                ds = tMapping.VendorMappingEdit();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    txtVendorName.Text = ds.Tables[0].Rows[0]["VendorName"].ToString();
                    txtVendorEmail.Text = ds.Tables[0].Rows[0]["VendorEmail"].ToString();
                    txtVendorMobileNo.Text = ds.Tables[0].Rows[0]["VendorMobileNo"].ToString();
                    int stateId = Convert.ToInt32(ds.Tables[0].Rows[0]["StateId"]);
                    int districtId = Convert.ToInt32(ds.Tables[0].Rows[0]["DistrictId"]);
                    if (ds.Tables[0].Rows[0]["SchemeId"].ToString() == "1")
                    {
                        ulbMappingId = Convert.ToInt32(ds.Tables[0].Rows[0]["ULBMappingId"]);
                        zoneMappingId = Convert.ToInt32(ds.Tables[0].Rows[0]["ZoneMappingId"]);
                    }
                    
                    string vendorType = Convert.ToString(ds.Tables[0].Rows[0]["VendorType"]);
                    int schemeId = Convert.ToInt32(ds.Tables[0].Rows[0]["SchemeId"]);

                    hdnDistricId.Value = Convert.ToString(districtId);
                    hdnULBMappingId.Value = Convert.ToString(ulbMappingId);
                    hdnZoneMappingId.Value = Convert.ToString(zoneMappingId);
                    if (stateId > 0)
                    {
                        ddlState.SelectedIndex = -1;
                        ddlState.Items.FindByValue(Convert.ToString(stateId)).Selected = true;
                    }
                    else
                        ddlState.SelectedIndex = 0;
                    if (districtId > 0)
                    {
                        ddlDistrict.SelectedIndex = -1;
                        SQLDBHelper.PopulateDropDownList(ddlDistrict, "District", "District", "DistrictId", true, "StateId=" + stateId);
                        ddlDistrict.Items.FindByValue(Convert.ToString(districtId)).Selected = true;
                    }
                    else
                    {
                        ddlDistrict.Items.Insert(0, "--Select--");
                        ddlDistrict.SelectedIndex = 0;
                    }
                    if (ulbMappingId > 0)
                    {
                        ddlULB.SelectedIndex = -1;
                        SQLDBHelper.PopulateDropDownList(ddlULB, "ULBMapping", "ULB", "ULBMappingId", true, "StateId=" + stateId + " AND DistrictId=" + districtId);
                        ddlULB.Items.FindByValue(Convert.ToString(ulbMappingId)).Selected = true;
                    }
                    else
                    {
                        ddlULB.Items.Insert(0, "--Select--");
                        ddlULB.SelectedIndex = 0;
                    }
                    if (zoneMappingId > 0)
                    {
                        ddlZone.SelectedIndex = -1;
                        SQLDBHelper.PopulateDropDownList(ddlZone, "ZoneMapping", "Zone", "ZoneMappingId", true, "StateId=" + stateId + " AND DistrictId=" + districtId + " AND ULBMappingId=" + ulbMappingId);
                        ddlZone.Items.FindByValue(Convert.ToString(zoneMappingId)).Selected = true;
                    }
                    else
                    {
                        ddlZone.Items.Insert(0, "--Select--");
                        ddlZone.SelectedIndex = 0;
                    }

                    if(schemeId>0)
                    {
                        ddlScheme.SelectedIndex = -1;
                        ddlScheme.Items.FindByValue(Convert.ToString(schemeId)).Selected = true;

                        ddlVendorType.SelectedIndex = -1;
                        DataSet dsVendorType = GetVendorType(schemeId);
                        ddlVendorType.DataSource = dsVendorType;                        
                        ddlVendorType.DataTextField = "VendorType";
                        ddlVendorType.DataValueField = "VendorTypeValue";
                        ddlVendorType.DataBind();
                        ddlVendorType.Items.FindByValue(Convert.ToString(vendorType)).Selected = true;
                        hdnVendorType.Value = Convert.ToString(vendorType);
                    }
                    else
                    {
                        ddlVendorType.Items.Insert(0, "--Select--");
                        ddlVendorType.SelectedIndex = 0;
                    }

                    ddlScheme.Enabled = false;
                    ddlVendorType.Enabled = false;
                    ddlState.Enabled = false;
                    ddlDistrict.Enabled = false;
                    ddlULB.Enabled = false;
                    ddlZone.Enabled = false;
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void gvVendorMapping_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Mapping tMapping = new Mapping();
                tMapping.VendorMappingId = Convert.ToInt32(gvVendorMapping.DataKeys[e.RowIndex].Value.ToString());
                Label lblstatus = ((Label)gvVendorMapping.Rows[e.RowIndex].FindControl("gvlblIsActive"));
                string strIsActive = lblstatus.Text;
                if (strIsActive == "False")
                {
                    tMapping.IsActive = true;
                }
                else
                {
                    tMapping.IsActive = false;
                }
                string tErrorMsg = string.Empty;
                if (tMapping.VendorMappingStatusUpdate(out tErrorMsg))
                {
                    FillGrid();
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void gvVendorMapping_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            Mapping tMapping = new Mapping();
            Int32 VendorMappingId = Convert.ToInt32(gvVendorMapping.DataKeys[e.RowIndex].Value.ToString());
            Label lblstatus = ((Label)gvVendorMapping.Rows[e.RowIndex].FindControl("gvlblIsActive"));
            string strIsActive = lblstatus.Text;
            try
            {
                tMapping.IsActive = Convert.ToBoolean(strIsActive);
                tMapping.VendorMappingId = VendorMappingId;
                string tErrorMsg = string.Empty;
                if (tMapping.VendorMappingDelete(out tErrorMsg))
                {
                    SetUI();
                }

                lblMsg.Text = tErrorMsg;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void gvVendorMapping_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvVendorMapping.PageIndex = e.NewPageIndex;
            FillGrid();
        }

        protected void gvVendorMapping_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string STRIsActive = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "IsActive"));

                    ImageButton imgbtnstatus = (ImageButton)e.Row.FindControl("imgbtnStatus");
                    if (STRIsActive == "False")
                    {
                        imgbtnstatus.ImageUrl = "~/Images/block.Jpg";
                        imgbtnstatus.ToolTip = "Click to Active";
                    }
                    else
                    {
                        imgbtnstatus.ImageUrl = "~/Images/active.gif";
                        imgbtnstatus.ToolTip = "Click to Block";
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        [System.Web.Services.WebMethod]
        public static ArrayList PopulateCities(int stateId)
        {
            ArrayList list = new ArrayList();
            String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            String strQuery = "select DistrictID, District from [District] where StateId = @StateId";
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@StateId", stateId);
                    cmd.CommandText = strQuery;
                    cmd.Connection = con;
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        list.Add(new ListItem(
                       sdr["District"].ToString(),
                       sdr["DistrictID"].ToString()
                        ));
                    }
                    con.Close();
                    return list;
                }
            }
        }

        [System.Web.Services.WebMethod]
        public static ArrayList PopulateULB(int stateId, int districtId)
        {
            ArrayList list = new ArrayList();
            String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            String strQuery = "select ULBMappingId, ULB from [ULBMapping] where StateId = @stateId AND DistrictId = @DistrictId";
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@StateId", stateId);
                    cmd.Parameters.AddWithValue("@DistrictId", districtId);
                    cmd.CommandText = strQuery;
                    cmd.Connection = con;
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        list.Add(new ListItem(
                       sdr["ULB"].ToString(),
                       sdr["ULBMappingId"].ToString()
                        ));
                    }
                    con.Close();
                    return list;
                }
            }
        }

        [System.Web.Services.WebMethod]
        public static ArrayList PopulateZone(int stateId, int districtId, int ulbMappingId)
        {
            ArrayList list = new ArrayList();
            String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            String strQuery = "select ZoneMappingId, Zone from [ZoneMapping] where StateId = @stateId AND DistrictId = @DistrictId AND ULBMappingId = @ULBMappingId";
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@StateId", stateId);
                    cmd.Parameters.AddWithValue("@DistrictId", districtId);
                    cmd.Parameters.AddWithValue("@ULBMappingId", ulbMappingId);
                    cmd.CommandText = strQuery;
                    cmd.Connection = con;
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        list.Add(new ListItem(
                       sdr["Zone"].ToString(),
                       sdr["ZoneMappingId"].ToString()
                        ));
                    }
                    con.Close();
                    return list;
                }
            }
        }

        [System.Web.Services.WebMethod]
        public static ArrayList PopulateVendorType(int SchemeId)
        {
            ArrayList list = new ArrayList();
            String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            String strQuery = "SELECT VendorTypeId,VendorType,VendorTypeValue,SchemeId From VendorType where SchemeId = @SchemeId";
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@SchemeId", SchemeId);
                    cmd.CommandText = strQuery;
                    cmd.Connection = con;
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        list.Add(new ListItem(
                       sdr["VendorType"].ToString(),
                       sdr["VendorTypeValue"].ToString()
                        ));
                    }
                    con.Close();
                    return list;
                }
            }
        }
        
        public DataSet GetVendorType(int SchemeId)
        {
            String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            String strQuery = "SELECT VendorTypeId,VendorType,VendorTypeValue,SchemeId From VendorType where SchemeId = @SchemeId";
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@SchemeId", SchemeId);
                    cmd.CommandText = strQuery;
                    cmd.Connection = con;
                    con.Open();
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
        }
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            DataTable dt = FillGridReturnDataTable();
            DataView dv = new DataView(dt);
            string SearchExpression = null;
            if (!String.IsNullOrEmpty(txtSearch.Text))
            {
                SearchExpression = string.Format("{0} '%{1}%'",
                gvVendorMapping.SortExpression, txtSearch.Text);

            }
            if (!String.IsNullOrEmpty(txtSearch.Text))
            {
                dv.RowFilter = "State like" + SearchExpression + "or District like" + SearchExpression + "or ULB like" + SearchExpression + 
                    "or Zone like" + SearchExpression + "or VendorType like" + SearchExpression + "or VendorName like" + SearchExpression
                    + "or VendorEmail like" + SearchExpression + "or VendorMobileNo like" + SearchExpression;
                gvVendorMapping.DataSource = dv;
                gvVendorMapping.DataBind();
            }
            else
            {
                gvVendorMapping.DataSource = dt;
                gvVendorMapping.DataBind();
            }



        }
        protected DataTable FillGridReturnDataTable()
        {
            DataTable dt = new DataTable();
            try
            {
                Mapping tMapping = new Mapping();

                DataSet ds = new DataSet();

                ds = tMapping.VendorMappingSelect();
                if (ds.Tables[0].Rows.Count > 0)
                {

                    dt = ds.Tables[0];
                }

            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
            return dt;
        }
    }
}
