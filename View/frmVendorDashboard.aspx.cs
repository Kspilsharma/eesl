﻿using CRM.Models;
using EESL.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Web.Services;
using System.Web.UI.WebControls;

namespace EESL.View
{
    public partial class frmVendorDashboard : System.Web.UI.Page
    {
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (UIManager.CurrentUserSession().VendorSchemeName == "Building")
            {
                gvInBoundCallScheme.Columns[3].Visible = false;
                gvInBoundCallScheme.Columns[5].Visible = false;
                gvInBoundCallScheme.Columns[6].Visible = true;
                Session["SchName"] = "Building";
            }
            else if (UIManager.CurrentUserSession().VendorSchemeName.ToUpper() == "AGDSM")
            {
                gvInBoundCallScheme.Columns[5].Visible = true;
                gvInBoundCallScheme.Columns[3].Visible = false;
                gvInBoundCallScheme.Columns[6].Visible = false;
                Session["SchName"] = "AGDSM";
            }
            else
            {
                gvInBoundCallScheme.Columns[3].Visible = true;
                gvInBoundCallScheme.Columns[5].Visible = false;
                gvInBoundCallScheme.Columns[6].Visible = false;
                Session["SchName"] = "Street Light";
            }
           
            if (!IsPostBack)
            {
                PopulateUI();
                if ((UIManager.CurrentUserSession().RoleName.ToUpper() == "VENDOR"))
                {
                    Dashboard();
                }
            }
        }

        private void PopulateUI()
        {
            txtFrom.Text = "";
            txtTo.Text = "";
        }

        protected void Dashboard()
        {
            try
            {
                string SchName = Session["SchName"].ToString();
                Session["vendor_dashboard"] = null;
                Dashboard tDashboard = new Dashboard();
                if (!String.IsNullOrEmpty(txtFrom.Text))
                {
                    tDashboard.DateFrom = Convert.ToDateTime(txtFrom.Text.Trim());
                }
                if (!String.IsNullOrEmpty(txtTo.Text))
                {
                    tDashboard.DateTo = Convert.ToDateTime(txtTo.Text.Trim());
                }
                tDashboard.VendorId = Convert.ToInt32(SQLDBHelper.getFieldValueByCondition("UserMaster", "VendorId", "UserId", Convert.ToString(UIManager.CurrentUserSession().UserId)));
                DataSet ds = tDashboard.DashboardVendor_Select();
                Session["vendor_dashboard"] = ds;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvInBoundCallStatus.DataSource = ds.Tables[0];
                    gvInBoundCallStatus.DataBind();
                    gvInBoundCallStatus.Visible = true;
                    int totalcalls = ds.Tables[0].AsEnumerable().Sum(row => row.Field<int>("TotalCalls"));
                    int Open = ds.Tables[0].AsEnumerable().Sum(row => row.Field<int>("Open"));
                    int Escalated = ds.Tables[0].AsEnumerable().Sum(row => row.Field<int>("Escalated"));
                    int Verified = ds.Tables[0].AsEnumerable().Sum(row => row.Field<int>("Verified"));
                    int NonVerified = ds.Tables[0].AsEnumerable().Sum(row => row.Field<int>("Non Verified"));
                    int Closed = ds.Tables[0].AsEnumerable().Sum(row => row.Field<int>("Closed"));
                    int Rectified = ds.Tables[0].AsEnumerable().Sum(row => row.Field<int>("Rectified"));

                    gvInBoundCallStatus.FooterRow.Cells[1].Text = "Total";
                    gvInBoundCallStatus.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallStatus.FooterRow.Cells[2].Text = totalcalls.ToString();
                    gvInBoundCallStatus.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallStatus.FooterRow.Cells[3].Text = Open.ToString();
                    gvInBoundCallStatus.FooterRow.Cells[3].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallStatus.FooterRow.Cells[4].Text = Escalated.ToString();
                    gvInBoundCallStatus.FooterRow.Cells[4].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallStatus.FooterRow.Cells[5].Text = Verified.ToString();
                    gvInBoundCallStatus.FooterRow.Cells[5].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallStatus.FooterRow.Cells[6].Text = NonVerified.ToString();
                    gvInBoundCallStatus.FooterRow.Cells[6].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallStatus.FooterRow.Cells[7].Text = Closed.ToString();
                    gvInBoundCallStatus.FooterRow.Cells[7].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallStatus.FooterRow.Cells[8].Text = Rectified.ToString();
                    gvInBoundCallStatus.FooterRow.Cells[8].HorizontalAlign = HorizontalAlign.Center;

                }
                else
                {
                    gvInBoundCallStatus.Visible = false;
                }

                if (ds.Tables[1].Rows.Count > 0)
                {
                    gvInBoundCallType.DataSource = ds.Tables[1];
                    gvInBoundCallType.DataBind();
                    gvInBoundCallType.Visible = true;
                    int totalcalls = ds.Tables[1].AsEnumerable().Sum(row => row.Field<int>("TotalCalls"));
                    int Query = ds.Tables[1].AsEnumerable().Sum(row => row.Field<int>("Query"));
                    int Registration = ds.Tables[1].AsEnumerable().Sum(row => row.Field<int>("Registration"));
                    int Complaint = ds.Tables[1].AsEnumerable().Sum(row => row.Field<int>("Complaint"));
                    int Other = ds.Tables[1].AsEnumerable().Sum(row => row.Field<int>("Other"));

                    gvInBoundCallType.FooterRow.Cells[1].Text = "Total";
                    gvInBoundCallType.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallType.FooterRow.Cells[2].Text = totalcalls.ToString();
                    gvInBoundCallType.FooterRow.Cells[2].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallType.FooterRow.Cells[3].Text = Query.ToString();
                    gvInBoundCallType.FooterRow.Cells[3].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallType.FooterRow.Cells[4].Text = Registration.ToString();
                    gvInBoundCallType.FooterRow.Cells[4].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallType.FooterRow.Cells[5].Text = Complaint.ToString();
                    gvInBoundCallType.FooterRow.Cells[5].HorizontalAlign = HorizontalAlign.Center;

                    gvInBoundCallType.FooterRow.Cells[6].Text = Other.ToString();
                    gvInBoundCallType.FooterRow.Cells[6].HorizontalAlign = HorizontalAlign.Center;


                }
                else
                {
                    gvInBoundCallType.Visible = false;
                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    gvInBoundCallScheme.DataSource = ds.Tables[2];
                    gvInBoundCallScheme.DataBind();
                    gvInBoundCallScheme.Visible = true;
                    int totalcalls = ds.Tables[2].AsEnumerable().Sum(row => row.Field<int>("TotalCalls"));
                    int totals = ds.Tables[2].AsEnumerable().Sum(row => row.Field<int>(SchName));
                    gvInBoundCallScheme.FooterRow.Cells[1].Text = "Total";
                    gvInBoundCallScheme.FooterRow.Cells[1].HorizontalAlign = HorizontalAlign.Center;
                    gvInBoundCallScheme.FooterRow.Cells[2].Text = totalcalls.ToString();

                    if (SchName == "AGDSM")
                    {
                        gvInBoundCallScheme.FooterRow.Cells[5].Text = totals.ToString();
                        gvInBoundCallStatus.FooterRow.Cells[5].HorizontalAlign = HorizontalAlign.Center;
                    }
                    else if (SchName == "Building")
                    {
                        gvInBoundCallScheme.FooterRow.Cells[6].Text = totals.ToString();
                        gvInBoundCallStatus.FooterRow.Cells[6].HorizontalAlign = HorizontalAlign.Center;
                    }
                    else if (SchName == "Street Light")
                    {
                        gvInBoundCallScheme.FooterRow.Cells[3].Text = totals.ToString();
                        gvInBoundCallStatus.FooterRow.Cells[3].HorizontalAlign = HorizontalAlign.Center;
                    }
                }
                else
                {
                    gvInBoundCallScheme.Visible = false;
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {

                if ((UIManager.CurrentUserSession().RoleName.ToUpper() == "VENDOR"))
                {
                    Dashboard();
                }

            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                PopulateUI();
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        [WebMethod(EnableSession = true)]
        public static GraphData GetChartData()
        {
            List<SchemeWiseGraphData> schemeWiseDataList = new List<SchemeWiseGraphData>();
            List<CallStatusWiseGraphData> callStatusWiseDataList = new List<CallStatusWiseGraphData>();
            List<CallCategoryWiseGraphData> callCategoryWiseDataList = new List<CallCategoryWiseGraphData>();
            DataSet dashboardData = (DataSet)System.Web.HttpContext.Current.Session["vendor_dashboard"];

            if (dashboardData.Tables[2].Rows.Count > 0)
            {
                schemeWiseDataList = (from b in dashboardData.Tables[2].AsEnumerable()
                                      select new
                                      {
                                          CallDate = b.Field<DateTime>("CallDate"),
                                          TotalCalls = b.Field<int?>("TotalCalls"),
                                          StreetLight = b.Field<int?>("Street Light"),
                                          Ujala = b.Field<int?>("Ujala"),
                                          AGDSM = b.Field<int?>("AGDSM"),
                                          Building = b.Field<int?>("Building"),
                                          Other = b.Field<int?>("Other")
                                      }).ToList()
                            .Select(r => new SchemeWiseGraphData
                            {
                                Y = r.CallDate.ToString("yyyy-MM-dd"),
                                TOTALCALLS = r.TotalCalls.HasValue ? r.TotalCalls.Value : 0,
                                SL = r.StreetLight.HasValue ? r.StreetLight.Value : 0,
                                UJALA = r.Ujala.HasValue ? r.Ujala.Value : 0,
                                AGDSM = r.AGDSM.HasValue ? r.AGDSM.Value : 0,
                                BUILDING = r.Building.HasValue ? r.Building.Value : 0,
                                OTHERS = r.Other.HasValue ? r.Other.Value : 0
                            }).ToList();
            }

            if (dashboardData.Tables[1].Rows.Count > 0)
            {
                callCategoryWiseDataList = (from b in dashboardData.Tables[1].AsEnumerable()
                                            select new
                                            {
                                                CallDate = b.Field<DateTime>("CallDate"),
                                                TotalCalls = b.Field<int?>("TotalCalls"),
                                                Query = b.Field<int?>("Query"),
                                                Registration = b.Field<int?>("Registration"),
                                                Complaint = b.Field<int?>("Complaint"),
                                                Other = b.Field<int?>("Other")
                                            }).ToList()
                            .Select(r => new CallCategoryWiseGraphData
                            {
                                Y = r.CallDate.ToString("yyyy-MM-dd"),
                                TOTALCALLS = r.TotalCalls.HasValue ? r.TotalCalls.Value : 0,
                                QUERY = r.Query.HasValue ? r.Query.Value : 0,
                                REGISTRATION = r.Registration.HasValue ? r.Registration.Value : 0,
                                COMPLAINT = r.Complaint.HasValue ? r.Complaint.Value : 0,
                                OTHERS = r.Other.HasValue ? r.Other.Value : 0
                            }).ToList();
            }

            if (dashboardData.Tables[0].Rows.Count > 0)
            {
                callStatusWiseDataList = (from b in dashboardData.Tables[0].AsEnumerable()
                                          select new
                                          {
                                              CallDate = b.Field<DateTime>("CallDate"),
                                              TotalCalls = b.Field<int?>("TotalCalls"),
                                              Open = b.Field<int?>("Open"),
                                              Escalated = b.Field<int?>("Escalated"),
                                              Verified = b.Field<int?>("Verified"),
                                              NonVerified = b.Field<int?>("Non Verified"),
                                              Closed = b.Field<int?>("Closed"),
                                              Rectified = b.Field<int?>("Rectified")
                                          }).ToList()
                            .Select(r => new CallStatusWiseGraphData
                            {
                                Y = r.CallDate.ToString("yyyy-MM-dd"),
                                TOTALCALLS = r.TotalCalls.HasValue ? r.TotalCalls.Value : 0,
                                OPEN = r.Open.HasValue ? r.Open.Value : 0,
                                ESCALATED = r.Escalated.HasValue ? r.Escalated.Value : 0,
                                VERIFIED = r.Verified.HasValue ? r.Verified.Value : 0,
                                NOTVERIFIED = r.NonVerified.HasValue ? r.NonVerified.Value : 0,
                                CLOSED = r.Closed.HasValue ? r.Closed.Value : 0,
                                RECTIFIED = r.Rectified.HasValue ? r.Rectified.Value : 0
                            }).ToList();
            }

            return new GraphData { SchemeWiseData = schemeWiseDataList, CallCategoryWiseData = callCategoryWiseDataList, CallStatusWiseData = callStatusWiseDataList };
        }
    }
}