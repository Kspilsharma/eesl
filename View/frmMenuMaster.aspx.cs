﻿using CRM.Models;
using System;
using System.Configuration;
using System.Data;
using System.Reflection;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CRM.View
{
    public partial class frmMenuMaster : UIBase
    {
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!IsPostBack)
            { SetUI(); }
        }

        protected void SetUI()
        {
            hdnId.Value = "-1";
            ddlParentMenu.Focus();
            txtMenuTitle.Text = "";
            txtDescription.Text = string.Empty;
            txtMenuUrl.Text = "";
            FillMenuddl();
            FillGrid();
            ddlParentMenu.Focus();
        }

        protected void FillMenuddl()
        {
            try
            {
                MenuMaster tMenuMaster = new MenuMaster();
                DataSet ds = new DataSet();
                ddlParentMenu.Items.Clear();
                ds = tMenuMaster.Select();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlParentMenu.DataValueField = "MenuId";
                    ddlParentMenu.DataTextField = "MenuTitle";
                    ddlParentMenu.DataSource = ds;
                    ddlParentMenu.DataBind();
                }
                ddlParentMenu.Items.Insert(0, "Please-Select");
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void FillGrid()
        {
            try
            {
                MenuMaster tMenuMaster = new MenuMaster();
                DataSet ds = new DataSet();
                ds = tMenuMaster.Select();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvMenu.DataSource = ds.Tables[0];
                    gvMenu.DataBind();
                    gvMenu.Visible = true;
                }
                else
                {
                    gvMenu.Visible = false;
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                MenuMaster tMenuMaster = new MenuMaster();
                DataSet ds = new DataSet();

                if (ddlParentMenu.SelectedIndex == 0)
                {
                    tMenuMaster.ParentId = 0;
                }
                else
                {
                    tMenuMaster.ParentId = Convert.ToInt64(ddlParentMenu.SelectedItem.Value);
                }

                tMenuMaster.MenuTitle = Convert.ToString(txtMenuTitle.Text.Trim());

                if (txtDescription.Text != "")
                {
                    tMenuMaster.Description = Convert.ToString(txtDescription.Text.Trim());
                }
                else
                {
                    tMenuMaster.Description = "";
                }

                if (txtMenuUrl.Text != "")
                {
                    tMenuMaster.MenuUrl = Convert.ToString(txtMenuUrl.Text.Trim());
                }
                else
                {
                    tMenuMaster.MenuUrl = "";
                }
                tMenuMaster.LoginId = UIManager.CurrentUserSession().UserId;
                string tErrorMsg = string.Empty;
                if (hdnId.Value == "-1")
                {
                    if (tMenuMaster.Insert(out tErrorMsg))
                    { SetUI(); }
                }
                else
                {
                    tMenuMaster.MenuId = Convert.ToInt64(hdnId.Value);
                    if (tMenuMaster.Update(out tErrorMsg))
                    { SetUI(); }
                    hdnId.Value = "-1";
                }

                lblMsg.Text = tErrorMsg;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            SetUI();
        }

        protected void gvMenu_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                MenuMaster tMenuMaster = new MenuMaster();
                DataSet ds = new DataSet();
                Label lblstatus = ((Label)gvMenu.Rows[e.NewEditIndex].FindControl("gvlblIsActive"));
                string strIsActive = lblstatus.Text;
                hdnId.Value = (gvMenu.DataKeys[e.NewEditIndex].Value.ToString());

                tMenuMaster.IsActive = Convert.ToBoolean(strIsActive);
                tMenuMaster.MenuId = Convert.ToInt64(hdnId.Value);
                ds = tMenuMaster.Select();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    txtMenuTitle.Text = ds.Tables[0].Rows[0]["MenuTitle"].ToString();
                    txtDescription.Text = ds.Tables[0].Rows[0]["Description"].ToString();

                    txtMenuUrl.Text = ds.Tables[0].Rows[0]["MenuUrl"].ToString();
                    if (ds.Tables[0].Rows[0]["ParentId"].ToString() != "0")
                    {
                        if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["ParentId"].ToString()))
                        {
                            ddlParentMenu.SelectedIndex = -1;
                            ddlParentMenu.Items.FindByValue(ds.Tables[0].Rows[0]["ParentId"].ToString()).Selected = true;
                        }
                        else
                        {
                            ddlParentMenu.SelectedIndex = 0;
                        }
                    }
                    else
                    {
                        ddlParentMenu.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void gvMenu_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                MenuMaster tMenuMaster = new MenuMaster();
                tMenuMaster.MenuId = Convert.ToInt64(gvMenu.DataKeys[e.RowIndex].Value.ToString());
                Label lblstatus = ((Label)gvMenu.Rows[e.RowIndex].FindControl("gvlblIsActive"));
                string strIsActive = lblstatus.Text;
                if (strIsActive == "False")
                {
                    tMenuMaster.IsActive = true;
                }
                else
                {
                    tMenuMaster.IsActive = false;
                }
                string tErrorMsg = string.Empty;
                if (tMenuMaster.StatusUpdate(out tErrorMsg))
                {
                    FillGrid();
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void gvMenu_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            MenuMaster tMenuMaster = new MenuMaster();
            Int64 MenuId = Convert.ToInt64(gvMenu.DataKeys[e.RowIndex].Value.ToString());
            Label lblstatus = ((Label)gvMenu.Rows[e.RowIndex].FindControl("gvlblIsActive"));
            string strIsActive = lblstatus.Text;
            try
            {
                tMenuMaster.IsActive = Convert.ToBoolean(strIsActive);
                tMenuMaster.MenuId = MenuId;
                string tErrorMsg = string.Empty;
                if (tMenuMaster.Delete(out tErrorMsg))
                {
                    SetUI();
                }

                lblMsg.Text = tErrorMsg;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void gvMenu_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvMenu.PageIndex = e.NewPageIndex;
            FillGrid();
        }

        protected void gvMenu_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string STRIsActive = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "IsActive"));

                    ImageButton imgbtnstatus = (ImageButton)e.Row.FindControl("imgbtnStatus");
                    if (STRIsActive == "False")
                    {
                        imgbtnstatus.ImageUrl = "~/Images/block.Jpg";
                        imgbtnstatus.ToolTip = "Click to Active";
                    }
                    else
                    {
                        imgbtnstatus.ImageUrl = "~/Images/active.gif";
                        imgbtnstatus.ToolTip = "Click to Block";
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }
    }
}