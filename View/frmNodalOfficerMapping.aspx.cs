﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CRM.Models;
using System.Reflection;
using System.Configuration;
using System.Collections;
using System.Data.SqlClient;
using System.IO;
using System.Web.Services;

namespace EESL.View
{
    public partial class frmNodalOfficerMapping : System.Web.UI.Page
    {
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!IsPostBack)
            { SetUI(); }
        }

        protected void SetUI()
        {
            hdnId.Value = "-1";
            ddlScheme.Focus();
            txtNOName.Text = "";
            txtNOEmail.Text = "";
            txtNOMobileNo.Text = "";
            SQLDBHelper.PopulateDropDownList(ddlScheme, "Scheme", "Scheme", "SchemeId", false, "");
            SQLDBHelper.PopulateDropDownList(ddlState, "State", "State", "StateId", false, "");
            FillGrid();
            FillGridReturnDataTable(); ;
        }

        protected void FillGrid()
        {
            try
            {
                Mapping tMapping = new Mapping();
                DataSet ds = new DataSet();
                ds = tMapping.NOMappingSelect();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvNOMapping.DataSource = ds.Tables[0];
                    gvNOMapping.DataBind();
                    gvNOMapping.Visible = true;
                }
                else
                {
                    gvNOMapping.Visible = false;
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void ExportDashboardDetail()
        {

            string sFileName = "NodalOfficersList-" + System.DateTime.Now.Date + ".xls";
            try
            {
                Mapping tMapping = new Mapping();
                DataSet ds = new DataSet();
                ds = tMapping.NOMappingSelect();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ExportDatatableToExcel(ds.Tables[0], sFileName);
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        public void ExportDatatableToExcel(DataTable table, string sFileName)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + sFileName);

            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR><BR><BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> <TR>");
            //am getting my grid's column headers
            int columnscount = table.Columns.Count;

            for (int j = 0; j < columnscount; j++)
            {      //write in new column
                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(table.Columns[j].ColumnName.ToString());
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (DataRow row in table.Rows)
            {//write in new row
                HttpContext.Current.Response.Write("<TR>");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    HttpContext.Current.Response.Write("<Td>");
                    HttpContext.Current.Response.Write(row[i].ToString());
                    HttpContext.Current.Response.Write("</Td>");
                }

                HttpContext.Current.Response.Write("</TR>");
            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }

        protected void imgbtnExportExcel_Click(object sender, ImageClickEventArgs e)
        {
            ExportDashboardDetail();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Mapping tMapping = new Mapping();
                DataSet ds = new DataSet();

                tMapping.SchemeId = Convert.ToInt32(ddlScheme.SelectedItem.Value);
                tMapping.StateId = Convert.ToInt32(ddlState.SelectedItem.Value);
                tMapping.DistrictId = Convert.ToInt32(hdnDistricId.Value);
                tMapping.NOName = Convert.ToString(txtNOName.Text.Trim());
                tMapping.NOEmail = Convert.ToString(txtNOEmail.Text.Trim());
                tMapping.NOMobileNo = Convert.ToString(txtNOMobileNo.Text.Trim());
                tMapping.LoginId = UIManager.CurrentUserSession().UserId;
                string tErrorMsg = string.Empty;
                if (hdnId.Value == "-1")
                {
                    if (tMapping.NOMappingInsert(out tErrorMsg))
                    {
                        HttpRuntime.Cache.Remove("NodalOfficerEmailMobileNo");
                        SetUI();
                    }
                }
                else
                {
                    tMapping.NOMappingId = Convert.ToInt32(hdnId.Value);
                    if (tMapping.NOMappingUpdate(out tErrorMsg))
                    {
                        HttpRuntime.Cache.Remove("NodalOfficerEmailMobileNo");
                        SetUI();
                    }
                    hdnId.Value = "-1";
                }

                lblMsg.Text = tErrorMsg;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            SetUI();
        }

        protected void gvNOMapping_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                Mapping tMapping = new Mapping();
                DataSet ds = new DataSet();
                Label lblstatus = ((Label)gvNOMapping.Rows[e.NewEditIndex].FindControl("gvlblIsActive"));
                string strIsActive = lblstatus.Text;
                hdnId.Value = (gvNOMapping.DataKeys[e.NewEditIndex].Value.ToString());

                tMapping.IsActive = Convert.ToBoolean(strIsActive);
                tMapping.NOMappingId = Convert.ToInt32(hdnId.Value);
                ds = tMapping.NOMappingEdit();
                if (ds.Tables[0].Rows.Count > 0)
                {
                    txtNOName.Text = ds.Tables[0].Rows[0]["NOName"].ToString();
                    txtNOEmail.Text = ds.Tables[0].Rows[0]["NOEmail"].ToString();
                    txtNOMobileNo.Text = ds.Tables[0].Rows[0]["NOMobileNo"].ToString();
                    int schemeId = Convert.ToInt32(ds.Tables[0].Rows[0]["SchemeId"]);
                    int stateId = Convert.ToInt32(ds.Tables[0].Rows[0]["StateId"]);
                    int districtId = Convert.ToInt32(ds.Tables[0].Rows[0]["DistrictId"]);
                    if (schemeId > 0)
                    {
                        ddlScheme.SelectedIndex = -1;
                        ddlScheme.Items.FindByValue(Convert.ToString(schemeId)).Selected = true;
                    }
                    else
                        ddlScheme.SelectedIndex = 0;
                    hdnDistricId.Value = Convert.ToString(districtId);
                    if (stateId > 0)
                    {
                        ddlState.SelectedIndex = -1;
                        ddlState.Items.FindByValue(Convert.ToString(stateId)).Selected = true;
                    }
                    else
                        ddlState.SelectedIndex = 0;
                    if (districtId > 0)
                    {
                        ddlDistrict.SelectedIndex = -1;
                        SQLDBHelper.PopulateDropDownList(ddlDistrict, "District", "District", "DistrictId", true, "StateId=" + stateId);
                        ddlDistrict.Items.FindByValue(Convert.ToString(districtId)).Selected = true;
                    }
                    else
                    {
                        ddlDistrict.Items.Insert(0, "--Select--");
                        ddlDistrict.SelectedIndex = 0;
                    }

                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void gvNOMapping_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            try
            {
                Mapping tMapping = new Mapping();
                tMapping.NOMappingId = Convert.ToInt32(gvNOMapping.DataKeys[e.RowIndex].Value.ToString());
                Label lblstatus = ((Label)gvNOMapping.Rows[e.RowIndex].FindControl("gvlblIsActive"));
                string strIsActive = lblstatus.Text;
                if (strIsActive == "False")
                {
                    tMapping.IsActive = true;
                }
                else
                {
                    tMapping.IsActive = false;
                }
                string tErrorMsg = string.Empty;
                if (tMapping.NOMappingStatusUpdate(out tErrorMsg))
                {
                    FillGrid();
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void gvNOMapping_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            Mapping tMapping = new Mapping();
            Int32 NOMappingId = Convert.ToInt32(gvNOMapping.DataKeys[e.RowIndex].Value.ToString());
            Label lblstatus = ((Label)gvNOMapping.Rows[e.RowIndex].FindControl("gvlblIsActive"));
            string strIsActive = lblstatus.Text;
            try
            {
                tMapping.IsActive = Convert.ToBoolean(strIsActive);
                tMapping.NOMappingId = NOMappingId;
                string tErrorMsg = string.Empty;
                if (tMapping.NOMappingDelete(out tErrorMsg))
                {
                    SetUI();
                }

                lblMsg.Text = tErrorMsg;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        protected void gvNOMapping_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvNOMapping.PageIndex = e.NewPageIndex;
            FillGrid();
        }
        protected void gvNOMapping_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string STRIsActive = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "IsActive"));

                    ImageButton imgbtnstatus = (ImageButton)e.Row.FindControl("imgbtnStatus");
                    if (STRIsActive == "False")
                    {
                        imgbtnstatus.ImageUrl = "~/Images/block.Jpg";
                        imgbtnstatus.ToolTip = "Click to Active";
                    }
                    else
                    {
                        imgbtnstatus.ImageUrl = "~/Images/active.gif";
                        imgbtnstatus.ToolTip = "Click to Block";
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
        }

        [System.Web.Services.WebMethod]
        public static ArrayList PopulateCities(int stateId)
        {
            ArrayList list = new ArrayList();
            String strConnString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            String strQuery = "select DistrictID, District from [District] where StateId = @StateId";
            using (SqlConnection con = new SqlConnection(strConnString))
            {
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.Parameters.AddWithValue("@StateId", stateId);
                    cmd.CommandText = strQuery;
                    cmd.Connection = con;
                    con.Open();
                    SqlDataReader sdr = cmd.ExecuteReader();
                    while (sdr.Read())
                    {
                        list.Add(new ListItem(
                       sdr["District"].ToString(),
                       sdr["DistrictID"].ToString()
                        ));
                    }
                    con.Close();
                    return list;
                }
            }
        }
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            DataTable dt = FillGridReturnDataTable();
            DataView dv = new DataView(dt);
            string SearchExpression = null;
            if (!String.IsNullOrEmpty(txtSearch.Text))
            {
                SearchExpression = string.Format("{0} '%{1}%'",
                gvNOMapping.SortExpression, txtSearch.Text);

            }
            if (!String.IsNullOrEmpty(txtSearch.Text))
            {
                dv.RowFilter = "State like" + SearchExpression + "or District like" + SearchExpression + "or Scheme like" + SearchExpression +
                    "or Noname like" + SearchExpression + "or NOEmail like" + SearchExpression + "or NOMobileNo like" + SearchExpression;
                gvNOMapping.DataSource = dv;
                gvNOMapping.DataBind();
            }
            else
            {
                gvNOMapping.DataSource = dt;
                gvNOMapping.DataBind();
            }



        }
        protected DataTable FillGridReturnDataTable()
        {
            DataTable dt = new DataTable();
            try
            {
                Mapping tMapping = new Mapping();

                DataSet ds = new DataSet();

                ds = tMapping.NOMappingSelect();
                if (ds.Tables[0].Rows.Count > 0)
                {

                    dt = ds.Tables[0];
                }

            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect("~/frmApplicationError.aspx", false);
            }
            return dt;
        }

    }
}