﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRM.Models;
using System.Reflection;
using System.Data;
using System.Configuration;
using System.Web.Services;
using EESL.Models;
using System.Collections.Specialized;
using System.Data.SqlClient;

namespace EESL.View
{
    public partial class frmvendorTickets : System.Web.UI.Page
    {
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {
            if(UIManager.CurrentUserSession().VendorSchemeName == "Building")
            {
                Response.Redirect("~/View/frmvendorTicketsBL.aspx", false);
            }
            else if (UIManager.CurrentUserSession().VendorSchemeName.ToUpper() == "AGDSM")
            {
                Response.Redirect("~/View/frmAgDSMvendorTickets.aspx", false);
            }
            else
            {
                if (!IsPostBack)
                {
                    PopulateUI();
                }
            }
            
        }

        private void PopulateUI()
        {
            txtNameOfCaller.Text = "";
            txtMobileNo.Text = "";
            txtUID.Text = "";
            txtFrom.Text = "";
            txtTo.Text = "";
            SQLDBHelper.PopulateDropDownList(ddlSource, "Source", "Source", "SourceId", false, "");
            SQLDBHelper.PopulateDropDownList(ddlLanguage, "Languages", "Language", "LanguageId", false, "");
            SQLDBHelper.PopulateDropDownList(ddlTicketType, "TicketType", "TicketType", "TicketTypeId", false, "");
        }

        private void ResetUI()
        {
            txtNameOfCaller.Text = "";
            txtMobileNo.Text = "";
            txtUID.Text = "";
            txtFrom.Text = "";
            txtTo.Text = "";
            ddlSource.ClearSelection();
            ddlSource.SelectedIndex = 0;
            ddlLanguage.ClearSelection();
            ddlLanguage.SelectedIndex = 0;
            ddlTicketType.ClearSelection();
            ddlTicketType.SelectedIndex = 0;
            ddlTicketStatus.ClearSelection();
            ddlTicketStatus.SelectedIndex = 0;
        }

        public static DataSet GetGridData(string sUID, string sNameOfCaller, string sMobileNo, string sSourceId, string sLanguageId, string sDateFrom, string sDateTo, string sTicketTypeId, string sKeyword, string sTicketStatus)
        {
            DataSet ds = new DataSet();
            try
            {
                Call tCall = new Call();

                if (!String.IsNullOrEmpty(sUID))
                {
                    tCall.UID = Convert.ToString(sUID);
                }
                if (!String.IsNullOrEmpty(sNameOfCaller))
                {
                    tCall.CallerName = Convert.ToString(sNameOfCaller.Trim());
                }
                if (!String.IsNullOrEmpty(sMobileNo))
                {
                    tCall.CallerNumber = Convert.ToString(sMobileNo);
                }
                if (sSourceId != "--Select--" && sSourceId != string.Empty)
                {
                    tCall.SourceId = Convert.ToInt32(sSourceId);
                }
                if (sLanguageId != "--Select--" && sLanguageId != string.Empty)
                {
                    tCall.LanguageId = Convert.ToInt32(sLanguageId);
                }
                if (!String.IsNullOrEmpty(sDateFrom))
                {
                    tCall.DateFrom = Convert.ToDateTime(sDateFrom.Trim());
                }
                if (!String.IsNullOrEmpty(sDateTo))
                {
                    tCall.DateTo = Convert.ToDateTime(sDateTo.Trim());
                }
                if (sTicketTypeId != "--Select--" && sTicketTypeId != string.Empty)
                {
                    tCall.TicketTypeId = Convert.ToInt32(sTicketTypeId);
                }
                if (!String.IsNullOrEmpty(sKeyword))
                {
                    tCall.Keyword = Convert.ToString(sKeyword);
                }
                tCall.TicketStatus = Convert.ToString(sTicketStatus);
                tCall.VendorId = Convert.ToInt32(SQLDBHelper.getFieldValueByCondition("UserMaster", "VendorId", "UserId", Convert.ToString(UIManager.CurrentUserSession().UserId)));
                ds = tCall.CallVendor_Search();
                return ds;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                return ds;
            }
        }

        [WebMethod]
        public static string GetDataFromDB(int? numRows, int? page, string sortField, string sortOrder, bool isSearch, string sUID, string sNameOfCaller, string sMobileNo, string sSourceId, string sLanguageId, string sDateFrom, string sDateTo, string sTicketTypeId, string sKeyword, string sTicketStatus)
        {
            string result = null;

            try
            {
                //--- retrieve the data
                //JobRegistration tJobRegistration = new JobRegistration();
                //DataTable dt = new DataTable();
                DataSet ds = new DataSet();
                ds = GetGridData(sUID, sNameOfCaller, sMobileNo, sSourceId, sLanguageId, sDateFrom, sDateTo, sTicketTypeId, sKeyword, sTicketStatus);

                //var query = from u in ds.Tables[0].AsEnumerable()
                //            select u;

                IEnumerable<VendorTickets> query = from u in ds.Tables[0].AsEnumerable()
                                                   select new VendorTickets
                                                   {
                                                       CallId = u["CallId"].ToString(),
                                                       CallerNumber = u["CallerNumber"].ToString(),
                                                       UID = u["UID"].ToString(),
                                                       CallerName = u["CallerName"].ToString(),
                                                       Address = u["Address"].ToString(),
                                                       Landmark = u["Landmark"].ToString(),
                                                       ComplaintFrom = u["ComplaintFrom"].ToString(),
                                                       StatusOfCall = u["LastStatus"].ToString(),
                                                       SL_WardNo = u["SL_WardNo"].ToString(),
                                                       SL_PoleNo = u["SL_PoleNo"].ToString(),
                                                       SL_AssignedToId = "--Select--", //u["SL_AssignedToId"].ToString(),
                                                       SL_LampTypeId = "--Select--",//u["SL_LampTypeId"].ToString(),
                                                       DateOfCall = u["DateOfCall"].ToString(),
                                                       DateUpdate = u["DateUpdate"].ToString(),
                                                       ComplaintHoursDiff = u["ComplaintHoursDiff"].ToString(),
                                                       TicketType = u["TicketType"].ToString(),
                                                       Zone = u["Zone"].ToString(),
                                                       VendorName = u["VendorName"].ToString(),
                                                       DateInsert = u["DateInsert"].ToString(),
                                                       StateId = u["StateId"].ToString(),
                                                       DistrictId = u["DistrictId"].ToString(),
                                                       SL_ULBId = u["SL_ULBId"].ToString(),
                                                       Remark = string.Empty,
                                                       History = string.Empty,
                                                       ReAssign = string.Empty,
                                                       Rectify = string.Empty
                                                   };

                //--- setup calculations 
                int pageIndex = page ?? 1; //--- current page 
                int pageSize = numRows ?? 10; //--- number of rows to show per page 
                int totalRecords = query.Count(); //--- number of total items from query 
                int totalPages = (int)Math.Ceiling((decimal)totalRecords / (decimal)pageSize); //--- number of pages 

                //--- filter dataset for paging and sorting 
                //IQueryable<DataRow> orderedRecords = query.OrderBy(sortField).AsQueryable();
                //IEnumerable<DataRow> sortedRecords = orderedRecords.ToList();

                IEnumerable<VendorTickets> sortedRecords = query.ToList();

                //if (sortOrder == "desc") sortedRecords = sortedRecords.Reverse();
                sortedRecords = sortedRecords
                    .Skip((pageIndex - 1) * pageSize) //--- page the data 
                    .Take(pageSize);

                //--- format json 
                var jsonData = new
                {
                    totalpages = totalPages, //--- number of pages 
                    page = pageIndex, //--- current page 
                    totalrecords = totalRecords, //--- total items 
                    rows = (from row in sortedRecords
                            select new
                            {
                                i = row.CallId,
                                cell = new string[] {
                                     row.CallId.ToString(),
                                     row.CallerNumber.ToString(),
                                     row.UID.ToString(),
                                     row.CallerName.ToString(),
                                     row.Address.ToString(),
                                     row.Landmark.ToString(),
                                     row.ComplaintFrom.ToString(),                                     
                                     row.StatusOfCall.ToString(),
                                     row.SL_WardNo.ToString(),
                                     row.SL_PoleNo.ToString(),
                                     row.SL_AssignedToId.ToString(),
                                     row.SL_LampTypeId.ToString(),
                                     row.Remark.ToString(),
                                     row.DateOfCall.ToString(),
                                     row.DateUpdate.ToString(),
                                     row.ComplaintHoursDiff.ToString(),
                                     row.History.ToString(),
                                     row.ReAssign.ToString(),
                                     row.Rectify.ToString(),
                                     row.StateId.ToString(),
                                     row.DistrictId.ToString(),
                                     row.SL_ULBId.ToString(),
                                     row.TicketType.ToString(),
                                     row.Zone.ToString(),
                                     row.VendorName.ToString(),
                                     row.DateInsert.ToString()


                                 }
                            }
                             ).ToArray()
                };
                result = Newtonsoft.Json.JsonConvert.SerializeObject(jsonData);
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                //Debug.WriteLine(ex);
            }
            finally
            {
                //if (db != null) db.Dispose();
            }
            return result;
        }

        [WebMethod]
        public static List<Dictionary<string, object>> GetDetailHistory(string sCallId)
        {
            DataTable dt = new DataTable();
            List<Dictionary<string, object>> _result;

            Call tCall = new Call();
            DataSet ds = new DataSet();
            tCall.CallId = Int32.Parse(sCallId);
            ds = tCall.GetCallHistory();

            dt = ds.Tables[0];

            _result =
                        dt.AsEnumerable().Select(dr =>
                        {
                            var dic = new Dictionary<string, object>();
                            dr.ItemArray.Aggregate(-1, (int i, object v) =>
                            {
                                i += 1; dic.Add(dt.Columns[i].ColumnName, v);
                                return i;
                            });
                            return dic;
                        }).ToList();

            return _result;
        }

        [WebMethod]
        public static List<Dictionary<string, object>> GetAssignedHistory(string sCallId)
        {
            DataTable dt = new DataTable();
            List<Dictionary<string, object>> _result;

            Call tCall = new Call();
            DataSet ds = new DataSet();
            tCall.CallId = Int32.Parse(sCallId);
            ds = tCall.GetCallHistory();

            dt = ds.Tables[1];

            _result =
                        dt.AsEnumerable().Select(dr =>
                        {
                            var dic = new Dictionary<string, object>();
                            dr.ItemArray.Aggregate(-1, (int i, object v) =>
                            {
                                i += 1; dic.Add(dt.Columns[i].ColumnName, v);
                                return i;
                            });
                            return dic;
                        }).ToList();

            return _result;
        }

        [WebMethod]
        public static string GetReAssignedTo(string stateid, string districtid, string ulbmappingid)
        {
            List<string> result = new List<string>();
            DataSet ds = new DataSet();
            VendorTickets reassigned = new VendorTickets();
            ds = reassigned.GetReAssignedTo(stateid, districtid, ulbmappingid);
            string opt = string.Empty;
            opt = "<option value=0>--Select--</option>";
            foreach (DataRow row in ds.Tables[0].Rows)
            {
                opt += "<option value=" + row["VendorMappingId"].ToString() + ">" + row["VendorName"].ToString() + "</option>";
            }
            return opt;
        }

        [WebMethod]
        public static string GetLampType()
        {
            string opt = string.Empty;

            if (HttpContext.Current.Session["LampType"] == null)
            {
                List<string> result = new List<string>();
                DataSet ds = new DataSet();
                VendorTickets reassigned = new VendorTickets();
                ds = reassigned.GetLampType();
                
                opt = "<option value=0>--Select--</option>";
                foreach (DataRow row in ds.Tables[0].Rows)
                {
                    opt += "<option value=" + row["LampTypeId"].ToString() + ">" + row["LampType"].ToString() + "</option>";
                }
                HttpContext.Current.Session["LampType"] = opt;
            }
            else
            {
                opt = HttpContext.Current.Session["LampType"].ToString();
            }
            return opt;
        }

        [WebMethod]
        public static string SaveReAssign(string sCallId, string sAssignTo, string sRemark)
        {
            string tErrorMsg = string.Empty;
            int userId = UIManager.CurrentUserSession().UserId;
            Call tCall = new Call();
            tCall.CallId = Int32.Parse(sCallId);
            tCall.SL_AssignedToId = Int32.Parse(sAssignTo);
            tCall.Remark = sRemark;
            tCall.UserId = userId;
            if (tCall.CallReAssigned_Update(out tErrorMsg))
            {
                
            }
            return tErrorMsg;
        }

        [WebMethod]
        public static string SaveRectify(string sCallId, string sLampType, string sRemark)
        {
            string tErrorMsg = string.Empty;
            int userId = UIManager.CurrentUserSession().UserId;

            Call tCall = new Call();
            tCall.CallId = Int32.Parse(sCallId);
            tCall.Sl_LampTypeId = Int32.Parse(sLampType);
            tCall.Remark = sRemark;
            tCall.UserId = userId;
            if (tCall.CallRectified_Update(out tErrorMsg))
            {
                
            }

            return tErrorMsg;
        }

        [WebMethod]
        public static string GetRemarkList()
        {
            List<string> result = new List<string>();
            string opt = string.Empty;
            opt = "<option value=0>--Select--</option>";
            opt += "<option value='CCMS Issue'>CCMS Issue</option>";
            opt += "<option value='Cable Fault'>Cable Fault</option>";
            opt += "<option value='Cable Fault (BSES)'>Cable Fault (BSES)</option>";
            opt += "<option value='Carbon Issue'>Carbon Issue</option>";
            opt += "<option value='Flickering'>Flickering</option>";
            opt += "<option value='Group failure'>Group failure</option>";
            opt += "<option value='Jumper'>Jumper</option>";
            opt += "<option value='Light Change'>Light Change</option>";
            opt += "<option value='Light Replaced'>Light Replaced</option>";
            opt += "<option value='Loose connection'>Loose connection</option>";
            opt += "<option value='SPD Change'>SPD Change</option>";
            opt += "<option value='Theft'>Theft</option>";
            opt += "<option value='Theft'>Light Hanging</option>";
            opt += "<option value='Theft'>Pole Issue</option>";

            return opt;
        }

        private static string GetOperator(string op)
        {
            string searchOper = "==";
            switch (op)
            {
                case "eq":  //equals
                case "ew":  //equals with
                    searchOper = "=="; break;
                case "ne": //notequal
                    searchOper = "!="; break;
                //case "bw": //begins with
                //    searchOper = "=="; break;
                //case "bn": //does not begin with
                //    searchOper = "=="; break;
                //case "en": //does not ends with
                //    searchOper = "=="; break;
                case "cn": //contains
                    searchOper = "LIKE"; break;
                case "nc": //does not contains
                    searchOper = "NOTLIKE"; break;
                //case "nu": //is null
                //    searchOper = "=="; break;
                //case "nn": //is not null
                //searchOper = "=="; break;
                //case "in": //is in 
                //    searchOper = "=="; break;
                //case "ni": //is not in
                //    searchOper = "=="; break;
                case "lt": //less
                    searchOper = "<"; break;
                case "le": //less or equal
                    searchOper = "<="; break;
                case "gt": //greater
                    searchOper = ">"; break;
                case "ge": //greater or equal
                    searchOper = ">="; break;
                case "AND": //greater or equal
                    searchOper = "&&"; break;
                case "OR": //greater or equal
                    searchOper = "||"; break;
                default:
                    searchOper = "=="; break;
            }
            return searchOper;
        }

        protected void ExportSearchDetail()
        {
            //try
            //{
            string sFileName = "VendorMISSummary-" + System.DateTime.Now.Date + ".xls";
            Call tCall = new Call();
            DataSet ds = new DataSet();
            if (!String.IsNullOrEmpty(txtUID.Text))
            {
                tCall.UID = Convert.ToString(txtUID.Text.Trim());
            }
            if (!String.IsNullOrEmpty(txtNameOfCaller.Text))
            {
                tCall.CallerName = Convert.ToString(txtNameOfCaller.Text.Trim());
            }
            if (!String.IsNullOrEmpty(txtMobileNo.Text))
            {
                tCall.CallerNumber = Convert.ToString(txtMobileNo.Text.Trim());
            }
            if (ddlSource.SelectedIndex > 0)
            {
                tCall.SourceId = Convert.ToInt32(ddlSource.SelectedItem.Value);
            }
            if (ddlLanguage.SelectedIndex > 0)
            {
                tCall.LanguageId = Convert.ToInt32(ddlLanguage.SelectedItem.Value);
            }
            if (!String.IsNullOrEmpty(txtFrom.Text))
            {
                tCall.DateFrom = Convert.ToDateTime(txtFrom.Text.Trim());
            }
            if (!String.IsNullOrEmpty(txtTo.Text))
            {
                tCall.DateTo = Convert.ToDateTime(txtTo.Text.Trim());
            }
            if (ddlTicketType.SelectedIndex > 0)
            {
                tCall.TicketTypeId = Convert.ToInt32(ddlTicketType.SelectedItem.Value);
            }
            if (!String.IsNullOrEmpty(txtKeyword.Text))
            {
                tCall.Keyword = Convert.ToString(txtKeyword.Text.Trim());
            }
            tCall.TicketStatus = Convert.ToString(ddlTicketStatus.SelectedItem.Value);
            tCall.VendorId = Convert.ToInt32(SQLDBHelper.getFieldValueByCondition("UserMaster", "VendorId", "UserId", Convert.ToString(UIManager.CurrentUserSession().UserId)));
            ds = tCall.CallVendor_Search();
            if (ds.Tables[0].Rows.Count > 0)
            {
                ExportDatatableToExcel(ds.Tables[0], sFileName);
            }

            //}
            //catch (Exception Ex)
            //{
            //    ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
            //    Response.Redirect("~/frmApplicationError.aspx", false);
            //}
        }

        public void ExportDatatableToExcel(DataTable table, string sFileName)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.ContentType = "application/ms-excel";
            HttpContext.Current.Response.Write(@"<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.0 Transitional//EN"">");
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + sFileName);

            HttpContext.Current.Response.Charset = "utf-8";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.GetEncoding("windows-1250");
            //sets font
            HttpContext.Current.Response.Write("<font style='font-size:10.0pt; font-family:Calibri;'>");
            HttpContext.Current.Response.Write("<BR><BR><BR>");
            //sets the table border, cell spacing, border color, font of the text, background, foreground, font height
            HttpContext.Current.Response.Write("<Table border='1' bgColor='#ffffff' " +
              "borderColor='#000000' cellSpacing='0' cellPadding='0' " +
              "style='font-size:10.0pt; font-family:Calibri; background:white;'> <TR>");
            //am getting my grid's column headers
            int columnscount = table.Columns.Count;

            for (int j = 0; j < columnscount; j++)
            {      //write in new column
                HttpContext.Current.Response.Write("<Td>");
                //Get column headers  and make it as bold in excel columns
                HttpContext.Current.Response.Write("<B>");
                HttpContext.Current.Response.Write(table.Columns[j].ColumnName.ToString());
                HttpContext.Current.Response.Write("</B>");
                HttpContext.Current.Response.Write("</Td>");
            }
            HttpContext.Current.Response.Write("</TR>");
            foreach (DataRow row in table.Rows)
            {//write in new row
                HttpContext.Current.Response.Write("<TR>");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    HttpContext.Current.Response.Write("<Td>");
                    HttpContext.Current.Response.Write(row[i].ToString());
                    HttpContext.Current.Response.Write("</Td>");
                }

                HttpContext.Current.Response.Write("</TR>");
            }
            HttpContext.Current.Response.Write("</Table>");
            HttpContext.Current.Response.Write("</font>");
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {

                //FillGrid();

            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                //Response.Redirect("/frmApplicationError.aspx", false);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                //PopulateUI();
                ResetUI();
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                //Response.Redirect("/frmApplicationError.aspx", false);
            }
        }

        //protected void btnHistory_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        Button btn = (Button)sender;
        //        GridViewRow gvr = (GridViewRow)btn.NamingContainer;
        //        Label gvlblCallId = (Label)gvr.FindControl("gvlblCallId");
        //        int callId = Convert.ToInt32(gvlblCallId.Text.Trim());
        //        FillCallDetailHistoryGrid(callId);
        //    }
        //    catch (Exception Ex)
        //    {
        //        ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);

        //    }
        //}
        //protected void FillCallDetailHistoryGrid(int callId)
        //{
        //    try
        //    {
        //        Call tCall = new Call();
        //        DataSet ds = new DataSet();
        //        tCall.CallId = callId;
        //        ds = tCall.GetCallHistory();
        //        if (ds.Tables[0].Rows.Count > 0 || ds.Tables[1].Rows.Count > 0)
        //        {
        //            if (ds.Tables[0].Rows.Count > 0)
        //            {
        //                gvCallDetailHistory.DataSource = ds.Tables[0];
        //                gvCallDetailHistory.DataBind();
        //                gvCallDetailHistory.Visible = true;
        //            }
        //            else
        //            {
        //                gvCallDetailHistory.Visible = false;
        //            }
        //            if (ds.Tables[1].Rows.Count > 0)
        //            {
        //                gvCallAssignedHistory.DataSource = ds.Tables[1];
        //                gvCallAssignedHistory.DataBind();
        //                gvCallAssignedHistory.Visible = true;
        //            }
        //            else
        //            {
        //                gvCallAssignedHistory.Visible = false;
        //            }
        //            modelHistory.Show();
        //        }


        //    }
        //    catch (Exception Ex)
        //    {
        //        ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
        //        Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
        //    }
        //}
        //protected void btnReAssign_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        string tErrorMsg = string.Empty;
        //        Button btn = (Button)sender;
        //        GridViewRow gvr = (GridViewRow)btn.NamingContainer;
        //        Label gvlblCallId = (Label)gvr.FindControl("gvlblCallId");
        //        DropDownList ddlReAssignedTo = (DropDownList)gvr.FindControl("ddlReAssignedTo");
        //        TextBox txtRemark = (TextBox)gvr.FindControl("txtRemark");
        //        int callId = Convert.ToInt32(gvlblCallId.Text.Trim());
        //        int userId = UIManager.CurrentUserSession().UserId;
        //        int assignToId = ddlReAssignedTo.SelectedIndex != 0 ? Convert.ToInt32(ddlReAssignedTo.SelectedItem.Value) : 0;
        //        string remark = Convert.ToString(txtRemark.Text.Trim());

        //        Call tCall = new Call();
        //        tCall.CallId = callId;
        //        tCall.SL_AssignedToId = assignToId;
        //        tCall.Remark = remark;
        //        tCall.UserId = userId;
        //        if (tCall.CallReAssigned_Update(out tErrorMsg))
        //        {
        //            //FillGrid();
        //            lblMessage.Text = tErrorMsg;
        //        }

        //    }
        //    catch (Exception Ex)
        //    {
        //        ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);

        //    }
        //}
        //protected void btnRectify_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        string tErrorMsg = string.Empty;
        //        Button btn = (Button)sender;
        //        GridViewRow gvr = (GridViewRow)btn.NamingContainer;
        //        Label gvlblCallId = (Label)gvr.FindControl("gvlblCallId");
        //        DropDownList ddlLampType = (DropDownList)gvr.FindControl("ddlLampType");
        //        TextBox txtRemark = (TextBox)gvr.FindControl("txtRemark");
        //        int callId = Convert.ToInt32(gvlblCallId.Text.Trim());
        //        int userId = UIManager.CurrentUserSession().UserId;
        //        int lampTypeId = ddlLampType.SelectedIndex != 0 ? Convert.ToInt32(ddlLampType.SelectedItem.Value) : 0;
        //        string remark = Convert.ToString(txtRemark.Text.Trim());
        //        Call tCall = new Call();
        //        tCall.CallId = callId;
        //        tCall.Sl_LampTypeId = lampTypeId;
        //        tCall.Remark = remark;
        //        tCall.UserId = userId;
        //        if (tCall.CallRectified_Update(out tErrorMsg))
        //        {
        //            //FillGrid();
        //            lblMessage.Text = tErrorMsg;
        //        }

        //    }
        //    catch (Exception Ex)
        //    {
        //        ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);

        //    }
        //}
        protected void imgbtnExportExcel_Click(object sender, System.Web.UI.ImageClickEventArgs e)
        {
            ExportSearchDetail();
        }
        //protected void gvCalllDetail_RowDataBound(object sender, GridViewRowEventArgs e)
        //{

        //    try
        //    {
        //        if (e.Row.RowType == DataControlRowType.DataRow)
        //        {
        //            string strStatus = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "LastStatus"));
        //            string stateId = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "StateId"));
        //            string districtId = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "DistrictId"));
        //            string ulbId = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "SL_ULBId"));
        //            string lampTypeId = Convert.ToString(DataBinder.Eval(e.Row.DataItem, "SL_LampTypeId"));
        //            Button btnReAssign = (Button)e.Row.FindControl("btnReAssign");
        //            Button btnRectify = (Button)e.Row.FindControl("btnRectify");
        //            DropDownList ddlLampType = (DropDownList)e.Row.FindControl("ddlLampType");
        //            SQLDBHelper.PopulateDropDownList(ddlLampType, "LampType", "LampType", "LampTypeId", false, "");
        //            if (lampTypeId != "0")
        //            {
        //                ddlLampType.SelectedIndex = -1;
        //                ddlLampType.Items.FindByValue(Convert.ToString(lampTypeId)).Selected = true;
        //            }

        //            DropDownList ddlReAssignedTo = (DropDownList)e.Row.FindControl("ddlReAssignedTo");
        //            SQLDBHelper.PopulateDropDownList(ddlReAssignedTo, "VendorMapping", "VendorName", "VendorMappingId", true, "StateId=" + stateId + "AND VendorType='CCMS' AND DistrictId=" + districtId + " AND ULBMappingId=" + ulbId);
        //            if (strStatus != "Rectified" && strStatus != "Not Verified" && strStatus != "Verified" && strStatus != "Closed")
        //            {
        //                btnReAssign.Enabled = true;
        //                btnRectify.Enabled = true;
        //                btnReAssign.Visible = true;
        //                btnRectify.Visible = true;
        //            }
        //            else
        //            {
        //                btnReAssign.Enabled = false;
        //                btnRectify.Enabled = false;
        //                btnReAssign.Visible = false;
        //                btnRectify.Visible = false;
        //            }

        //            if (strStatus == "Open" || strStatus == "Escalated")
        //            {
        //                e.Row.Attributes["style"] = "background-color: #FEF9E7";
        //            }
        //            else if (strStatus == "Rectified")
        //            {
        //                e.Row.Attributes["style"] = "background-color: #F9E79F";
        //            }
        //            else if (strStatus == "Not Verified")
        //            {
        //                e.Row.Attributes["style"] = "background-color: #EBF5FB";
        //            }
        //            else if (strStatus == "Verified")
        //            {
        //                e.Row.Attributes["style"] = "background-color: #ABEBC6";
        //            }

        //        }
        //    }
        //    catch (Exception Ex)
        //    {
        //        ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
        //        Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
        //    }
        //}

        //Rectify button click event
        //protected void btnRectifySelected_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        string tErrorMsg = string.Empty;
        //        string strname = string.Empty;
        //        Boolean flagCheckFound = false;

        //        foreach (GridViewRow gvr in gvCalllDetail.Rows)
        //        {
        //            CheckBox chk = (CheckBox)gvr.FindControl("chkRectify");
        //            if (chk != null & chk.Checked)
        //            {
        //                flagCheckFound = true;
        //                Label gvlblCallId = (Label)gvr.FindControl("gvlblCallId");
        //                DropDownList ddlLampType = (DropDownList)gvr.FindControl("ddlLampType");
        //                TextBox txtRemark = (TextBox)gvr.FindControl("txtRemark");
        //                int callId = Convert.ToInt32(gvlblCallId.Text.Trim());
        //                int userId = UIManager.CurrentUserSession().UserId;
        //                int lampTypeId = ddlLampType.SelectedIndex != 0 ? Convert.ToInt32(ddlLampType.SelectedItem.Value) : 0;
        //                string remark = Convert.ToString(txtRemark.Text.Trim());
        //                Call tCall = new Call();
        //                tCall.CallId = callId;
        //                tCall.Sl_LampTypeId = lampTypeId;
        //                tCall.Remark = remark;
        //                tCall.UserId = userId;
        //                if (tCall.CallRectified_Update(out tErrorMsg))
        //                {

        //                    lblMessage.Text = tErrorMsg;
        //                }
        //            }
        //        }
        //        if (flagCheckFound == true)
        //        {
        //            FillGrid();
        //        }
        //    }
        //    catch (Exception Ex)
        //    {
        //        ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);

        //    }
        //}
    }
}