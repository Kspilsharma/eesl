﻿using CRM.Models;
using System;
using System.Configuration;
using System.Data;
using System.Reflection;
using System.Web;
using System.Web.UI.WebControls;

namespace EESL.View
{
    public partial class frmSMSEmailConfigurationSetting : UIBase
    {
        private string AppBaseURL = ConfigurationManager.AppSettings["WebBaseURL"].ToString();

        protected void Page_Load(object sender, EventArgs e)
        {
            lblMsg.Text = "";
            if (!IsPostBack)
            {
                SetUI();
            }
        }

        protected void SetUI()
        {
            SQLDBHelper.PopulateDropDownList(ddlSettingType, "SettingTypeMaster", "Name", "Id", false, "");
            FillDataList();
        }

        protected void ddlSettingType_SelectedIndexChanged(object sender, EventArgs e)
        {
            FillDataList();
        }

        private void FillDataList()
        {
            try
            {
                SMSEmailConfigurationSetting tSMSEmailConfigurationSetting = new SMSEmailConfigurationSetting();
                tSMSEmailConfigurationSetting.SettingTypeId = ddlSettingType.SelectedIndex == 0 ? 0 : Convert.ToInt32(ddlSettingType.SelectedItem.Value);
                DataSet dtcommon = tSMSEmailConfigurationSetting.Select();

                if (dtcommon.Tables[0].Rows.Count > 0)
                {
                    dlSettings.DataSource = dtcommon.Tables[0];
                    dlSettings.DataBind();
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            try
            {
                SMSEmailConfigurationSetting tSMSEmailConfigurationSetting = new SMSEmailConfigurationSetting();
                tSMSEmailConfigurationSetting.SettingTypeId = Convert.ToInt32(ddlSettingType.SelectedItem.Value);
                string tErrorMsg = string.Empty;
                bool tResult = false;
                foreach (DataListItem item in dlSettings.Items)
                {
                    Label lblBatchId = item.FindControl("lblStatusOfCallId") as Label;
                    CheckBox chkSMS = item.FindControl("chkSMS") as CheckBox;
                    CheckBox chkEMail = item.FindControl("chkEMail") as CheckBox;
                    tSMSEmailConfigurationSetting.StatusOfCallId = Convert.ToInt32(lblBatchId.Text.Trim());
                    tSMSEmailConfigurationSetting.sendSMS = chkSMS.Checked;
                    tSMSEmailConfigurationSetting.sendEmail = chkEMail.Checked;
                    tResult = tSMSEmailConfigurationSetting.UpdateSMSEmailConfiguration(out tErrorMsg);
                }

                if (tResult)
                {
                    ddlSettingType.SelectedIndex = 0;
                    FillDataList();
                }

                HttpRuntime.Cache.Remove("SMSEmailConfigurationSettings");
                lblMsg.Text = tErrorMsg;
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
                Response.Redirect(AppBaseURL + "/frmApplicationError.aspx", false);
            }
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            SetUI();
        }
    }
}