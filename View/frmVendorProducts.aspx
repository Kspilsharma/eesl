﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteNew.Master" AutoEventWireup="true" CodeBehind="frmVendorProducts.aspx.cs" Inherits="EESL.View.frmVendorProducts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.min.js"></script>
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {

        });
        var pageUrl = '<%=ResolveUrl("~/View/frmVendorProducts.aspx")%>'
        function PopulateVendorType() {
            if ($('#<%=ddlScheme.ClientID%>').val() == "--Select--") {
                $('#<%=ddlVendorType.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            }
            else {
                $('#<%=ddlVendorType.ClientID %>').empty().append('<option selected="selected" value="0">Loading...</option>');
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/PopulateVendorType',
                    data: '{SchemeId: ' + $('#<%=ddlScheme.ClientID%>').val() + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnVendorTypePopulated,
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
        }

        function OnVendorTypePopulated(response) {
            PopulateControl(response.d, $("#<%=ddlVendorType.ClientID %>"), "VendorType");
        }

        function PopulateCities() {
            $('#<%=ddlVendor.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');

            if ($('#<%=ddlState.ClientID%>').val() == "--Select--") {
                $('#<%=ddlDistrict.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            }
            else {
                $('#<%=ddlDistrict.ClientID %>').empty().append('<option selected="selected" value="0">Loading...</option>');
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/PopulateCities',
                    data: '{stateId: ' + $('#<%=ddlState.ClientID%>').val() + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnCitiesPopulated,
                    failure: function (response) {
                        alert(response.d);
                    }
                });
                PopulateVendor();
            }
        }

        function OnCitiesPopulated(response) {
            PopulateControl(response.d, $("#<%=ddlDistrict.ClientID %>"), "District");
        }


        function PopulateVendor() {

            <%--if ($('#<%=ddlULB.ClientID%>').val() == "--Select--") {
                        $('#<%=ddlVendor.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            }
            else {--%>
            $('#<%=ddlVendor.ClientID %>').empty().append('<option selected="selected" value="0">Loading...</option>');
            $.ajax({
                type: "POST",
                url: pageUrl + '/PopulateVendor',
                data: '{schemeId: ' + $('#<%=ddlScheme.ClientID%>').val() + ',vendortypeId: ' + $('#<%=ddlVendorType.ClientID%>').val() + ',stateId: ' + $('#<%=ddlState.ClientID%>').val() + ',districtId: ' + $('#<%=ddlDistrict.ClientID%>').val() + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnVendorPopulated,
                failure: function (response) {
                    alert(response.d);
                }
            });
            //}
        }

        function OnVendorPopulated(response) {
            PopulateControl(response.d, $("#<%=ddlVendor.ClientID %>"), "Vendor");
        }

        function PopulateControl(list, control, type) {
            if (list.length > 0) {
                control.empty().append('<option selected="selected" value="0">--Select--</option>');
                $.each(list, function () {
                    control.append($("<option></option>").val(this['Value']).html(this['Text']));
                });
            }
            else {
                if (type == "District") {
                    $('#<%=ddlDistrict.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
                }
                else if (type == "VendorType") {
                    $('#<%=ddlVendorType.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
                }
                else if (type == "Vendor") {
                    $('#<%=ddlVendor.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
                }
            }
        }

        function PopulateProducts() {
            
            $.ajax({
                type: "POST",
                url: pageUrl + '/PopulateProducts',
                data: '{vendortypeId: ' + $('#<%=ddlVendorType.ClientID%>').val() + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: OnProductsPopulate,
                failure: function (response) {
                    alert(response.d);
                }
            });
        }

        function OnProductsPopulate(response) {
            var checkboxlistItems = JSON.parse(response.d);
            var table = $('<table></table>');
            var counter = 0;
            $(checkboxlistItems).each(function () {
                
                if (this.IsChecked == "true") {
                    table.append($('<tr></tr>').append($('<td></td>').append($('<input>').attr('checked', true).attr({
                        type: 'checkbox', name: 'chklistitem', runat: 'server', value: this.Value, id: 'chklistitem' + counter
                    })).append(
                        $('<label>').attr({
                            for: 'chklistitem' + counter++
                        }).text(this.Name))));
                }
                else
                {
                    table.append($('<tr></tr>').append($('<td></td>').append($('<input>').attr('checked', false).attr({
                        type: 'checkbox', name: 'chklistitem', runat: 'server', value: this.Value, id: 'chklistitem' + counter
                    })).append(
                        $('<label>').attr({
                            for: 'chklistitem' + counter++
                        }).text(this.Name))));
                }
            });

            $('#chklstProducts').append(table);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="Heading" align="center" style="color: #101010">
                <asp:Label ID="lblPageHeader" runat="server" Text="Vendor Product Mapping" CssClass="bold"></asp:Label>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
        <tr>
            <td align="left" valign="top" width="15%"></td>
            <td valign="top">
                <fieldset class="fieldset">
                    <legend class="Legendheading" align="left">Vendor Product Mapping</legend>
                    <table width="100%">
                        <tr>
                            <td style="width: 80%;">
                                <table cellpadding="0" cellspacing="3px" width="100%" style="margin: 0px; padding: 0px;">
                                    <tr>
                                        <td colspan="2" align="left">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="failureNotification"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:92px;" align="left" class="tdlabel" valign="top">Scheme<span style="color: red">&nbsp;*</span> &nbsp;:</td>
                                        <td valign="top" align="left" class="tddata" style="width:180px;"><asp:DropDownList ID="ddlScheme" TabIndex="100" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlScheme_SelectedIndexChanged" /></td>
                                        <td><asp:RequiredFieldValidator ID="rfvScheme" ForeColor="Maroon" ControlToValidate="ddlScheme" InitialValue="--Select--" runat="server" ErrorMessage="Select Scheme" Display="Dynamic" ValidationGroup="Save" Font-Bold="True" Font-Size="X-Small"></asp:RequiredFieldValidator></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="tdlabel" valign="top">Vendor Type<span style="color: red">&nbsp;*</span>&nbsp;:</td>
                                        <td valign="top" align="left" class="tddata"><asp:DropDownList ID="ddlVendorType" runat="server" Width="120px" AutoPostBack="true" TabIndex="101" OnSelectedIndexChanged="ddlVendorType_SelectedIndexChanged"></asp:DropDownList></td>
                                        <td><asp:RequiredFieldValidator ID="rfvVendorType" ForeColor="Maroon" ControlToValidate="ddlVendorType" InitialValue="0" runat="server" ErrorMessage="Select Vendor Type" Display="Dynamic" ValidationGroup="Save" Font-Bold="True" Font-Size="X-Small"></asp:RequiredFieldValidator></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="tdlabel" valign="top">State<span style="color: red">&nbsp;*</span> &nbsp;:</td>
                                        <td valign="top" align="left" class="tddata"><asp:DropDownList ID="ddlState" runat="server" Width="180px" AutoPostBack="true" TabIndex="102" OnSelectedIndexChanged="ddlState_SelectedIndexChanged" /></td>
                                        <td><asp:RequiredFieldValidator ID="rfvState" ForeColor="Maroon" ControlToValidate="ddlState" InitialValue="--Select--" runat="server" ErrorMessage="Select State" Display="Dynamic" ValidationGroup="Save" Font-Bold="True" Font-Size="X-Small"></asp:RequiredFieldValidator></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="tdlabel" valign="top">District<span style="color: red">&nbsp;*</span> &nbsp;:</td>
                                        <td valign="top" align="left" class="tddata"><asp:DropDownList ID="ddlDistrict" runat="server" Width="180px" AutoPostBack="true" TabIndex="103" OnSelectedIndexChanged="ddlDistrict_SelectedIndexChanged" /></td>
                                        <td><asp:RequiredFieldValidator ID="rfvDistrict" ForeColor="Maroon" ControlToValidate="ddlDistrict" InitialValue="0" runat="server" ErrorMessage="Select District" Display="Dynamic" ValidationGroup="Save" Font-Bold="True" Font-Size="X-Small"></asp:RequiredFieldValidator></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="tdlabel" valign="top">Vendor<span style="color: red">&nbsp;*</span> &nbsp;:</td>
                                        <td valign="top" align="left" class="tddata"><asp:DropDownList ID="ddlVendor" runat="server" Width="180px" TabIndex="104" AutoPostBack="true" OnSelectedIndexChanged="ddlVendor_SelectedIndexChanged" /></td>
                                        <td><asp:RequiredFieldValidator ID="rfvVendor" ForeColor="Maroon" ControlToValidate="ddlVendor" InitialValue="0" runat="server" ErrorMessage="Select Vendor" Display="Dynamic" ValidationGroup="Save" Font-Bold="True" Font-Size="X-Small"></asp:RequiredFieldValidator></td>
                                    </tr>
                                    <tr>
                                        <td align="left">&nbsp;</td>
                                        <td align="left">
                                            <asp:Button ID="btnSave" Text="Save" TabIndex="105" runat="server" ValidationGroup="Save" OnClick="btnSave_Click"/>
                                            <asp:Button ID="btnReset" Text="Reset" TabIndex="106" runat="server" OnClick="btnReset_Click" />
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="left"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">Note: User can not edit already associated vendors with products.</td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top">
                                <table>
                                    <tr>
                                        <td>Available Product</td>
                                    </tr>
                                    <tr>
                                        <td>                                            
                                            <div style="width: 380px; border-style: solid; overflow: auto; border-width: 1px; height: 190px;">
                                                <asp:CheckBoxList Width="360px" ID="chklstProducts" runat="server">
                                                </asp:CheckBoxList>
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>

                </fieldset>
            </td>
            <td align="left" valign="top" width="15%"></td>
        </tr>
        <tr>
            <td>&nbsp;
                <asp:HiddenField ID="hdnId" runat="server" Value="-1" />
                <asp:HiddenField ID="hdnDistricId" runat="server" Value="0" />
                <asp:HiddenField ID="hdnULBMappingId" runat="server" Value="0" />
                <asp:HiddenField ID="hdnVendorType" runat="server" Value="0" />
                <asp:HiddenField ID="hdnZoneMappingId" runat="server" Value="0" />
            </td>
        </tr>
        
    </table>
</asp:Content>
