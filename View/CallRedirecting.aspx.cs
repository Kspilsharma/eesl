﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRM.Models;

namespace CRM.View
{
    public partial class CallRedirecting : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string ariaCallerId = string.Empty;
            string ariaUniqueId = string.Empty;
            string ariaAgentId = string.Empty;
            string ariaAgentPhone = string.Empty;
            string ariaVoiceFile = string.Empty;
            string ariaCallParam = string.Empty;

            if (!IsPostBack)
            {

                ariaCallerId = Convert.ToString(Request.QueryString["cid"]);
                ariaUniqueId = Convert.ToString(Request.QueryString["uid"]);
                ariaAgentId = Convert.ToString(Request.QueryString["aid"]);
                ariaAgentPhone = Convert.ToString(Request.QueryString["ap"]);
                ariaVoiceFile = Convert.ToString(Request.QueryString["vfn"]);
                ariaCallParam = Convert.ToString(Request.QueryString["cf"]);
                

                if (!string.IsNullOrEmpty(ariaCallerId))
                {
                    UserMaster tUserMaster = new UserMaster();
                    tUserMaster.UserName = ariaAgentId;
                    string sAgentType = tUserMaster.getAgentType();
                    if (sAgentType.ToUpper() == "InBound".ToUpper())
                    {
                        Response.Redirect("~/View/frmCallEntry.aspx?cid=" + ariaCallerId + "&uid=" + ariaUniqueId + "&aid=" + ariaAgentId + "&ap=" + ariaAgentPhone + "&vfn=" + ariaVoiceFile + "&cf=" + ariaCallParam, false);
                    }
                    else if (sAgentType.ToUpper() == "OutBound".ToUpper())
                    {
                        Response.Redirect("~/View/frmCallEntry.aspx?cid=" + ariaCallerId + "&uid=" + ariaUniqueId + "&aid=" + ariaAgentId + "&ap=" + ariaAgentPhone + "&vfn=" + ariaVoiceFile + "&cf=" + ariaCallParam, false);
                    }
                }
            }
        }
    }
}