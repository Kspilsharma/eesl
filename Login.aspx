﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="CRM.Login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>EESL Login</title>
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server" autocomplete="off">
<div class="loginpage">
    <div class="header">
        <div class="title">
            <h1>
                EESL
            </h1>
        </div>
        <div class="loginDisplay">
        </div>
        <div class="clear hideSkiplink">
        </div>
    </div>
    <div style="height: 400px">
        <center>
            <h2>
                Log in.
            </h2>
            <p>
                
                Please enter your username and password.
            </p>
            <table border="0" cellpadding="0" cellspacing="1" width="430px">
                <tr>
                    <td align="left">
                        <span class="failureNotification">
                            <asp:Literal ID="FailureText" runat="server"></asp:Literal>
                        </span>
                    </td>
                </tr>
                <tr>
                    <td style="height: 300px; width: 100%">
                        <fieldset class="login">
                            <legend>Account Information</legend>
                            <p>
                                <asp:Label ID="UserNameLabel" runat="server" AssociatedControlID="UserName">Username:</asp:Label>
                                <asp:TextBox ID="UserName" runat="server" CssClass="textEntry" 
                                    Style="text-transform: none" MaxLength="20"></asp:TextBox>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:RequiredFieldValidator ID="UserNameRequired" runat="server" ControlToValidate="UserName"
                                    SetFocusOnError="true" CssClass="failureNotification" ErrorMessage="The user name field is required."
                                    ToolTip="The user name field is required." ValidationGroup="LoginUserValidationGroup" />
                            </p>
                            <p>
                                <asp:Label ID="PasswordLabel" runat="server" AssociatedControlID="Password">Password:</asp:Label>
                                <asp:TextBox ID="Password" runat="server" CssClass="passwordEntry" 
                                    TextMode="Password" Style="text-transform: none" MaxLength="12"></asp:TextBox>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:RequiredFieldValidator ID="PasswordRequired" runat="server" ControlToValidate="Password"
                                    SetFocusOnError="true" CssClass="failureNotification" ErrorMessage="The password field is required."
                                    ToolTip="The password field is required." ValidationGroup="LoginUserValidationGroup" />
                            </p>
                            <p>
                                <asp:CheckBox ID="RememberMe" runat="server" />
                                <asp:Label ID="RememberMeLabel" runat="server" AssociatedControlID="RememberMe" CssClass="inline">Keep me logged in</asp:Label>
                            </p>
                        </fieldset>
                        <p class="submitButton">
                            <asp:Button ID="LoginButton" runat="server" CommandName="Login" Text="Log In" 
                                ValidationGroup="LoginUserValidationGroup" onclick="LoginButton_Click" />
                        </p>
                    </td>
                </tr>
            </table>
        </center>
    </div>
    <div class="clear">
    </div>
    <div class="footer">
        <div class="loginDisplay">
            <p>
                &copy;
                <%: DateTime.Now.Year %>
                - EESL</p>
        </div>
    </div>
</div>
    </form>
</body>
</html>
