﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace CRM
{
    /// <summary>
    /// Summary description for CRMCallRequestHandler
    /// </summary>
    public class CRMCallRequestHandler : IHttpHandler
    {
        string ariaCallerId = string.Empty;
        string ariaUniqueId = string.Empty;
        string ariaAgentId = string.Empty;
        string ariaAgentPhone = string.Empty;
        string ariaVoiceFile = string.Empty;

        public void ProcessRequest(HttpContext context)
        {
            string jsonResponse = string.Empty;
            context.Response.ContentType = "application/json";
            ariaCallerId = context.Request.Params["cid"];
            ariaUniqueId = context.Request.Params["uid"];
            ariaAgentId = context.Request.Params["aid"];
            ariaAgentPhone = context.Request.Params["ap"];
            ariaVoiceFile = context.Request.Params["vfn"];

            try
            {
                if (!string.IsNullOrEmpty(ariaCallerId) && !string.IsNullOrEmpty(ariaUniqueId) && !string.IsNullOrEmpty(ariaAgentId) && !string.IsNullOrEmpty(ariaAgentPhone) && !string.IsNullOrEmpty(ariaVoiceFile))
                {
                    string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
                    using (SqlConnection conn = new SqlConnection(constr))
                    {
                        using (SqlCommand cmd = new SqlCommand("INSERT INTO CallRequestDetails (AriaCallerId, AriaUniqueId, AriaAgentId, AriaAgentPhone, AriaVoiceFile) VALUES (@AriaCallerId, @AriaUniqueId, @AriaAgentId, @AriaAgentPhone, @AriaVoiceFile)"))
                        {
                            cmd.Parameters.AddWithValue("@AriaCallerId", ariaCallerId);
                            cmd.Parameters.AddWithValue("@AriaUniqueId", ariaUniqueId);
                            cmd.Parameters.AddWithValue("@AriaAgentId", ariaAgentId);
                            cmd.Parameters.AddWithValue("@AriaAgentPhone", ariaAgentPhone);
                            cmd.Parameters.AddWithValue("@AriaVoiceFile", ariaVoiceFile);
                            cmd.Connection = conn;
                            conn.Open();
                            cmd.ExecuteNonQuery();
                            conn.Close();
                            jsonResponse = string.Format("{0}({1});", true, "Success");
                        }
                    }
                }
                else
                {
                    jsonResponse = string.Format("{0}({1});", false, "Insufficient Parameter supplied..");
                }
            }
            catch (Exception ex)
            {
                jsonResponse = string.Format("{0}({1});", false, ex.Message);
            }

            context.Response.ContentType = "text/json";
            context.Response.Write(jsonResponse);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}