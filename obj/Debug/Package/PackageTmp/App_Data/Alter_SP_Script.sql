/****** Object:  StoredProcedure [dbo].[USP_Call_Insert]    Script Date: 05-12-2017 12:45:10 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[USP_Call_Insert] 
  --Basic Info 
  @LanguageId               INT = 0, 
  @OtherLanguage            NVARCHAR(100) = NULL, 
  @CallerNumber             NVARCHAR(100) = NULL, 
  @CallerName               NVARCHAR(100) = NULL, 
  @Address                  NVARCHAR(MAX) = NULL, 
  @Landmark                 NVARCHAR(100) = NULL, 
  @StateId                  INT = 0, 
  @DistrictId               INT = 0, 
  @OtherDistrict            NVARCHAR(100) = NULL, 

  -- Call Info (Common Call Info) 
  @UID                      NVARCHAR(200) = NULL, 
  @CurrentStatusId          INT = 0, 
  @SourceId                 INT = 0, 
  @CallCategoryId           INT = 0, 
  @SchemeId                 INT = 0, 

  -- Call Info (Street Light Scheme) 
  @SL_ULBId                 INT = 0, 
  @SL_ZoneId                INT = 0, 
  @SL_OtherZone             NVARCHAR(100) = NULL, 
  @SL_WardNo                NVARCHAR(100) = NULL, 
  @SL_PoleNo                NVARCHAR(100) = NULL, 
  @SL_TicketTypeId          INT = 0, 
  @SL_OtherTicketType       NVARCHAR(100) = NULL, 
  @SL_AssignedToId          INT = 0, 
  @SL_OtherAssignedTo       NVARCHAR(100) = NULL, 
  @SL_ComplaintFromId       INT = 0, 
  @SL_OtherComplaintFrom    NVARCHAR(100) = NULL, 
  @SL_AreaType              NVARCHAR(100) = NULL, 
  @SL_LightTypeId           INT = 0, 

  -- Call Info (Ujala Scheme) 
  @UJ_DiscomId              INT = 0, 
  @UJ_Email                 NVARCHAR(100) = NULL, 
  @UJ_Town                  NVARCHAR(100) = NULL, 
  @UJ_ConsumerNo            NVARCHAR(100) = NULL, 
  @UJ_Products              NVARCHAR(100) = NULL, 

  -- Call Info (AGDSM Scheme) 
  @AGDSM_Town               NVARCHAR(100) = NULL, 
  @AGDSM_DistributionCenter NVARCHAR(100) = NULL, 
  @AGDSM_ConsumerNo         NVARCHAR(100) = NULL, 
  @AGDSM_USCORServiceConnNo NVARCHAR(100) = NULL, 
  @AGDSM_PanelBoardMobNo    NVARCHAR(100) = NULL, 
  @AGDSM_HPkWRating         NVARCHAR(100) = NULL, 
  @AGDSM_ComplaintReason    NVARCHAR(100) = NULL, 
  @AGDSM_AssignedToId       INT = 0, 

  -- Call Info (Building Scheme) 
  @BL_BuildingId            INT = 0, 
  @BL_FlatNo                NVARCHAR(100) = NULL, 
  @BL_ProductTypes          NVARCHAR(100) = NULL, 
  @BL_ProductTypesQty       NVARCHAR(100) = NULL, 
  @BL_LEDProductTypes       NVARCHAR(100) = NULL, 
  @BL_ACProductTypes        NVARCHAR(100) = NULL, 
  @BL_ReferenceNo           NVARCHAR(100) = NULL, 

  --Call History       
  @EscalatedToId            INT = 0, 
  @Remark                   NVARCHAR(MAX) = NULL, 

  --Building Vendors 
  @BL_AssignedToIds         NVARCHAR(200) = '', 

  --Common Parameter 
  @Agent                    VARCHAR(50), 
  @SPOut                    NVARCHAR(200) OUTPUT, 
  @DMLType                  NVARCHAR(200) OUTPUT 
AS 
  BEGIN 
      --SET NOCOUNT ON; 
      SET @SPOut = 'ERROR' 
      SET @DMLType = 'INSERT' 

      BEGIN TRY 
          DECLARE @UserId INT = 0 

          SET @UserId = (SELECT UserId 
                         FROM   UserMaster WITH (NOLOCK) 
                         WHERE  UserName = @Agent) 

          DECLARE @CallId AS INT 
          DECLARE @splitTable TABLE 
            ( 
               [Id]         INT IDENTITY (1, 1), 
               AssignedToId INT 
            ) 
          DECLARE @counter INT = 1, 
                  @max     INT = 0 
          DECLARE @AssignedToId INT 

          IF EXISTS(SELECT 1 
                    FROM   CallDetailMaster WITH (NOLOCK) 
                    WHERE  [UID] = @UID) 
            BEGIN 
                BEGIN TRAN 

                SET @DMLType = 'UPDATE' 

                UPDATE [CallBasicInfoMaster] --WITH (UPDLOCK) 
                SET    [LanguageId] = @LanguageId, 
                       [OtherLanguage] = @OtherLanguage, 
                       [CallerNumber] = @CallerNumber, 
                       [CallerName] = @CallerName, 
                       [Address] = @Address, 
                       [Landmark] = @Landmark, 
                       [StateId] = @StateId, 
                       [DistrictId] = @DistrictId, 
                       [OtherDistrict] = @OtherDistrict, 
                       [DateUpdate] = Getdate(), 
                       [UpdateId] = @UserId 
                WHERE  [UID] = @UID 

                UPDATE [CallDetailMaster] --WITH (UPDLOCK) 
                SET    [CurrentStatusId] = @CurrentStatusId, 
                       [SourceId] = @SourceId, 
                       [SchemeId] = @SchemeId, 
                       [CallCategoryId] = @CallcategoryId, 
                       [SL_ULBId] = @SL_ULBId, 
                       [SL_ZoneId] = @SL_ZoneId, 
                       [SL_OtherZone] = @SL_OtherZone, 
                       [SL_WardNo] = @SL_WardNo, 
                       [SL_PoleNo] = @SL_PoleNo, 
                       [SL_TicketTypeId] = @SL_TicketTypeId, 
                       [SL_OtherTicketType] = @SL_OtherTicketType, 
                       [SL_AssignedToId] = @SL_AssignedToId, 
                       [SL_OtherAssignedTo] = @SL_OtherAssignedTo, 
                       [SL_ComplaintFromId] = @SL_ComplaintFromId, 
                       [SL_OtherComplaintFrom] = @SL_OtherComplaintFrom, 
                       [SL_AreaType] = @SL_AreaType, 
                       [SL_LightTypeId] = @SL_LightTypeId, 
                       [UJ_DiscomId] = @UJ_DiscomId, 
                       [UJ_Email] = @UJ_Email, 
                       [UJ_Town] = @UJ_Town, 
                       [UJ_ConsumerNo] = @UJ_ConsumerNo, 
                       [UJ_Products] = @UJ_Products, 
                       [AGDSM_Town] = @AGDSM_Town, 
                       [AGDSM_DistributionCenter] = @AGDSM_DistributionCenter, 
                       [AGDSM_ConsumerNo] = @AGDSM_ConsumerNo, 
                       [AGDSM_USCORServiceConnNo] = @AGDSM_USCORServiceConnNo, 
                       [AGDSM_PanelBoardMobNo] = @AGDSM_PanelBoardMobNo, 
                       [AGDSM_HPkWRating] = @AGDSM_HPkWRating, 
                       [AGDSM_ComplaintReason] = @AGDSM_ComplaintReason, 
                       [AGDSM_AssignedToId] = @AGDSM_AssignedToId, 
                       [BL_BuildingId] = @BL_BuildingId, 
                       [BL_FlatNo] = @BL_FlatNo, 
                       [BL_ProductTypes] = @BL_ProductTypes, 
                       [BL_ProductTypesQty] = @BL_ProductTypesQty, 
                       [BL_LEDProductTypes] = @BL_LEDProductTypes, 
                       [BL_ACProductTypes] = @BL_ACProductTypes, 
                       [BL_ReferenceNo] = @BL_ReferenceNo, 
                       [DateUpdate] = Getdate(), 
                       [UpdateId] = @UserId 
                WHERE  [UID] = @UID 

                SET @CallId = (SELECT CallId 
                               FROM   CallDetailMaster WITH (NOLOCK) 
                               WHERE  [UID] = @UID) 

                INSERT INTO [CallDetailHistory] 
                            ([CallId], 
                             [StatusOfCallId], 
                             [EscalatedToId], 
                             [Remark], 
                             [DateInsert], 
                             [LoginId]) 
                VALUES      (@CallId, 
                             @CurrentStatusId, 
                             @EscalatedToId, 
                             @Remark, 
                             Getdate(), 
                             @UserId) 

                IF EXISTS (SELECT 1 
                           FROM   Scheme 
                           WHERE  SchemeId = @SchemeId 
                                  AND Scheme = 'Building') 
                  BEGIN 
                      INSERT INTO @splitTable 
                      SELECT * 
                      FROM   [dbo].[StringSplitXML](@BL_AssignedToIds, ',') 

                      SELECT @max = Count(Id) 
                      FROM   @splitTable 

                      -- Loop  
                      WHILE @counter <= @max 
                        BEGIN 
                            SET @AssignedToId = (SELECT AssignedToId 
                                                 FROM   @splitTable 
                                                 WHERE  Id = @counter); 

                            IF NOT EXISTS (SELECT 1 
                                           FROM   [CallAssignedToHistory] 
                                           WHERE  CallId = @CallId 
                                                  AND [AssignedToId] = 
                                                      @AssignedToId) 
                              BEGIN 
                                  INSERT INTO [CallAssignedToHistory] 
                                              ([CallId], 
                                               [AssignedToId], 
                                               [AssignedToRemark], 
                                               [DateInsert], 
                                               [LoginId]) 
                                  VALUES      (@CallId, 
                                               @AssignedToId, 
                                               '', 
                                               Getdate(), 
                                               @UserId) 
                              END 

                            SET @counter = @counter + 1 
                        END 
                  END 
                ELSE 
                  BEGIN 
                      IF EXISTS (SELECT 1 
                                 FROM   Scheme 
                                 WHERE  SchemeId = @SchemeId 
                                        AND Upper(Scheme) = 'AGDSM') 
                        BEGIN 
                            SET @AssignedToId = @AGDSM_AssignedToId; 
                        END 
                      ELSE 
                        BEGIN 
                            SET @AssignedToId = @SL_AssignedToId; 
                        END

                      INSERT INTO [CallAssignedToHistory] 
                                  ([CallId], 
                                   [AssignedToId], 
                                   [AssignedToRemark], 
                                   [DateInsert], 
                                   [LoginId]) 
                      VALUES      (@CallId, 
                                   @AssignedToId, 
                                   '', 
                                   Getdate(), 
                                   @UserId) 
                  END 

                IF ( @@ERROR <> 0 ) 
                  GOTO ERR_HANDLER 

                COMMIT TRAN 
            END 
          ELSE 
            BEGIN 
                BEGIN TRAN 

                SET @DMLType = 'INSERT' 

                IF EXISTS(SELECT cdm.CallID 
                          FROM   CallDetailMaster CDM 
                                 JOIN CallBasicInfoMaster CBIM 
                                   ON CBIM.CallerNumber = cdm.CallerNumber 
                          WHERE  cdm.CallerNumber = @CallerNumber 
                                 AND Upper(Ltrim(Rtrim(callername))) = Upper( 
                                     Ltrim(Rtrim(@CallerName))) 
                                 AND Upper(Ltrim(Rtrim(CBIM.address))) = Upper( 
                                     Ltrim(Rtrim(@Address 
                                           ))) 
                                 AND ( ( cdm.CurrentStatusId = 1 ) 
                                        OR ( cdm.CurrentStatusId = 2 ) 
                                        OR ( cdm.CurrentStatusId = 3 ) 
                                        OR ( cdm.CurrentStatusId = 4 ) 
                                        OR ( cdm.CurrentStatusId = 6 ) )) 
                  BEGIN 
                      DECLARE @RaiseErrorComment1 VARCHAR(500) 

                      SET @RaiseErrorComment1= 
  'Not a unique customer with address INSERT Statment from dialer. CallerNo.:' 
  + @CallerNumber--+' Userid:'+@UserId 
  RAISERROR(@RaiseErrorComment1,20,-1) WITH LOG 
  END 

  IF NOT EXISTS(SELECT CallId 
                FROM   CallDetailMaster 
                WHERE  CallerNumber = @CallerNumber 
                       AND Dateadd(MI, Datediff(MI, 0, DateInsert), 0) = 
                           Dateadd(MI, Datediff(MI, 0, 
                                       Getdate()), 0)) 
    BEGIN 
        DECLARE @UniqueID AS NVARCHAR(50) 
        DECLARE @count AS INT 
        DECLARE @SchemeAbbreviation AS NVARCHAR(50) 
        DECLARE @StateAbbreviation AS NVARCHAR(50) 
        DECLARE @LikeStr AS NVARCHAR(50) 
        -- Code Added By Kalu Singh on 01-11-2017 
        SET @SchemeAbbreviation = ISNULL((SELECT Scheme.SchemeAbbreviation 
                                          FROM   Scheme 
                                          WHERE  Scheme.SchemeId = @SchemeId), 
                                  'XX') 
        SET @StateAbbreviation = ISNULL((SELECT State.StateAbbreviation 
                                         FROM   State 
                                         WHERE  State.StateId = @StateId), 
                                 'XX') 
        -- Added by KaluSingh on 21-10-2017 05:28 PM 
        --Code Start 
        --To Get Max Id of Scheme and State Id 
        SET @LikeStr = @SchemeAbbreviation + @StateAbbreviation 
        --Set @count = ISNULL( 
        --(Select Max(ISNULL(CONVERT(BIGINT,SUBSTRING(CDM.UID, 5, 8)),0))  
        --From CallDetailMaster CDM  
        --INNER JOIN CallBasicInfoMaster CBIM ON CDM.UID = CBIM.UID 
        --Where CBIM.StateId = @StateId AND CDM.SchemeId = @SchemeId 
        --GROUP BY CBIM.StateId,CDM.SchemeId 
        --),0) 
        SET @count = ISNULL((SELECT Max(ISNULL(CONVERT(BIGINT, 
                                               Substring(UID, 5, 8 
                                               )) 
                                        , 0)) 
                             FROM   CallDetailMaster 
                             WHERE  UID LIKE '' + @LikeStr + '%' 
                                    AND SchemeId = @SchemeId), 0) 
        --Code End 
        SET @UniqueID = @SchemeAbbreviation + @StateAbbreviation 
                        + DBO.fnNumPadLeft(@count + 1, 8) 
        SET @UID = @UniqueID 

        DECLARE @RaiseErrorComment VARCHAR(500) 

        SET @RaiseErrorComment= 
        'Not a unique id INSERT Statment from dialer. CallerNo.:' 
        + @CallerNumber + ' UniqueId:' + @UID --+' UserId:'+@UserId 
        IF EXISTS(SELECT 1 
                  FROM   CallDetailMaster WITH (NOLOCK) 
                  WHERE  [UID] = @UID) 
          BEGIN 
              RAISERROR(@RaiseErrorComment,20,-1) WITH LOG 
          END 

        INSERT [CallDetailMaster] 
               ([CallerNumber], 
                [UID], 
                [DateOfCall], 
                [SourceId], 
                [SchemeId], 
                [CallCategoryId], 
                [SL_ULBId], 
                [SL_ZoneId], 
                [SL_OtherZone], 
                [SL_WardNo], 
                [SL_PoleNo], 
                [SL_TicketTypeId], 
                [SL_OtherTicketType], 
                [SL_AssignedToId], 
                [SL_OtherAssignedTo], 
                [SL_ComplaintFromId], 
                [SL_OtherComplaintFrom], 
                [UJ_DiscomId], 
                [UJ_Email], 
                [UJ_Town], 
                [UJ_ConsumerNo], 
                [UJ_Products], 
                [AGDSM_Town], 
                [AGDSM_DistributionCenter], 
                [AGDSM_ConsumerNo], 
                [AGDSM_USCORServiceConnNo], 
                [AGDSM_PanelBoardMobNo], 
                [AGDSM_HPkWRating], 
                [AGDSM_ComplaintReason], 
                [AGDSM_AssignedToId], 
                [BL_BuildingId], 
                [BL_FlatNo], 
                [BL_ProductTypes], 
                [BL_ProductTypesQty], 
                [BL_LEDProductTypes], 
                [BL_ACProductTypes], 
                [BL_ReferenceNo], 
                [BL_AssignedToIds], 
                [CurrentStatusId], 
                [DateInsert], 
                [LoginId], 
                [SL_AreaType], 
                [SL_LightTypeId]) 
        VALUES (@CallerNumber, 
                @UID, 
                Getdate(), 
                @SourceId, 
                @SchemeId, 
                @CallCategoryId, 
                @SL_ULBId, 
                @SL_ZoneId, 
                @SL_OtherZone, 
                @SL_WardNo, 
                @SL_PoleNo, 
                @SL_TicketTypeId, 
                @SL_OtherTicketType, 
                @SL_AssignedToId, 
                @SL_OtherAssignedTo, 
                @SL_ComplaintFromId, 
                @SL_OtherComplaintFrom, 
                @UJ_DiscomId, 
                @UJ_Email, 
                @UJ_Town, 
                @UJ_ConsumerNo, 
                @UJ_Products, 
                @AGDSM_Town, 
                @AGDSM_DistributionCenter, 
                @AGDSM_ConsumerNo, 
                @AGDSM_USCORServiceConnNo, 
                @AGDSM_PanelBoardMobNo, 
                @AGDSM_HPkWRating, 
                @AGDSM_ComplaintReason, 
                @AGDSM_AssignedToId, 
                @BL_BuildingId, 
                @BL_FlatNo, 
                @BL_ProductTypes, 
                @BL_ProductTypesQty, 
                @BL_LEDProductTypes, 
                @BL_ACProductTypes, 
                @BL_ReferenceNo, 
                @BL_AssignedToIds, 
                @CurrentStatusId, 
                Getdate(), 
                @UserId, 
                @SL_AreaType, 
                @SL_LightTypeId ) 

        SET @CallId =Scope_identity() 

        IF ( @@ERROR <> 0 ) 
          GOTO ERR_HANDLER 

        INSERT [CallBasicInfoMaster] 
               ([LanguageId], 
                [OtherLanguage], 
                [CallerNumber], 
                [CallerName], 
                [Address], 
                [Landmark], 
                [StateId], 
                [DistrictId], 
                [OtherDistrict], 
                [DateInsert], 
                [LoginId], 
                [uid]) 
        VALUES (@LanguageId, 
                @OtherLanguage, 
                @CallerNumber, 
                @CallerName, 
                @Address, 
                @Landmark, 
                @StateId, 
                @DistrictId, 
                @OtherDistrict, 
                Getdate(), 
                @UserId, 
                @UID ) 

        IF ( @@ERROR <> 0 ) 
          GOTO ERR_HANDLER 

        --Set @CallId = (SELECT CallId FROM CallDetailMaster WHERE [UID] = @UID) 
        INSERT INTO [CallDetailHistory] 
                    ([CallId], 
                     [StatusOfCallId], 
                     [EscalatedToId], 
                     [Remark], 
                     [DateInsert], 
                     [LoginId]) 
        VALUES      (@CallId, 
                     @CurrentStatusId, 
                     @EscalatedToId, 
                     @Remark, 
                     Getdate(), 
                     @UserId ) 

        IF ( @@ERROR <> 0 ) 
          GOTO ERR_HANDLER 

        IF EXISTS (SELECT 1 
                   FROM   Scheme 
                   WHERE  SchemeId = @SchemeId 
                          AND Scheme = 'Building') 
          BEGIN 
              INSERT INTO @splitTable 
              SELECT * 
              FROM   [dbo].[StringSplitXML](@BL_AssignedToIds, ',') 

              SELECT @max = Count(Id) 
              FROM   @splitTable 

              -- Loop  
              WHILE @counter <= @max 
                BEGIN 
                    SET @AssignedToId = (SELECT AssignedToId 
                                         FROM   @splitTable 
                                         WHERE  Id = @counter); 

                    IF NOT EXISTS (SELECT 1 
                                   FROM   [CallAssignedToHistory] 
                                   WHERE  CallId = @CallId 
                                          AND [AssignedToId] = @AssignedToId) 
                      BEGIN 
                          INSERT INTO [CallAssignedToHistory] 
                                      ([CallId], 
                                       [AssignedToId], 
                                       [AssignedToRemark], 
                                       [DateInsert], 
                                       [LoginId]) 
                          VALUES      (@CallId, 
                                       @AssignedToId, 
                                       '', 
                                       Getdate(), 
                                       @UserId) 
                      END 

                    SET @counter = @counter + 1 
                END 
          END 
        ELSE 
            BEGIN 
                IF EXISTS (SELECT 1 
                           FROM   Scheme 
                           WHERE  SchemeId = @SchemeId 
                                  AND Upper(Scheme) = 'AGDSM') 
                  BEGIN 
                      SET @AssignedToId = @AGDSM_AssignedToId; 
                  END 
                ELSE 
                  BEGIN 
                      SET @AssignedToId = @SL_AssignedToId; 
                  END 

                INSERT INTO [CallAssignedToHistory] 
                            ([CallId], 
                             [AssignedToId], 
                             [AssignedToRemark], 
                             [DateInsert], 
                             [LoginId]) 
                VALUES      (@CallId, 
                             @AssignedToId, 
                             '', 
                             Getdate(), 
                             @UserId) 
            END 

        IF ( @@ERROR <> 0 ) 
          GOTO ERR_HANDLER 
    END 

  COMMIT TRAN 
  END 

  SET @SPOut = @UID 

  RETURN 0 

  ERR_HANDLER: 

  PRINT 'Unexpected error occurred!' 

  SET @SPOut = 
  'Unexpected error occurred! Transaction is blocked in dialer page.' 

  ROLLBACK TRAN 

  RETURN 1 
  END TRY 

      BEGIN CATCH 
          SET @DMLType = 'ERROR' 
          SET @SPOut = (SELECT ' Error Message: ' + ERROR_MESSAGE() 
                               + ' Error Line No :' 
                               + CONVERT(VARCHAR, ERROR_LINE())) 

          INSERT INTO DBO.ErrorLogger_Database 
                      ([ErrorLine], 
                       [ErrorMessage], 
                       [ErrorNo], 
                       [ErrorOccuredDate], 
                       [ErrorProcedure], 
                       [ErrorSeverity], 
                       [ErrorState]) 
          VALUES      (ERROR_LINE(), 
                       ERROR_MESSAGE(), 
                       ERROR_NUMBER(), 
                       Getdate(), 
                       ERROR_PROCEDURE(), 
                       ERROR_SEVERITY(), 
                       ERROR_STATE()) 
      END CATCH 
  END 
GO

/****** Object:  StoredProcedure [dbo].[USP_CallDetails_MobileNo]    Script Date: 05-12-2017 12:45:10 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[USP_CallDetails_MobileNo] @CallerNumber AS NVARCHAR(100)= 
NULL 
AS 
  BEGIN 
      DECLARE @statusId INT 

      SET @statusId = ISNULL((SELECT StatusOfCallId 
                              FROM   [StatusOfCall] WITH (NOLOCK) 
                              WHERE  StatusOfCall = 'Closed'), 0) 

      DECLARE @UID VARCHAR(100) 

      SET @UID = (SELECT TOP 1 [UID] 
                  FROM   [CallDetailMaster] WITH (NOLOCK) 
                  WHERE  CallerNumber = @CallerNumber 
                         AND CurrentStatusId <> @statusId 
                  ORDER  BY CallId DESC) 

      SELECT TOP 1 CallBasicInfoMaster.[ID], 
                   CallBasicInfoMaster.[LanguageId], 
                   CallBasicInfoMaster.[OtherLanguage], 
                   CallBasicInfoMaster.[CallerNumber], 
                   CallBasicInfoMaster.[CallerName], 
                   CallBasicInfoMaster.[Address], 
                   CallBasicInfoMaster.[Landmark], 
                   CallBasicInfoMaster.[StateId], 
                   CallBasicInfoMaster.[DistrictId], 
                   CallBasicInfoMaster.[OtherDistrict], 
                   CallBasicInfoMaster.[DateInsert], 
                   CallBasicInfoMaster.[LoginId], 
                   [State].State, 
                   [District].District 
      FROM   [CallBasicInfoMaster] WITH (NOLOCK) 
             LEFT JOIN State 
                    ON CallBasicInfoMaster.StateId = State.StateId 
             LEFT JOIN District 
                    ON CallBasicInfoMaster.DistrictId = District.DistrictId 
      WHERE  [UID] = @UID 
      ORDER  BY ID DESC 

      SELECT TOP 1 [CallID]                             AS 'CallID', 
                   [CallDetailMaster].[CallerNumber], 
                   [CallDetailMaster].[UID], 
                   [CallDetailMaster].[DateOfCall], 
                   [CallDetailMaster].[SourceId], 
                   [CallDetailMaster].[SchemeId], 
                   [CallDetailMaster].[CallCategoryId], 
                   [CallDetailMaster].[SL_ULBId], 
                   [CallDetailMaster].[SL_ZoneId], 
                   [CallDetailMaster].[SL_OtherZone], 
                   [CallDetailMaster].[SL_WardNo], 
                   [CallDetailMaster].[SL_PoleNo], 
                   [CallDetailMaster].[SL_TicketTypeId], 
                   [CallDetailMaster].[SL_OtherTicketType], 
                   [CallDetailMaster].[SL_AssignedToId], 
                   [CallDetailMaster].[SL_OtherAssignedTo], 
                   [CallDetailMaster].[SL_ComplaintFromId], 
                   [CallDetailMaster].[SL_OtherComplaintFrom], 
                   [CallDetailMaster].SL_AreaType, 
                   [CallDetailMaster].SL_LightTypeId, 
                   [CallDetailMaster].[UJ_DiscomId], 
                   [CallDetailMaster].[UJ_Email], 
                   [CallDetailMaster].[UJ_Town], 
                   [CallDetailMaster].[UJ_ConsumerNo], 
                   [CallDetailMaster].[UJ_Products], 
                   [CallDetailMaster].[AGDSM_Town], 
                   [CallDetailMaster].[AGDSM_DistributionCenter], 
                   [CallDetailMaster].[AGDSM_ConsumerNo], 
                   [CallDetailMaster].[AGDSM_USCORServiceConnNo], 
                   [CallDetailMaster].[AGDSM_PanelBoardMobNo], 
                   [CallDetailMaster].[AGDSM_HPkWRating], 
                   [CallDetailMaster].[AGDSM_ComplaintReason], 
                   [CallDetailMaster].[AGDSM_AssignedToId], 
                   [CallDetailMaster].[BL_BuildingId], 
                   [CallDetailMaster].[BL_FlatNo], 
                   [CallDetailMaster].[BL_ProductTypes], 
                   [CallDetailMaster].[BL_ProductTypesQty], 
                   [CallDetailMaster].[BL_ReferenceNo], 
                   [CallDetailMaster].[BL_AssignedToIds], 
                   [CallDetailMaster].[CurrentStatusId], 
                   [CallDetailMaster].[DateInsert], 
                   [CallDetailMaster].[LoginId], 
                   ISNULL(ULBMapping.ULB, '')           AS ULB, 
                   ISNULL(ZoneMapping.Zone, '')         AS Zone, 
                   ISNULL(DiscomMapping.Discom, '')     AS Discom, 
                   ISNULL(BuildingMapping.Building, '') AS Building 
      FROM   [CallDetailMaster] WITH (NOLOCK) 
             LEFT JOIN ULBMapping 
                    ON CallDetailMaster.SL_ULBId = ULBMapping.ULBMappingId 
             LEFT JOIN ZoneMapping 
                    ON CallDetailMaster.SL_ZoneId = ZoneMapping.ZoneMappingId 
             LEFT JOIN DiscomMapping 
                    ON CallDetailMaster.UJ_DiscomId = 
                       DiscomMapping.DiscomMappingId 
             LEFT JOIN BuildingMapping 
                    ON CallDetailMaster.BL_BuildingId = 
                       BuildingMapping.BuildingMappingId 
      WHERE  CallerNumber = @CallerNumber 
             AND CurrentStatusId <> @statusId 
      ORDER  BY CallId DESC 

      DECLARE @callId INT 

      SET @CallId = (SELECT TOP 1 [CallID] 
                     FROM   [CallDetailMaster] WITH (NOLOCK) 
                     WHERE  CallerNumber = @CallerNumber 
                            AND CurrentStatusId <> @statusId 
                     ORDER  BY CallId DESC) 

      SELECT TOP 1 [Remark], 
                   [EscalatedToId] 
      FROM   [CallDetailHistory] 
      WHERE  CallId = @CallID 
      ORDER  BY HistoryId DESC 

      SELECT CDM.CallId, 
             CDM.UID                                   AS 'UID', 
             CONVERT(VARCHAR(10), CDM.DateOfCall, 105) AS 'DateOfCall', 
             S.Scheme                                  AS 'Scheme', 
             CC.CallCategory                           AS 'Category', 
             SOC.StatusOfCall                          AS 'LastStatus' 
      FROM   CallDetailMaster CDM WITH (NOLOCK) 
             INNER JOIN CallBasicInfoMaster CBIM WITH (NOLOCK) 
                     ON CDM.UID = CBIM.UID 
             INNER JOIN CallCategory CC WITH (NOLOCK) 
                     ON CDM.CallCategoryId = CC.CallCategoryId 
             INNER JOIN StatusOfCall SOC WITH (NOLOCK) 
                     ON CDM.CurrentStatusId = SOC.StatusOfCallId 
             INNER JOIN Scheme S WITH (NOLOCK) 
                     ON CDM.SchemeId = S.SchemeId 
      WHERE  CDM.CallerNumber = @CallerNumber 
      ORDER  BY CDM.CallId DESC 

      --Call History Assigned Details 
      SELECT [AssignedToHistoryId], 
             [CallId], 
             [AssignedToId], 
             [AssignedToRemark], 
             [VendorName], 
             CASE [IsRectify] 
               WHEN 1 THEN 'Rectify' 
               ELSE 'Pending' 
             END AS [Status] 
      FROM   [dbo].[CallAssignedToHistory] CAH 
             INNER JOIN VendorMapping VM WITH (NOLOCK) 
                     ON CAH.AssignedToId = VM.VendorMappingId 
      WHERE  CallId = @CallID 
  END 
GO

/****** Object:  StoredProcedure [dbo].[USP_CallDetails_UID]    Script Date: 05-12-2017 12:45:10 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[USP_CallDetails_UID] @UID AS NVARCHAR(100)=NULL 
AS 
  BEGIN 
      SELECT TOP 1 CallDetailMaster.CallID, 
                   CallBasicInfoMaster.LanguageId, 
                   CallBasicInfoMaster.OtherLanguage, 
                   CallDetailMaster.SchemeId, 
                   CallBasicInfoMaster.CallerName, 
                   CallBasicInfoMaster.CallerNumber, 
                   CallDetailMaster.CallCategoryId, 
                   CallBasicInfoMaster.Address, 
                   CallBasicInfoMaster.Landmark, 
                   CallBasicInfoMaster.StateId, 
                   CallBasicInfoMaster.DistrictId, 
                   CallBasicInfoMaster.OtherDistrict, 
                   CallDetailMaster.UID, 
                   CallDetailMaster.SourceId, 
                   CallDetailMaster.SL_ULBId, 
                   CallDetailMaster.SL_ZoneId, 
                   CallDetailMaster.SL_OtherZone, 
                   CallDetailMaster.SL_WardNo, 
                   CallDetailMaster.SL_PoleNo, 
                   CallDetailMaster.SL_TicketTypeId, 
                   CallDetailMaster.SL_OtherTicketType, 
                   CallDetailMaster.SL_AssignedToId, 
                   CallDetailMaster.SL_OtherAssignedTo, 
                   CallDetailMaster.SL_ComplaintFromId, 
                   CallDetailMaster.SL_OtherComplaintFrom, 
                   CallDetailMaster.SL_AreaType, 
                   CallDetailMaster.SL_LightTypeId, 
                   CallDetailMaster.UJ_DiscomId, 
                   CallDetailMaster.UJ_Email, 
                   CallDetailMaster.UJ_Town, 
                   CallDetailMaster.UJ_ConsumerNo, 
                   CallDetailMaster.UJ_Products, 
                   CallDetailMaster.AGDSM_Town, 
                   CallDetailMaster.AGDSM_DistributionCenter, 
                   CallDetailMaster.AGDSM_ConsumerNo, 
                   CallDetailMaster.[AGDSM_USCORServiceConnNo], 
                   CallDetailMaster.[AGDSM_PanelBoardMobNo], 
                   CallDetailMaster.[AGDSM_HPkWRating], 
                   CallDetailMaster.[AGDSM_ComplaintReason], 
                   CallDetailMaster.[AGDSM_AssignedToId], 
                   CallDetailMaster.BL_BuildingId, 
                   CallDetailMaster.BL_FlatNo, 
                   CallDetailMaster.BL_ProductTypes, 
                   CallDetailMaster.BL_ProductTypesQty, 
                   CallDetailMaster.BL_ReferenceNo, 
                   CallDetailMaster.[BL_AssignedToIds], 
                   CallDetailMaster.CurrentStatusId, 
                   UserMaster.UserName, 
                   State.State, 
                   District.District, 
                   ISNULL(ULBMapping.ULB, '')           AS ULB, 
                   ISNULL(ZoneMapping.Zone, '')         AS Zone, 
                   ISNULL(DiscomMapping.Discom, '')     AS Discom, 
                   ISNULL(BuildingMapping.Building, '') AS Building 
      FROM   CallDetailMaster WITH (NOLOCK) 
             INNER JOIN CallBasicInfoMaster WITH (NOLOCK) 
                     ON CallDetailMaster.UID = CallBasicInfoMaster.UID 
             INNER JOIN UserMaster WITH (NOLOCK) 
                     ON CallDetailMaster.LoginId = UserMaster.UserId 
             INNER JOIN State WITH (NOLOCK) 
                     ON CallBasicInfoMaster.StateId = State.StateId 
             INNER JOIN District WITH (NOLOCK) 
                     ON CallBasicInfoMaster.DistrictId = District.DistrictId 
             LEFT JOIN ULBMapping 
                    ON CallDetailMaster.SL_ULBId = ULBMapping.ULBMappingId 
             LEFT JOIN ZoneMapping 
                    ON CallDetailMaster.SL_ZoneId = ZoneMapping.ZoneMappingId 
             LEFT JOIN DiscomMapping 
                    ON CallDetailMaster.UJ_DiscomId = 
                       DiscomMapping.DiscomMappingId 
             LEFT JOIN BuildingMapping 
                    ON CallDetailMaster.BL_BuildingId = 
                       BuildingMapping.BuildingMappingId 
      WHERE  CallDetailMaster.UID = @UID 
      ORDER  BY CallDetailMaster.CallId DESC 

      DECLARE @CallerNumber NVARCHAR(100) 

      SET @CallerNumber = (SELECT TOP 1 CallerNumber 
                           FROM   CallDetailMaster WITH (NOLOCK) 
                           WHERE  [UID] = @UID) 

      SELECT CDM.CallId, 
             CDM.UID                                   AS 'UID', 
             CONVERT(VARCHAR(10), CDM.DateOfCall, 105) AS 'DateOfCall', 
             S.Scheme                                  AS 'Scheme', 
             CC.CallCategory                           AS 'Category', 
             SOC.StatusOfCall                          AS 'LastStatus' 
      FROM   CallDetailMaster CDM WITH (NOLOCK) 
             INNER JOIN CallBasicInfoMaster CBIM WITH (NOLOCK) 
                     ON CDM.UID = CBIM.UID 
             INNER JOIN CallCategory CC WITH (NOLOCK) 
                     ON CDM.CallCategoryId = CC.CallCategoryId 
             INNER JOIN StatusOfCall SOC WITH (NOLOCK) 
                     ON CDM.CurrentStatusId = SOC.StatusOfCallId 
             INNER JOIN Scheme S 
                     ON CDM.SchemeId = S.SchemeId 
      WHERE  CDM.CallerNumber = @CallerNumber 
      ORDER  BY CDM.CallId DESC 

      DECLARE @callId INT 

      SET @CallId = (SELECT TOP 1 [CallID] 
                     FROM   [CallDetailMaster] WITH (NOLOCK) 
                     WHERE  CallerNumber = @CallerNumber 
                            AND UID = @UID 
                     ORDER  BY CallId DESC) 

      SELECT TOP 1 [Remark], 
                   [EscalatedToId] 
      FROM   [CallDetailHistory] WITH (NOLOCK) 
      WHERE  CallId = @CallID 
      ORDER  BY HistoryId DESC 

      --Call History Assigned Details 
      SELECT [AssignedToHistoryId], 
             [CallId], 
             [AssignedToId], 
             [AssignedToRemark], 
             [VendorName], 
             CASE [IsRectify] 
               WHEN 1 THEN 'Rectify' 
               ELSE 'Pending' 
             END AS [Status] 
      FROM   [dbo].[CallAssignedToHistory] CAH 
             INNER JOIN VendorMapping VM WITH (NOLOCK) 
                     ON CAH.AssignedToId = VM.VendorMappingId 
      WHERE  CallId = @CallID 
  END 
GO

/****** Object:  StoredProcedure [dbo].[USP_InBoundCall_Insert]    Script Date: 05-12-2017 12:45:10 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[USP_InBoundCall_Insert] 
  --Basic Info 
  @LanguageId               INT = 0, 
  @OtherLanguage            NVARCHAR(100) = NULL, 
  @CallerNumber             NVARCHAR(100) = NULL, 
  @CallerName               NVARCHAR(100) = NULL, 
  @Address                  NVARCHAR(MAX) = NULL, 
  @Landmark                 NVARCHAR(100) = NULL, 
  @StateId                  INT = 0, 
  @DistrictId               INT = 0, 
  @OtherDistrict            NVARCHAR(100) = NULL, 

  -- Call Info (Common Call Info) 
  @UID                      NVARCHAR(200) = NULL, 
  @CurrentStatusId          INT = 0, 
  @SourceId                 INT = 0, 
  @CallCategoryId           INT = 0, 
  @SchemeId                 INT = 0, 

  -- Call Info (Street Light Scheme) 
  @SL_ULBId                 INT = 0, 
  @SL_ZoneId                INT = 0, 
  @SL_OtherZone             NVARCHAR(100) = NULL, 
  @SL_WardNo                NVARCHAR(100) = NULL, 
  @SL_PoleNo                NVARCHAR(100) = NULL, 
  @SL_TicketTypeId          INT = 0, 
  @SL_OtherTicketType       NVARCHAR(100) = NULL, 
  @SL_AssignedToId          INT = 0, 
  @SL_OtherAssignedTo       NVARCHAR(100) = NULL, 
  @SL_ComplaintFromId       INT = 0, 
  @SL_OtherComplaintFrom    NVARCHAR(100) = NULL, 
  @SL_AreaType              NVARCHAR(100) = NULL, 
  @SL_LightTypeId           INT = 0, 

  -- Call Info (Ujala Scheme) 
  @UJ_DiscomId              INT = 0, 
  @UJ_Email                 NVARCHAR(100) = NULL, 
  @UJ_Town                  NVARCHAR(100) = NULL, 
  @UJ_ConsumerNo            NVARCHAR(100) = NULL, 
  @UJ_Products              NVARCHAR(100) = NULL, 

  -- Call Info (AGDSM Scheme) 
  @AGDSM_Town               NVARCHAR(100) = NULL, 
  @AGDSM_DistributionCenter NVARCHAR(100) = NULL, 
  @AGDSM_ConsumerNo         NVARCHAR(100) = NULL, 
  @AGDSM_USCORServiceConnNo NVARCHAR(100) = NULL, 
  @AGDSM_PanelBoardMobNo    NVARCHAR(100) = NULL, 
  @AGDSM_HPkWRating         NVARCHAR(100) = NULL, 
  @AGDSM_ComplaintReason    NVARCHAR(100) = NULL, 
  @AGDSM_AssignedToId       INT = 0, 

  -- Call Info (Building Scheme) 
  @BL_BuildingId            INT = 0, 
  @BL_FlatNo                NVARCHAR(100) = NULL, 
  @BL_ProductTypes          NVARCHAR(100) = NULL, 
  @BL_ProductTypesQty       NVARCHAR(100) = NULL, 
  @BL_LEDProductTypes       NVARCHAR(100) = NULL, 
  @BL_ACProductTypes        NVARCHAR(100) = NULL, 
  @BL_ReferenceNo           NVARCHAR(100) = NULL, 

  --Call History       
  @EscalatedToId            INT = 0, 
  @Remark                   NVARCHAR(MAX) = NULL, 

  --Building Vendors 
  @BL_AssignedToIds         NVARCHAR(200) = '', 

  --Common Parameter 
  @UserId                   INT, 
  @SPOut                    NVARCHAR(200) OUTPUT, 
  @DMLType                  NVARCHAR(200) OUTPUT 
AS 
  BEGIN 
      --SET NOCOUNT ON; 
      SET @SPOut = 'ERROR' 
      SET @DMLType = 'INSERT' 

      BEGIN TRY 
          DECLARE @CallId AS INT 
          DECLARE @splitTable TABLE 
            ( 
               [Id]         INT IDENTITY (1, 1), 
               AssignedToId INT 
            ) 
          DECLARE @counter INT = 1, 
                  @max     INT = 0 
          DECLARE @AssignedToId INT 

          IF EXISTS (SELECT 1 
                     FROM   CallDetailMaster WITH (NOLOCK) 
                     WHERE  [UID] = @UID) 
            BEGIN 
                BEGIN TRAN 

                SET @DMLType = 'UPDATE' 

                UPDATE [CallBasicInfoMaster] --WITH (UPDLOCK) 
                SET    [LanguageId] = @LanguageId, 
                       [OtherLanguage] = @OtherLanguage, 
                       [CallerNumber] = @CallerNumber, 
                       [CallerName] = @CallerName, 
                       [Address] = @Address, 
                       [Landmark] = @Landmark, 
                       [StateId] = @StateId, 
                       [DistrictId] = @DistrictId, 
                       [OtherDistrict] = @OtherDistrict, 
                       [DateUpdate] = Getdate(), 
                       [UpdateId] = @UserId 
                WHERE  UID = @UID 

                IF ( @@ERROR <> 0 ) 
                  GOTO ERR_HANDLER 

                UPDATE [CallDetailMaster] WITH (UPDLOCK) 
                SET    [CurrentStatusId] = @CurrentStatusId, 
                       [SourceId] = @SourceId, 
                       [SchemeId] = @SchemeId, 
                       [CallCategoryId] = @CallCategoryId, 
                       [SL_ULBId] = @SL_ULBId, 
                       [SL_ZoneId] = @SL_ZoneId, 
                       [SL_OtherZone] = @SL_OtherZone, 
                       [SL_WardNo] = @SL_WardNo, 
                       [SL_PoleNo] = @SL_PoleNo, 
                       [SL_TicketTypeId] = @SL_TicketTypeId, 
                       [SL_OtherTicketType] = @SL_OtherTicketType, 
                       [SL_AssignedToId] = @SL_AssignedToId, 
                       [SL_OtherAssignedTo] = @SL_OtherAssignedTo, 
                       [SL_ComplaintFromId] = @SL_ComplaintFromId, 
                       [SL_OtherComplaintFrom] = @SL_OtherComplaintFrom, 
                       [SL_AreaType] = @SL_AreaType, 
                       [SL_LightTypeId] = @SL_LightTypeId, 
                       [UJ_DiscomId] = @UJ_DiscomId, 
                       [UJ_Email] = @UJ_Email, 
                       [UJ_Town] = @UJ_Town, 
                       [UJ_ConsumerNo] = @UJ_ConsumerNo, 
                       [UJ_Products] = @UJ_Products, 
                       [AGDSM_Town] = @AGDSM_Town, 
                       [AGDSM_DistributionCenter] = @AGDSM_DistributionCenter, 
                       [AGDSM_ConsumerNo] = @AGDSM_ConsumerNo, 
                       [AGDSM_USCORServiceConnNo] = @AGDSM_USCORServiceConnNo, 
                       [AGDSM_PanelBoardMobNo] = @AGDSM_PanelBoardMobNo, 
                       [AGDSM_HPkWRating] = @AGDSM_HPkWRating, 
                       [AGDSM_ComplaintReason] = @AGDSM_ComplaintReason, 
                       [AGDSM_AssignedToId] = @AGDSM_AssignedToId, 
                       [BL_BuildingId] = @BL_BuildingId, 
                       [BL_FlatNo] = @BL_FlatNo, 
                       [BL_ProductTypes] = @BL_ProductTypes, 
                       [BL_ProductTypesQty] = @BL_ProductTypesQty, 
                       [BL_LEDProductTypes] = @BL_LEDProductTypes, 
                       [BL_ACProductTypes] = @BL_ACProductTypes, 
                       [BL_ReferenceNo] = @BL_ReferenceNo, 
                       [BL_AssignedToIds] = @BL_AssignedToIds, 
                       [DateUpdate] = Getdate(), 
                       [UpdateId] = @UserId 
                WHERE  [UID] = @UID 

                IF ( @@ERROR <> 0 ) 
                  GOTO ERR_HANDLER 

                SET @CallId = (SELECT CallId 
                               FROM   CallDetailMaster WITH (NOLOCK) 
                               WHERE  [UID] = @UID) 

                INSERT INTO [CallDetailHistory] 
                            ([CallId], 
                             [StatusOfCallId], 
                             [EscalatedToId], 
                             [Remark], 
                             [DateInsert], 
                             [LoginId]) 
                VALUES      (@CallId, 
                             @CurrentStatusId, 
                             @EscalatedToId, 
                             @Remark, 
                             Getdate(), 
                             @UserId) 

                IF ( @@ERROR <> 0 ) 
                  GOTO ERR_HANDLER 

                IF EXISTS (SELECT 1 
                           FROM   Scheme 
                           WHERE  SchemeId = @SchemeId 
                                  AND Scheme = 'Building') 
                  BEGIN 
                      INSERT INTO @splitTable 
                      SELECT * 
                      FROM   [dbo].[StringSplitXML](@BL_AssignedToIds, ',') 

                      SELECT @max = Count(Id) 
                      FROM   @splitTable 

                      -- Loop  
                      WHILE @counter <= @max 
                        BEGIN 
                            SET @AssignedToId = (SELECT AssignedToId 
                                                 FROM   @splitTable 
                                                 WHERE  Id = @counter); 

                            IF NOT EXISTS (SELECT 1 
                                           FROM   [CallAssignedToHistory] 
                                           WHERE  CallId = @CallId 
                                                  AND [AssignedToId] = 
                                                      @AssignedToId) 
                              BEGIN 
                                  INSERT INTO [CallAssignedToHistory] 
                                              ([CallId], 
                                               [AssignedToId], 
                                               [AssignedToRemark], 
                                               [DateInsert], 
                                               [LoginId]) 
                                  VALUES      (@CallId, 
                                               @AssignedToId, 
                                               '', 
                                               Getdate(), 
                                               @UserId) 
                              END 

                            SET @counter = @counter + 1 
                        END 
                  END 
                ELSE 
                  BEGIN 
                      IF EXISTS (SELECT 1 
                                 FROM   Scheme 
                                 WHERE  SchemeId = @SchemeId 
                                        AND Upper(Scheme) = 'AGDSM') 
                        BEGIN 
                            SET @AssignedToId = @AGDSM_AssignedToId; 
                        END 
                      ELSE 
                        BEGIN 
                            SET @AssignedToId = @SL_AssignedToId; 
                        END 

                      INSERT INTO [CallAssignedToHistory] 
                                  ([CallId], 
                                   [AssignedToId], 
                                   [AssignedToRemark], 
                                   [DateInsert], 
                                   [LoginId]) 
                      VALUES      (@CallId, 
                                   @AssignedToId, 
                                   '', 
                                   Getdate(), 
                                   @UserId) 
                  END 

                IF ( @@ERROR <> 0 ) 
                  GOTO ERR_HANDLER 

                COMMIT TRAN 
            END 
          ELSE 
            BEGIN 
                BEGIN TRAN 

                SET @DMLType = 'INSERT' 

                IF EXISTS (SELECT 1 
                           FROM   CallDetailMaster CDM WITH (NOLOCK) 
                                  JOIN CallBasicInfoMaster CBIM WITH (NOLOCK) 
                                    ON CBIM.CallerNumber = cdm.CallerNumber 
                           WHERE  cdm.CallerNumber = @CallerNumber 
                                  AND Upper(Ltrim(Rtrim(callername))) = Upper( 
                                      Ltrim(Rtrim(@CallerName))) 
                                  AND Upper(Ltrim(Rtrim(CBIM.address))) = Upper( 
                                      Ltrim(Rtrim(@Address))) 
                                  AND ( ( cdm.CurrentStatusId = 1 ) 
                                         OR ( cdm.CurrentStatusId = 2 ) 
                                         OR ( cdm.CurrentStatusId = 3 ) 
                                         OR ( cdm.CurrentStatusId = 4 ) 
                                         OR ( cdm.CurrentStatusId = 6 ) )) 
                  BEGIN 
                      DECLARE @RaiseErrorComment1 VARCHAR(500) 

                      SET @RaiseErrorComment1 = 
'Not a unique customer with address INSERT Statment from InboundPage. CallerNo.:' 
+ @CallerNumber + ' UserId:' 
+ Cast(@UserId AS VARCHAR) 

    RAISERROR (@RaiseErrorComment1,20,-1) WITH LOG 
END 

    IF NOT EXISTS (SELECT CallId 
                   FROM   CallDetailMaster 
                   WHERE  CallerNumber = @CallerNumber 
                          AND Dateadd(MI, Datediff(MI, 0, DateInsert), 0) = 
                              Dateadd(MI, Datediff(MI, 0, 
                                          Getdate()), 0)) 
      BEGIN 
          DECLARE @UniqueID AS NVARCHAR(50) 
          DECLARE @count AS INT 
          DECLARE @SchemeAbbreviation AS NVARCHAR(50) 
          DECLARE @StateAbbreviation AS NVARCHAR(50) 
          DECLARE @LikeStr AS NVARCHAR(50) 
          -- Code Added By Kalu Singh on 01-11-2017  
          SET @SchemeAbbreviation = ISNULL((SELECT Scheme.SchemeAbbreviation 
                                            FROM   Scheme 
                                            WHERE  Scheme.SchemeId = @SchemeId), 
                                    'XX') 
          SET @StateAbbreviation = ISNULL((SELECT State.StateAbbreviation 
                                           FROM   State 
                                           WHERE  State.StateId = @StateId), 
                                   'XX') 
          -- Added by KaluSingh on 21-10-2017 05:28 PM 
          --Code Start 
          --To Get Max Id of Scheme and State Id 
          SET @LikeStr = @SchemeAbbreviation + @StateAbbreviation 
          --Set @count = ISNULL( 
          --(Select Max(ISNULL(CONVERT(BIGINT,SUBSTRING(CDM.UID, 5, 8)),0))  
          --From CallDetailMaster CDM  
          --INNER JOIN CallBasicInfoMaster CBIM ON CDM.UID = CBIM.UID 
          --Where CBIM.StateId = @StateId AND CDM.SchemeId = @SchemeId 
          --GROUP BY CBIM.StateId,CDM.SchemeId 
          --),0) 
          SET @count = ISNULL((SELECT Max(ISNULL(CONVERT(BIGINT, 
                                                 Substring(UID, 5, 8 
                                                 )) 
                                          , 0)) 
                               FROM   CallDetailMaster 
                               WHERE  UID LIKE '' + @LikeStr + '%' 
                                      AND SchemeId = @SchemeId), 0) 
          --Code End 
          SET @UniqueID = @SchemeAbbreviation + @StateAbbreviation 
                          + DBO.fnNumPadLeft(@count + 1, 8) 
          SET @UID = @UniqueID 

          DECLARE @RaiseErrorComment VARCHAR(500) 

          SET @RaiseErrorComment = 
          'Not a unique id INSERT Statment from InboundPage. CallerNo.:' 
          + @CallerNumber + ' UniqueId:' 
          + Cast(@UID AS VARCHAR) + ' UserId' 
          + Cast(@UserId AS VARCHAR) 

          IF EXISTS (SELECT 1 
                     FROM   CallDetailMaster 
                     WHERE  [UID] = @UID) 
            BEGIN 
                RAISERROR (@RaiseErrorComment,20,-1) WITH LOG 
            END 

          INSERT [CallDetailMaster] 
                 ([CallerNumber], 
                  [UID], 
                  [DateOfCall], 
                  [SourceId], 
                  [SchemeId], 
                  [CallCategoryId], 
                  [SL_ULBId], 
                  [SL_ZoneId], 
                  [SL_OtherZone], 
                  [SL_WardNo], 
                  [SL_PoleNo], 
                  [SL_TicketTypeId], 
                  [SL_OtherTicketType], 
                  [SL_AssignedToId], 
                  [SL_OtherAssignedTo], 
                  [SL_ComplaintFromId], 
                  [SL_OtherComplaintFrom], 
                  [UJ_DiscomId], 
                  [UJ_Email], 
                  [UJ_Town], 
                  [UJ_ConsumerNo], 
                  [UJ_Products], 
                  [AGDSM_Town], 
                  [AGDSM_DistributionCenter], 
                  [AGDSM_ConsumerNo], 
                  [AGDSM_USCORServiceConnNo], 
                  [AGDSM_PanelBoardMobNo], 
                  [AGDSM_HPkWRating], 
                  [AGDSM_ComplaintReason], 
                  [AGDSM_AssignedToId], 
                  [BL_BuildingId], 
                  [BL_FlatNo], 
                  [BL_ProductTypes], 
                  [BL_ProductTypesQty], 
                  [BL_LEDProductTypes], 
                  [BL_ACProductTypes], 
                  [BL_ReferenceNo], 
                  [BL_AssignedToIds], 
                  [CurrentStatusId], 
                  [DateInsert], 
                  [LoginId], 
                  [SL_AreaType], 
                  [SL_LightTypeId]) 
          VALUES (@CallerNumber, 
                  @UID, 
                  Getdate(), 
                  @SourceId, 
                  @SchemeId, 
                  @CallCategoryId, 
                  @SL_ULBId, 
                  @SL_ZoneId, 
                  @SL_OtherZone, 
                  @SL_WardNo, 
                  @SL_PoleNo, 
                  @SL_TicketTypeId, 
                  @SL_OtherTicketType, 
                  @SL_AssignedToId, 
                  @SL_OtherAssignedTo, 
                  @SL_ComplaintFromId, 
                  @SL_OtherComplaintFrom, 
                  @UJ_DiscomId, 
                  @UJ_Email, 
                  @UJ_Town, 
                  @UJ_ConsumerNo, 
                  @UJ_Products, 
                  @AGDSM_Town, 
                  @AGDSM_DistributionCenter, 
                  @AGDSM_ConsumerNo, 
                  @AGDSM_USCORServiceConnNo, 
                  @AGDSM_PanelBoardMobNo, 
                  @AGDSM_HPkWRating, 
                  @AGDSM_ComplaintReason, 
                  @AGDSM_AssignedToId, 
                  @BL_BuildingId, 
                  @BL_FlatNo, 
                  @BL_ProductTypes, 
                  @BL_ProductTypesQty, 
                  @BL_LEDProductTypes, 
                  @BL_ACProductTypes, 
                  @BL_ReferenceNo, 
                  @BL_AssignedToIds, 
                  @CurrentStatusId, 
                  Getdate(), 
                  @UserId, 
                  @SL_AreaType, 
                  @SL_LightTypeId) 

          SET @CallId = Scope_identity() 

          IF ( @@ERROR <> 0 ) 
            GOTO ERR_HANDLER 

          INSERT [CallBasicInfoMaster] 
                 ([LanguageId], 
                  [OtherLanguage], 
                  [CallerNumber], 
                  [CallerName], 
                  [Address], 
                  [Landmark], 
                  [StateId], 
                  [DistrictId], 
                  [OtherDistrict], 
                  [DateInsert], 
                  [LoginId], 
                  [uid]) 
          VALUES (@LanguageId, 
                  @OtherLanguage, 
                  @CallerNumber, 
                  @CallerName, 
                  @Address, 
                  @Landmark, 
                  @StateId, 
                  @DistrictId, 
                  @OtherDistrict, 
                  Getdate(), 
                  @UserId, 
                  @UID) 

          IF ( @@ERROR <> 0 ) 
            GOTO ERR_HANDLER 

          --Set @CallId = (SELECT CallId FROM CallDetailMaster WHERE [UID] = @UID) 
          INSERT INTO [CallDetailHistory] 
                      ([CallId], 
                       [StatusOfCallId], 
                       [EscalatedToId], 
                       [Remark], 
                       [DateInsert], 
                       [LoginId]) 
          VALUES      (@CallId, 
                       @CurrentStatusId, 
                       @EscalatedToId, 
                       @Remark, 
                       Getdate(), 
                       @UserId) 

          IF ( @@ERROR <> 0 ) 
            GOTO ERR_HANDLER 

          IF EXISTS (SELECT 1 
                     FROM   Scheme 
                     WHERE  SchemeId = @SchemeId 
                            AND Scheme = 'Building') 
            BEGIN 
                INSERT INTO @splitTable 
                SELECT * 
                FROM   [dbo].[StringSplitXML](@BL_AssignedToIds, ',') 

                SELECT @max = Count(Id) 
                FROM   @splitTable 

                -- Loop  
                WHILE @counter <= @max 
                  BEGIN 
                      SET @AssignedToId = (SELECT AssignedToId 
                                           FROM   @splitTable 
                                           WHERE  Id = @counter); 

                      IF NOT EXISTS (SELECT 1 
                                     FROM   [CallAssignedToHistory] 
                                     WHERE  CallId = @CallId 
                                            AND [AssignedToId] = @AssignedToId) 
                        BEGIN 
                            INSERT INTO [CallAssignedToHistory] 
                                        ([CallId], 
                                         [AssignedToId], 
                                         [AssignedToRemark], 
                                         [DateInsert], 
                                         [LoginId]) 
                            VALUES      (@CallId, 
                                         @AssignedToId, 
                                         '', 
                                         Getdate(), 
                                         @UserId) 
                        END 

                      SET @counter = @counter + 1 
                  END 
            END 
          ELSE 
            BEGIN 
                IF EXISTS (SELECT 1 
                           FROM   Scheme 
                           WHERE  SchemeId = @SchemeId 
                                  AND Upper(Scheme) = 'AGDSM') 
                  BEGIN 
                      SET @AssignedToId = @AGDSM_AssignedToId; 
                  END 
                ELSE 
                  BEGIN 
                      SET @AssignedToId = @SL_AssignedToId; 
                  END 

                INSERT INTO [CallAssignedToHistory] 
                            ([CallId], 
                             [AssignedToId], 
                             [AssignedToRemark], 
                             [DateInsert], 
                             [LoginId]) 
                VALUES      (@CallId, 
                             @AssignedToId, 
                             '', 
                             Getdate(), 
                             @UserId) 
            END 

          IF ( @@ERROR <> 0 ) 
            GOTO ERR_HANDLER 
      END 

    COMMIT TRAN 
END 

    --SET @SPOut = 'In-Bound Call Updated with Unique ID (' +  @UID + ')'     
    SET @SPOut = @UID 

    RETURN 0 

    ERR_HANDLER: 

    PRINT 'Unexpected error occurred!' 

    SET @SPOut = 'Unexpected error occurred! Transaction is blocked.' 

    ROLLBACK TRAN 

    RETURN 1 
END TRY 

    BEGIN CATCH 
        SET @DMLType = 'ERROR' 
        SET @SPOut = (SELECT ' Error Message: ' + ERROR_MESSAGE() 
                             + ' Error Line No :' 
                             + CONVERT(VARCHAR, ERROR_LINE())) 

        INSERT INTO DBO.ErrorLogger_Database 
                    ([ErrorLine], 
                     [ErrorMessage], 
                     [ErrorNo], 
                     [ErrorOccuredDate], 
                     [ErrorProcedure], 
                     [ErrorSeverity], 
                     [ErrorState]) 
        VALUES      (ERROR_LINE(), 
                     ERROR_MESSAGE(), 
                     ERROR_NUMBER(), 
                     Getdate(), 
                     ERROR_PROCEDURE(), 
                     ERROR_SEVERITY(), 
                     ERROR_STATE()) 
    END CATCH 
END 
GO


