﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmSessionExpired.aspx.cs"
    Inherits="CRM.frmSessionExpired" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Session Expired</title>
    <link href="~/Styles/Site.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="loginpage">
            <div class="header">
                <div class="title">
                    <h1>
                        EESL
                    </h1>
                </div>
                <div class="loginDisplay">
                </div>
                <div class="clear hideSkiplink">
                </div>
            </div>
            <div style="height: 400px">
                <center>
                    <table cellpadding="0" cellspacing="1" border="0" style="width: 100%">
                        <tr>
                            <td align="center" valign="middle">
                                <h3>
                                    <b>Your session has been expired.</b>
                                </h3>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="middle" style="height: 50px; color: #0D6F93; font-weight: bold;
                                padding-top: 45px; padding-bottom: 30px">
                                <asp:Label ID="lblError" runat="server" Text="For security reasons, your session expires after some duration of inactivity. Please login again."></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="height: 50px" valign="middle">
                                &nbsp; <a id="imgbtnJdVVNL" href="~/Login.aspx" runat="server" style="border: none;
                                    outline: 0;">Click Here for Login. </a>
                            </td>
                        </tr>
                    </table>
                </center>
            </div>
            <div class="clear">
            </div>
            <div class="header">
                <div class="loginDisplay">
                    <p>
                        &copy;
                        <%: DateTime.Now.Year %>
                        - EESL</p>
                </div>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
