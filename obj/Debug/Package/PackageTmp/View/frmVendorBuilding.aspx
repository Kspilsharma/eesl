﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteNew.Master" AutoEventWireup="true" CodeBehind="frmVendorBuilding.aspx.cs" Inherits="EESL.View.frmVendorBuilding" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script src="../Scripts/jquery-ui.min.js" type="text/javascript"></script>
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="Heading" align="center" style="color: #101010">
                <asp:Label ID="lblPageHeader" runat="server" Text="Building Vendor Mapping" CssClass="bold"></asp:Label>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
        <tr>
            <td align="left" valign="top" width="15%"></td>
            <td valign="top">
                <fieldset class="fieldset">
                    <legend class="Legendheading" align="left">Building Vendor Mapping</legend>
                    <table width="100%">
                        <tr>
                            <td style="width: 80%;">
                                <table cellpadding="0" cellspacing="3px" width="100%" style="margin: 0px; padding: 0px;">
                                    <tr>
                                        <td colspan="2" align="left">
                                            <asp:Label ID="lblMsg" runat="server" CssClass="failureNotification"></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="tdlabel" valign="top">State<span style="color: red">&nbsp;*</span> &nbsp;:</td>
                                        <td valign="top" align="left" class="tddata"><asp:DropDownList ID="ddlState" runat="server" Width="180px" AutoPostBack="true" TabIndex="102" OnSelectedIndexChanged="ddlState_SelectedIndexChanged"/></td>
                                        <td><asp:RequiredFieldValidator ID="rfvState" ForeColor="Maroon" ControlToValidate="ddlState" InitialValue="--Select--" runat="server" ErrorMessage="Select State" Display="Dynamic" ValidationGroup="Save" Font-Bold="True" Font-Size="X-Small"></asp:RequiredFieldValidator></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="tdlabel" valign="top">District<span style="color: red">&nbsp;*</span> &nbsp;:</td>
                                        <td valign="top" align="left" class="tddata"><asp:DropDownList ID="ddlDistrict" runat="server" Width="180px" AutoPostBack="true" TabIndex="103" OnSelectedIndexChanged="ddlDistrict_SelectedIndexChanged"/></td>
                                        <td><asp:RequiredFieldValidator ID="rfvDistrict" ForeColor="Maroon" ControlToValidate="ddlDistrict" InitialValue="0" runat="server" ErrorMessage="Select District" Display="Dynamic" ValidationGroup="Save" Font-Bold="True" Font-Size="X-Small"></asp:RequiredFieldValidator></td>
                                    </tr>
                                    <tr>
                                        <td align="left" class="tdlabel" valign="top">Building<span style="color: red">&nbsp;*</span> &nbsp;:</td>
                                        <td valign="top" align="left" class="tddata"><asp:DropDownList ID="ddlBuilding" runat="server" Width="180px" TabIndex="104" AutoPostBack="true" OnSelectedIndexChanged="ddlBuilding_SelectedIndexChanged"  /></td>
                                        <td><asp:RequiredFieldValidator ID="rfvBuilding" ForeColor="Maroon" ControlToValidate="ddlBuilding" InitialValue="0" runat="server" ErrorMessage="Select Vendor" Display="Dynamic" ValidationGroup="Save" Font-Bold="True" Font-Size="X-Small"></asp:RequiredFieldValidator></td>
                                    </tr>
                                    <tr>
                                        <td align="left">&nbsp;</td>
                                        <td align="left">
                                            <asp:Button ID="btnSave" Text="Save" TabIndex="105" runat="server" ValidationGroup="Save" OnClick="btnSave_Click"/>
                                            <asp:Button ID="btnReset" Text="Reset" TabIndex="106" runat="server" OnClick="btnReset_Click" />
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" align="left"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">Note: User can not edit already associated vendors with building.</td>
                                    </tr>
                                </table>
                            </td>
                            <td valign="top">
                                <table>
                                    <tr>
                                        <td>Available Vendor(s)</td>
                                    </tr>
                                    <tr>
                                        <td>                                            
                                            <div style="width: 380px; border-style: solid; overflow: auto; border-width: 1px; height: 190px;">
                                                <asp:CheckBoxList Width="360px" ID="chklstVendors" runat="server">
                                                </asp:CheckBoxList>
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                            </td>
                        </tr>
                    </table>

                </fieldset>
            </td>
            <td align="left" valign="top" width="15%"></td>
        </tr>
        <tr>
            <td>&nbsp;
                <asp:HiddenField ID="hdnId" runat="server" Value="-1" />
                <asp:HiddenField ID="hdnDistricId" runat="server" Value="0" />
                <asp:HiddenField ID="hdnULBMappingId" runat="server" Value="0" />
                <asp:HiddenField ID="hdnVendorType" runat="server" Value="0" />
                <asp:HiddenField ID="hdnZoneMappingId" runat="server" Value="0" />
            </td>
        </tr>
        
    </table>
</asp:Content>
