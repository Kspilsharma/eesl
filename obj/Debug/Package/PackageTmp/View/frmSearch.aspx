﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteNew.Master" AutoEventWireup="true" CodeBehind="frmSearch.aspx.cs" Inherits="CRM.View.frmSearch" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.min.js"></script>
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(function () {
            $("#<%= txtFrom.ClientID  %>").datepicker(
           {
               dateFormat: 'dd-mm-yy',
               //minDate: new Date(),
               changeMonth: true,
               numberOfMonths: 1,
               onSelect: function (selected) {
                   var arr = selected.split("-");
                   var dmy = arr[1] + "-" + arr[0] + "-" + arr[2];
                   var dt = new Date(dmy);
                   dt.setDate(dt.getDate());
                   $("#<%= txtTo.ClientID  %>").datepicker("option", "minDate", dt);
               }

           });//.datepicker("setDate", new Date());


            $("#<%= txtTo.ClientID  %>").datepicker(
            {
                dateFormat: 'dd-mm-yy',
                //minDate: new Date(),
                changeMonth: true,
                numberOfMonths: 1,
                onSelect: function (selected) {
                    var arr = selected.split("-");
                    var dmy = arr[1] + "-" + arr[0] + "-" + arr[2];
                    var dt = new Date(dmy);
                    dt.setDate(dt.getDate());
                    $("#<%= txtFrom.ClientID  %>").datepicker("option", "maxDate", dt);
                }

            });//.datepicker("setDate", new Date());


        });

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="Heading" align="center" style="color: #101010">
                <asp:Label ID="lblPageHeader" runat="server" Text="Search" CssClass="bold"></asp:Label>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
        <tr>
            <td colspan="2" align="left" valign="top" width="100%">
                <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
                    <tr>
                        <td>
                            <fieldset class="fieldset">
                                <legend class="Legendheading">Call Search</legend>
                                <table id="tblUIDFilter" runat="server" cellpadding="0" cellspacing="3" style="width: 100%; margin: 0px; padding: 0px;">
                                    <tr>
                                        <td style="width: 10%" class="tdlabel">Caller Number</td>
                                        <td style="width: 15%" class="tdlabel">
                                            <asp:TextBox ID="txtMobileNo" runat="server" MaxLength="20"></asp:TextBox>
                                        </td>
                                        <td colspan="2" style="width: 35%" class="tdlabel"></td>
                                        <td style="width: 5%" class="tdlabel"></td>
                                        <td style="width: 10%" class="tdlabel"></td>
                                        <td style="width: 5%" class="tdlabel"></td>
                                        <td style="width: 15%" class="tdlabel"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" style="width: 100%; height: 3px;" class="tddata"></td>
                                    </tr>
                                    <tr>
                                        <td class="tddata">Date Search By</td>
                                        <td>
                                            <asp:DropDownList ID="ddlDateSearchBy" runat="server" Width="150px">
                                                <asp:ListItem Value="DateOfCall">Date Of Call</asp:ListItem>
                                                <asp:ListItem Value="LastUpdateDate">Activity date</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td colspan="2" style="width: 100%">
                                            <table>
                                                <tr>
                                                    <td>From</td>
                                                    <td>
                                                        <asp:TextBox type="text" ID="txtFrom" runat="server" Width="125px" /></td>
                                                    <td>To</td>
                                                    <td>
                                                        <asp:TextBox type="text" ID="txtTo" runat="server" Width="125px" /></td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>(UID#)</td>
                                        <td>
                                            <asp:TextBox ID="txtUID" runat="server" MaxLength="100" Width="130px"></asp:TextBox></td>
                                        <td>Name</td>
                                        <td>
                                            <asp:TextBox ID="txtNameOfCaller" runat="server" Width="120px" MaxLength="50"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" style="width: 100%; height: 3px;" class="tddata"></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%" class="tdlabel">Language</td>
                                        <td style="width: 15%" class="tdlabel">
                                            <asp:DropDownList ID="ddlLanguage" runat="server" Width="150px" />
                                        </td>
                                        <td style="width: 10%" class="tdlabel">Status </td>
                                        <td style="width: 15%" class="tdlabel">
                                            <asp:DropDownList ID="ddlStatusOfCall" runat="server" Width="150px" />

                                        </td>
                                        <td style="width: 10%" class="tdlabel">Scheme </td>
                                        <td style="width: 15%" class="tdlabel">
                                            <asp:DropDownList ID="ddlScheme" AutoPostBack="true" runat="server" Width="150px" OnSelectedIndexChanged="ddlScheme_SelectedIndexChanged" />
                                        </td>
                                        <td style="width: 10%" class="tdlabel">Category </td>
                                        <td style="width: 15%" class="tdlabel">
                                            <asp:DropDownList ID="ddlCategoryOfCall" runat="server" Width="150px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" style="width: 100%; height: 3px;" class="tddata"></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%" class="tdlabel">State</td>
                                        <td style="width: 15%" class="tdlabel">
                                            <asp:DropDownList ID="ddlState" runat="server" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddlState_SelectedIndexChanged" />
                                        </td>
                                        <td style="width: 10%" class="tdlabel">District</td>
                                        <td style="width: 15%" class="tdlabel">
                                            <asp:DropDownList ID="ddlDistrict" AutoPostBack="True" runat="server" Width="150px" OnSelectedIndexChanged="ddlDistrict_SelectedIndexChanged" />
                                        </td>
                                        <td style="width: 10%" class="tdlabel">Source </td>
                                        <td style="width: 15%" class="tdlabel">
                                            <asp:DropDownList ID="ddlSource" runat="server" Width="150px" />
                                        </td>
                                        <td style="width: 10%" class="tdlabel">Complaint Hours (&gt;=)</td>
                                        <td style="width: 15%" class="tdlabel">
                                            <asp:DropDownList ID="ddlComplaintHours" runat="server" Width="150px" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" style="width: 100%; height: 3px;" class="tddata"></td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%" class="tdlabel">Keyword</td>
                                        <td class="tdlabel" colspan="3">
                                            <asp:TextBox ID="txtKeyword" runat="server" MaxLength="100"></asp:TextBox>
                                        </td>
                                        <td style="width: 10%" class="tdlabel">&nbsp;</td>
                                        <td style="width: 15%" class="tdlabel">&nbsp;</td>
                                        <td style="width: 10%" class="tdlabel">&nbsp;</td>
                                        <td style="width: 15%" class="tdlabel">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" style="width: 100%; height: 3px;" class="tddata"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="8">
                                            <div id="divStreetOptions" style="border-width:1px; border-style:solid;" runat="server">
                                                <table id="tblstreetoptions" style="width: 100%;" runat="server">
                                                    <tr style="background-color: #08668a; color: white;">
                                                        <td colspan="8" style="padding: 5px 8px 5px 8px;">Street light options</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="padding-left:8px;">ULB</td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlSLULB" Width="150px" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSLULB_SelectedIndexChanged" />
                                                        </td>
                                                        <td>Zone</td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlSLZone" Width="150px" runat="server" />
                                                        </td>
                                                        <td>Ward</td>
                                                        <td>
                                                            <asp:TextBox ID="txtSLWardNo" runat="server" Width="150px" MaxLength="100"></asp:TextBox>
                                                        </td>
                                                        <td>Complaint From</td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlSLComplaintFrom" Width="150px" runat="server" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
        <tr>
            <td style="width: 40%">
                <asp:HiddenField ID="hdnCallEntryId" runat="server" Value="0" />
            </td>
            <td colspan="2" align="right">
                <asp:Button ID="btnSearch" runat="server" Text="Search" ValidationGroup="Search" AccessKey="S" UseSubmitBehavior="False" OnClick="btnSearch_Click" />
                &nbsp;<asp:Button ID="btnReset" runat="server" Text="Reset" UseSubmitBehavior="False" OnClick="btnReset_Click" />
                &nbsp;
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
        <tr>
            <td align="left">
                <asp:Label ID="lblMessage" runat="server" Font-Bold="False" CssClass="failureNotification"></asp:Label>
            </td>
        </tr>
        <tr>
            <td align="left">
                <table>
                    <tr>
                        <td>
                            <asp:ImageButton ID="imgbtnExportExcel" runat="server" Height="25px" ImageUrl="~/Images/excel.jpg" OnClick="imgbtnExportExcel_Click" ToolTip="Export to Excel" Width="25px" /></td>
                        <td style="width: 80%;"></td>
                        <td align="right" style="width: 100%;">Records found: </td>
                        <td>
                            <asp:Label ID="lblRecordsFound" runat="server" Text="0"></asp:Label></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:GridView ID="gvCalllDetail" TabIndex="106" PageSize="50" Width="100%" runat="server"
                    AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="CallId" EmptyDataText="No Record Found" CellPadding="4"
                    ForeColor="#333333" GridLines="None" CssClass="tabStyle" OnPageIndexChanging="gvCalllDetail_PageIndexChanging">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:TemplateField HeaderText="Sr.">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CallId" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="gvlblCallId" runat="server" Text='<%# Bind("CallId") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Caller No">
                            <ItemTemplate>
                                <asp:Label ID="gvlblCallerNo" runat="server" Text='<%# Bind("CallerNumber") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Unique ID">
                            <ItemTemplate>
                                <asp:Label ID="gvlblUID" runat="server" Text='<%# Bind("UID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category">
                            <ItemTemplate>
                                <asp:Label ID="gvlblCategory" runat="server" Text='<%# Bind("Category") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Scheme">
                            <ItemTemplate>
                                <asp:Label ID="gvlblScheme" runat="server" Text='<%# Bind("Scheme") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date Of Call">
                            <ItemTemplate>
                                <asp:Label ID="gvlblDateOfCall" runat="server" Text='<%# Bind("DateInsert") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Last Update">
                            <ItemTemplate>
                                <asp:Label ID="gvlblLastDateOfCall" runat="server" Text='<%# Bind("DateUpdate") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Last Status">
                            <ItemTemplate>
                                <asp:Label ID="gvlblLastStatus" runat="server" Text='<%# Bind("LastStatus") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TAT(Hrs.)">
                            <ItemTemplate>
                                <asp:Label ID="gvlblComplaintHoursDiff" runat="server" Text='<%# Bind("ComplaintHoursDiff") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="History">
                            <ItemTemplate>
                                <asp:Button ID="btnHistory" Text="History" runat="server" OnClick="btnHistory_Click" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Response">
                            <ItemTemplate>
                                <asp:Button ID="btnResponse" Text="Response" runat="server" OnClick="btnResponse_Click" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit" Visible="false">
                            <ItemTemplate>
                                <asp:Button ID="btnEdit" Text="Edit" runat="server" OnClick="btnEdit_Click" />
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="Black" HorizontalAlign="Center" />
                    <RowStyle HorizontalAlign="Center" VerticalAlign="Top" BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
                <br />
                <br />
                <br />
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:ModalPopupExtender ID="modelHistory" runat="server" BackgroundCssClass="modalBackground" CancelControlID="btnhide" DropShadow="true" Enabled="True" OnOkScript="onOk()" PopupControlID="ModalPanel" RepositionMode="RepositionOnWindowResize " TargetControlID="btnhide">
                </asp:ModalPopupExtender>
                <asp:Panel ID="ModalPanel" runat="server" CssClass="ModelPanel">
                    <div style="text-align: center; width: 100%">
                        <asp:Label ID="lblHeader" runat="server" CssClass="modalHeading" Text="History"></asp:Label>
                    </div>
                    <div style="text-align: center; width: 100%">
                        <asp:GridView ID="gvCallDetailHistory" TabIndex="106" runat="server" Width="100%"
                            AutoGenerateColumns="False" DataKeyNames="HistoryId" EmptyDataText="No Record Found" CellPadding="4"
                            ForeColor="#333333" GridLines="None" CssClass="tabStyle">
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField HeaderText="Sr.">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="HistoryId" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="gvlblHistoryId" runat="server" Text='<%# Bind("HistoryId") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Date">
                                    <ItemTemplate>
                                        <asp:Label ID="gvlblDate" runat="server" Text='<%# Bind("Date") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <asp:Label ID="gvlblStatus" runat="server" Text='<%# Bind("Status") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Response">
                                    <ItemTemplate>
                                        <asp:Label ID="gvlblResponse" runat="server" Text='<%# Bind("Response") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="EscalatedTo">
                                    <ItemTemplate>
                                        <asp:Label ID="gvlblEscalatedTo" runat="server" Text='<%# Bind("EscalatedTo") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <EditRowStyle BackColor="#2461BF" />
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#2461BF" ForeColor="Black" HorizontalAlign="Center" />
                            <RowStyle HorizontalAlign="Center" VerticalAlign="Top" BackColor="#EFF3FB" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                        </asp:GridView>
                    </div>
                    <asp:Button ID="btnhide" runat="server" Text="Close" />
                    <br />
                </asp:Panel>
            </td>
        </tr>

    </table>
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
</asp:Content>
