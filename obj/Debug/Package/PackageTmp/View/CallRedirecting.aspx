﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CallRedirecting.aspx.cs" Inherits="CRM.View.CallRedirecting" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td class="Heading" align="center">
                        <asp:Label ID="lblCallRedirecting" runat="server" Text="Call Redirecting......" CssClass="bold" Font-Size="XX-Large"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
