﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="frmUserThresold.aspx.cs" Inherits="CRM.View.frmUserThresold" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="Heading" align="center" style="background-color: #000000; color: #FFFFFF">
                <asp:Label ID="lblPageHeader" runat="server" Text="User Thresold" CssClass="bold"></asp:Label>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
        <tr>
            <td align="left" valign="top" width="15%"></td>
            <td valign="top" width="70%">
                <fieldset class="fieldset">
                    <legend class="Legendheading" align="left">User Master</legend>
                    <table cellpadding="0" cellspacing="3px" width="100%" style="width: 100%;">
                        <tr>
                            <td colspan="2" align="left" valign="top">
                                <asp:Label ID="lblMsg" runat="server" CssClass="failureNotification"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top">
                                <asp:Label ID="lblRole" runat="server" Text="Role"></asp:Label>
                            </td>
                            <td valign="top" align="left" class="tddata">
                                <asp:RadioButtonList ID="rdolstRole" runat="server" AutoPostBack="True" OnSelectedIndexChanged="rdolstRole_SelectedIndexChanged" RepeatDirection="Horizontal" Width="155px" TabIndex="101">
                                    <asp:ListItem Selected="True" Value="4">Operator</asp:ListItem>
                                    <asp:ListItem Value="3">QA</asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top">User</td>
                            <td valign="top" align="left" class="tddata">
                                <asp:DropDownList ID="ddlUserQA" TabIndex="102" runat="server" Width="155px">
                                </asp:DropDownList>
                                <span>
                                    <asp:RequiredFieldValidator ID="rfvUser" runat="server" ControlToValidate="ddlUserQA"
                                        ForeColor="Red" InitialValue="Please-Select" ErrorMessage="*"></asp:RequiredFieldValidator>
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top">
                                <asp:Label ID="lblQAThresold" runat="server"></asp:Label>
                            </td>
                            <td valign="top" align="left" class="tddata">
                                <asp:TextBox ID="txtQAThresold" runat="server" TabIndex="103" Style="text-transform: none"
                                    MaxLength="3" Width="150px"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="fteQAThresold" runat="server" Enabled="true" FilterType="Custom, Numbers"
                                    TargetControlID="txtQAThresold" />
                                <span>
                                    <asp:RequiredFieldValidator ID="rfvQAThresold" runat="server" ControlToValidate="txtQAThresold"
                                        ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                    &nbsp;</span><asp:RangeValidator ID="rvQAThresold" runat="server"
                                    ControlToValidate="txtQAThresold"
                                    ErrorMessage="Please enter value between 0 and 100!" MaximumValue="100" MinimumValue="0"
                                    Type="Integer" ForeColor="Red"></asp:RangeValidator>

                            </td>
                        </tr>
                        <tr id="QA2" runat="server">
                            <td align="left" class="tdlabel" valign="top">
                                <asp:Label ID="lblQA2Thresold" runat="server"></asp:Label>
                            </td>
                            <td valign="top" align="left" class="tddata">
                                <asp:TextBox ID="txtQA2Thresold" runat="server" onblur="return passwordLenCheck();"
                                    TabIndex="104" Style="text-transform: none" MaxLength="3" Width="150px"></asp:TextBox>
                                <asp:FilteredTextBoxExtender ID="fteQA2Thresold" runat="server" Enabled="true" FilterType="Custom, Numbers"
                                    TargetControlID="txtQA2Thresold" />
                                <span>
                                    <asp:RequiredFieldValidator ID="rfvQA2Thresold" runat="server" ControlToValidate="txtQA2Thresold"
                                        ErrorMessage="*" ForeColor="Red"></asp:RequiredFieldValidator>
                                    &nbsp;</span><asp:RangeValidator ID="rvQA2Thresold" runat="server"
                                    ControlToValidate="txtQA2Thresold"
                                    ErrorMessage="Please enter value between 0 and 100!" MaximumValue="100" MinimumValue="0"
                                    Type="Integer" ForeColor="Red"></asp:RangeValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">&nbsp;
                            </td>
                            <td align="left" class="tddata">
                                <asp:Button ID="btnSubmit" Text="Sumbit" TabIndex="105" runat="server" OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnReset" Text="Reset" TabIndex="106" runat="server"
                                    OnClick="btnReset_Click" CausesValidation="False" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left">
                                <asp:HiddenField ID="hdnId" runat="server" Value="-1" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
            <td align="left" valign="top" width="15%"></td>
        </tr>
        <tr>
            <td align="left" valign="top" width="15%"></td>
            <td valign="top" width="70%">
                <table border="0" width="100%">
                    <tr>
                        <td align="center">
                            <asp:GridView ID="gvUserThresold" PageSize="100" TabIndex="107" runat="server" Width="100%"
                                AutoGenerateColumns="False" OnPageIndexChanging="gvUserThresold_PageIndexChanging"
                                OnRowDataBound="gvUserThresold_RowDataBound" OnRowDeleting="gvUserThresold_RowDeleting"
                                OnRowEditing="gvUserThresold_RowEditing" CellPadding="4" ForeColor="#333333"
                                GridLines="None" OnRowUpdating="gvUserThresold_RowUpdating" DataKeyNames="ID">
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr.">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Role">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblRoleName" runat="server" Text='<%# Bind("RoleName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="User Name">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblUserName" runat="server" Text='<%# Bind("UserName") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="QA-1 Thresold">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblQA1" runat="server" Text='<%# Bind("QAThresold") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="QA-2 Thresold">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblQA2" runat="server" Text='<%# Bind("QA2Thresold") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit" Visible="False">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnEdit" CommandName="Edit" runat="server" ImageUrl="~/Images/edit.png"
                                                OnClientClick="return confirm('Are you sure! You want to Edit Seleted Row?');" CausesValidation="False" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnStatus" CommandName="Update" runat="server" ImageUrl="~/Images/active.gif"
                                                OnClientClick="return confirm('Are you sure! You want to change status?');" CausesValidation="False" />
                                            <asp:Label ID="gvlblIsActive" runat="server" Text='<%# Bind("IsActive") %>' Visible="False" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnDelete" CommandName="Delete" runat="server" ImageUrl="~/Images/delete.gif"
                                                OnClientClick="return confirm('Are you sure! You want to Delete Seleted Row?');"
                                                Enabled='<%# (Convert.ToString(Eval("UserName"))=="admin" ? false : true) %>' CausesValidation="False" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle BackColor="#2461BF" />
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="Black" HorizontalAlign="Center" />
                                <RowStyle HorizontalAlign="Center" VerticalAlign="Top" BackColor="#EFF3FB" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
            <td align="left" valign="top" width="15%"></td>
        </tr>
    </table>
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
</asp:Content>
