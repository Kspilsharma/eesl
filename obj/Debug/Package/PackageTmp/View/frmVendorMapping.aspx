﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteNew.Master" AutoEventWireup="true" CodeBehind="frmVendorMapping.aspx.cs" Inherits="EESL.View.frmVendorMapping" EnableEventValidation="false" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.min.js"></script>
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        $(document).ready(function () {

            $('#<% =ddlScheme.ClientID %>').change(function (e) {
                if ($('#<% =ddlScheme.ClientID %>').val() == '--Select--' || $('#<% =ddlScheme.ClientID %>').val() == '0') {
                    document.getElementById("rfvScheme").style.display = "block";
                }
                else {
                    document.getElementById("rfvScheme").style.display = "none";
                }
            });

            $('#<% =ddlVendorType.ClientID %>').change(function (e) {
                var vendortypeId = $('#<% =ddlVendorType.ClientID %>').val();
                $('#<% =hdnVendorType.ClientID %>').attr('value', vendortypeId);

                if ($('#<% =ddlVendorType.ClientID %>').val() == '--Select--' || $('#<% =ddlVendorType.ClientID %>').val() == '0') {
                    document.getElementById("rfvVendorType").style.display = "block";
                }
                else {
                    document.getElementById("rfvVendorType").style.display = "none";
                }
            });

            $('#<% =ddlState.ClientID %>').change(function (e) {
                if ($('#<% =ddlState.ClientID %>').val() == '--Select--' || $('#<% =ddlState.ClientID %>').val() == '0') {
                    document.getElementById("rfvState").style.display = "block";
                }
                else {
                    document.getElementById("rfvState").style.display = "none";
                }
            });

            $('#<% =ddlDistrict.ClientID %>').change(function (e) {
                var districId = $('#<% =ddlDistrict.ClientID %>').val();
                $('#<% =hdnDistricId.ClientID %>').attr('value', districId);

                if ($('#<% =ddlDistrict.ClientID %>').val() == '--Select--' || $('#<% =ddlDistrict.ClientID %>').val() == '0') {
                    document.getElementById("rfvDistrict").style.display = "block";
                }
                else {
                    document.getElementById("rfvDistrict").style.display = "none";
                }
            });

            $('#<% =ddlULB.ClientID %>').change(function (e) {
                var ulbMappingId = $('#<% =ddlULB.ClientID %>').val();
                $('#<% =hdnULBMappingId.ClientID %>').attr('value', ulbMappingId);

                if ($('#<% =ddlULB.ClientID %>').val() == '--Select--' || $('#<% =ddlULB.ClientID %>').val() == '0') {
                    document.getElementById("rfvULB").style.display = "block";
                }
                else {
                    document.getElementById("rfvULB").style.display = "none";
                }
            });

            $('#<% =ddlZone.ClientID %>').change(function (e) {
                var zoneMappingId = $('#<% =ddlZone.ClientID %>').val();
                $('#<% =hdnZoneMappingId.ClientID %>').attr('value', zoneMappingId);

                if ($('#<% =ddlZone.ClientID %>').val() == '--Select--' || $('#<% =ddlZone.ClientID %>').val() == '0') {
                    document.getElementById("rfvZone").style.display = "block";
                }
                else {
                    document.getElementById("rfvZone").style.display = "none";
                }
            });

            $('#<% =txtVendorName.ClientID %>').change(function (e) {
                if ($('#<% =txtVendorName.ClientID %>').val() == '') {
                    document.getElementById("rfvVendorName").style.display = "block";
                }
                else {
                    document.getElementById("rfvVendorName").style.display = "none";
                }
            });

            $('#<% =txtVendorEmail.ClientID %>').change(function (e) {
                if ($('#<% =txtVendorEmail.ClientID %>').val() == '') {
                    document.getElementById("rfvVendorEmail").style.display = "block";
                }
                else {
                    document.getElementById("rfvVendorEmail").style.display = "none";
                }
            });

            $('#<% =txtVendorMobileNo.ClientID %>').change(function (e) {
                if ($('#<% =txtVendorMobileNo.ClientID %>').val() == '') {
                    document.getElementById("rfvVendorMobileNo").style.display = "block";
                }
                else {
                    document.getElementById("rfvVendorMobileNo").style.display = "none";
                }
            });
        });

        var pageUrl = '<%=ResolveUrl("~/View/frmVendorMapping.aspx")%>'
        function PopulateCities() {

            if ($('#<%=ddlState.ClientID%>').val() == "--Select--") {
                $('#<%=ddlDistrict.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            }
            else {
                $('#<%=ddlDistrict.ClientID %>').empty().append('<option selected="selected" value="0">Loading...</option>');
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/PopulateCities',
                    data: '{stateId: ' + $('#<%=ddlState.ClientID%>').val() + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnCitiesPopulated,
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
        }

        function OnCitiesPopulated(response) {
            PopulateControl(response.d, $("#<%=ddlDistrict.ClientID %>"), "District");
        }

        function PopulateULB() {
            if ($('#<%=ddlScheme.ClientID%>').val() == "4") {
                return;
            }
            if ($('#<%=ddlDistrict.ClientID%>').val() == "--Select--") {
                $('#<%=ddlULB.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            }
            else {
                $('#<%=ddlULB.ClientID %>').empty().append('<option selected="selected" value="0">Loading...</option>');
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/PopulateULB',
                    data: '{stateId: ' + $('#<%=ddlState.ClientID%>').val() + ',districtId: ' + $('#<%=ddlDistrict.ClientID%>').val() + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnULBPopulated,
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
        }

        function OnULBPopulated(response) {
            PopulateControl(response.d, $("#<%=ddlULB.ClientID %>"), "ULB");
        }

        function PopulateZone() {

            if ($('#<%=ddlULB.ClientID%>').val() == "--Select--") {
                $('#<%=ddlZone.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            }
            else {
                $('#<%=ddlZone.ClientID %>').empty().append('<option selected="selected" value="0">Loading...</option>');
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/PopulateZone',
                    data: '{stateId: ' + $('#<%=ddlState.ClientID%>').val() + ',districtId: ' + $('#<%=ddlDistrict.ClientID%>').val() + ',ulbMappingId: ' + $('#<%=ddlULB.ClientID%>').val() + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnZonePopulated,
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
        }

        function OnZonePopulated(response) {
            PopulateControl(response.d, $("#<%=ddlZone.ClientID %>"), "Zone");
        }

        function PopulateVendorType() {
            ClearDropDowns();
            if ($('#<%=ddlScheme.ClientID%>').val() == "4" || $('#<%=ddlScheme.ClientID%>').val() == "3") {
                $('#<%=ddlULB.ClientID%>').attr('disabled', 'disabled');
                $('#<%=ddlZone.ClientID%>').attr('disabled', 'disabled');
                document.getElementById("rfvULB").style.display = "none";
                document.getElementById("rfvZone").style.display = "none";
            }
            else {
                $('#<%=ddlULB.ClientID%>').attr('disabled', false);
                $('#<%=ddlZone.ClientID%>').attr('disabled', false);
            }

            if ($('#<%=ddlScheme.ClientID%>').val() == "--Select--") {
                $('#<%=ddlVendorType.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            }
            else {
                $('#<%=ddlVendorType.ClientID %>').empty().append('<option selected="selected" value="0">Loading...</option>');
                $.ajax({
                    type: "POST",
                    url: pageUrl + '/PopulateVendorType',
                    data: '{SchemeId: ' + $('#<%=ddlScheme.ClientID%>').val() + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: OnVendorTypePopulated,
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
        }

        function OnVendorTypePopulated(response) {
            PopulateControl(response.d, $("#<%=ddlVendorType.ClientID %>"), "VendorType");
        }

        function ClearDropDowns() {
            $('#<%=ddlVendorType.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            $('#<%=ddlState.ClientID %>').val("0");
            $('#<%=ddlDistrict.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            $('#<%=ddlULB.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
            $('#<%=ddlZone.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
        }

        function PopulateControl(list, control, type) {
            if (list.length > 0) {
                control.empty().append('<option selected="selected" value="0">--Select--</option>');
                $.each(list, function () {
                    control.append($("<option></option>").val(this['Value']).html(this['Text']));
                });
            }
            else {
                if (type == "District") {
                    $('#<%=ddlDistrict.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
                }
                else if (type == "ULB") {
                    $('#<%=ddlULB.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
                }
                else if (type == "Zone") {
                    $('#<%=ddlZone.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
                }
                else if (type == "VendorType") {
                    $('#<%=ddlVendorType.ClientID %>').empty().append('<option selected="selected" value="0">--Select--</option>');
                }
            }
        }

        function Validate() {
            var isValid = true;
            document.getElementById("rfvScheme").style.display = "none";
            document.getElementById("rfvVendorType").style.display = "none";
            document.getElementById("rfvState").style.display = "none";
            document.getElementById("rfvDistrict").style.display = "none";
            document.getElementById("rfvULB").style.display = "none";
            document.getElementById("rfvZone").style.display = "none";
            document.getElementById("rfvVendorName").style.display = "none";
            document.getElementById("rfvVendorEmail").style.display = "none";
            document.getElementById("rfvVendorMobileNo").style.display = "none";

            if ($('#<% =ddlScheme.ClientID %>').val() == '--Select--' || $('#<% =ddlScheme.ClientID %>').val() == '0') {
                isValid = false;
                document.getElementById("rfvScheme").style.display = "block";
            }

            if ($('#<% =ddlVendorType.ClientID %>').val() == '--Select--' || $('#<% =ddlVendorType.ClientID %>').val() == '0') {
                isValid = false;
                document.getElementById("rfvVendorType").style.display = "block";
            }

            if ($('#<% =ddlState.ClientID %>').val() == '--Select--' || $('#<% =ddlState.ClientID %>').val() == '0') {
                isValid = false;
                document.getElementById("rfvState").style.display = "block";
            }

            if ($('#<% =ddlDistrict.ClientID %>').val() == '--Select--' || $('#<% =ddlDistrict.ClientID %>').val() == '0') {
                isValid = false;
                document.getElementById("rfvDistrict").style.display = "block";
            }

            if ($('#<% =ddlScheme.ClientID %>').val() == '--Select--' || $('#<% =ddlScheme.ClientID %>').val() == '1') {

                if ($('#<% =ddlULB.ClientID %>').val() == '--Select--' || $('#<% =ddlULB.ClientID %>').val() == '0') {
                    isValid = false;
                    document.getElementById("rfvULB").style.display = "block";
                }

                if ($('#<% =ddlZone.ClientID %>').val() == '--Select--' || $('#<% =ddlZone.ClientID %>').val() == '0') {
                    isValid = false;
                    document.getElementById("rfvZone").style.display = "block";
                }
            }
            if ($('#<% =txtVendorName.ClientID %>').val() == '') {
                isValid = false;
                document.getElementById("rfvVendorName").style.display = "block";
            }
            if ($('#<% =txtVendorEmail.ClientID %>').val() == '') {
                isValid = false;
                document.getElementById("rfvVendorEmail").style.display = "block";
            }
            if ($('#<% =txtVendorMobileNo.ClientID %>').val() == '') {
                isValid = false;
                document.getElementById("rfvVendorMobileNo").style.display = "block";
            }

            return isValid;
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="Heading" align="center" style="color: #101010">
                <asp:Label ID="lblPageHeader" runat="server" Text="Vendor Mapping" CssClass="bold"></asp:Label>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
        <tr>
            <td align="left" valign="top" width="15%"></td>
            <td valign="top" width="70%">
                <fieldset class="fieldset">
                    <legend class="Legendheading" align="left">Vendor Mapping</legend>
                    <table cellpadding="0" cellspacing="3px" width="100%" style="margin: 0px; padding: 0px;">
                        <tr>
                            <td colspan="2" align="left">
                                <asp:Label ID="lblMsg" runat="server" CssClass="failureNotification"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="30%" class="tdlabel" valign="top">Scheme<span style="color: red">&nbsp;*</span> &nbsp;:</td>
                            <td valign="top" align="left" class="tddata" width="70%">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlScheme" runat="server" onchange="PopulateVendorType();" />
                                        </td>
                                        <td>
                                            <label id="rfvScheme" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Select Scheme</label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="30%" class="tdlabel" valign="top">Vendor Type<span style="color: red">&nbsp;*</span>&nbsp;:</td>
                            <td valign="top" align="left" class="tddata" width="70%">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlVendorType" runat="server" Width="150px"></asp:DropDownList>
                                        </td>
                                        <td>
                                            <label id="rfvVendorType" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Select Vendor Type</label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" width="30%" class="tdlabel" valign="top">State<span style="color: red">&nbsp;*</span> &nbsp;:</td>
                            <td valign="top" align="left" class="tddata" width="70%">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlState" runat="server" Width="250px" onchange="PopulateCities();"  />
                                        </td>
                                        <td>
                                            <label id="rfvState" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Select State</label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td align="left" width="30%" class="tdlabel" valign="top">District<span style="color: red">&nbsp;*</span> &nbsp;:</td>
                            <td valign="top" align="left" class="tddata" width="70%">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlDistrict" runat="server" Width="250px" onchange="PopulateULB();"  />
                                        </td>
                                        <td>
                                            <label id="rfvDistrict" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Select District</label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td align="left" class="tdlabel" valign="top" width="30%">ULB<span style="color: red">&nbsp;*</span> &nbsp;:</td>

                            <td valign="top" align="left" class="tddata" width="70%">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlULB" runat="server" Width="250px" onchange="PopulateZone();" />
                                        </td>
                                        <td>
                                            <label id="rfvULB" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Select ULB</label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td align="left" class="tdlabel" valign="top" width="30%">Zone<span style="color: red">&nbsp;*</span> &nbsp;:</td>

                            <td valign="top" align="left" class="tddata" width="70%">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlZone" runat="server" Width="250px" />
                                        </td>
                                        <td>
                                            <label id="rfvZone" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Select Zone</label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td align="left" class="tdlabel" valign="top" width="30%">Vendor Name<span style="color: red">&nbsp;*</span> &nbsp;:</td>

                            <td valign="top" align="left" class="tddata" width="70%">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtVendorName" runat="server" Width="250px" Style="text-transform: none" MaxLength="50"></asp:TextBox>
                                        </td>
                                        <td>
                                            <label id="rfvVendorName" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Enter Vendor Name</label>
                                            <asp:FilteredTextBoxExtender ID="fteVendorName" runat="server" Enabled="true" TargetControlID="txtVendorName" FilterType="Custom,LowercaseLetters,UppercaseLetters,Numbers" FilterMode="ValidChars" ValidChars=" " />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top" width="30%">Vendor Email<span style="color: red">&nbsp;*</span> &nbsp;: </td>
                            <td valign="top" align="left" class="tddata" width="70%">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtVendorEmail" runat="server" Width="250px" Style="text-transform: none" MaxLength="50"></asp:TextBox>
                                        </td>
                                        <td>
                                            <label id="rfvVendorEmail" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Enter Vendor Email</label>
                                            <asp:RegularExpressionValidator ID="revVendorEmail" runat="server" ForeColor="Maroon" ControlToValidate="txtVendorEmail" ErrorMessage="Invalid Email." Display="Dynamic" ValidationGroup="Save" Font-Bold="True" Font-Size="X-Small" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top" width="30%">Vendor MobileNo<span style="color: red">&nbsp;*</span> &nbsp;:</td>
                            <td valign="top" align="left" class="tddata" width="70%">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtVendorMobileNo" runat="server" Width="250px" Style="text-transform: none" MaxLength="12"></asp:TextBox>
                                        </td>
                                        <td>
                                            <label id="rfvVendorMobileNo" style="display: none; color: Maroon; font-size: x-small; font-weight: bold;">Enter Vendor Mobile No.</label>
                                            <asp:FilteredTextBoxExtender ID="fteVendorMobileNo" runat="server" Enabled="true" TargetControlID="txtVendorMobileNo" FilterType="Custom,Numbers" FilterMode="ValidChars" ValidChars="-" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td align="left">&nbsp;
                            </td>
                            <td align="left">
                                <asp:Button ID="btnSave" Text="Save" OnClientClick=" return Validate();" runat="server" ValidationGroup="Save" OnClick="btnSave_Click" />
                                <asp:Button ID="btnReset" Text="Reset" runat="server" OnClick="btnReset_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left">
                                <asp:HiddenField ID="hdnId" runat="server" Value="-1" />
                                <asp:HiddenField ID="hdnDistricId" runat="server" Value="0" />
                                <asp:HiddenField ID="hdnULBMappingId" runat="server" Value="0" />
                                <asp:HiddenField ID="hdnVendorType" runat="server" Value="0" />
                                <asp:ScriptManager ID="ScriptManager1" runat="server">
                                </asp:ScriptManager>
                                <asp:HiddenField ID="hdnZoneMappingId" runat="server" Value="0" />
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
            <td align="left" valign="top" width="15%"></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td align="left" valign="top" width="15%"></td>
            <td valign="top" width="70%">
                <table border="0" width="100%">
                    <tr>
                        <td align="right">
                            <asp:ImageButton ID="imgbtnExportExcel" ToolTip="Export to Excel" ImageUrl="~/Images/excel.jpg" Width="25px" Height="25px" runat="server" OnClick="imgbtnExportExcel_Click" />
                        </td>
                    </tr>
                     <tr>
                        <td align="left">
                          <text>Search:</text> <asp:TextBox ID="txtSearch" runat="server" Width="15%" AutoPostBack="true" OnTextChanged="txtSearch_TextChanged"></asp:TextBox>
                            <hr/>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:GridView ID="gvVendorMapping" PageSize="50" Width="100%" runat="server"
                                AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="VendorMappingId" OnPageIndexChanging="gvVendorMapping_PageIndexChanging"
                                OnRowDataBound="gvVendorMapping_RowDataBound" OnRowDeleting="gvVendorMapping_RowDeleting" OnRowEditing="gvVendorMapping_RowEditing"
                                OnRowUpdating="gvVendorMapping_RowUpdating" EmptyDataText="No Record Found" CellPadding="4"
                                ForeColor="#333333" class="tabStyle" GridLines="None">
                                <AlternatingRowStyle BackColor="" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Sr.">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="State">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblState" runat="server" Text='<%# Bind("State") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="District">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblDistrict" runat="server" Text='<%# Bind("District") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ULB">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblULB" runat="server" Text='<%# Bind("ULB") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Zone">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblZone" runat="server" Text='<%# Bind("Zone") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Type">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblVendorType" runat="server" Text='<%# Bind("VendorType") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Name">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblVendorName" runat="server" Text='<%# Bind("VendorName") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor Email">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblVendorEmail" runat="server" Text='<%# Bind("VendorEmail") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Vendor MobileNo">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblVendorMobileNo" runat="server" Text='<%# Bind("VendorMobileNo") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnEdit" CommandName="Edit" runat="server" ImageUrl="~/Images/edit.png"
                                                OnClientClick="return confirm('Are you sure! You want to Edit Seleted Row?');" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnStatus" CommandName="Update" runat="server" ImageUrl="~/Images/active.gif"
                                                OnClientClick="return confirm('Are you sure! You want to change status?');" />
                                            <asp:Label ID="gvlblIsActive" runat="server" Text='<%# Bind("IsActive") %>' Visible="False" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:TemplateField HeaderText="Delete">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnDelete" CommandName="Delete" runat="server" ImageUrl="~/Images/delete.gif"
                                                OnClientClick="return confirm('Are you sure! You want to Delete Seleted Row?');" />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                                </Columns>
                                <EditRowStyle BackColor="#2461BF" />
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="Black" HorizontalAlign="Center" />
                                <RowStyle HorizontalAlign="Center" VerticalAlign="Top" BackColor="" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                            <br />
                            <br />
                            <br />
                        </td>
                    </tr>
                </table>
            </td>
            <td align="left" valign="top" width="15%"></td>
        </tr>
    </table>
</asp:Content>
