﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="frmConfigurationSetting.aspx.cs" Inherits="CRM.View.frmConfigurationSetting" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td align="center">
                <table cellpadding="1" cellspacing="1" border="0" style="width: 50%;">
                    <tr>
                        <td colspan="2" style="width: 100%">
                            <asp:Label ID="lblMessage" runat="server" SkinID="baseError" CssClass="failureNotification"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="width: 100%">&nbsp; &nbsp;</td>
                    </tr>
                    <tr>
                        <td class="tdlabel">Image Folder Path</td>
                        <td class="tddata" align="left">
                            <asp:TextBox ID="txtImageFolderPath" runat="server" TabIndex="101" Width="300" MaxLength="100"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="tddata">&nbsp;</td>
                        <td class="tddata">
                            &nbsp; &nbsp;</td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                                &nbsp;</td>
                        <td align="left">
                            <asp:Button ID="btnUpdate" runat="server" TabIndex="102" Text="Update" OnClick="btnUpdate_Click" />

                        </td>
                    </tr>
                    
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
