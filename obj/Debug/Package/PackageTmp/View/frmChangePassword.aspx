﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="frmChangePassword.aspx.cs" Inherits="CRM.View.frmChangePassword" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="Heading" align="center" style="color: #101010">
                <asp:Label ID="lblPageHeader" runat="server" Text="Change Password" CssClass="bold"></asp:Label>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
        <tr>
            <td align="left" valign="top" width="10%">
            </td>
            <td valign="top" width="80%">
                <fieldset class="fieldset">
                    <legend class="Legendheading">
                        <asp:Label ID="lblChangePassword" runat="server" Text="ChangePassword"></asp:Label>
                    </legend>
                    <table cellpadding="0" cellspacing="3px" width="100%" style="margin: 0px; padding: 0px;">
                        <tr>
                            <td style="width: 20%" class="tdlabel">
                                <asp:Label ID="lblOldPassword" runat="server" Text="OldPassword"></asp:Label>
                                <span class="failureNotification">*</span>
                            </td>
                            <td style="width: 80%">
                                <asp:TextBox ID="txtOldPassword" runat="server" MaxLength="12" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvOldPassword" runat="server" Display="Static" ControlToValidate="txtOldPassword"
                                    ErrorMessage="*" ValidationGroup="Submit" CssClass="failureNotification"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdlabel" style="width: 20%">
                                <asp:Label ID="lblNewPassword" runat="server" Text="NewPassword"></asp:Label>
                                <span class="failureNotification">*</span>
                            </td>
                            <td style="width: 80%">
                                <asp:TextBox ID="txtNewPassword" runat="server" MaxLength="12" TextMode="Password"></asp:TextBox>
                                <span>
                                    <asp:RequiredFieldValidator ID="rfvNewPassword" runat="server" Display="Dynamic"
                                        ControlToValidate="txtNewPassword" CssClass="failureNotification" ErrorMessage="*"
                                        ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revPassword" runat="server" ErrorMessage="Password length must be between 6 to 12 characters.!"
                                        ControlToValidate="txtNewPassword" CssClass="failureNotification" ValidationExpression="^[a-zA-Z0-9'@&#._\s]{6,12}$" />
                                    <asp:FilteredTextBoxExtender ID="fteNewPassword" runat="server" Enabled="true" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                        TargetControlID="txtNewPassword" ValidChars="#@&`._" />
                                </span>
                            </td>
                        </tr>
                        <tr>
                            <td class="tdlabel" style="width: 20%">
                                <asp:Label ID="lblConfirmPassword" runat="server" Text="ConfirmPassword"></asp:Label>
                                <span class="failureNotification">*</span>
                            </td>
                            <td style="width: 80%">
                                <asp:TextBox ID="txtConfirmPassword" runat="server" MaxLength="12" TextMode="Password"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" Display="Dynamic"
                                    ControlToValidate="txtConfirmPassword" ErrorMessage="*" ValidationGroup="Submit"
                                    CssClass="failureNotification"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="cvConfirmPassword" runat="server" Display="Dynamic" ErrorMessage="Password mismatch !"
                                    ControlToCompare="txtNewPassword" ControlToValidate="txtConfirmPassword" ValidationGroup="Submit"
                                    CssClass="failureNotification"></asp:CompareValidator>
                                <asp:FilteredTextBoxExtender ID="fteConfirmPassword" runat="server" Enabled="true"
                                    FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters" TargetControlID="txtConfirmPassword"
                                    ValidChars="#@&`._" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 20%">
                                &nbsp;
                            </td>
                            <td style="width: 80%">
                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"
                                    ValidationGroup="Submit" />
                                <asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" CausesValidation="False" />
                            </td>
                        </tr>
                        <tr>
                            <td class="tdlabel" style="font-weight: bold; width: 80%;" colspan="2">
                                Password Policy :
                            </td>
                        </tr>
                        <tr>
                            <td class="tddata" colspan="2" style="width: 80%">
                                1. Length must be between 6 to 12 characters.
                            </td>
                        </tr>
                        <tr>
                            <td class="tddata" colspan="2" style="width: 80%">
                                2. Only contain @ &amp; # . _ special characters .
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
            <td align="left" valign="top" width="10%">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Button ID="btnhide" runat="server" Style="display: none" />
                <asp:ModalPopupExtender ID="modelSubmit" runat="server" DropShadow="true" BackgroundCssClass="modalBackground"
                    CancelControlID="btnhide" Enabled="True" OnOkScript="onOk()" PopupControlID="ModalPanel"
                    RepositionMode="RepositionOnWindowResize " TargetControlID="btnhide">
                </asp:ModalPopupExtender>
                <asp:Panel ID="ModalPanel" runat="server" CssClass="ModelPanel" Style="display: block">
                    <div style="text-align: center; width: 100%">
                        <asp:Label ID="lblHeader" runat="server" CssClass="modalHeading" Text="Message"></asp:Label></div>
                    <br />
                    <asp:Label ID="lblMessage" runat="server"></asp:Label>
                    <br />
                    <br />
                    <br />
                    <asp:Button ID="btnOk" runat="server" Text="OK" CausesValidation="false" />
                    <br />
                </asp:Panel>
            </td>
        </tr>
    </table>
    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>
</asp:Content>
