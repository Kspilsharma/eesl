﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="frmSMSEmailConfigurationSetting.aspx.cs" Inherits="EESL.View.frmSMSEmailConfigurationSetting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 10pt;
        }
        .table
        {
            border: 1px solid #ccc;
            border-collapse: collapse;
            width: 200px;
        }
        .table th
        {
            background-color: #F7F7F7;
            color: #333;
            font-weight: bold;
        }
        .table th, .table td
        {
            padding: 5px;
            border: 1px solid #ccc;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="Heading" align="center" style="color: #101010">
                <asp:Label ID="lblPageHeader" runat="server" Text="SMS Email Configuration" CssClass="bold"></asp:Label>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
        <tr>
            <td align="left" valign="top" width="25%"></td>
            <td valign="top" width="50%">
                <fieldset class="fieldset">
                    <legend class="Legendheading" align="left">SMS Email Configuration</legend>
                    <table cellpadding="0" cellspacing="3px" width="100%" style="margin: 0px; padding: 0px;">
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblMsg" ForeColor="Red" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top">
                                <asp:Label ID="lblRole" runat="server" Text="Type"></asp:Label>
                            </td>
                            <td valign="top" align="left" class="tddata">
                                <asp:DropDownList ID="ddlSettingType" runat="server" TabIndex="101" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlSettingType_SelectedIndexChanged" Width="155px">
                                </asp:DropDownList>
                                &nbsp;<asp:RequiredFieldValidator ID="RFVUSERTYPE" runat="server" ControlToValidate="ddlSettingType"
                                    InitialValue="0" ErrorMessage="*" ForeColor="Red" ValidationGroup="sub"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top">Setting
                            </td>
                            <td valign="top" align="left" class="tddata">&nbsp;
                                <asp:DataList ID="dlSettings" runat="server" RepeatColumns="5" CellSpacing="3" RepeatLayout="Table">
                                    <ItemTemplate>
                                        <table class="table">
                                            <tr>
                                                <th colspan="2">
                                                    <b><%# Eval("StatusOfCall") %></b>
                                                    <asp:Label ID="lblStatusOfCallId" Visible="false" runat="server" Text='<%# Bind("StatusOfCallId") %>' />
                                                </th>
                                            </tr>
                                            <tr>
                                                <td>Sms
                                                </td>
                                                <td>Email
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <asp:CheckBox ID="chkSMS" runat="server" Checked='<%# Bind("sendSMS") %>' />
                                                </td>
                                                <td>
                                                    <asp:CheckBox ID="chkEMail" runat="server" Checked='<%# Bind("sendEmail") %>'/>
                                                </td>
                                            </tr>
                                        </table>
                                    </ItemTemplate>
                                </asp:DataList>
                                
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="2">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td align="left">&nbsp;
                            </td>
                            <td align="left" class="tddata">
                                <asp:Button ID="btnSubmit" Text="Update" TabIndex="103" runat="server" ValidationGroup="sub"
                                    OnClick="btnSubmit_Click" />
                                &nbsp;<asp:Button ID="btnReset" Text="Reset" TabIndex="104" runat="server"
                                    OnClick="btnReset_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left">&nbsp;
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
            <td align="left" valign="top" width="25%"></td>
        </tr>
    </table>
</asp:Content>
