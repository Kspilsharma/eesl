﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WithoutOuth.Master" AutoEventWireup="true" CodeBehind="frmFAQs.aspx.cs" Inherits="CRM.View.frmFAQs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="Heading" align="center" style="color: #101010">
                <asp:Label ID="lblPageHeader" runat="server" Text="FAQ's" CssClass="bold"></asp:Label>
            </td>
        </tr>
    </table>
    <table border="0" width="100%">
        <tr>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td>&nbsp;<table class="auto-style1">
                <tr>
                    <td style="width: 10%" class="tdlabel">Scheme : </td>
                    <td style="width: 40%" class="tddata">
                        <asp:DropDownList ID="ddlScheme" runat="server" AutoPostBack="True" Width="100%" OnSelectedIndexChanged="ddlScheme_SelectedIndexChanged" />
                    </td>
                    <td style="width: 10%" class="tdlabel">Keyword : </td>
                    <td style="width: 40%" class="tddata">
                        <asp:TextBox ID="txtKeyword" runat="server" Width="100%" AutoPostBack="True" OnTextChanged="txtKeyword_TextChanged"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:GridView ID="gvFAQs" TabIndex="106" PageSize="50" Width="100%" runat="server"
                    AllowPaging="True" AutoGenerateColumns="False" EmptyDataText="No Record Found" CellPadding="4"
                    ForeColor="#333333" GridLines="None" CssClass="tabStyle" OnPageIndexChanging="gvFAQs_PageIndexChanging">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <table width="100%" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <p>
                                                <b>Question&nbsp; : &nbsp;</b>
                                                <asp:Label ID="gvlblQuestion" runat="server" Text='<%# Bind("Question") %>' />
                                            </p>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <p>
                                                <b>Answer&nbsp; : &nbsp;</b>
                                                <asp:Label ID="Label1" runat="server" Text='<%# Bind("Answer") %>' />
                                            </p>
                                        </td>
                                    </tr>
                                </table>

                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="Black" HorizontalAlign="Center" />
                    <RowStyle HorizontalAlign="Center" VerticalAlign="Top" BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
