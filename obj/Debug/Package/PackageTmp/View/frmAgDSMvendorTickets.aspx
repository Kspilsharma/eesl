﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="frmAgDSMvendorTickets.aspx.cs" Inherits="EESL.View.frmAgDSMvendorTickets" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <%-- <script src="../Scripts/jquery.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui.min.js"></script>
    <link href="../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />--%>




    <link href="../Styles/jquery-ui.css" rel="stylesheet" />
    <link href="../Styles/ui.jqgrid.css" rel="stylesheet" />

    <script type="text/javascript" src="../Scripts/jquery-1.8.3.js"></script>
    <script type="text/javascript" src="../Scripts/grid.locale-en.js"></script>
    <script type="text/javascript" src="../Scripts/jquery.jqGrid.js"></script>
    <script type="text/javascript" src="../Scripts/jquery-ui-1.12.1.custom.js"></script>

    <link href="../Styles/ui.jqgrid.css" rel="stylesheet" />

    <link href="../Styles/VendorTicketsGridView.css" rel="stylesheet" />

    <script type="text/javascript">
        $(function () {
            $("#<%= txtFrom.ClientID  %>").datepicker(
           {
               dateFormat: 'dd-mm-yy',
               //minDate: new Date(),
               changeMonth: true,
               numberOfMonths: 1,
               onSelect: function (selected) {
                   var arr = selected.split("-");
                   var dmy = arr[1] + "-" + arr[0] + "-" + arr[2];
                   var dt = new Date(dmy);
                   dt.setDate(dt.getDate());
                   $("#<%= txtTo.ClientID  %>").datepicker("option", "minDate", dt);
               }

           });//.datepicker("setDate", new Date());


            $("#<%= txtTo.ClientID  %>").datepicker(
            {
                dateFormat: 'dd-mm-yy',
                //minDate: new Date(),
                changeMonth: true,
                numberOfMonths: 1,
                onSelect: function (selected) {
                    var arr = selected.split("-");
                    var dmy = arr[1] + "-" + arr[0] + "-" + arr[2];
                    var dt = new Date(dmy);
                    dt.setDate(dt.getDate());
                    $("#<%= txtFrom.ClientID  %>").datepicker("option", "maxDate", dt);
                }

            });//.datepicker("setDate", new Date());
        });
    </script>
    <script type="text/javascript">

        function updateDialog(action) {
            return {
                url: 'Default.aspx/UpdateData',
                closeOnEscape: true,
                closeAfterEdit: true,
                closeAfterAdd: true,
                datatype: 'json',
                ajaxEditOptions: { contentType: "application/json" },
                serializeEditData: function (data) {
                    var postData = { 'data': data };
                    return JSON.stringify(postData);;
                },
                beforeShowForm: function (form) {
                    var dlgDiv = $("#editmoddataGrid");
                    var parentDiv = dlgDiv.parent();
                    var dlgWidth = dlgDiv.width();
                    var parentWidth = parentDiv.width();
                    var dlgHeight = dlgDiv.height();
                    var parentHeight = parentDiv.height();
                    dlgDiv[0].style.top = Math.round((parentHeight - dlgHeight) / 1) + "px";
                    dlgDiv[0].style.left = Math.round((parentWidth - dlgWidth) / 1.25) + "px";
                }
            };
        }

        function setPostData() {
            var _result;
            _result = {
                sUID: $("#<%= txtUID.ClientID  %>").val(),
                    sNameOfCaller: $("#<%= txtNameOfCaller.ClientID  %>").val(),
                    sMobileNo: $("#<%= txtMobileNo.ClientID  %>").val(),
                    sSourceId: $("#<%= ddlSource.ClientID  %>").val(),
                    sLanguageId: $("#<%= ddlLanguage.ClientID  %>").val(),
                    sDateFrom: $("#<%= txtFrom.ClientID  %>").val(),
                    sDateTo: $("#<%= txtTo.ClientID  %>").val(),
                    sTicketTypeId: $("#<%= ddlTicketType.ClientID  %>").val(),
                    sKeyword: $("#<%= txtKeyword.ClientID  %>").val(),
                    sTicketStatus: $("#<%= ddlTicketStatus.ClientID  %>").val()
                };
                return _result;
            }
        
            $(document).ready(function () {

                ShowGrid();

                function ShowGrid() {
                    DisplayGrid();
                }

                var lastsel;
                function DisplayGrid() {
                    var grid = $("#dataGrid");
                    $("#dataGrid").jqGrid({
                        // setup custom parameter names to pass to server 
                        prmNames: { search: "isSearch", nd: null, rows: "numRows", page: "page", sort: "sortField", order: "sortOrder" },
                        // add by default to avoid webmethod parameter conflicts 
                        postData: setPostData(),
                        // setup ajax call to webmethod 
                        datatype: function (postdata) {
                            mtype: "GET",
                                $.ajax({
                                    url: 'frmAgDSMvendorTickets.aspx/GetDataFromDB',
                                    type: "POST",
                                    contentType: "application/json; charset=utf-8",
                                    data: JSON.stringify(postdata),
                                    dataType: "json",
                                    success: function (data, st) {
                                        if (st == "success") {
                                            var grid = jQuery("#dataGrid")[0];
                                            grid.addJSONData(JSON.parse(data.d));
                                        }
                                    },
                                    error: function () {
                                        alert("Error with AJAX callback");
                                    }
                                });
                        },
                        // this is what jqGrid is looking for in json callback 
                        jsonReader: {
                            root: "rows",
                            page: "page",
                            total: "totalpages",
                            records: "totalrecords",
                            cell: "cell",
                            id: "0",
                            userdata: "userdata",
                            repeatitems: true
                        },
                        colNames: ['CallId', 'Caller Number', 'Unique ID', 'Caller Name', 'Address', 'Division', 'Complaint From', 'Last Status', 'District', 'USC No', 'PumpSet Type', 'Call Remark', 'Vendor Comment', 'Date Of Call', 'Last Update', 'TAT(Hrs.)', 'History', 'Rectify', 'StateId', 'DistrictId', 'VendorName', 'DateInsert'],
                        colModel: [
                                        { name: 'CallId', index: 'CallId', width: 20, hidden: true },
                                        { name: 'CallerNumber', index: 'CallerNumber', search: false, sortable: false, width: 90, align: 'center' },
                                        { name: 'UID', index: 'UID', sortable: false, search: false, width: 90, align: 'center' },
                                        { name: 'CallerName', index: 'CallerName', search: false, sortable: false, width: 100, align: 'center' },
                                        { name: 'Address', index: 'Address', search: false, sortable: false, width: 120, align: 'center' },
                                        { name: 'Landmark', index: 'Landmark', width: 160, search: false, sortable: false, align: 'center' },
                                        { name: 'ComplaintFrom', index: 'ComplaintFrom', width: 160, search: false, sortable: false, align: 'center' },
                                        { name: 'LastStatus', index: 'LastStatus', sortable: false, search: false, width: 70, align: 'center' },
                                        { name: 'District', index: 'District', search: false, sortable: false, width: 60, align: 'center' },
                                        { name: 'USCNo', index: 'USCNo', width: 80, search: false, sortable: false, align: 'center' },
                                        //{ name: 'AGDSM_AssignedToId', index: 'AGDSM_AssignedToId', width: 85, search: false, sortable: false, align: 'center' },
                                        { name: 'PumpsetType', index: 'PumpsetType', width: 100, search: false, sortable: false, align: 'center' },
                                        { name: 'CallRemark', index: 'CallRemark', width: 100, search: false, sortable: false, align: 'center', },
                                        { name: 'VendorComment', index: 'VendorComment', width: 100, search: false, sortable: false, align: 'center', editable: true, edittype: "textarea", editoptions: { rows: "2", cols: "10" } },
                                        { name: 'DateOfCall', index: 'DateOfCall', width: 80, search: false, sortable: false, align: 'center' },
                                        { name: 'DateUpdate', index: 'DateUpdate', width: 80, search: false, sortable: false, align: 'center' },
                                        { name: 'ComplaintHoursDiff', index: 'ComplaintHoursDiff', width: 80, search: false, sortable: false, align: 'center' },
                                        {
                                            name: 'History', index: 'History', width: 80, search: false, sortable: false, align: 'center',
                                            jsonmap: "CallId",
                                            formatter: function (cellvalue, options, rowObject) {
                                                return "<input type='button' value='History' onclick=ShowHistory('" + rowObject[0] + "')\>";
                                            }
                                        }, //button type col

                                        //{
                                        //    name: 'ReAssign', index: 'ReAssign', width: 80, search: false, sortable: false, align: 'center',
                                        //    jsonmap: "CallId",
                                        //    formatter: function (cellvalue, options, rowObject) {
                                        //        return "<input type='button' value='ReAssign' disabled='' onclick='ShowReAssign(" + rowObject[0] + ")'\>";
                                        //    }
                                        //}, //button type col
                                        {
                                            name: 'Rectify', index: 'Rectify', width: 80, search: false, sortable: false, align: 'center',
                                            jsonmap: "CallId",
                                            formatter: function (cellvalue, options, rowObject) {
                                                if ($('#<%=ddlTicketStatus.ClientID%>').val() == "R") {
                                                    return "<input type='button' value='Rectify' style='display:none' onclick='RectifyClick(" + rowObject[0] + "," + rowObject[1] + ")'\>";
                                                }
                                                else {
                                                    return "<input type='button' value='Rectify' onclick='RectifyClick(" + rowObject[0] + "," + rowObject[1] + ")'\>";
                                                }
                                            }
                                        }, //button type col
                                        { name: 'StateId', index: 'StateId', width: 80, search: false, sortable: false, align: 'center', hidden: true },
                                        { name: 'DistrictId', index: 'DistrictId', width: 80, search: false, sortable: false, align: 'center', hidden: true },
                                        { name: 'VendorName', index: 'VendorName', width: 80, search: false, sortable: false, align: 'center', hidden: true },
                                        { name: 'DateInsert', index: 'DateInsert', width: 80, search: false, sortable: false, align: 'center', hidden: true }
                        ],
                        rowNum: 60,
                        rowList: [60, 120, 200],
                        rownumbers: true,
                        hidegrid: false,
                        pager: '#pager',
                        sortname: "",
                        sortorder: "",
                        multiselect: true,
                        viewrecords: true,
                        height: 375,
                        width: 1230,
                        shrinkToFit: false,
                        forceFit: true,
                        search: false,
                        refresh: false,
                        onSelectRow: function (id) {
                            selectedRowData(id);
                        },
                        emptyrecords: "No records to display",
                        caption: "Vendor Tickets"
                    });
                }
                //$("#dataGrid").jqGrid('setFrozenColumns');
                function selectedRowData(row_id) {
                    //$('#dataGrid').jqGrid('saveRow', row_id).editRow(row_id, true, false, reload);

                    if (row_id && row_id !== lastsel) {
                        //jQuery('#dataGrid').jqGrid('restoreRow', lastsel);
                        var grid = jQuery("#dataGrid");
                        var rowKey = grid.getGridParam("selrow");

                        if ($("#" + row_id).attr("rowEdited") === "true") {
                            // the row having id=rowid is in editing mode
                        }
                        else {
                            $("#" + row_id).attr('rowEdited', 'true');

                            if (rowKey) {
                                var ret = jQuery("#dataGrid").jqGrid('getRowData', rowKey);

                                var stateid = ret.StateId;
                                var districtid = ret.DistrictId;

                                //var be = "<textarea title='Vendor Comment' maxlength='250' id='txtComment" + row_id + "' cols='10' rows='2'></textarea>";
                                //$("#dataGrid").jqGrid('setRowData', row_id, { Remark: be });

                                //$.ajax({
                                //    type: "POST",
                                //    contentType: "application/json; charset=utf-8",
                                //    url: "frmAgDSMvendorTickets.aspx/GetRemarkList",
                                //    data: "",
                                //    dataType: "json",
                                //    success: function (data) {
                                //        var be = "<select id='ddlRemark" + row_id + "' title='Remark'>" + data.d + "</select>";
                                //        $("#dataGrid").jqGrid('setRowData', row_id, { Remark: be });
                                //    },
                                //    error: function (result) {
                                //        alert("No Data Available");
                                //    }
                                //});

                                //setAssignedTo(stateid, districtid, ulbid, row_id);
                                setPumpsetType(row_id);
                            }
                            jQuery('#dataGrid').jqGrid('editRow', row_id, true);
                            lastsel = row_id;
                        }

                    }
                }

                function setAssignedTo(stateid, districtid, ulbid, row_id) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "frmAgDSMvendorTickets.aspx/GetReAssignedTo",
                        data: "{'stateid':'" + stateid + "','districtid':'" + districtid + "','ulbmappingid':'" + ulbid + "'}",
                        dataType: "json",
                        success: function (data) {
                            var be = "<select id='ddlAssignedTo" + row_id + "' title='ReAssigned To'>" + data.d + "</select>";
                            $("#dataGrid").jqGrid('setRowData', row_id, { PumpsetType: be });
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                }

                function setPumpsetType(row_id) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "frmAgDSMvendorTickets.aspx/GetPumpsetType",
                        data: "",
                        dataType: "json",
                        success: function (data) {
                            var be = "<select id='ddlPumpsetType" + row_id + "' title='PumpSet Type'>" + data.d + "</select>";
                            $("#dataGrid").jqGrid('setRowData', row_id, { PumpsetType: be });
                        },
                        error: function (result) {
                            alert("No Match");
                        }
                    });
                }

                function reload(row_id, result) {
                    $("#dataGrid").trigger("reloadGrid");
                }

            });

            function ShowHistory(id) {
                $("#dataGridHistory").jqGrid('setGridParam', {
                    postData: { 'sCallId': id }
                });
                $("#dataGridHistory").trigger("reloadGrid");

                $("#gdCallAssignedHistory").jqGrid('setGridParam', {
                    postData: { 'sCallId': id }
                });
                $("#gdCallAssignedHistory").trigger("reloadGrid");

                BuildHistoryGrid(id);
                BuildAssignedHistoryGrid(id);

                $("#modal_dialog").dialog({
                    height: 600,
                    width: 650,
                    title: "History",
                    buttons: {
                        Close: function () {
                            $(this).dialog('close');
                        }
                    },
                    modal: true
                });
            }

            function ShowReAssign(id) {
                var ret = jQuery("#dataGrid").jqGrid('getRowData', id);
                if (ret.SL_AssignedToId != "--Select--") {
                    var sAssignTo = $('#ddlAssignedTo' + id).val();
                    if (sAssignTo != "0") {
                        var sRemark = $('#' + id + '_VendorComment').val();

                        SaveReAssign(id, sAssignTo, sRemark);
                    }
                    else {
                        alert('Call is not Re-Assigned. Please select Re-Assigned To');
                    }
                }
                else {
                    alert('Call is not Re-Assigned. Please select Re-Assigned To');
                }
            }

            function SaveReAssign(sCallId, sAssignTo, sRemark) {
                $.ajax({
                    type: "POST",
                    url: "frmAgDSMvendorTickets.aspx/SaveReAssign",
                    data: "{ sCallId: '" + sCallId + "', sAssignTo: '" + sAssignTo + "',sRemark: '" + sRemark + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: "true",
                    cache: "false",
                    success: function (msg) {
                        $("#<%= lblMessage.ClientID  %>").text(msg.d);
                    if (msg.d == "Call is Re-Assigned Successfully.") {
                        $("#dataGrid").trigger("reloadGrid");
                    }
                },
                Error: function (x, e) {
                    alert('Failed to update.');
                }
            });
            }

            function RectifyClick(id) {
                var ret = jQuery("#dataGrid").jqGrid('getRowData', id);
                var pumpsetId = "";

                if (ret.PumpsetType != "--Select--") {
                    pumpsetId = $('#ddlPumpsetType' + id).val();
                }
                var sComment = $('#' + id + '_VendorComment').val();
                SaveRectify(id, sComment, pumpsetId);
            }

            function SaveRectify(sCallId, sComment, sPumpsetId) {
                $.ajax({
                    type: "POST",
                    url: "frmAgDSMvendorTickets.aspx/SaveRectify",
                    data: "{ sCallId: '" + sCallId + "', sRemark: '" + sComment + "', sPumpsetId: '" + sPumpsetId+"'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: "true",
                    cache: "false",
                    success: function (msg) {
                        $("#<%= lblMessage.ClientID  %>").text(msg.d);
                    if (msg.d == "Call is Rectified Successfully") {
                        $("#dataGrid").trigger("reloadGrid");
                    }
                },
                Error: function (x, e) {
                    alert('Failed to update.');
                }
            });
            }

            function RectifySelected() {
                var $grid = jQuery("#dataGrid"), i, item,
                ids = $grid.jqGrid("getGridParam", "selarrrow");

                if (ids.length == 0) {
                    alert('No row is selected.');
                }
                else {
                    var conf = confirm('Are you sure! You want to rectify the selected call(s)?');
                    if (conf == true) {
                        for (i = 0; i < ids.length; i++) {
                            //item = $grid.jqGrid("getLocalRow", ids[i]);// This is how to get row data                    
                            RectifyClick(ids[i]);
                            $("#<%= lblMessage.ClientID  %>").text(ids.length + 'record(s) saved successfully');
                }
            }
        }
    }

    function SearchClick() {
        var postdt = setPostData();
        $("#dataGrid").jqGrid('setGridParam', {
            postData: setPostData()
        });
        $("#dataGrid").trigger("reloadGrid");
    }
        
    </script>

    <script type="text/javascript">

        function BuildHistoryGrid(vcallid) {
            $("#dataGridHistory").jqGrid({
                url: 'frmAgDSMvendorTickets.aspx/GetDetailHistory',
                datatype: 'json',
                postData: {
                    sCallId: vcallid
                },
                mtype: 'POST',
                serializeGridData: function (postData) {
                    return JSON.stringify(postData);
                },
                ajaxGridOptions: { contentType: "application/json" },
                loadonce: false,
                colNames: ['HistoryId', 'Date', 'Status', 'Response', 'EscalatedTo'],
                colModel: [
                                { name: 'HistoryId', index: 'HistoryId', width: 80, hidden: true },
                                { name: 'Date', index: 'Date', width: 140 },
                                { name: 'Status', index: 'Status', width: 160 },
                                { name: 'Response', index: 'Response', width: 180 },
                                { name: 'EscalatedTo', index: 'EscalatedTo', width: 180 }
                ],
                pager: '#pagerHistory',
                rowNum: 60,
                rowList: [60, 100, 200],
                viewrecords: true,
                width: 610,
                gridview: true,
                hidegrid: false,
                jsonReader: {
                    page: function (obj) { return 1; },
                    total: function (obj) { return 1; },
                    records: function (obj) { return obj.d.length; },
                    root: function (obj) { return obj.d; },
                    repeatitems: true,
                    id: "0"
                },
                emptyrecords: "No records to display",
                caption: "Call History"
            });
        }

        function BuildAssignedHistoryGrid(vcallid) {
            $("#gdCallAssignedHistory").jqGrid({
                url: 'frmAgDSMvendorTickets.aspx/GetAssignedHistory',
                datatype: 'json',
                postData: {
                    sCallId: vcallid
                },
                mtype: 'POST',
                serializeGridData: function (postData) {
                    return JSON.stringify(postData);
                },
                ajaxGridOptions: { contentType: "application/json" },
                loadonce: false,
                colNames: ['HistoryId', 'Date', 'AssignedTo', 'Remark'],
                colModel: [
                                { name: 'AssignedToHistoryId', index: 'AssignedToHistoryId', width: 80, hidden: true },
                                { name: 'Date', index: 'Date', width: 140 },
                                { name: 'AssignedTo', index: 'AssignedTo', width: 160 },
                                { name: 'Remark', index: 'Remark', width: 180 }//,
                                //{ name: 'CallerNumber', index: 'CallerNumber', width: 180 },
                                //{ name: 'UID', index: 'UID', width: 180 }
                ],
                pager: '#gdPagerCallAssignedHistory',
                rowNum: 60,
                rowList: [60, 100, 200],
                viewrecords: true,
                width: 610,
                gridview: true,
                hidegrid: false,
                jsonReader: {
                    page: function (obj) { return 1; },
                    total: function (obj) { return 1; },
                    records: function (obj) { return obj.d.length; },
                    root: function (obj) { return obj.d; },
                    repeatitems: true,
                    id: "0"
                },
                emptyrecords: "No records to display",
                caption: "Call Assigned History"
            });
        }

    </script>

    <style type="text/css">
        .clearcss {
            background-color:red;
        }
        .circle_rectificationpending {
            border: 2px solid #a1a1a1;
            padding: 3px 8px;
            background: #FEF9E7;
            width: 2px;
            border-radius: 100%;
            margin-left: auto;
            margin-right: auto;
            width: 1%;
        }

        .circle_rectified {
            border: 2px solid #a1a1a1;
            padding: 3px 8px;
            background: #F9E79F;
            width: 2px;
            border-radius: 100%;
            margin-left: auto;
            margin-right: auto;
            width: 1%;
        }

        .circle_verificationpending {
            border: 2px solid #a1a1a1;
            padding: 3px 8px;
            background: #EBF5FB;
            width: 2px;
            border-radius: 100%;
            margin-left: auto;
            margin-right: auto;
            width: 1%;
        }

        .circle_verified {
            border: 2px solid #a1a1a1;
            padding: 3px 8px;
            background: #ABEBC6;
            width: 2px;
            border-radius: 100%;
            margin-left: auto;
            margin-right: auto;
            width: 1%;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="Heading" align="center" style="color: #101010">
                <asp:Label ID="lblPageHeader" runat="server" Text="Search" CssClass="bold"></asp:Label>

            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
        <tr>
            <td colspan="2" align="left" valign="top" width="100%">
                <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
                    <tr>
                        <td>
                            <fieldset class="fieldset">
                                <legend class="Legendheading">Call Search</legend>
                                <table id="tblUIDFilter" runat="server" cellpadding="0" cellspacing="3" style="width: 100%; margin: 0px; padding: 0px;">
                                    <tr>
                                        <td style="width: 10%" class="tdlabel">Caller Number</td>
                                        <td style="width: 15%" class="tdlabel">
                                            <asp:TextBox ID="txtMobileNo" runat="server" MaxLength="20"></asp:TextBox>
                                        </td>
                                        <td colspan="2" style="width: 35%" class="tdlabel">
                                            <asp:TextBox type="text" ID="txtFrom" runat="server" Width="125px" />To<asp:TextBox type="text" ID="txtTo" runat="server" Width="125px" />
                                        </td>
                                        <td style="width: 5%" class="tdlabel">(UID#)</td>
                                        <td style="width: 10%" class="tdlabel">
                                            <asp:TextBox ID="txtUID" runat="server" MaxLength="100" Width="100%"></asp:TextBox>
                                        </td>
                                        <td style="width: 5%" class="tdlabel">Name</td>
                                        <td style="width: 15%" class="tdlabel">
                                            <asp:TextBox ID="txtNameOfCaller" runat="server" Width="100%" MaxLength="50"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" style="width: 100%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%" class="tdlabel">Language</td>
                                        <td style="width: 15%" class="tdlabel">
                                            <asp:DropDownList ID="ddlLanguage" runat="server" Width="150px" />

                                        </td>
                                        <td style="width: 10%" class="tdlabel">Source </td>
                                        <td style="width: 15%" class="tdlabel">
                                            <asp:DropDownList ID="ddlSource" runat="server" Width="150px" />

                                        </td>
                                        <td style="width: 10%" class="tdlabel">Ticket Status </td>
                                        <td style="width: 15%" class="tdlabel">
                                            <asp:DropDownList ID="ddlTicketStatus" runat="server" Width="150px">
                                                <asp:ListItem Selected="True" Value="A">All</asp:ListItem>
                                                <asp:ListItem Value="RP">Pending for Rectification</asp:ListItem>
                                                <asp:ListItem Value="R">Rectified</asp:ListItem>
                                                <asp:ListItem Value="VP">Pending for Verification</asp:ListItem>
                                                <asp:ListItem Value="V">Verified</asp:ListItem>
                                            </asp:DropDownList>

                                        </td>
                                        <td style="width: 10%" class="tdlabel">Ticket Type </td>
                                        <td style="width: 15%" class="tdlabel">
                                            <asp:DropDownList ID="ddlTicketType" runat="server" Width="150px" >
                                                <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="Agri Pumpset" Value="Agri Pumpset"></asp:ListItem>
                                            </asp:DropDownList>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" style="width: 100%" class="tddata">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%" class="tdlabel">Keyword</td>
                                        <td class="tdlabel" colspan="3">
                                            <asp:TextBox ID="txtKeyword" runat="server" MaxLength="100"></asp:TextBox>
                                        </td>
                                        <td style="width: 10%" class="tdlabel">&nbsp;</td>
                                        <td style="width: 15%" class="tdlabel">&nbsp;</td>
                                        <td style="width: 10%" class="tdlabel">&nbsp;</td>
                                        <td style="width: 15%" class="tdlabel" align="right">
                                            <input type="button" style="height: 27px;" value="Search" onclick="SearchClick()" id="btnSearch" />
                                            <%--<asp:Button ID="btnSearch" runat="server" Text="Search" ValidationGroup="Search" AccessKey="S" OnClick="btnSearch_Click" />--%>
                                            &nbsp;<asp:Button ID="btnReset" runat="server" Text="Reset" OnClick="btnReset_Click" />
                                            &nbsp; 
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>
                </table>
                <asp:HiddenField ID="hdnCallEntryId" runat="server" Value="0" />
            </td>
        </tr>
    </table>

    <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
        <tr>
            <td align="left">
                <%--lblmessage is here--%>
            </td>
        </tr>
        <tr>
            <td align="left">
                <table width="100%" cellpadding="3" cellspacing="4">
                    <tr>
                        <td>
                            <input type="button" id="btnRectifySelected" value="Rectify Selected" onclick="RectifySelected()" />
                        </td>
                        <td width="30%">&nbsp;
                        </td>
                        <td width="30%">
                            <asp:Label ID="lblMessage" runat="server" Font-Bold="False" CssClass="failureNotification"></asp:Label>
                            <%--<b>Rectification Pending</b>&nbsp;<span class="circle_rectificationpending"></span>--%></td>
                        <td width="15%"><%--<b>Rectified</b>&nbsp;<span class="circle_rectified"></span>--%></td>
                        <td width="20%"><%--<b>Verification Pending</b>&nbsp;<span class="circle_verificationpending"></span>--%></td>
                        <td width="15%">                            
                            <%--<b>Verified</b>&nbsp;<span class="circle_verified"></span>--%></td>
                        <td width="30%" align="right">
                            <asp:ImageButton ID="imgbtnExportExcel" runat="server" Height="25px" ImageUrl="~/Images/excel.jpg" OnClick="imgbtnExportExcel_Click" ToolTip="Export to Excel" Width="25px" />
                            <%--<asp:Button ID="btnRectifySelected" Text="Rectify Selected" runat="server" OnClientClick="return confirm('Are you sure! You want to rectify the selected call(s)?');" OnClick="btnRectifySelected_Click" />--%>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">

                <table id="dataGrid" style="text-align: center;"></table>
                <div id="pager"></div>
            </td>
        </tr>
        <tr>
            <td>
                <div id="modal_dialog" style="display: none">
                    <table id="dataGridHistory" style="text-align: center;"></table>
                    <div id="pagerHistory"></div>
                    &nbsp;
                    <table id="gdCallAssignedHistory" style="text-align: center;"></table>
                    <div id="gdPagerCallAssignedHistory"></div>
                </div>
            </td>
        </tr>       

    </table>

    <asp:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </asp:ToolkitScriptManager>

    <script type="text/javascript">
        $("#<%= btnReset.ClientID  %>").css("background", "ButtonFace");
        $("#<%= btnReset.ClientID  %>").css("border-color", "black");
        $("#<%= btnReset.ClientID  %>").css("color", "black");
        $("#<%= btnReset.ClientID  %>").css("height", "25px");
        $("#<%= btnReset.ClientID  %>").css("padding", "6px 8px 8px 8px");
        $("#<%= btnReset.ClientID  %>").css({ '-webkit-box-shadow': 'none', '-moz-box-shadow': 'none', 'box-shadow': 'none' });
    </script>
</asp:Content>
