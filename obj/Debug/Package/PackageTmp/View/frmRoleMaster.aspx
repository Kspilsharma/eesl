﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="frmRoleMaster.aspx.cs" Inherits="CRM.View.frmRoleMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="Heading" align="center" style="background-color: #000000; color: #FFFFFF">
                <asp:Label ID="lblPageHeader" runat="server" Text="Role Master" CssClass="bold"></asp:Label>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
        <tr>
            <td align="left" valign="top" width="25%"></td>
            <td valign="top" width="50%">
                <table border="0" width="100%">
                    <tr>
                        <td colspan="2" align="left">
                            <asp:Label ID="lblMessage" runat="server" CssClass="failureNotification"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="tdlabel">
                            <asp:Label ID="lblRole" runat="server" Text="Role"></asp:Label>
                        </td>
                        <td valign="top" align="left" class="tddata">
                            <asp:TextBox ID="txtRole" runat="server" TabIndex="101" Style="text-transform: uppercase"
                                MaxLength="50" Width="150"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="rfvRole"
                                    runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtRole" ValidationGroup="sub"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="tdlabel">
                            <asp:Label ID="Label1" runat="server" Text="Contact No."></asp:Label>
                        </td>
                        <td valign="top" align="left" class="tddata">
                            <asp:TextBox ID="txtContactNo" runat="server" TabIndex="102" Style="text-transform: uppercase" MaxLength="50" Width="150"></asp:TextBox>&nbsp;
                            <asp:RequiredFieldValidator ID="rfvContactNo" runat="server" ErrorMessage="*" ForeColor="Red" ControlToValidate="txtContactNo" ValidationGroup="sub"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">&nbsp;
                        </td>
                        <td align="left">
                            <asp:Button ID="btnSubmit" Text="Submit" TabIndex="103" runat="server" ValidationGroup="sub"
                                OnClick="btnSubmit_Click" />
                            <asp:Button ID="btnReset" Text="Reset" TabIndex="104" runat="server" OnClick="btnReset_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="left">
                            <asp:HiddenField ID="hdnId" runat="server" Value="-1" />
                        </td>
                    </tr>
                </table>
            </td>
            <td align="left" valign="top" width="25%"></td>
        </tr>
        <tr>
            <td align="left" valign="top" width="15%"></td>
            <td valign="top" width="70%">
                <table border="0" width="100%">
                    <tr>
                        <td>
                            <asp:GridView ID="gvRole" Width="80%" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                DataKeyNames="RoleId" TabIndex="104" OnPageIndexChanging="gvRole_PageIndexChanging"
                                OnRowDataBound="gvRole_RowDataBound" OnRowDeleting="gvRole_RowDeleting" OnRowEditing="gvRole_RowEditing"
                                OnRowUpdating="gvRole_RowUpdating" CellPadding="4" ForeColor="#333333" GridLines="None" CssClass="tabStyle">
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Id">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblRoleId" SkinID="lblBlack" runat="server" Text='<%# Bind("RoleId") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Role">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblRole" runat="server" SkinID="lblBlack" Text='<%# Bind("RoleName") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="ContactNo">
                                        <ItemTemplate>
                                            <asp:Label ID="gvlblContactNo" runat="server" SkinID="lblBlack" Text='<%# Bind("ContactNo") %>' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Edit">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnEdit" CommandName="Edit" runat="server" ImageUrl="~/Images/edit.png"
                                                OnClientClick="return confirm('Are you sure! You want to Edit Seleted Row?');" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnStatus" CommandName="Update" runat="server" ImageUrl="~/Images/active.gif"
                                                OnClientClick="return confirm('Are you sure! You want to change status?');" />
                                            <asp:Label ID="gvlblIsActive" runat="server" Text='<%# Bind("IsActive") %>' Visible="False" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Delete" Visible="false">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgbtnDelete" CommandName="Delete" runat="server" ImageUrl="~/Images/delete.gif"
                                                OnClientClick="return confirm('Are you sure! You want to Delete Seleted Row?');" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle BackColor="#2461BF" />
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="Black" HorizontalAlign="Center" />
                                <RowStyle HorizontalAlign="Center" VerticalAlign="Top" BackColor="#EFF3FB" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </td>
            <td align="left" valign="top" width="15%"></td>
        </tr>
    </table>
</asp:Content>
