﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteNew.Master" AutoEventWireup="true" CodeBehind="DashboardDetail.aspx.cs" Inherits="EESL.View.DashboardDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <table cellpadding="2" cellspacing ="2" width="100%">
        <tr>
             <td class="Heading" align="center" style="color: #101010">
                <asp:Label ID="lblPageHeader" runat="server" Text="Dashboard Detail" CssClass="bold"></asp:Label>
            </td>
        </tr>
         <tr>
             <td class="Heading" align="center" style="color: #101010">
                 <table width="100%">
                     <tr>
                         <td align="left">
                             <asp:ImageButton ID="imgbtnBack" ToolTip="Back" ImageUrl="~/Images/back.jpg" Width="25px" Height="25px" runat="server" OnClick="imgbtnBack_Click"  />
                         </td>
                         <td align="right">
                             <asp:ImageButton ID="imgbtnExportExcel" ToolTip="Export to Excel" ImageUrl="~/Images/excel.jpg" Width="25px" Height="25px" runat="server" OnClick="imgbtnExportExcel_Click" />
                         </td>
                     </tr>
                 </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center" style="margin-left: 120px">
                <asp:GridView ID="gvDashboardDetail" TabIndex="106" PageSize="50" Width="100%" runat="server"
                    AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="CallId" EmptyDataText="No Record Found" CellPadding="4"
                    ForeColor="#333333" GridLines="None" CssClass="tabStyle" OnPageIndexChanging="gvDashboardDetail_PageIndexChanging">
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:TemplateField HeaderText="Sr.">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CallId" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="gvlblCallId" runat="server" Text='<%# Bind("CallId") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Caller Number">
                            <ItemTemplate>
                                <asp:Label ID="gvlblCallerNumber" runat="server" Text='<%# Bind("CallerNumber") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Unique ID">
                            <ItemTemplate>
                                <asp:Label ID="gvlblUID" runat="server" Text='<%# Bind("UID") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Category">
                            <ItemTemplate>
                                <asp:Label ID="gvlblCategory" runat="server" Text='<%# Bind("Category") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Scheme">
                            <ItemTemplate>
                                <asp:Label ID="gvlblScheme" runat="server" Text='<%# Bind("Scheme") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date Of Call">
                            <ItemTemplate>
                                <asp:Label ID="gvlblDateOfCall" runat="server"  Text='<%# Eval("DateOfCall", "{0:dd-MM-yyyy}") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Last Status">
                            <ItemTemplate>
                                <asp:Label ID="gvlblLastStatus" runat="server" Text='<%# Bind("LastStatus") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Detail">
                            <ItemTemplate>
                                <asp:Button ID="btnDashboardDetail" Text="Detail" runat="server" OnClick="btnDashboardDetail_Click" />
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                    <EditRowStyle BackColor="#2461BF" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#2461BF" ForeColor="Black" HorizontalAlign="Center" />
                    <RowStyle HorizontalAlign="Center" VerticalAlign="Top" BackColor="#EFF3FB" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                </asp:GridView>
                <br />
                <br />
                <br />
                &nbsp;&nbsp;</td>
        </tr>
    </table>
</asp:Content>
