﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="frmUserProfile.aspx.cs" Inherits="CRM.View.frmUserProfile" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">

        function OnCheckBoxCheckChanged(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                if (nxtSibling && nxtSibling.nodeType == 1)//check if nxt sibling is not null & is an element node
                {
                    if (nxtSibling.tagName.toLowerCase() == "div") //if node has children
                    {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);
            }
        }
        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }
        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;

            if (parentNodeTable) {
                var checkUncheckSwitch;

                if (check) //checkbox checked
                {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }
                else //checkbox unchecked
                {
                    checkUncheckSwitch = true;
                }

                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }
        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) //check if the child node is an element node
                {
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        //                    if (!prevChkBox.checked) {
                        //                        return false;
                        //                    }
                    }
                }
            }
            return true;
        }
        //utility function to get the container of an element by tagname
        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }
    </script>
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="Heading" align="center" style="color: #101010">
                <asp:Label ID="lblPageHeader" runat="server" Text="User Profile" CssClass="bold"></asp:Label>
            </td>
        </tr>
    </table>
    <table cellpadding="0" cellspacing="3px" border="0" style="width: 100%;">
        <tr>
            <td align="left" valign="top" width="25%">
            </td>
            <td valign="top" width="50%">
                <fieldset class="fieldset">
                    <legend class="Legendheading" align="left">User Profile</legend>
                    <table cellpadding="0" cellspacing="3px" width="100%" style="margin: 0px; padding: 0px;">
                        <tr>
                            <td colspan="2">
                                <asp:Label ID="lblMsg" ForeColor="Red" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top">
                                <asp:Label ID="lblRole" runat="server" Text="Role"></asp:Label>
                            </td>
                            <td valign="top" align="left" class="tddata">
                                <asp:DropDownList ID="ddlRole" runat="server" TabIndex="101" AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlRole_SelectedIndexChanged" Width="155px">
                                </asp:DropDownList>
                                &nbsp;<asp:RequiredFieldValidator ID="RFVUSERTYPE" runat="server" ControlToValidate="ddlRole"
                                    InitialValue="Please-Select" ErrorMessage="*" ForeColor="Red" ValidationGroup="sub"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" class="tdlabel" valign="top">
                                Menu List
                            </td>
                            <td valign="top" align="left" class="tddata">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="left" valign="top">
                                &nbsp;
                            </td>
                            <td align="left" class="tddata">
                                <asp:TreeView ID="tvUserProfile" Width="95%" runat="server" ShowCheckBoxes="All"
                                    TabIndex="102" ShowLines="false" ForeColor="Black" CssClass="tddata" 
                                    Height="100%">
                                    <LeafNodeStyle ChildNodesPadding="5px" />
                                </asp:TreeView>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                &nbsp;
                            </td>
                            <td align="left" class="tddata">
                                <asp:Button ID="btnSubmit" Text="Submit" TabIndex="103" runat="server" ValidationGroup="sub"
                                    OnClick="btnSubmit_Click" />
                                &nbsp;<asp:Button ID="btnReset" Text="Reset" TabIndex="104" runat="server" 
                                    OnClick="btnReset_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="left">
                                &nbsp;
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </td>
            <td align="left" valign="top" width="25%">
            </td>
        </tr>
    </table>
</asp:Content>
