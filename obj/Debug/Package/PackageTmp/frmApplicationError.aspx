﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true"
    CodeBehind="frmApplicationError.aspx.cs" Inherits="CRM.frmApplicationError" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tr>
            <td class="Heading" align="center" style="background-color: #000000; color: #FFFFFF">
                <asp:Label ID="lblPageHeader" runat="server" Text="Message" CssClass="bold"></asp:Label>
            </td>
        </tr>
    </table>
    <div id="maindiverror">
        <div id="imagediv">
            <img src="../Images/Error.png" alt="" /><br />
            <span>Oops..... !</span></div>
        <div id="contentdiv">
            <span>There is some error found in the Page you have requested. Problems with this Page
                might prevent it from being displayed properly or functioning properly.</span>
            <br />
            <br />
            <div id="errorlist">
                <b>Please try the following:</b><br />
                1. Try the request after some time.
                <br />
                2. Try another link.
                <br />
            </div>
        </div>
    </div>
</asp:Content>
