﻿using CRM.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace EESL.Controllers
{
    public class CommonController : ApiController
    {
        private Object thisLock = new Object();

        //Get Compliant Details By Mobile No
        [HttpGet]
        public IList<Compliant> GetCompliantByNumber(string Number, string StatusValue)
        {
            IList<Compliant> comlist = new List<Compliant>();
            string Sql = string.Empty;
            string retunmsg = string.Empty;
            if (!string.IsNullOrEmpty(Number))
            {
                //long n;
               // bool isNumeric = long.TryParse(value, out n);
               // if (isNumeric)
                {
                    Sql = @"Select CDM.CallID,CDM.UID,CBIM.CallerName,CDM.CallerNumber,DateOfCall as CallDate,StatusOfCall as Status,Scheme,
                          ((Select top 1 Remark from CallDetailHistory where CallId = CDM.CallID order by DateInsert desc)) as Remark,CBIM.Address
                          from CallDetailMaster CDM inner join CallBasicInfoMaster CBIM on CBIM.UID = CDM.UID inner join StatusOfCall SOC
                          on CDM.CurrentStatusId = SOC.StatusOfCallId inner join Scheme SC on SC.SchemeId = CDM.SchemeId 
                          where CDM.CallerNumber = '" + Number + "'";
                }
                string addtional = string.Empty;
                if (StatusValue == "2")
                {
                    Sql += " and StatusOfCall = 'Open'";
                }
                else if (StatusValue == "3")
                {
                    Sql += " and StatusOfCall = 'Closed'";
                }

            }

            Sql += " order by DateOfCall desc";

            DataTable dt = SQLDBHelper.GetDataInTable(Sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                comlist = (from DataRow dr in dt.Rows
                           select new Compliant()
                           {
                               CallId = Convert.ToInt32(dr["CallID"].ToString()),
                               UID = dr["UID"].ToString(),
                               CallerName = dr["CallerName"].ToString(),
                               CallerNo = dr["CallerNumber"].ToString(),
                               Address = dr["Address"].ToString(),
                               DateOfCall = Convert.ToDateTime(dr["CallDate"].ToString()),
                               Scheme = dr["Scheme"].ToString(),
                               Status = dr["Status"].ToString(),
                               Remark = dr["Remark"].ToString()
                           }).ToList();
            }
            else
            {
                comlist = new List<Compliant>();
            }
            return comlist;
        }

        //Get Compliant Details By UID
        [HttpGet]
        public IList<Compliant> GetCompliantByUID(string UID)
        {
            IList<Compliant> comlist = new List<Compliant>();
            string Sql = @"Select CDM.CallID,CDM.UID,CBIM.CallerName,CDM.CallerNumber,DateOfCall as CallDate,StatusOfCall as Status,Scheme,
                          ((Select top 1 Remark from CallDetailHistory where CallId = CDM.CallID order by DateInsert desc)) as Remark
                          from CallDetailMaster CDM inner join CallBasicInfoMaster CBIM on CBIM.UID = CDM.UID inner join StatusOfCall SOC
                          on CDM.CurrentStatusId = SOC.StatusOfCallId inner join Scheme SC on SC.SchemeId = CDM.SchemeId 
                          where CDM.UID = '" + UID + "'";

            DataTable dt = SQLDBHelper.GetDataInTable(Sql);
            if (dt != null && dt.Rows.Count > 0)
            {
                comlist = (from DataRow dr in dt.Rows
                           select new Compliant()
                           {
                               CallId = Convert.ToInt32(dr["CallID"].ToString()),
                               UID = dr["UID"].ToString(),
                               CallerName = dr["CallerName"].ToString(),
                               CallerNo = dr["CallerNumber"].ToString(),
                               DateOfCall = Convert.ToDateTime(dr["CallDate"].ToString()),
                               Scheme = dr["Scheme"].ToString(),
                               Status = dr["Status"].ToString(),
                               Remark = dr["Remark"].ToString()
                           }).ToList();
            }
            return comlist;
        }

        //Post Compliant Details
        [HttpPost]
        public string PostCompliant([FromBody] Call value)
        {
            string CompliantNo = string.Empty;
            ReturnMessage returnmsg = new ReturnMessage();
            try
            {
                string strAddress = string.Empty;
                string strAddressBL = string.Empty;
                string strSMSAddress = string.Empty;
                string tErrorMsg = string.Empty;
                string tDMLType = string.Empty;
                Call objCall = new Call();
                objCall.CallerName = Convert.ToString(value.CallerName.Trim().ToUpper());
                objCall.Address = Convert.ToString(value.Address.Trim());
                objCall.Landmark = Convert.ToString(value.Landmark.Trim());
                objCall.CallerNumber = Convert.ToString(value.CallerNumber.Trim());
                objCall.Remark = Convert.ToString(value.Remark.Trim()).ToUpper();
                //objCall.LanguageId = ddlLanguage.SelectedIndex == 0 ? 0 : Convert.ToInt32(ddlLanguage.SelectedItem.Value);
                objCall.LanguageId = Convert.ToInt32(SQLDBHelper.getFieldValueByCondition("Languages", "LanguageId", "Language", "English"));
                objCall.StateId = Convert.ToInt32(SQLDBHelper.getFieldValueByCondition("State", "StateId", "StateId", value.StateId.ToString()));
                objCall.DistrictId = Convert.ToInt32(SQLDBHelper.getFieldValueByCondition("District", "DistrictId", "DistrictId", value.DistrictId.ToString()));
                objCall.SourceId = Convert.ToInt32(SQLDBHelper.getFieldValueByCondition("Source", "SourceId", "Source", "Mobile"));
                objCall.SchemeId = Convert.ToInt32(SQLDBHelper.getFieldValueByCondition("Scheme", "SchemeId", "SchemeId", value.SchemeId.ToString()));
                if (objCall.SchemeId == 1)
                {
                    if (value.SL_LightTypeId == 2)
                    {
                        objCall.CallCategoryId = 1;
                        objCall.StatusOfCallId = 5;
                    }
                    else
                    {
                        objCall.CallCategoryId = Convert.ToInt32(SQLDBHelper.getFieldValueByCondition("CallCategory", "CallCategoryId", "CallCategory", "Complaint"));
                        objCall.StatusOfCallId = Convert.ToInt32(SQLDBHelper.getFieldValueByCondition("StatusOfCall", "StatusOfCallId", "StatusOfCall", "Open"));
                    }
                }
                else
                {
                    objCall.CallCategoryId = Convert.ToInt32(SQLDBHelper.getFieldValueByCondition("CallCategory", "CallCategoryId", "CallCategory", "Complaint"));
                    objCall.StatusOfCallId = Convert.ToInt32(SQLDBHelper.getFieldValueByCondition("StatusOfCall", "StatusOfCallId", "StatusOfCall", "Open"));
                }

                objCall.EscalatedToId = 0;
                if (value.SL_ULBId > 0)
                    objCall.SL_ULBId = Convert.ToInt32(SQLDBHelper.getFieldValueByCondition("ULBMapping", "ULBMappingId", "ULBMappingId", value.SL_ULBId.ToString()));

                if (value.SL_ZoneId > 0)
                {
                    objCall.SL_ZoneId = Convert.ToInt32(SQLDBHelper.getFieldValueByCondition("ZoneMapping", "ZoneMappingId", "ZoneMappingId", value.SL_ZoneId.ToString()));
                }
                objCall.SL_WardNo = value.SL_WardNo;
                objCall.SL_PoleNo = value.SL_PoleNo;

                objCall.SL_AreaType = value.SL_AreaType;
                objCall.SL_LightTypeId = value.SL_LightTypeId == 0 ? 0 : Convert.ToInt32(value.SL_LightTypeId);

                objCall.SL_TicketTypeId = value.SL_TicketTypeId == 0 ? 0 : Convert.ToInt32(value.SL_TicketTypeId);

                objCall.SL_AssignedToId = value.SL_AssignedToId;
                objCall.SL_ComplaintFromId = Convert.ToInt32(SQLDBHelper.getFieldValueByCondition("ComplaintFrom", "ComplaintFromId", "ComplaintFrom", "Resident"));

                objCall.UJ_DiscomId = value.UJ_DiscomId;
                objCall.UJ_Email = value.UJ_Email;
                objCall.UJ_Town = value.UJ_Town;
                objCall.UJ_ConsumerNo = value.UJ_ConsumerNo;
                string sProducts = "";
                string strProducts = "";

                
                //strProducts += chklstUJProducts.Items[i].Text.ToString() + ",";
               
                //if (sProducts != "")
                //{
                //    sProducts = sProducts.Substring(0, sProducts.Length - 1);
                //    strProducts = strProducts.Substring(0, strProducts.Length - 1);
                //}
                objCall.UJ_Products = value.UJ_Products;

                objCall.AGDSM_Town = value.AGDSM_Town;
                objCall.AGDSM_DistributionCenter = value.AGDSM_DistributionCenter;
                objCall.AGDSM_ConsumerNo = value.AGDSM_ConsumerNo;

                objCall.BL_BuildingId = value.BL_BuildingId;
                objCall.BL_FlatNo = value.BL_FlatNo;
                objCall.BL_ReferenceNo = value.BL_ReferenceNo;
                if (string.IsNullOrEmpty(value.BL_AssignedToIds))
                {
                    String strQuery = "SELECT DISTINCT VPM.VendorMappingId"
                             + " FROM[dbo].[VendorMapping] VM INNER JOIN[dbo].[VendorProductMapping] VPM"
                             + " ON VM.VendorMappingId = VPM.VendorMappingId "
                             + " WHERE StateId = "+ value.StateId +" AND DistrictId = "+ value.DistrictId +" AND VPM.ProductTypeId IN ('"+ value.BL_ProductTypes + "')";
                    DataTable dtAssign = SQLDBHelper.GetDataInTable(strQuery);
                    if (dtAssign.Rows.Count > 0)
                    {
                        string AssignToIds = dtAssign.Rows[0][0].ToString();
                        value.BL_AssignedToIds = AssignToIds;
                    }
                    objCall.BL_AssignedToIds = value.BL_AssignedToIds;
                }

                string sProductTypes = "";
                string strProductTypes = "";
                string sBLProductTypes = "";
                string strBLProductTypes = "";
                string sBLACProductTypes = "";
                string strBLACProductTypes = "";
                string producttype = string.Empty, producttypeId = string.Empty;
                DataTable productTypes = GetProductTypes();


                sProductTypes = value.BL_ProductTypes;
                sBLProductTypes = value.BL_LEDProductTypes;
                sBLACProductTypes = value.BL_ACProductTypes;
                objCall.BL_ProductTypesQty = "";

                string[] ProductStr = null;
                if (sProductTypes.Length > 0)
                {
                    ProductStr = sProductTypes.Split(',');
                    objCall.BL_ProductTypesQty = "1";
                }

                //for (int i = 0; i < chklstBLProductTypes.Items.Count; i++)
                //{
                //    producttype = value.BLProductTypes;
                //    producttypeId = chklstBLProductTypes.Items[i].Value.ToString();

                // if (chklstBLProductTypes.Items[i].Selected == true)

                if (ProductStr != null && ProductStr.Length > 0)
                {
                    string StrPiD = string.Empty;
                    foreach(string str in ProductStr)
                    {
                        
                        int i;
                        bool IsNumeric = int.TryParse(str, out i);
                        if (IsNumeric)
                        {
                            DataRow[] prodTypes = productTypes.Select("[ProductTypeId] = " + Convert.ToInt32(str));
                            if (prodTypes.Count() > 0)
                            {
                                if (Convert.ToInt32(prodTypes[0]["VendorTypeId"]) == 3)
                                {
                                    sBLProductTypes += producttypeId + ",";
                                    strBLProductTypes += producttype + ",";
                                }
                                else if (Convert.ToInt32(prodTypes[0]["VendorTypeId"]) == 4)
                                {
                                    sBLACProductTypes += producttypeId + ",";
                                    strBLACProductTypes += producttype + ",";
                                }
                            }

                            sProductTypes += producttypeId + ",";
                            strProductTypes += producttype + ",";
                        }
                        if (string.IsNullOrEmpty(StrPiD))
                            StrPiD = "1";
                        else
                            StrPiD = StrPiD + "," + "1";

                    }
                    objCall.BL_ProductTypesQty = StrPiD;

                }


                if (sProductTypes != "null" && !string.IsNullOrEmpty(sProductTypes))
                {
                    sProductTypes = sProductTypes.Substring(0, sProductTypes.Length - 1);
                    //strProductTypes = strProductTypes.Substring(0, strProductTypes.Length - 1);
                }

                if (sBLProductTypes != "null" && !string.IsNullOrEmpty(sBLProductTypes))
                {
                    sBLProductTypes = sBLProductTypes.Substring(0, sBLProductTypes.Length - 1);
                    //strBLProductTypes = strBLProductTypes.Substring(0, strBLProductTypes.Length - 1);
                }

                if (sBLACProductTypes != "null" && !string.IsNullOrEmpty(sBLACProductTypes))
                {
                    sBLACProductTypes = sBLACProductTypes.Substring(0, sBLACProductTypes.Length - 1);
                    //strBLACProductTypes = strBLACProductTypes.Substring(0, strBLACProductTypes.Length - 1);
                }

                objCall.BL_ProductTypes = sProductTypes;
                objCall.BL_LEDProductTypes = sBLProductTypes;
                objCall.BL_ACProductTypes = sBLACProductTypes;

                //Common Address
                string ULBName = string.Empty;
                string StateName = SQLDBHelper.getFieldValueByCondition("State", "State", "StateId", objCall.StateId.ToString());
                string DistrictName = SQLDBHelper.getFieldValueByCondition("District", "District", "DistrictId", objCall.DistrictId.ToString());
                string SchemeName = SQLDBHelper.getFieldValueByCondition("Scheme", "Scheme", "SchemeId", objCall.SchemeId.ToString());
                if (value.SL_ULBId > 0)
                    ULBName = SQLDBHelper.getFieldValueByCondition("ULBMapping", "ULB", "ULBMappingId", objCall.SL_ULBId.ToString());
                string DiscomName = SQLDBHelper.getFieldValueByCondition("DiscomMapping", "Discom", "DiscomMappingId", value.UJ_DiscomId.ToString());
                string BuildingName = SQLDBHelper.getFieldValueByCondition("BuildingMapping", "Building", "BuildingMappingId", value.BL_BuildingId.ToString());
                strAddress = value.Address.Trim().ToUpper() + "<br />" +
                            "Landmark : " + value.Landmark.Trim().ToUpper() + "<br />" +
                            "State : " + StateName + "<br />" +
                            "District : " + DistrictName + "<br/>";


                //Scheme Detail Address Start (Street Light,Ujala,AGDSM,Building)
                if (SchemeName == "Street Light")
                {
                    strAddress += "ULB : " + ULBName + "<br />" +
                           "Zone : <br />" +
                           "Ward No : " + value.SL_WardNo.ToUpper() + "<br />" +
                           "Pole No : " + value.SL_PoleNo.ToUpper() + "<br />";

                    strSMSAddress = "Landmark : " + value.Landmark.Trim().ToUpper() + ", Pole No : " + value.SL_PoleNo.ToUpper();


                }
                else if (SchemeName == "UJALA/NEEFP")
                {
                    strAddress += "Discom : " + DiscomName + "<br />" +
                             "Email : " + value.UJ_Email.ToUpper() + "<br />" +
                             "Town : " + value.UJ_Town.ToUpper() + "<br />" +
                             "Consumer No : " + value.UJ_ConsumerNo.ToUpper() + "<br />" +
                             "Products : " + strProducts + "<br />";
                    strSMSAddress = "";
                }
                else if (SchemeName == "AGDSM")
                {
                    strAddress += "Town : " + value.AGDSM_Town.ToUpper() + "<br />" +
                             "Distribution Center : " + value.AGDSM_DistributionCenter.ToUpper() + "<br />" +
                             "Consumer No : " + value.AGDSM_ConsumerNo.ToUpper();
                    strSMSAddress = "";
                }
                else if (SchemeName == "Building")
                {
                    strAddress += "Building : " + BuildingName + "<br />" +
                     "Flat No : " + value.BL_FlatNo.ToUpper() + "<br />" +
                     "Reference No : " + value.BL_ReferenceNo.ToUpper() + "<br />" +
                     "Product Type : " + strBLProductTypes;

                    strAddressBL += "Building : " + BuildingName + "<br />" +
                             "Flat No : " + value.BL_FlatNo.ToUpper() + "<br />" +
                             "Reference No : " + value.BL_ReferenceNo.ToUpper() + "<br />" +
                             "Product Type : " + strBLACProductTypes;
                    strSMSAddress = "";
                }

                objCall.ImageBase64 = value.ImageBase64;
                objCall.Longitude = value.Longitude;
                objCall.Latitude = value.Latitude;
                objCall.IsMobile = true;
                // Scheme Detail Address End
                //ViewState["StateId"] = objCall.StateId;
                //ViewState["DistrictId"] = objCall.DistrictId;
                //ViewState["SchemeId"] = objCall.SchemeId;
                //ViewState["ULBId"] = objCall.SL_ULBId;
                //ViewState["ZoneId"] = objCall.SL_ZoneId;
                //ViewState["AssignToId"] = objCall.SL_AssignedToId;
                //ViewState["DiscomId"] = objCall.UJ_DiscomId;
                //ViewState["BuildingId"] = objCall.BL_BuildingId;

               // if (txtCaptcha.Text == this.Session["CaptchaImageText"].ToString())
                {
                    lock (thisLock)
                    {
                        if (objCall.ConsumerEntry_Insert(out tErrorMsg, out tDMLType))
                        {
                            returnmsg.CompliantNo = tErrorMsg;
                            returnmsg.Status = "200";
                            returnmsg.ErrorMessage = "";

                            int callStatus = Convert.ToInt32(SQLDBHelper.getFieldValueByCondition("StatusOfCall", "StatusOfCallId", "StatusOfCall", "Open"));
                            string strCallStatus = Convert.ToString(SQLDBHelper.getFieldValueByCondition("StatusOfCall", "StatusOfCall", "StatusOfCall", "Open"));

                            DataTable allNOEmailMobileNos = GetNOEmailMobileNo();
                            List<string> noToEmail = new List<string>();
                            List<string> noToMobileNo = new List<string>();
                            if (allNOEmailMobileNos.Rows.Count > 0)
                            {
                                DataRow[] noEmailMobileNos = allNOEmailMobileNos.Select("[SchemeId] = " + objCall.SchemeId + " AND [StateId] = " + objCall.StateId + " AND [DistrictId] = " + objCall.DistrictId);
                                foreach (DataRow row in noEmailMobileNos)
                                {
                                    string noEmail = Convert.ToString(row["NOEmail"]);
                                    if (!string.IsNullOrEmpty(noEmail)) { noToEmail.Add(noEmail); }
                                    string noMobileNo = Convert.ToString(row["NOMobileNo"]);
                                    if (!string.IsNullOrEmpty(noMobileNo)) { noToMobileNo.Add(noMobileNo); }
                                }
                            }

                            DataTable allVendorEmailMobileNos = GetVendorEmailMobileNo();
                            List<string> vendorToEmail = new List<string>();
                            List<string> vendorToMobileNo = new List<string>();
                            List<string> vendorToEmailBLAC = new List<string>();
                            List<string> vendorToMobileNoBLAC = new List<string>();
                            if (allVendorEmailMobileNos.Rows.Count > 0)
                            {
                                DataRow[] vendorEmailMobileNos;
                                if (SchemeName == "Building")
                                {
                                    string selectedVendorIds = string.Join("','", objCall.BL_AssignedToIds.Split(','));
                                    vendorEmailMobileNos = allVendorEmailMobileNos.Select("VendorMappingId IN ('" + selectedVendorIds + "')");
                                    foreach (DataRow row in vendorEmailMobileNos)
                                    {

                                        if (Convert.ToString(row["VendorType"]) == "3")
                                        {
                                            string vendorEmail = Convert.ToString(row["VendorEmail"]);
                                            if (!string.IsNullOrEmpty(vendorEmail)) { vendorToEmail.Add(vendorEmail); }
                                            string vendorMobileNo = Convert.ToString(row["VendorMobileNo"]);
                                            if (!string.IsNullOrEmpty(vendorMobileNo)) { vendorToMobileNo.Add(vendorMobileNo); }
                                        }
                                        else if (Convert.ToString(row["VendorType"]) == "4")
                                        {
                                            string vendorEmail = Convert.ToString(row["VendorEmail"]);
                                            if (!string.IsNullOrEmpty(vendorEmail)) { vendorToEmailBLAC.Add(vendorEmail); }
                                            string vendorMobileNo = Convert.ToString(row["VendorMobileNo"]);
                                            if (!string.IsNullOrEmpty(vendorMobileNo)) { vendorToMobileNoBLAC.Add(vendorMobileNo); }
                                        }
                                    }
                                }
                                else
                                {
                                    vendorEmailMobileNos = allVendorEmailMobileNos.Select("VendorMappingId = " + objCall.SL_AssignedToId);
                                    foreach (DataRow row in vendorEmailMobileNos)
                                    {
                                        string vendorEmail = Convert.ToString(row["VendorEmail"]);
                                        if (!string.IsNullOrEmpty(vendorEmail)) { vendorToEmail.Add(vendorEmail); }
                                        string vendorMobileNo = Convert.ToString(row["VendorMobileNo"]);
                                        if (!string.IsNullOrEmpty(vendorMobileNo)) { vendorToMobileNo.Add(vendorMobileNo); }
                                    }
                                }
                            }

                            string roleContactNo = Convert.ToString(SQLDBHelper.getFieldValueByCondition("RoleMaster", "ContactNo", "RoleName", "Consumer"));

                            DataTable smsEmailSetting = GetSMSEmailSettings();
                            //Nodal Officer
                            DataRow[] noSetting = smsEmailSetting.Select("[StatusOfCallId] = " + callStatus + " AND [SettingTypeId] = " + Convert.ToInt32(SettingType.NodalOfficer));
                            if (noSetting.Count() > 0)
                            {
                                if (Convert.ToBoolean(noSetting[0]["SendSMS"]) && noToMobileNo.Count > 0)
                                {
                                    SendSMS(Convert.ToInt32(SettingType.NodalOfficer), objCall.SchemeId, "", callStatus, string.Join(";", noToMobileNo), tErrorMsg.Trim(), roleContactNo, strSMSAddress);
                                }
                                if (Convert.ToBoolean(noSetting[0]["SendEmail"]) && noToEmail.Count > 0)
                                {
                                    SendEmail(Convert.ToInt32(SettingType.NodalOfficer), objCall.SchemeId, "", callStatus, string.Join(";", noToEmail), strCallStatus, tErrorMsg.Trim(), value.CallerName.Trim().ToUpper(), value.CallerNumber.Trim(), "Complaint", value.Remark.Trim(), Convert.ToString(value.Remark.Trim()).ToUpper(), strAddress);
                                }
                            }

                            //Vendor
                            DataRow[] vendorSetting = smsEmailSetting.Select("[StatusOfCallId] = " + callStatus + " AND [SettingTypeId] = " + Convert.ToInt32(SettingType.Vendor));
                            if (vendorSetting.Count() > 0)
                            {
                                if (SchemeName == "Building")
                                {
                                    if (Convert.ToBoolean(vendorSetting[0]["SendSMS"]) && vendorToMobileNo.Count > 0)
                                    {
                                        SendSMS(Convert.ToInt32(SettingType.Vendor), objCall.SchemeId, "3", callStatus, string.Join(";", vendorToMobileNo), tErrorMsg.Trim(), roleContactNo, strSMSAddress);
                                    }
                                    if (Convert.ToBoolean(vendorSetting[0]["SendEmail"]) && vendorToEmail.Count > 0)
                                    {
                                        SendEmail(Convert.ToInt32(SettingType.Vendor), objCall.SchemeId, "3", callStatus, string.Join(";", vendorToEmail), strCallStatus, tErrorMsg.Trim(), value.CallerName.Trim().ToUpper(), value.CallerNumber.Trim(), "Complaint", SchemeName.Trim(), Convert.ToString(value.Remark.Trim()).ToUpper(), strAddress);
                                    }

                                    if (Convert.ToBoolean(vendorSetting[0]["SendSMS"]) && vendorToMobileNoBLAC.Count > 0)
                                    {
                                        SendSMS(Convert.ToInt32(SettingType.Vendor), objCall.SchemeId, "4", callStatus, string.Join(";", vendorToMobileNoBLAC), tErrorMsg.Trim(), roleContactNo, strSMSAddress);
                                    }
                                    if (Convert.ToBoolean(vendorSetting[0]["SendEmail"]) && vendorToEmailBLAC.Count > 0)
                                    {
                                        SendEmail(Convert.ToInt32(SettingType.Vendor), objCall.SchemeId, "4", callStatus, string.Join(";", vendorToEmailBLAC), strCallStatus, tErrorMsg.Trim(), value.CallerName.Trim().ToUpper(), value.CallerNumber.Trim(), "Complaint", SchemeName.Trim(), Convert.ToString(value.Remark.Trim()).ToUpper(), strAddressBL);
                                    }
                                }
                                else
                                {
                                    if (Convert.ToBoolean(vendorSetting[0]["SendSMS"]) && vendorToMobileNo.Count > 0)
                                    {
                                        SendSMS(Convert.ToInt32(SettingType.Vendor), objCall.SchemeId, "", callStatus, string.Join(";", vendorToMobileNo), tErrorMsg.Trim(), roleContactNo, strSMSAddress);
                                    }
                                    if (Convert.ToBoolean(vendorSetting[0]["SendEmail"]) && vendorToEmail.Count > 0)
                                    {
                                        SendEmail(Convert.ToInt32(SettingType.Vendor), objCall.SchemeId, "", callStatus, string.Join(";", vendorToEmail), strCallStatus, tErrorMsg.Trim(), value.CallerName.Trim().ToUpper(), value.CallerNumber.Trim(), "Complaint", SchemeName.Trim(), Convert.ToString(value.Remark.Trim()).ToUpper(), strAddress);
                                    }
                                }
                            }

                            //Customer
                            DataRow[] customerSetting = smsEmailSetting.Select("[StatusOfCallId] = " + callStatus + " AND [SettingTypeId] = " + Convert.ToInt32(SettingType.Customer));
                            if (customerSetting.Count() > 0)
                            {
                                if (Convert.ToBoolean(customerSetting[0]["SendSMS"]) && !string.IsNullOrEmpty(value.CallerNumber))
                                {
                                    SendSMS(Convert.ToInt32(SettingType.Customer), objCall.SchemeId, "", callStatus, string.Join(";", value.CallerNumber.Trim()), tErrorMsg.Trim(), roleContactNo, strSMSAddress);
                                }
                                if (Convert.ToBoolean(customerSetting[0]["SendEmail"]) && !string.IsNullOrEmpty(value.UJ_Email))
                                {
                                    SendEmail(Convert.ToInt32(SettingType.Customer), objCall.SchemeId, "", callStatus, string.Join(";", value.UJ_Email.Trim()), strCallStatus, tErrorMsg.Trim(), value.CallerName.Trim().ToUpper(), value.CallerNumber.Trim(), "Complaint", SchemeName, Convert.ToString(value.Remark.Trim()).ToUpper(), strAddress);
                                }
                            }

                            //lblModelUID.Text = tErrorMsg;
                           // modelUID.Show();
                            //ResetUI();
                        }
                        else
                        {
                            //lblPopUpMessage.Text = tErrorMsg;
                            //modelMessage.Show();
                            //ResetUI();
                            returnmsg.CompliantNo = "";
                            returnmsg.Status = "409";
                            returnmsg.ErrorMessage = tErrorMsg;
                        }
                    }
                    //ViewState["StateId"] = 0;
                    //ViewState["DistrictId"] = 0;
                    //ViewState["SchemeId"] = 0;
                    //ViewState["ULBId"] = 0;
                    //ViewState["ZoneId"] = 0;
                    //ViewState["AssignToId"] = 0;
                    //ViewState["DiscomId"] = 0;
                    //ViewState["BuildingId"] = 0;

                    //lblMessage.Text = tErrorMsg;
                }
                //else
                //{
                //    //ResetUI();
                //    AutoFillUI();
                //    lblCaptchaMsg.Text = "Captcha code is not valid.";
                //    txtCaptcha.Text = "";
                //    txtCaptcha.Focus();

                //}
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
            }

            JavaScriptSerializer js = new JavaScriptSerializer();
            CompliantNo = js.Serialize(returnmsg);
            return CompliantNo;
        }
        
        public class ReturnMessage
        {
            public string CompliantNo;
            public string Status;
            public string ErrorMessage;
        }

        public class Compliant
        {
            public int CallId;
            public string UID;
            public string CallerName;
            public string CallerNo;
            public string Address;
            public DateTime DateOfCall;
            public string Scheme;
            public string Status;
            public string Remark;
        }

        private DataTable GetVendorEmailMobileNo()
        {
            DataTable emailMobileNo = new DataTable();
            try
            {
                if (HttpRuntime.Cache["VendorEmailMobileNo"] == null)
                {
                    Call tCall = new Call();
                    HttpRuntime.Cache["VendorEmailMobileNo"] = tCall.VendorEmailMobileSelectAll().Tables[0];
                }
                emailMobileNo = (DataTable)HttpRuntime.Cache["VendorEmailMobileNo"];
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
            }
            return emailMobileNo;
        }

        private DataTable GetNOEmailMobileNo()
        {
            DataTable emailMobileNo = new DataTable();
            try
            {
                if (HttpRuntime.Cache["NodalOfficerEmailMobileNo"] == null)
                {
                    Call tCall = new Call();
                    HttpRuntime.Cache["NodalOfficerEmailMobileNo"] = tCall.NodalOfficerEmailMobileSelectAll().Tables[0];
                }
                emailMobileNo = (DataTable)HttpRuntime.Cache["NodalOfficerEmailMobileNo"];
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
            }
            return emailMobileNo;
        }

        private DataTable GetSMSEmailSettings()
        {
            DataTable setting = new DataTable();
            try
            {
                if (HttpRuntime.Cache["SMSEmailConfigurationSettings"] == null)
                {
                    SMSEmailConfigurationSetting tSMSEmailConfigurationSetting = new SMSEmailConfigurationSetting();
                    HttpRuntime.Cache["SMSEmailConfigurationSettings"] = tSMSEmailConfigurationSetting.SelectAll().Tables[0];
                }
                setting = (DataTable)HttpRuntime.Cache["SMSEmailConfigurationSettings"];
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
            }
            return setting;
        }

        private DataTable GetProductTypes()
        {
            DataTable productTypes = new DataTable();
            try
            {
                if (HttpRuntime.Cache["ProductTypes"] == null)
                {
                    Call tCall = new Call();
                    HttpRuntime.Cache["ProductTypes"] = tCall.ProductTypesSelectAll().Tables[0];
                }
                productTypes = (DataTable)HttpRuntime.Cache["ProductTypes"];
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
            }
            return productTypes;
        }

        private void SendEmail(int settingType, int schemeId, string vendorType, int callStatus, string toEmail, string strCallStatus, string uid, string customerName, string customerNo, string complaintType, string schemeName, string remark, string address)
        {
            try
            {
                if (HttpRuntime.Cache["SMSEmailTemplates"] == null)
                {
                    SMSEmailTemplates tSMSEmailTemplates = new SMSEmailTemplates();
                    HttpRuntime.Cache["SMSEmailTemplates"] = tSMSEmailTemplates.SelectAll().Tables[0];
                }

                DataTable templates = (DataTable)HttpRuntime.Cache["SMSEmailTemplates"];
                DataRow[] result = templates.Select("[StatusOfCallId] = " + callStatus + " AND [SettingTypeId] = " + settingType);

                string subject = string.Empty, body = string.Empty;
                subject = Convert.ToString(result[0]["EmailTemplateSubject"]);
                subject = subject.Replace("{Status}", strCallStatus);
                subject = subject.Replace("{ComplaintType}", complaintType);
                subject = subject.Replace("{SchemeName}", schemeName);
                subject = subject.Replace("{UID}", uid);

                body = Convert.ToString(result[0]["EmailTemplateBody"]);
                body = body.Replace("{UID}", uid);
                body = body.Replace("{CustomerName}", customerName);
                body = body.Replace("{CustomerNumber}", customerNo);
                body = body.Replace("{ComplaintType}", complaintType);
                body = body.Replace("{CallDescription}", remark);
                body = body.Replace("{CallDescription}", remark);
                body = body.Replace("{Address}", address);

                EmailUtils.SendEmail(uid, callStatus, settingType, schemeId, vendorType, subject, body, toEmail);
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
            }
        }

        private void SendSMS(int settingType, int schemeId, string vendorType, int callStatus, string toSMS, string uid, string contactNo, string address)
        {
            try
            {
                if (HttpRuntime.Cache["SMSEmailTemplates"] == null)
                {
                    SMSEmailTemplates tSMSEmailTemplates = new SMSEmailTemplates();
                    HttpRuntime.Cache["SMSEmailTemplates"] = tSMSEmailTemplates.SelectAll().Tables[0];
                }

                DataTable templates = (DataTable)HttpRuntime.Cache["SMSEmailTemplates"];
                DataRow[] result = templates.Select("[StatusOfCallId] = " + callStatus + " AND [SettingTypeId] = " + settingType);

                string body = string.Empty;
                body = Convert.ToString(result[0]["SMSTemplateBody"]);
                body = body.Replace("{UID}", uid);
                body = body.Replace("{ContactNo}", contactNo);
                body = body.Replace("{Address}", address);

                if (!string.IsNullOrEmpty(toSMS))
                {
                    string[] arrMobileNos = new string[] { };
                    arrMobileNos = toSMS.Split(';');
                    for (int i = 0; i < arrMobileNos.Length; i++)
                    {
                        EmailUtils.SendSMS(uid, callStatus, settingType, schemeId, vendorType, body, arrMobileNos[i]);
                    }
                }
            }
            catch (Exception Ex)
            {
                ExceptionUtility.LogException(Ex, MethodBase.GetCurrentMethod().DeclaringType.Name + "-" + MethodBase.GetCurrentMethod().Name);
            }
        }

    }
}
