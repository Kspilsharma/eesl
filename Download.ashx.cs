﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace EESL
{
    /// <summary>
    /// Summary description for Download
    /// </summary>
    public class Download : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string FilePath = HttpContext.Current.Request.QueryString["path"];
            string destPath = context.Server.MapPath(FilePath);
            string FileName = string.Empty;
            // Check to see if file exist
            FileInfo fi = new FileInfo(destPath);

            // If the file exist on the server then add it to the database
            if (fi.Exists)
            {
                string[] Str = FilePath.Split('/');
                FileName = Str[1].ToString();
                HttpContext.Current.Response.ClearHeaders();
                HttpContext.Current.Response.ClearContent();
                HttpContext.Current.Response.AppendHeader("Content-Length", fi.Length.ToString());
                HttpContext.Current.Response.ContentType = "application/image"; ;
                HttpContext.Current.Response.AppendHeader("Content-Disposition", "attachment; filename="+ FileName + "");
                HttpContext.Current.Response.WriteFile(destPath);
                HttpContext.Current.Response.End();
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}